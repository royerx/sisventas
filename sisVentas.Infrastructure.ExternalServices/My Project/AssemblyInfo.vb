﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general de un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos valores de atributo para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("sisVentas.Infrastructure.ExternalServices")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("sisVentas.Infrastructure.ExternalServices")>
<Assembly: AssemblyCopyright("Copyright ©  2020")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como id. de typelib si este proyecto se expone a COM.
<Assembly: Guid("67aab6bb-c759-4355-8feb-bfdd7524ac50")>

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión
' utilizando el carácter "*", como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
