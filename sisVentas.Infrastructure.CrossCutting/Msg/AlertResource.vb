﻿Imports System.Drawing
''' <summary>
''' Clase Recursos de mensajes de alerta: 
''' Asigna los valores por defecto para los diferentes tipos de mensajes
''' </summary>
Public Class AlertResource

    'Valores por defecto para los diferentes tipos de mensajes e imagenes
    Public Const SUCCESS = "Success"
    Public Const WARNING = "Warning"
    Public Const ERRO = "Error"
    Public Const INFO = "Info"
    Public Const QUESTION = "Question"

    ''' <summary> Método que retorna el color de acuerdo al tipo de mensaje</summary>
    ''' <param name="type">tipo de mensaje a ser mostrado</param>
    Public Shared Function Color(type As String) As Color
        Select Case type
            Case "Success"
                Return Color.FromArgb(34, 187, 51)
            Case "Warning"
                Return Color.FromArgb(255, 193, 7)
            Case "Error"
                Return Color.FromArgb(187, 33, 36)
            Case "Info"
                Return Color.FromArgb(91, 192, 222)
            Case Else
                Return Color.FromArgb(170, 170, 170)
        End Select
    End Function

    ''' <summary> Método que retorna la imagen de acuerdo al tipo de mensaje</summary>
    ''' <param name="type">tipo de mensaje a ser mostrado</param>
    Public Shared Function Img(type As String) As Image
        Select Case type
            Case "Success"
                Return My.Resources.imgSuccess
            Case "Warning"
                Return My.Resources.imgWarning
            Case "Error"
                Return My.Resources.imgError
            Case "Info"
                Return My.Resources.imgInfo
            Case Else
                Return Nothing
        End Select
    End Function
End Class
