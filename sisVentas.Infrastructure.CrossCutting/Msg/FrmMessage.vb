﻿Imports Transitions

Public Class FrmMessage
    '''<summary>Constructor que inyecta los parametros del formulario</summary>
    '''<param name="type">Tipo de mensaje</param>
    '''<param name="title">Titulo del mensaje</param>
    '''<param name="message">Mensaje que se muestra</param>
    '''<param name="icon">Icono del mensaje a usar</param>
    Private Sub New(type As String, title As String, message As String, icon As String)
        InitializeComponent()
        lblMessage.Text = message
        lblTitle.Text = title
        pnlHead.BackColor = AlertResource.Color(type)
        img.Image = AlertResource.Img(icon)
    End Sub

    '''<summary>Método estatico que muestra el mensaje</summary>
    '''<param name="type">Tipo de mensaje</param>
    '''<param name="title">Titulo del mensaje</param>
    '''<param name="message">Mensaje que se muestra</param>
    Public Shared Sub ShowMessage(type As String, title As String, message As String)
        Dim frm = New FrmMessage(type, title, message, type)
        frm.ShowDialog()
    End Sub

    '''<summary>Método estatico que muestra el mensaje</summary>
    '''<param name="type">Tipo de mensaje</param>
    '''<param name="title">Titulo del mensaje</param>
    '''<param name="message">Mensaje que se muestra</param>
    Public Shared Sub ShowMessage(type As String, title As String, message As String, img As String)
        Dim frm = New FrmMessage(type, title, message, img)
        frm.ShowDialog()
    End Sub

    '''<summary>Método que implementa las transiciones</summary>
    Private Sub FrmMessage_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(800))
        t.add(Me, "Opacity", 1.0R)
        t.add(img, "Left", 75)
        t.run()
    End Sub

    '''<summary>Método que desecha el formulario</summary>
    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        Dispose()
    End Sub
End Class