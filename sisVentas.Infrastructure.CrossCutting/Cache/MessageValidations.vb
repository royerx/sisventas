﻿''' <summary>
''' Mensajes de Validación: 
''' Permite registrar los mensajes de validacion usados por defecto
''' </summary>
Public Module MessageValidations
    Public Const REQUIRED = "Campo Requerido"
    Public Const REGULAR_EXPRESSION_DEFAUL = "No se Acepta Caracteres especiales"
End Module
