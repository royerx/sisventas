﻿''' <summary>
''' Valores del Sistema: 
''' Permite registrar los valores por defecto del sistema
''' </summary>
Public Module Values
    Public Const CLIENT = "CLIENTE"
    Public ClientId

    Public Const EMPLOYEE = "EMPLEADO"
    Public EmployeeId

    Public Const SUPPLIER = "PROVEEDOR"
    Public SupplierId

    Public Const ConnectionString = "Server=(local);Initial Catalog=SisVentasBD;Integrated Security=true;"
    Public Const IGV = 0.18
    Public Const Admin = "SUPERADMIN"
    Public Const PRIVATE_KEY = "SolucionesIntegrales234123"

    'Permisos para Equipos de Cómputo
    Public Const ApplianceDescriptionList = "applianceDescription.list"
    Public Const ApplianceDescriptionDetail = "applianceDescription.detail"
    Public Const ApplianceDescriptionCreate = "applianceDescription.create"
    Public Const ApplianceDescriptionUpdate = "applianceDescription.update"
    Public Const ApplianceDescriptionDelete = "applianceDescription.delete"

    Public Const ApplianceList = "appliance.list"
    Public Const ApplianceDetail = "appliance.detail"
    Public Const ApplianceCreate = "appliance.create"
    Public Const ApplianceUpdate = "appliance.update"
    Public Const ApplianceDelete = "appliance.delete"

    Public Const CategoryList = "category.list"
    Public Const CategoryDetail = "category.detail"
    Public Const CategoryCreate = "category.create"
    Public Const CategoryUpdate = "category.update"
    Public Const CategoryDelete = "category.delete"

    Public Const BrandList = "brand.list"
    Public Const BrandDetail = "brand.detail"
    Public Const BrandCreate = "brand.create"
    Public Const BrandUpdate = "brand.update"
    Public Const BrandDelete = "brand.delete"

    'Permisos para Ventas
    Public Const SaleList = "sale.list"
    Public Const SaleDetail = "sale.detail"
    Public Const SaleCreate = "sale.create"
    Public Const SaleUpdate = "sale.update"
    Public Const SaleDelete = "sale.delete"

    'Permisos para Servicios
    Public Const ServiceList = "service.list"
    Public Const ServiceDetail = "service.detail"
    Public Const ServiceCreate = "service.create"
    Public Const ServiceUpdate = "service.update"
    Public Const ServiceDelete = "service.delete"

    'Permisos para Clientes
    Public Const ClientList = "client.list"
    Public Const ClientDetail = "client.detail"
    Public Const ClientCreate = "client.create"
    Public Const ClientUpdate = "client.update"
    Public Const ClientDelete = "client.delete"

    'Permisos para Compras
    Public Const BuyList = "buy.list"
    Public Const BuyDetail = "buy.detail"
    Public Const BuyCreate = "buy.create"
    Public Const BuyUpdate = "buy.update"
    Public Const BuyDelete = "buy.delete"

    'Permisos para Empleados
    Public Const EmployeeList = "employee.list"
    Public Const EmployeeDetail = "employee.detail"
    Public Const EmployeeCreate = "employee.create"
    Public Const EmployeeUpdate = "employee.update"
    Public Const EmployeeDelete = "employee.delete"
    Public Const EmployeeAddRoles = "employee.addroles"

    'Permisos para Reportes
    Public Const ReportList = "report.list"
    Public Const ReportDetail = "report.detail"
    Public Const ReportCreate = "report.create"
    Public Const ReportUpdate = "report.update"
    Public Const ReportDelete = "report.delete"

    'Permisos para Mantenimiento
    Public Const ManagementList = "management.list"
    Public Const ManagementDetail = "management.detail"
    Public Const ManagementCreate = "management.create"
    Public Const ManagementUpdate = "management.update"
    Public Const ManagementDelete = "management.delete"

    Public Const PersonList = "person.list"
    Public Const PersonDetail = "person.detail"
    Public Const PersonCreate = "person.create"
    Public Const PersonUpdate = "person.update"
    Public Const PersonDelete = "person.delete"
    Public Const PersonDetailByDocument = "person.detailbydocument"

    Public Const PersonTypeList = "personType.list"
    Public Const PersonTypeDetail = "personType.detail"
    Public Const PersonTypeCreate = "personType.create"
    Public Const PersonTypeUpdate = "personType.update"
    Public Const PersonTypeDelete = "personType.delete"

    Public Const PermissionList = "permission.list"
    Public Const PermissionDetail = "permission.detail"
    Public Const PermissionCreate = "permission.create"
    Public Const PermissionUpdate = "permission.update"
    Public Const PermissionDelete = "permission.delete"
    Public Const TableList = "table.list"

    Public Const RoleList = "role.list"
    Public Const RoleDetail = "role.detail"
    Public Const RoleCreate = "role.create"
    Public Const RoleUpdate = "role.update"
    Public Const RoleDelete = "role.delete"

    Public Const RolePermissions = "role.permissions"
    Public Const RoleSavePermissions = "role.savepermissions"

    Public Const QuotedShow = "quoted.show"

    Public Const OrderList = "order.list"
    Public Const OrderDetail = "order.detail"
    Public Const OrderCreate = "order.create"
    Public Const OrderUpdate = "order.update"
    Public Const OrderDelete = "order.delete"

    Public Const OrderDetailList = "orderdetail.list"
    Public Const OrderDetailDetail = "orderdetail.detail"
    Public Const OrderDetailCreate = "orderdetail.create"
    Public Const OrderDetailUpdate = "orderdetail.update"
    Public Const OrderDetailDelete = "orderdetail.delete"

    Public Const VoucherList = "voucher.list"
    Public Const VoucherDetail = "voucher.detail"
    Public Const VoucherCreate = "voucher.create"
    Public Const VoucherUpdate = "voucher.update"
    Public Const VoucherDelete = "voucher.delete"

    Public Const EntryList = "entry.list"
    Public Const EntryDetail = "entry.detail"
    Public Const EntryCreate = "entry.create"
    Public Const EntryUpdate = "entry.update"
    Public Const EntryDelete = "entry.delete"

    Public Const TechnicalReportList = "technicalreport.list"
    Public Const TechnicalReportDetail = "technicalreport.detail"
    Public Const TechnicalReportCreate = "technicalreport.create"
    Public Const TechnicalReportUpdate = "technicalreport.update"
    Public Const TechnicalReportDelete = "technicalreport.delete"

    Public Const ReceptionSheetList = "receptionsheet.list"
    Public Const ReceptionSheetDetail = "receptionsheet.detail"
    Public Const ReceptionSheetCreate = "receptionsheet.create"
    Public Const ReceptionSheetUpdate = "receptionsheet.update"
    Public Const ReceptionSheetDelete = "receptionsheet.delete"

End Module
