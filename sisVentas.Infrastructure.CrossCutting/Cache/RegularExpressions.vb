﻿''' <summary>
''' Expresiones Regulares: 
''' Permite registrar las expreciones regulares para los atributos
''' </summary>
Public Module RegularExpressions
    Public Const DEFAULT_NAME = "^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ.'_-]*)*)+$"
    Public Const DEFAULT_DESCRIPTION = "^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ*()/.:" & Chr(34) & "'°_-]*)*)+$"
    Public Const PART_NUMBER = "^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ/'_-]*)*)+$"
    Public Const NUMBER = "^([0-9])*$"
End Module
