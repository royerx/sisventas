﻿Imports System.IO
Imports System.Security.Cryptography

Public Module Crypt
    Private Function Encrypt(clearData() As Byte, key() As Byte, iv() As Byte) As Byte()
        Dim stream = New MemoryStream()
        Dim algorithm = Rijndael.Create()
        algorithm.Key = key
        algorithm.IV = iv
        Dim cryptStream = New CryptoStream(stream, algorithm.CreateEncryptor(), CryptoStreamMode.Write)
        cryptStream.Write(clearData, 0, clearData.Length)
        cryptStream.Close()
        Dim encryptedData() = stream.ToArray
        Return encryptedData
    End Function

    Public Function Encrypt(data As String, pass As String, bits As Integer) As String
        Dim clearBytes() = System.Text.Encoding.Unicode.GetBytes(data)
        Dim pdb = New Rfc2898DeriveBytes(pass, New Byte() {&H0, &H1, &H2, &H1C, &H1D, &H1E, &H3, &H4, &H5, &HF, &H20, &H21, &HAD, &HAF, &HA4})
        If bits = 128 Then
            Dim encryptedData = Encrypt(clearBytes, pdb.GetBytes(16), pdb.GetBytes(16))
            Return Convert.ToBase64String(encryptedData)
        ElseIf bits = 192 Then
            Dim encryptedData = Encrypt(clearBytes, pdb.GetBytes(24), pdb.GetBytes(16))
            Return Convert.ToBase64String(encryptedData)
        ElseIf bits = 256 Then
            Dim encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16))
            Return Convert.ToBase64String(encryptedData)
        Else
            Return String.Concat(bits)
        End If
    End Function

    Private Function Decrypt(cipherData() As Byte, key() As Byte, iv() As Byte) As Byte()
        On Error Resume Next
        Dim stream = New MemoryStream()
        Dim algorithm = Rijndael.Create()
        algorithm.Key = key
        algorithm.IV = iv
        Dim cryptStream = New CryptoStream(stream, algorithm.CreateDecryptor(), CryptoStreamMode.Write)
        cryptStream.Write(cipherData, 0, cipherData.Length)
        cryptStream.Close()
        Dim decryptedData() = stream.ToArray
        Return decryptedData
    End Function

    Public Function Decrypt(data As String, pass As String, bits As Integer) As String
        Try
            Dim cipherBytes() = Convert.FromBase64String(data)
            Dim pdb = New Rfc2898DeriveBytes(pass, New Byte() {&H0, &H1, &H2, &H1C, &H1D, &H1E, &H3, &H4, &H5, &HF, &H20, &H21, &HAD, &HAF, &HA4})
            If bits = 128 Then
                Dim decryptedData = Decrypt(cipherBytes, pdb.GetBytes(16), pdb.GetBytes(16))
                Return System.Text.Encoding.Unicode.GetString(decryptedData)
            ElseIf bits = 192 Then
                Dim decryptedData = Decrypt(cipherBytes, pdb.GetBytes(24), pdb.GetBytes(16))
                Return System.Text.Encoding.Unicode.GetString(decryptedData)
            ElseIf bits = 256 Then
                Dim decryptedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16))
                Return System.Text.Encoding.Unicode.GetString(decryptedData)
            Else
                Return String.Concat(bits)
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Error de Encriptación", "Hubo un error al intentar encritar algun dato, intente de nuevo")
            Return Nothing
        End Try
    End Function

End Module
