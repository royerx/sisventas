﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Categoria: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de categoria
''' </summary>
''' <inheritdoc/>
Public Class CategoryService
    Implements ICategoryService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Category)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Category) Implements IGenericService(Of Category).GetAll
        If auth.Can(Values.CategoryList) Then
            Using context = unitOfWork.Create
                list = context.Repository.CategoryRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Category) Implements IGenericService(Of Category).GetAllMemory
        If auth.Can(Values.CategoryList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.CategoryRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Name.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Category Implements IGenericService(Of Category).GetById
        If auth.Can(Values.CategoryDetail) Then
            Using context = unitOfWork.Create
                Return context.Repository.CategoryRepository.GetById(id)
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If

    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="category">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(category As Category) As Integer Implements IGenericService(Of Category).Add
        If auth.Can(Values.CategoryCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.CategoryRepository.Add(category)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="category">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(category As Category) As Integer Implements IGenericService(Of Category).Update
        If auth.Can(Values.CategoryUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.CategoryRepository.Update(category)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Category).Delete
        If auth.Can(Values.CategoryDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.CategoryRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function
End Class
