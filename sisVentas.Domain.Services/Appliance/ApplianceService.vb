﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Detalle de Equipo de Cómputo: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de detalle de equipo de cómputo
''' </summary>
''' <inheritdoc/>
Public Class ApplianceService
    Implements IApplianceService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Appliance)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Appliance) Implements IGenericService(Of Appliance).GetAll
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.ApplianceRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Appliance) Implements IGenericService(Of Appliance).GetAllMemory
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ApplianceRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Serie.Contains(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance) Implements IApplianceService.GetAll
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.ApplianceRepository.GetAll(search, applianceDescriptionId)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance) Implements IApplianceService.GetAllMemory
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ApplianceRepository.GetAll("", applianceDescriptionId)
                End If
                Dim memory = list.FindAll(Function(item) item.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetBySerieMemory(search As String) As Appliance Implements IApplianceService.GetBySerieMemory
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ApplianceRepository.GetAll("")
                End If
                Dim memory = list.Find(Function(item) item.Serie = search)
                If memory IsNot Nothing Then
                    If memory.State Then
                        Return memory
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "EQUIPO DE CÓMPUTO", "El equipo no esta disponible y/o esta vendido")
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad</summary>
    '''<param name="serie">Serie que va a ser filtrada</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetBySerie(serie As String) As Appliance Implements IApplianceService.GetBySerie
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create()
                Dim record = context.Repository.ApplianceRepository.GetBySerie(serie)
                If record IsNot Nothing Then
                    record.ApplianceDescription = context.Repository.ApplianceDescriptionRepository.GetById(record.ApplianceDescriptionId)
                    record.ApplianceDescription.Category = context.Repository.CategoryRepository.GetById(record.ApplianceDescription.CategoryId)
                    record.ApplianceDescription.Brand = context.Repository.BrandRepository.GetById(record.ApplianceDescription.BrandId)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Appliance Implements IGenericService(Of Appliance).GetById
        If auth.Can(Values.ApplianceDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.ApplianceRepository.GetById(id)
                record.applianceDescription = context.Repository.ApplianceDescriptionRepository.GetById(record.ApplianceDescriptionId)
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="appliance">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(appliance As Appliance) As Integer Implements IGenericService(Of Appliance).Add
        If auth.Can(Values.ApplianceCreate) Then
            Using context = unitOfWork.Create
                'Employee.Id = context.Repository.PersonRepository.Add(Employee.Person)
                'res = context.Repository.PersonRepository.AddTypePerson(Employee.Id, Values.EmployeeId)
                res = context.Repository.ApplianceRepository.Add(appliance)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="appliance">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(appliance As Appliance) As Integer Implements IGenericService(Of Appliance).Update
        If auth.Can(Values.ApplianceUpdate) Then
            Using context = unitOfWork.Create
                'res = context.Repository.PersonRepository.Update(Employee.Person)
                res = context.Repository.ApplianceRepository.Update(appliance)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Appliance).Delete
        If auth.Can(Values.ApplianceDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.ApplianceRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="model">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetByModel(model As String) As ApplianceDescription Implements IApplianceService.GetByModel
        If auth.Can(Values.ApplianceList) Then
            Using context = unitOfWork.Create
                Dim appliance = context.Repository.ApplianceRepository.GetByModel(model)
                If appliance IsNot Nothing Then
                    appliance.Category = context.Repository.CategoryRepository.GetById(appliance.CategoryId)
                    appliance.Brand = context.Repository.BrandRepository.GetById(appliance.BrandId)
                End If
                Return appliance
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
