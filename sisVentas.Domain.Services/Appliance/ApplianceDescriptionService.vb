﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Equipo de Cómputo: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de equipo de cómputo
''' </summary>
''' <inheritdoc/>
Public Class ApplianceDescriptionService
    Implements IApplianceDescriptionService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of ApplianceDescription)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ApplianceDescription) Implements IGenericService(Of ApplianceDescription).GetAll
        If auth.Can(Values.ApplianceDescriptionList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.ApplianceDescriptionRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of ApplianceDescription) Implements IGenericService(Of ApplianceDescription).GetAllMemory
        If auth.Can(Values.ApplianceDescriptionList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ApplianceDescriptionRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Category.Name.ToUpper.Contains(search.ToUpper) Or item.Brand.Name.ToUpper.Contains(search.ToUpper) Or item.ApplianceName.ToUpper.Contains(search.ToUpper) Or item.Model.ToUpper.Contains(search.ToUpper) Or item.PartNumber.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As ApplianceDescription Implements IGenericService(Of ApplianceDescription).GetById
        If auth.Can(Values.ApplianceDescriptionDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.ApplianceDescriptionRepository.GetById(id)
                record.Category = context.Repository.CategoryRepository.GetById(record.CategoryId)
                record.Brand = context.Repository.BrandRepository.GetById(record.BrandId)
                record.Stock = context.Repository.ApplianceDescriptionRepository.GetStock(id)
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="applianceDescription">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(applianceDescription As ApplianceDescription) As Integer Implements IGenericService(Of ApplianceDescription).Add
        If auth.Can(Values.ApplianceDescriptionCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.ApplianceDescriptionRepository.Add(applianceDescription)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="applianceDescription">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(applianceDescription As ApplianceDescription) As Integer Implements IGenericService(Of ApplianceDescription).Update
        If auth.Can(Values.ApplianceDescriptionUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.ApplianceDescriptionRepository.Update(applianceDescription)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of ApplianceDescription).Delete
        If auth.Can(Values.ApplianceDescriptionDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.ApplianceDescriptionRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function
End Class
