﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Reporte Técnico: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de reporte técnico
''' </summary>
''' <inheritdoc/>
Public Class TechnicalReportService
    Implements ITechnicalReportService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of TechnicalReport)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of TechnicalReport) Implements IGenericService(Of TechnicalReport).GetAll
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.TechnicalReportRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of TechnicalReport) Implements IGenericService(Of TechnicalReport).GetAllMemory
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.TechnicalReportRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Appliance.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport) Implements ITechnicalReportService.GetAll
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.TechnicalReportRepository.GetAll(search, beginDate, endDate)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport) Implements ITechnicalReportService.GetAllMemory
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.TechnicalReportRepository.GetAll("", beginDate, endDate)
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Appliance.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As TechnicalReport Implements IGenericService(Of TechnicalReport).GetById
        If auth.Can(Values.TechnicalReportDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.TechnicalReportRepository.GetById(id)
                If record IsNot Nothing Then
                    record.Client = context.Repository.PersonRepository.GetById(record.ClientId)
                    record.Employee = context.Repository.EmployeeRepository.GetById(record.EmployeeId)
                    record.Appliance = context.Repository.ApplianceRepository.GetById(record.ApplianceId)
                    record.Appliance.ApplianceDescription = context.Repository.ApplianceDescriptionRepository.GetById(record.Appliance.ApplianceDescriptionId)
                    record.Appliance.ApplianceDescription.Category = context.Repository.CategoryRepository.GetById(record.Appliance.ApplianceDescription.CategoryId)
                    record.Appliance.ApplianceDescription.Brand = context.Repository.BrandRepository.GetById(record.Appliance.ApplianceDescription.BrandId)
                    record.Vouchers = context.Repository.VoucherRepository.GetByTechnicalReportId(id)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="technicalReport">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(technicalReport As TechnicalReport) As Integer Implements IGenericService(Of TechnicalReport).Add
        If auth.Can(Values.TechnicalReportCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.TechnicalReportRepository.Add(technicalReport)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="technicalReport">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(technicalReport As TechnicalReport) As Integer Implements IGenericService(Of TechnicalReport).Update
        If auth.Can(Values.TechnicalReportUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.TechnicalReportRepository.Update(technicalReport)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of TechnicalReport).Delete
        If auth.Can(Values.TechnicalReportDelete) Then
            Using context = unitOfWork.Create
                Dim vouchers = context.Repository.VoucherRepository.GetByTechnicalReportId(id)
                If vouchers.Count = 0 Then
                    res = context.Repository.TechnicalReportRepository.Delete(id)
                    context.SaveChanges()
                    Return res
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Reporte Técnico", "El Reporte Técnico NO Fue Eliminado, Esta asignado a un Comprobante de Pago")
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener servicios</summary>
    '''<returns>Lista de Servicios</returns>
    Function GetServices() As List(Of String) Implements ITechnicalReportService.GetServices
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create
                Return context.Repository.TechnicalReportRepository.GetServices
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener el precio de un servicio</summary>
    '''<param name="service">Nombre del servicio</param>
    '''<returns>Precio</returns>
    Function GetServicePrice(service As String) As Double Implements ITechnicalReportService.GetServicePrice
        If auth.Can(Values.TechnicalReportList) Then
            Using context = unitOfWork.Create
                Return context.Repository.TechnicalReportRepository.GetServicePrice(service)
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener ficha de recepción por reporte técnico</summary>
    '''<param name="technicalReportId">Identificador de reporte técnico</param>
    '''<returns>ficha de recepción</returns>
    Function GetReceptionSheet(technicalReportId As Integer) As ReceptionSheet Implements ITechnicalReportService.GetReceptionSheet
        If auth.Can(Values.ReceptionSheetDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.TechnicalReportRepository.GetReceptionSheet(technicalReportId)
                If record IsNot Nothing Then
                    record.Client = context.Repository.PersonRepository.GetById(record.ClientId)
                    record.TechnicalReport = context.Repository.TechnicalReportRepository.GetById(record.TechnicalReportId)
                    record.TechnicalReport.Appliance = context.Repository.ApplianceRepository.GetById(record.TechnicalReport.ApplianceId)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
