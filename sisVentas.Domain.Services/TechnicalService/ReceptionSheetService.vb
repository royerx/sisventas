﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Ficha de Recepción: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de ficha de recepción
''' </summary>
''' <inheritdoc/>
Public Class ReceptionSheetService
    Implements IReceptionSheetService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of ReceptionSheet)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ReceptionSheet) Implements IGenericService(Of ReceptionSheet).GetAll
        If auth.Can(Values.ReceptionSheetList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.ReceptionSheetRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of ReceptionSheet) Implements IGenericService(Of ReceptionSheet).GetAllMemory
        If auth.Can(Values.ReceptionSheetList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ReceptionSheetRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.TechnicalReport.Appliance.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheet) Implements IReceptionSheetService.GetAll
        If auth.Can(Values.ReceptionSheetList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.ReceptionSheetRepository.GetAll(search, beginDate, endDate)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheet) Implements IReceptionSheetService.GetAllMemory
        If auth.Can(Values.ReceptionSheetList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.ReceptionSheetRepository.GetAll("", beginDate, endDate)
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.TechnicalReport.Appliance.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As ReceptionSheet Implements IGenericService(Of ReceptionSheet).GetById
        If auth.Can(Values.ReceptionSheetDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.ReceptionSheetRepository.GetById(id)
                If record IsNot Nothing Then
                    record.Client = context.Repository.PersonRepository.GetById(record.ClientId)
                    record.TechnicalReport = context.Repository.TechnicalReportRepository.GetById(record.TechnicalReportId)
                    record.TechnicalReport.Appliance = context.Repository.ApplianceRepository.GetById(record.TechnicalReport.ApplianceId)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="receptionSheet">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(receptionSheet As ReceptionSheet) As Integer Implements IGenericService(Of ReceptionSheet).Add
        If auth.Can(Values.ReceptionSheetCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.ReceptionSheetRepository.Add(receptionSheet)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="ReceptionSheet">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(receptionSheet As ReceptionSheet) As Integer Implements IGenericService(Of ReceptionSheet).Update
        If auth.Can(Values.ReceptionSheetUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.ReceptionSheetRepository.Update(receptionSheet)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of ReceptionSheet).Delete
        If auth.Can(Values.ReceptionSheetDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.ReceptionSheetRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
