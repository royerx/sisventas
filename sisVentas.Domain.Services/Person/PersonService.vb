﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Persona: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de persona
''' </summary>
''' <inheritdoc/>
Public Class PersonService
    Implements IPersonService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Person)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Person) Implements IGenericService(Of Person).GetAll
        If auth.Can(Values.PersonList) Then
            Using context = unitOfWork.Create
                list = context.Repository.PersonRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Person) Implements IGenericService(Of Person).GetAllMemory
        If auth.Can(Values.PersonList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.PersonRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.LastName.ToUpper.Contains(search.ToUpper) Or item.Name.ToUpper.Contains(search.ToUpper) Or item.DocumentNumber.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de personas mediate el tipo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Function GetAll(search As String, type As String) As IEnumerable(Of Person) Implements IPersonService.GetAll
        If auth.Can(Values.PersonList) Then
            Using context = unitOfWork.Create
                list = context.Repository.PersonRepository.GetAll(search, type)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de personas mediate el tipo Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Function GetAllMemory(search As String, type As String) As IEnumerable(Of Person) Implements IPersonService.GetAllMemory
        If auth.Can(Values.PersonList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.PersonRepository.GetAll("", type)
                End If
                Dim memory = list.FindAll(Function(item) item.LastName.ToUpper.Contains(search.ToUpper) Or item.Name.ToUpper.Contains(search.ToUpper) Or item.DocumentNumber.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Person Implements IGenericService(Of Person).GetById
        If auth.Can(Values.PersonDetail) Then
            Using context = unitOfWork.Create
                Return context.Repository.PersonRepository.GetById(id)
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="person">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(person As Person) As Integer Implements IGenericService(Of Person).Add
        If auth.Can(Values.PersonCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.PersonRepository.Add(person)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If

    End Function

    '''<summary>Método para agregar una persona y su tipo</summary>
    '''<param name="person">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de Persona</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(person As Person, typeId As Integer) As Integer Implements IPersonService.Add
        If auth.Can(Values.PersonCreate) Then
            Using context = unitOfWork.Create
                If person.Id = 0 Then
                    person.Id = context.Repository.PersonRepository.Add(person)
                End If
                If context.Repository.PersonRepository.HasType(person.Id, typeId) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "REGISTRO GUARDADO", person.DocumentNumber & " ya se encuentra registrado")
                Else
                    res = context.Repository.PersonRepository.AddTypePerson(person.Id, typeId)
                End If
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If

    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="person">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(person As Person) As Integer Implements IGenericService(Of Person).Update
        If auth.Can(Values.PersonUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.PersonRepository.Update(person)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If

    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Person).Delete
        If auth.Can(Values.PersonDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.PersonRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para eliminar una persona y su tipo</summary>
    '''<param name="personId">Identificador de Entidad</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Public Function Delete(personId As Integer, typeId As Integer) As Integer Implements IPersonService.Delete
        If auth.Can(Values.PersonDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.PersonRepository.DeleteTypePerson(personId, typeId)
                If context.Repository.PersonRepository.HasPersonTypes(personId) = 0 Then
                    res = context.Repository.PersonRepository.Delete(personId)
                End If
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As Person Implements IPersonService.GetByDocument
        If auth.Can(Values.PersonDetailByDocument) Then
            Using context = unitOfWork.Create
                Return context.Repository.PersonRepository.GetByDocument(document)
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
