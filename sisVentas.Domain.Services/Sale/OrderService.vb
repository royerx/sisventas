﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Nota de Pedido: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de nota de pedido
''' </summary>
''' <inheritdoc/>
Public Class OrderService
    Implements IOrderService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Order)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Order) Implements IGenericService(Of Order).GetAll
        If auth.Can(Values.OrderList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.OrderRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Order) Implements IGenericService(Of Order).GetAllMemory
        If auth.Can(Values.OrderList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.OrderRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order) Implements IOrderService.GetAll
        If auth.Can(Values.OrderList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.OrderRepository.GetAll(search, beginDate, endDate)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order) Implements IOrderService.GetAllMemory
        If auth.Can(Values.OrderList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.OrderRepository.GetAll("", beginDate, endDate)
                End If
                Dim memory = list.FindAll(Function(item) item.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Order Implements IGenericService(Of Order).GetById
        If auth.Can(Values.OrderDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.OrderRepository.GetById(id)
                If record IsNot Nothing Then
                    record.Client = context.Repository.PersonRepository.GetById(record.ClientId)
                    record.Employee = context.Repository.EmployeeRepository.GetById(record.EmployeeId)
                    record.OrderDetails = context.Repository.OrderDetailRepository.GetByOrderId(id)
                    record.Vouchers = context.Repository.VoucherRepository.GetByOrderId(id)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="order">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(order As Order) As Integer Implements IGenericService(Of Order).Add
        If auth.Can(Values.OrderCreate) Then
            Using context = unitOfWork.Create
                If order.OrderDetails.Count > 0 Then
                    order.Id = context.Repository.OrderRepository.Add(order)
                    For Each item In order.OrderDetails
                        item.OrderId = order.Id
                        res += context.Repository.OrderDetailRepository.Add(item)
                        res += context.Repository.ApplianceRepository.ChangeSatate(item.Appliance.Id, False)
                    Next
                    context.SaveChanges()
                    Return order.Id
                Else
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="order">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(order As Order) As Integer Implements IGenericService(Of Order).Update
        If auth.Can(Values.OrderUpdate) Then
            Using context = unitOfWork.Create
                If order.OrderDetails.Count > 0 Then
                    If order.Vouchers.Count = 0 Then
                        res = context.Repository.OrderRepository.Update(order)
                        res = context.Repository.OrderDetailRepository.DeleteByOrderId(order.Id)
                        For Each item In order.OrderDetails
                            item.OrderId = order.Id
                            res += context.Repository.OrderDetailRepository.Add(item)
                            res += context.Repository.ApplianceRepository.ChangeSatate(item.Appliance.Id, False)
                        Next
                        context.SaveChanges()
                        Return res
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Nota de Pedido", "La Nota de Pedido NO Fue Modificada, Esta asignada a un Comprobante de Pago")
                        Return Nothing
                    End If
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Nota de Pedido", "La Nota de Pedido NO Fue Modificada, no se permite guardar sin items")
                    Return Nothing
                End If

            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Order).Delete
        If auth.Can(Values.OrderDelete) Then
            Using context = unitOfWork.Create
                Dim vouchers = context.Repository.VoucherRepository.GetByOrderId(id)
                If vouchers.Count = 0 Then
                    res = context.Repository.OrderDetailRepository.DeleteByOrderId(id)
                    res = context.Repository.OrderRepository.Delete(id)
                    context.SaveChanges()
                    Return res
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Nota de Pedido", "La Nota de Pedido NO Fue Eliminada, Esta asignada a un Comprobante de Pago")
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método agregar y verificar duplicado de series</summary>
    '''<param name="orderDetails">Lista de detalles de nota de pedido</param>
    '''<param name="detail">detalle a ser agregago</param>
    '''<returns> si hay repetidos o no</returns>
    Function AddAppliance(orderDetails As List(Of OrderDetail), detail As OrderDetail) As IEnumerable(Of OrderDetail) Implements IOrderService.AddAppliance
        If auth.Can(Values.OrderCreate) Then
            Dim memory = New List(Of OrderDetail)
            If orderDetails IsNot Nothing Then
                memory = orderDetails.FindAll(Function(item) item.Appliance.Id = detail.Appliance.Id)
            Else
                orderDetails = New List(Of OrderDetail)
            End If

            If memory.Count > 0 Then
                FrmMessage.ShowMessage(AlertResource.ERRO, "GUARDAR EQUIPO", "La Nota de Pedido NO Fue Modificada, Esta asignada a un Comprobante de Pago")
            Else
                orderDetails.Add(detail)
            End If
            Return orderDetails
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function
End Class
