﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Comprobante de Pago: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de comprobante de pago
''' </summary>
''' <inheritdoc/>
Public Class VoucherService
    Implements IVoucherService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Voucher)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Voucher) Implements IGenericService(Of Voucher).GetAll
        If auth.Can(Values.VoucherList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.VoucherRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Voucher) Implements IGenericService(Of Voucher).GetAllMemory
        If auth.Can(Values.VoucherList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.VoucherRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Order.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Order.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Order.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Order.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher) Implements IVoucherService.GetAll
        If auth.Can(Values.VoucherList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.VoucherRepository.GetAll(search, beginDate, endDate)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher) Implements IVoucherService.GetAllMemory
        If auth.Can(Values.VoucherList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.VoucherRepository.GetAll("", beginDate, endDate)
                End If
                Dim memory = list.FindAll(Function(item) item.Order.Client.LastName.ToUpper.Contains(search.ToUpper) Or item.Order.Client.Name.ToUpper.Contains(search.ToUpper) Or item.Order.Client.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Order.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Voucher Implements IGenericService(Of Voucher).GetById
        If auth.Can(Values.VoucherDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.VoucherRepository.GetById(id)
                record.Employee = context.Repository.EmployeeRepository.GetById(record.EmployeeId)
                If record.VoucherType = "Order" Then
                    record.Order = context.Repository.OrderRepository.GetByVoucherId(record.Id)
                    record.Order.Client = context.Repository.PersonRepository.GetById(record.Order.ClientId)
                    record.Order.Employee = context.Repository.EmployeeRepository.GetById(record.Order.EmployeeId)
                    record.Order.OrderDetails = context.Repository.OrderDetailRepository.GetByOrderId(record.Order.Id)
                ElseIf record.VoucherType = "TechnicalReport" Then
                    record.TechnicalReport = context.Repository.TechnicalReportRepository.GetByVoucherId(record.Id)
                    record.TechnicalReport.Client = context.Repository.PersonRepository.GetById(record.TechnicalReport.ClientId)
                    record.TechnicalReport.Employee = context.Repository.EmployeeRepository.GetById(record.TechnicalReport.EmployeeId)
                    record.TechnicalReport.Appliance = context.Repository.ApplianceRepository.GetById(record.TechnicalReport.ApplianceId)
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="voucher">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(voucher As Voucher) As Integer Implements IGenericService(Of Voucher).Add
        If auth.Can(Values.VoucherCreate) Then
            Using context = unitOfWork.Create
                voucher.Id = context.Repository.VoucherRepository.Add(voucher)
                If voucher.VoucherType = "Order" Then
                    res = context.Repository.VoucherRepository.AddOrder(voucher.Id, voucher.Order.Id)
                ElseIf voucher.VoucherType = "TechnicalReport" Then
                    res = context.Repository.VoucherRepository.AddTechnicalReport(voucher.Id, voucher.TechnicalReport.Id)
                End If
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="voucher">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(voucher As Voucher) As Integer Implements IGenericService(Of Voucher).Update
        If auth.Can(Values.VoucherUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.VoucherRepository.Update(voucher)
                If voucher.VoucherType = "Order" Then
                    res = context.Repository.VoucherRepository.DeleteTechnicalReport(voucher.Id)
                    res = context.Repository.VoucherRepository.DeleteOrder(voucher.Id)
                    res = context.Repository.VoucherRepository.AddOrder(voucher.Id, voucher.Order.Id)
                ElseIf voucher.VoucherType = "TechnicalReport" Then
                    res = context.Repository.VoucherRepository.DeleteTechnicalReport(voucher.Id)
                    res = context.Repository.VoucherRepository.DeleteOrder(voucher.Id)
                    res = context.Repository.VoucherRepository.AddTechnicalReport(voucher.Id, voucher.TechnicalReport.Id)
                End If
                context.SaveChanges()
                Return res

            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Voucher).Delete
        If auth.Can(Values.VoucherDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.VoucherRepository.DeleteOrder(id)
                res = context.Repository.VoucherRepository.DeleteTechnicalReport(id)
                res = context.Repository.VoucherRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String) Implements IVoucherService.GetNumber
        If auth.Can(Values.VoucherCreate) Then
            Using context = unitOfWork.Create
                Return context.Repository.VoucherRepository.GetNumber(type)
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
