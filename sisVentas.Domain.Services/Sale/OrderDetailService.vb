﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Detalle de Nota de Pedido: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de detalle de nota de pedido
''' </summary>
''' <inheritdoc/>
Public Class OrderDetailService
    Implements IOrderDetailService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of OrderDetail)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of OrderDetail) Implements IGenericService(Of OrderDetail).GetAll
        If auth.Can(Values.OrderDetailList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.OrderDetailRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of OrderDetail) Implements IGenericService(Of OrderDetail).GetAllMemory
        If auth.Can(Values.OrderDetailList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.OrderDetailRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Appliance.Serie.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As OrderDetail Implements IGenericService(Of OrderDetail).GetById
        If auth.Can(Values.OrderDetailDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.OrderDetailRepository.GetById(id)
                record.Appliance = context.Repository.ApplianceRepository.GetById(record.ApplianceId)
                record.Order = context.Repository.OrderRepository.GetById(record.OrderId)
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="orderDetail">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(orderDetail As OrderDetail) As Integer Implements IGenericService(Of OrderDetail).Add
        If auth.Can(Values.OrderDetailCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.OrderDetailRepository.Add(orderDetail)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="orderDetail">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(orderDetail As OrderDetail) As Integer Implements IGenericService(Of OrderDetail).Update
        If auth.Can(Values.OrderDetailUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.OrderDetailRepository.Update(orderDetail)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of OrderDetail).Delete
        If auth.Can(Values.OrderDetailDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.OrderDetailRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
