﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Entrada: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de entrada
''' </summary>
''' <inheritdoc/>
Public Class EntryService
    Implements IEntryService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Entry)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Entry) Implements IGenericService(Of Entry).GetAll
        If auth.Can(Values.EntryList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.EntryRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Entry) Implements IGenericService(Of Entry).GetAllMemory
        If auth.Can(Values.EntryList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.EntryRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Supplier.LastName.ToUpper.Contains(search.ToUpper) Or item.Supplier.Name.ToUpper.Contains(search.ToUpper) Or item.Supplier.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry) Implements IEntryService.GetAll
        If auth.Can(Values.EntryList) Then
            Using context = unitOfWork.Create()
                list = context.Repository.EntryRepository.GetAll(search, beginDate, endDate)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry) Implements IEntryService.GetAllMemory
        If auth.Can(Values.EntryList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.EntryRepository.GetAll("", beginDate, endDate)
                End If
                Dim memory = list.FindAll(Function(item) item.Supplier.LastName.ToUpper.Contains(search.ToUpper) Or item.Supplier.Name.ToUpper.Contains(search.ToUpper) Or item.Supplier.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Employee.Login.ToUpper.Contains(search.ToUpper) Or item.Id = Val(search))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Entry Implements IGenericService(Of Entry).GetById
        If auth.Can(Values.EntryDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.EntryRepository.GetById(id)
                If record IsNot Nothing Then
                    record.Employee = context.Repository.EmployeeRepository.GetById(record.EmployeeId)
                    record.Supplier = context.Repository.PersonRepository.GetById(record.SupplierId)
                    record.Appliances = context.Repository.EntryRepository.GetAppliances(id)
                    For Each item In record.Appliances
                        item.ApplianceDescription = context.Repository.ApplianceDescriptionRepository.GetById(item.ApplianceDescriptionId)
                        item.ApplianceDescription.Category = context.Repository.CategoryRepository.GetById(item.ApplianceDescription.CategoryId)
                        item.ApplianceDescription.Brand = context.Repository.BrandRepository.GetById(item.ApplianceDescription.BrandId)
                    Next
                End If
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="entry">Entidad a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(entry As Entry) As Integer Implements IGenericService(Of Entry).Add
        If auth.Can(Values.EntryCreate) Then
            Using context = unitOfWork.Create
                If entry.Appliances.Count > 0 Then
                    entry.Id = context.Repository.EntryRepository.Add(entry)
                    For Each item In entry.Appliances
                        If context.Repository.ApplianceRepository.GetBySerie(item.Serie) Is Nothing Then
                            item.EntryId = entry.Id
                            res = context.Repository.ApplianceRepository.Add(item)
                        Else
                            FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Compra", "El número de serie " & item.Serie & "Ya se encuentra registrado")
                        End If
                    Next
                    context.SaveChanges()
                    Return res
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Compra", "La compra no puede ser agregada si no tiene equipos")
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="entry">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entry As Entry) As Integer Implements IGenericService(Of Entry).Update
        If auth.Can(Values.EntryUpdate) Then
            Using context = unitOfWork.Create
                If entry.Appliances.Count > 0 Then
                    res = context.Repository.EntryRepository.Update(entry)
                    For Each item In entry.Appliances
                        If context.Repository.ApplianceRepository.GetBySerie(item.Serie) Is Nothing Then
                            item.EntryId = entry.Id
                            res = context.Repository.ApplianceRepository.Add(item)
                        End If
                    Next
                    context.SaveChanges()
                    Return res
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Compra", "La compra no puede ser modificada si no tiene equipos")
                    Return Nothing
                End If
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Entry).Delete
        If auth.Can(Values.EntryDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.ApplianceRepository.DeleteByEntryId(id)
                res = context.Repository.EntryRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
