﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Empleado: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de empleado
''' </summary>
''' <inheritdoc/>
Public Class AuthService
    Implements IAuthService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork


    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
    End Sub

    '''<summary>Método iniciar sesion</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Public Function Login(user As String, pass As String) As Boolean Implements IAuthService.Login
        Using context = unitOfWork.Create
            pass = Crypt.Encrypt(pass, Values.PRIVATE_KEY, Integer.Parse("256"))
            Dim employeeId = context.Repository.EmployeeRepository.Login(user, pass)
            If employeeId Then
                Dim Employee = context.Repository.EmployeeRepository.GetById(employeeId)
                Employee.Person = context.Repository.PersonRepository.GetById(employeeId)
                Employee.Roles = context.Repository.RoleRepository.GetByEmployeeId(employeeId)
                Employee.UserActive(Employee)
                Values.ClientId = context.Repository.PersonTypeRepository.GetAll(Values.CLIENT).ElementAt(0).Id
                Values.EmployeeId = context.Repository.PersonTypeRepository.GetAll(Values.EMPLOYEE).ElementAt(0).Id
                Values.SupplierId = context.Repository.PersonTypeRepository.GetAll(Values.SUPPLIER).ElementAt(0).Id
                Return True
            Else
                Return False
            End If
        End Using
    End Function

    '''<summary>Método inicializar rol escogido</summary>
    '''<param name="role">Rol seleccionado</param>
    Public Sub SelectedRole(role As Role) Implements IAuthService.SelectedRole
        Using context = unitOfWork.Create
            Dim employee = New Employee
            role.Permissions = context.Repository.PermissionRepository.GetByRoleId(role.Id)
            employee.SelectedRole(role)
        End Using
    End Sub

    '''<summary>Método verificar permiso</summary>
    '''<param name="permission">Slug del permiso a verificar</param>
    '''<returns>Si tiene o no el permiso</returns>
    Public Function Can(permission As String) As Boolean Implements IAuthService.Can
        Using context = unitOfWork.Create
            Dim employee = context.Repository.EmployeeRepository.GetById(ActiveUser.Employee.Id)
            Dim role = context.Repository.RoleRepository.GetById(ActiveUser.SelectedRole.Id)
            If employee IsNot Nothing And role IsNot Nothing Then
                If role.Name = Values.Admin Then
                    Return True
                Else
                    Return context.Repository.EmployeeRepository.Can(employee.Id, role.Id, permission)
                End If
            Else
                Return False
            End If
        End Using
    End Function

    '''<summary>Método para enviar correo de recuperacion de contraseña a un empleado</summary>
    '''<param name="email">Correo Electrónico Empleado</param>
    '''<returns>Mensaje de Cofirmacion</returns>
    Function GetPassByEmail(email As String) As String Implements IAuthService.GetPassByEmail
        Using context = unitOfWork.Create
            Return context.Repository.EmployeeRepository.GetPassByEmail(email)
        End Using
    End Function

End Class
