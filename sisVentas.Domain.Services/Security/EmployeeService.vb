﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Empleado: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de empleado
''' </summary>
''' <inheritdoc/>
Public Class EmployeeService
    Implements IEmployeeService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Employee)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Employee) Implements IGenericService(Of Employee).GetAll
        If auth.Can(Values.EmployeeList) Then
            Using context = unitOfWork.Create
                list = context.Repository.EmployeeRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Employee) Implements IGenericService(Of Employee).GetAllMemory
        If auth.Can(Values.EmployeeList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.EmployeeRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Person.LastName.ToUpper.Contains(search.ToUpper) Or item.Person.Name.ToUpper.Contains(search.ToUpper) Or item.Person.DocumentNumber.ToUpper.Contains(search.ToUpper) Or item.Login.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Employee Implements IGenericService(Of Employee).GetById
        If auth.Can(Values.EmployeeDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.EmployeeRepository.GetById(id)
                record.Person = context.Repository.PersonRepository.GetById(id)
                record.Roles = context.Repository.RoleRepository.GetByEmployeeId(record.Id)
                record.Pass = Crypt.Decrypt(record.Pass, Values.PRIVATE_KEY, Integer.Parse("256"))
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="employee">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(employee As Employee) As Integer Implements IGenericService(Of Employee).Add
        If auth.Can(Values.EmployeeCreate) Then
            Using context = unitOfWork.Create
                employee.Id = context.Repository.PersonRepository.Add(employee.Person)
                res = context.Repository.PersonRepository.AddTypePerson(employee.Id, Values.EmployeeId)
                res = context.Repository.EmployeeRepository.Add(employee)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un empleado y su tipo</summary>
    '''<param name="employee">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Public Function Add(employee As Employee, typeId As Integer) As Integer Implements IEmployeeService.Add
        If auth.Can(Values.EmployeeCreate) Then
            Using context = unitOfWork.Create
                employee.Pass = Crypt.Encrypt(employee.Pass, Values.PRIVATE_KEY, Integer.Parse("256"))
                If employee.Person.Id = 0 Then
                    employee.Person.Id = context.Repository.PersonRepository.Add(employee.Person)
                End If
                If context.Repository.PersonRepository.HasType(employee.Person.Id, typeId) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "REGISTRO GUARDADO", employee.Person.DocumentNumber & " ya se encuentra registrado")
                Else
                    res = context.Repository.PersonRepository.AddTypePerson(employee.Person.Id, typeId)
                    employee.Id = employee.Person.Id
                    res = context.Repository.EmployeeRepository.Add(employee)
                    For Each item In employee.Roles
                        res = context.Repository.EmployeeRepository.AddRole(employee.Person.Id, item.Id)
                    Next
                End If
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If

    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="employee">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(employee As Employee) As Integer Implements IGenericService(Of Employee).Update
        If auth.Can(Values.EmployeeUpdate) Then
            Using context = unitOfWork.Create
                employee.Pass = Crypt.Encrypt(employee.Pass, Values.PRIVATE_KEY, Integer.Parse("256"))
                res = context.Repository.PersonRepository.Update(employee.Person)
                res = context.Repository.EmployeeRepository.Update(employee)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Employee).Delete
        If auth.Can(Values.EmployeeDelete) Then
            Using context = unitOfWork.Create

                res = context.Repository.PersonRepository.DeleteTypePerson(id, Values.EmployeeId)
                res = context.Repository.EmployeeRepository.DeleteRoles(id)
                res = context.Repository.EmployeeRepository.Delete(id)
                If context.Repository.PersonRepository.HasPersonTypes(id) = 0 Then
                    res = context.Repository.PersonRepository.Delete(id)
                End If
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="rolesId">Identificadores de Roles</param>
    '''<returns>Filas afectadas</returns>
    Function AddRole(employeeId As Integer, rolesId As List(Of Integer)) As Integer Implements IEmployeeService.AddRoles
        If auth.Can(Values.EmployeeAddRoles) Then
            Using context = unitOfWork.Create
                context.Repository.EmployeeRepository.DeleteRoles(employeeId)
                For Each item In rolesId
                    res += context.Repository.EmployeeRepository.AddRole(employeeId, item)
                Next
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

End Class
