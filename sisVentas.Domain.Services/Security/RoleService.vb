﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Rol: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de rol
''' </summary>
''' <inheritdoc/>
Public Class RoleService
    Implements IRoleService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Role)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Role) Implements IGenericService(Of Role).GetAll
        If auth.Can(Values.RoleList) Then
            Using context = unitOfWork.Create
                list = context.Repository.RoleRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Role) Implements IGenericService(Of Role).GetAllMemory
        If auth.Can(Values.RoleList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.RoleRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Name.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Role Implements IGenericService(Of Role).GetById
        If auth.Can(Values.RoleDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.RoleRepository.GetById(id)
                record.Permissions = context.Repository.PermissionRepository.GetByRoleId(record.Id)
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="role">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(role As Role) As Integer Implements IGenericService(Of Role).Add
        If auth.Can(Values.RoleCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.RoleRepository.Add(role)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="role">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(role As Role) As Integer Implements IGenericService(Of Role).Update
        If auth.Can(Values.RoleUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.RoleRepository.Update(role)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Role).Delete
        If auth.Can(Values.RoleDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.RoleRepository.DeletePermissions(id)
                res = context.Repository.RoleRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="permissionIds">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Public Function SavePermissions(roleId As Integer, permissionIds As IEnumerable(Of Integer)) As Integer Implements IRoleService.SavePermissions
        If auth.Can(Values.RoleSavePermissions) Then
            Using context = unitOfWork.Create
                res = context.Repository.RoleRepository.DeletePermissions(roleId)
                res = context.Repository.RoleRepository.SavePermissions(roleId, permissionIds)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function
End Class
