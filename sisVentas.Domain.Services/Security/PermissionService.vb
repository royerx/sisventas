﻿Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Servicio de Dominio de Permiso: 
''' Hereda los metodos del servicio de dominio generico e implenta los metodos correspondientes al servicio de dominio de permiso
''' </summary>
''' <inheritdoc/>
Public Class PermissionService
    Implements IPermissionService

    'Declaracion de la unidad de trabajo agregar los servicios de dominio
    Private ReadOnly unitOfWork As IUnitOfWork

    'Servicio de dominio para brindar los permisos
    Private ReadOnly auth As IAuthService

    'Variable de respuesta de consulta NonQuery
    Private res

    'lista guardada en memoria
    Private list As List(Of Permission)

    '''<summary>Constructor de la clase que inyecta la unidad de trabajo</summary>
    '''<param name="unitOfWork">Unidad de Trabajo</param>
    Public Sub New(unitOfWork As IUnitOfWork)
        Me.unitOfWork = unitOfWork
        auth = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Permission) Implements IGenericService(Of Permission).GetAll
        If auth.Can(Values.PermissionList) Then
            Using context = unitOfWork.Create
                list = context.Repository.PermissionRepository.GetAll(search)
                Return list
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guadada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Permission) Implements IGenericService(Of Permission).GetAllMemory
        If auth.Can(Values.PermissionList) Then
            Using context = unitOfWork.Create()
                If list Is Nothing Then
                    list = context.Repository.PermissionRepository.GetAll("")
                End If
                Dim memory = list.FindAll(Function(item) item.Name.ToUpper.Contains(search.ToUpper) Or item.Slug.ToUpper.Contains(search.ToUpper))
                Return memory
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Permission Implements IGenericService(Of Permission).GetById
        If auth.Can(Values.PermissionDetail) Then
            Using context = unitOfWork.Create
                Dim record = context.Repository.PermissionRepository.GetById(id)
                Return record
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="permission">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(permission As Permission) As Integer Implements IGenericService(Of Permission).Add
        If auth.Can(Values.PermissionCreate) Then
            Using context = unitOfWork.Create
                res = context.Repository.PermissionRepository.Add(permission)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="permission">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(permission As Permission) As Integer Implements IGenericService(Of Permission).Update
        If auth.Can(Values.PermissionUpdate) Then
            Using context = unitOfWork.Create
                res = context.Repository.PermissionRepository.Update(permission)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericService(Of Permission).Delete
        If auth.Can(Values.PermissionDelete) Then
            Using context = unitOfWork.Create
                res = context.Repository.PermissionRepository.Delete(id)
                context.SaveChanges()
                Return res
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function

    '''<summary>Método listar tablas</summary>
    '''<returns>Lista de nombres de tablas de la base de datos</returns>
    Public Function Tables() As IEnumerable(Of String) Implements IPermissionService.Tables
        If auth.Can(Values.TableList) Then
            Using context = unitOfWork.Create
                Return context.Repository.PermissionRepository.Tables()
            End Using
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "ACCIÓN NO AUTORIZADA", "No tiene el permiso para realizar esta acción")
            Return Nothing
        End If
    End Function
End Class
