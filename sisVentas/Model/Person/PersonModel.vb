﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Persona: 
''' Representa a una Persona y valida los datos
''' </summary>
Public Class PersonModel

    Public Property Id As Integer

    Public Property FullName As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Name As String

    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property LastName As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(11, MinimumLength:=8, ErrorMessage:="Máximo 11 caracteres y minimo 8 caracteres")>
    <RegularExpression(RegularExpressions.NUMBER, ErrorMessage:="Solo dígitos")>
    Public Property DocumentNumber As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Address As String

    <StringLength(11, ErrorMessage:="Máximo 11 caracteres")>
    <RegularExpression(RegularExpressions.NUMBER, ErrorMessage:="Solo dígitos")>
    Public Property Phone As String

    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <DataType(DataType.EmailAddress, ErrorMessage:="Formato no válido")>
    Public Property Email As String

    Public Overridable Property Employee As EmployeeModel

    'Lista de Tipos de persona al cual pertenece
    Public Overridable Property PersonTypes As ICollection(Of PersonTypeModel)

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre de la categoria</returns>
    Public Overrides Function ToString() As String
        Select Case DocumentNumber.Length
            Case 8
                Return "DNI: " & DocumentNumber & "             > " & LastName & " " & Name
            Case 11
                Return "RUC: " & DocumentNumber & "     > " & LastName & " " & Name
            Case Else
                Return DocumentNumber & "           > " & LastName & " " & Name
        End Select
    End Function

End Class
