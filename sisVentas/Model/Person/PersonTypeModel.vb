﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad de Modelo Tipo de Persona: 
''' Representa las personas a la cual pertenece un Tipo de Persona y valida los datos
''' </summary>
Public Class PersonTypeModel
    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Type As String

    'Lista de persona a los cuales pertenecen a la Caregoria
    Public Overridable Property Persons As ICollection(Of PersonModel)

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Tipo de Persona</returns>
    Public Overrides Function ToString() As String
        Return Type
    End Function
End Class
