﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Detalle de Cotización: 
''' Representa un detalle de una cotización que incluye a un equipo de computo
''' </summary>
Public Class OrderDetailModel

    Public Property Id As Long

    Public ReadOnly Property Detail
        Get
            If Order IsNot Nothing Then
                Return Appliance.ToString
            ElseIf ApplianceDescription IsNot Nothing Then
                Return ApplianceDescription.ToString
            Else
                Return ""
            End If
        End Get
    End Property

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property ApplianceDescription As ApplianceDescriptionModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Appliance As ApplianceModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <Range(1, Integer.MaxValue, ErrorMessage:="Números mayores a 0")>
    Public Property Quantity As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <Range(0.00, Double.MaxValue, ErrorMessage:="Solo decimales")>
    Public Property Price As Double

    Public Property SubTotal As Double

    Public Overridable Property Order As OrderModel

End Class
