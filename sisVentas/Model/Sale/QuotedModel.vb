﻿
''' <summary>
''' Entidad de Modelo Cotización: 
''' Representa una cotización que se realiza por un empleado
''' </summary>
Public Class QuotedModel
    Public Property Id As Integer

    Public Overridable Property Employee As EmployeeModel

    Public Property Qdate As Date

    Public Property Total As Double

    'Lista de Equipos de Cómuto a los cuales tiene la cotización
    Public Overridable Property OrderDetails As ICollection(Of OrderDetailModel)

End Class
