﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Nota de Pedido: 
''' Representa una nota de pedido de una venta
''' </summary>
Public Class OrderModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Client As PersonModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property Ndate As Date

    Public Property Total As Double

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Employee As EmployeeModel

    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property OrderDetails As ICollection(Of OrderDetailModel)

    Public Overridable Property Vouchers As ICollection(Of VoucherModel)

End Class
