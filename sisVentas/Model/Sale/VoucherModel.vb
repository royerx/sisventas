﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Comprobate de Pago: 
''' Representa un Comprobate de Pago de una venta
''' </summary>
Public Class VoucherModel

    Public Property Id As Integer

    Public ReadOnly Property VoucherNumber As String
        Get
            Return Serie & "-" & Correlative
        End Get
    End Property
    Public ReadOnly Property Client As String
        Get
            If Order IsNot Nothing Then
                Return Order.Client.FullName
            ElseIf TechnicalReport IsNot Nothing Then
                Return TechnicalReport.Client.FullName
            Else
                Return ""
            End If

        End Get
    End Property

    Public ReadOnly Property OrderEmployee As String
        Get
            If Order IsNot Nothing Then
                Return Order.Employee.Login
            Else
                Return ""
            End If
        End Get
    End Property


    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property Vdate As Date

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(10, ErrorMessage:="Máximo 10 Caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Type As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(4, MinimumLength:=4, ErrorMessage:="Solo 4 Caracteres")>
    <RegularExpression("^([0-9])*$", ErrorMessage:="Solo Digitos")>
    Public Property Serie As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(8, MinimumLength:=8, ErrorMessage:="Solo 8 Caracteres")>
    <RegularExpression("^([0-9])*$", ErrorMessage:="Solo Digitos")>
    Public Property Correlative As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(15, ErrorMessage:="Máximo 15 Caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property State As String

    Public Property Total As Double

    Public Property Igv As Double

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Employee As EmployeeModel

    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

    Public Property VoucherType As String

    Public Overridable Property Order As OrderModel

    Public Overridable Property TechnicalReport As TechnicalReportModel


End Class
