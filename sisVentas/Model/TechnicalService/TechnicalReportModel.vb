﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Reporte Técnico: 
''' Representa un reporte técnico de un servicio
''' </summary>
Public Class TechnicalReportModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Client As PersonModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Appliance As ApplianceModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 Caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Service As String

    Public ReadOnly Property Quantity As String = 1

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <Range(0.00, Double.MaxValue, ErrorMessage:="Solo decimales")>
    Public Property Price As Double

    Public ReadOnly Property SubTotal As String
        Get
            Return Price * Quantity
        End Get
    End Property

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property EntryDate As Date

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property DeliveryDate As Date

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Employee As EmployeeModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Failure As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Diagnosis As String

    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Observation As String

    Public Overridable Property Vouchers As ICollection(Of VoucherModel)

    Public ReadOnly Property ApplianceCategory
        Get
            If Appliance.ApplianceDescription IsNot Nothing Then
                Return Appliance.ApplianceDescription.Category.ToString
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property ApplianceBrand
        Get
            If Appliance.ApplianceDescription IsNot Nothing Then
                Return Appliance.ApplianceDescription.Brand.ToString
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property ApplianceModel
        Get
            If Appliance.ApplianceDescription IsNot Nothing Then
                Return Appliance.ApplianceDescription.Model
            Else
                Return ""
            End If
        End Get
    End Property

End Class
