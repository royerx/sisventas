﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Ficha de Recepción: 
''' Representa una ficha de recepción de un reporte técnico
''' </summary>
Public Class ReceptionSheetModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property TechnicalReport As TechnicalReportModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Client As PersonModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property Rdate As Date

    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

End Class
