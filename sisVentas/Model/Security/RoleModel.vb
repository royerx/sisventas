﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Rol: 
''' Representa a un Rol y valida los datos
''' </summary>
Public Class RoleModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Name As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Description As String

    'Emploados que tienen este rol
    Public Overridable Property Employees As ICollection(Of EmployeeModel)

    'Permisos que tienen este rol
    Public Overridable Property Permissions As ICollection(Of PermissionModel)

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre del Rol</returns>
    Public Overrides Function ToString() As String
        Return Name
    End Function

End Class
