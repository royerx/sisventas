﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Permiso: 
''' Representa a un Permiso y valida los datos
''' </summary>
Public Class PermissionModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Name As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Slug As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Description As String

    'Roles a los que pertenece el permisio
    Public Overridable Property Roles As ICollection(Of RoleModel)

End Class
