﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Empleado: 
''' Representa a una Empleado y valida los datos
''' </summary>
Public Class EmployeeModel

    Public Property Id As Integer

    Public Overridable Property Person As PersonModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Login As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(50, ErrorMessage:="Máximo 50 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Pass As String


    'Datos de Persona
    Public Property FullName As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Name As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property LastName As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(11, MinimumLength:=8, ErrorMessage:="Máximo 11 caracteres y minimo 8 caracteres")>
    <RegularExpression(RegularExpressions.NUMBER, ErrorMessage:="Solo dígitos")>
    Public Property DocumentNumber As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Address As String

    <StringLength(11, ErrorMessage:="Máximo 11 caracteres")>
    <RegularExpression(RegularExpressions.NUMBER, ErrorMessage:="Solo dígitos")>
    Public Property Phone As String

    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <DataType(DataType.EmailAddress, ErrorMessage:="Formato no válido")>
    Public Property Email As String

    'Lista de roles que tiene un empleado
    Public Overridable Property Roles As ICollection(Of RoleModel)

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre de la categoria</returns>
    Public Overrides Function ToString() As String
        Return Login
    End Function

End Class
