﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad de Modelo Marca: 
''' Representa las marcas a la cual pertenece un Equipo de Cómputo y valida los datos
''' </summary>
Public Class BrandModel
    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Name As String

    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

    'Lista de Equipos de Cómuto a los cuales pertenecen a la marca
    Public Overridable Property ApplianceDescriptions As ICollection(Of ApplianceDescriptionModel)

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre de la Marca</returns>
    Public Overrides Function ToString() As String
        Return Name
    End Function

    '''<summary>Método que sorbreescribe el metodo equals de la clase por el id</summary>
    '''<returns>si es igual o no</returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing OrElse [GetType]() <> obj.[GetType]() Then Return False
        Dim c As BrandModel = CType(obj, BrandModel)
        Return (Id = c.Id)
    End Function

End Class
