﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Detalle de Equipo de Computo: 
''' Representa a un Detalle de Equipo de Cómputo y valida los datos
''' </summary>
Public Class ApplianceModel

    Public Property Id As Integer

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property ApplianceDescription As ApplianceDescriptionModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Serie As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <RegularExpression("True|False", ErrorMessage:="Valor no Válido")>
    Public Property State As Boolean

    Public Overridable Property Order As OrderModel

    Public Overridable Property TechnicalReport As TechnicalReportModel

    Public Overridable Property Entry As EntryModel

    <Range(0.00, Double.MaxValue, ErrorMessage:="Solo decimales")>
    Public Overridable Property BuyPrice As Double

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre de la Marca</returns>
    Public Overrides Function ToString() As String
        If TechnicalReport IsNot Nothing Then
            Return Serie
        ElseIf Order Is Nothing Then
            Return IIf(State, "✔ ➞ " & Serie, " ✘  ➞ " & Serie)
        Else
            Return ApplianceDescription.Category.Name & " " & ApplianceDescription.Brand.Name & " " & ApplianceDescription.ApplianceName & " " & ApplianceDescription.Model & " " & ApplianceDescription.Detail & vbCrLf & "S/N: " & Serie
        End If

    End Function

End Class
