﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting

''' <summary>
''' Entidad Equipo de Computo: 
''' Representa a un Equipo de Cómputo y valida los datos
''' </summary>
Public Class ApplianceDescriptionModel

    Public Property Id As Integer

    Public Overridable Property Category As CategoryModel

    Public Overridable Property Brand As BrandModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(100, ErrorMessage:="Máximo 100 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property ApplianceName As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(50, ErrorMessage:="Máximo 50 caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Model As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(30, ErrorMessage:="Máximo 30 caracteres")>
    <RegularExpression(RegularExpressions.PART_NUMBER, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property PartNumber As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <Range(0.00, Double.MaxValue, ErrorMessage:="Solo números decimales")>
    Public Property Price As Double

    Public Property Stock As Integer

    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre de la Marca</returns>
    Public Overrides Function ToString() As String
        Return Category.Name & " " & Brand.Name & " " & ApplianceName & " " & Model & " " & Detail
    End Function

    '''<summary>Método que sorbreescribe el metodo equals de la clase por el id</summary>
    '''<returns>si es igual o no</returns>
    Public Overrides Function Equals(obj As Object) As Boolean
        If obj Is Nothing OrElse [GetType]() <> obj.[GetType]() Then Return False
        Dim c As BrandModel = CType(obj, BrandModel)
        Return (Id = c.Id)
    End Function

End Class
