﻿Imports System.ComponentModel.DataAnnotations
Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Modelo Entrada: 
''' Representa una entrada
''' </summary>
Public Class EntryModel

    Public Property Id As Integer

    Public ReadOnly Property VoucherNumber As String
        Get
            Return Serie & "-" & Correlative
        End Get
    End Property

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Supplier As PersonModel

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <DataType(DataType.Date, ErrorMessage:="Fecha No Válida")>
    Public Property Edate As Date

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(120, ErrorMessage:="Máximo 120 Caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property DocumentType As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(8, ErrorMessage:="Máximo 8 Caracteres")>
    <RegularExpression("^([0-9])*$", ErrorMessage:="Solo Digitos")>
    Public Property Serie As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(16, ErrorMessage:="Máximo 16 Caracteres")>
    <RegularExpression("^([0-9])*$", ErrorMessage:="Solo Digitos")>
    Public Property Correlative As String

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    <StringLength(15, ErrorMessage:="Máximo 15 Caracteres")>
    <RegularExpression(RegularExpressions.DEFAULT_NAME, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property State As String

    Public Property Total As Double

    Public Property Igv As Double

    <Required(ErrorMessage:=MessageValidations.REQUIRED)>
    Public Overridable Property Employee As EmployeeModel

    <RegularExpression(RegularExpressions.DEFAULT_DESCRIPTION, ErrorMessage:=MessageValidations.REGULAR_EXPRESSION_DEFAUL)>
    Public Property Detail As String

    Public Overridable Property Appliances As ICollection(Of ApplianceModel)

End Class
