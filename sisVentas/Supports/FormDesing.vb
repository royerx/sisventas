﻿''' <summary>
''' Clase Diseño de Formularios: 
''' Implementa los metodos base para el diseño de los formularios
''' </summary>
Public Class FormDesing

    '''<summary>Método para convertir una caja de texto a una de tipo placeholder de html</summary>
    '''<param name="txt">Caja de texto a ser convertida</param>
    '''<param name="text">Texto que se muestra en el placeholder</param>
    Public Shared Sub Placeholder(txt As TextBox, text As String)
        If txt.Text = text Then
            txt.Text = ""
        ElseIf txt.Text = "" Then
            txt.Text = text
        End If
    End Sub

    '''<summary>Método para validar solo numeros</summary>
    '''<param name="e">Evento de tipeo de tecla</param>
    Public Shared Sub OnlyNumbers(e As KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    '''<summary>Método para validar solo numeros decimales</summary>
    '''<param name="e">Evento de tipeo de tecla</param>
    '''<param name="text">Caja de Texto</param>
    Public Shared Sub OnlyDecimal(e As KeyPressEventArgs, text As TextBox)
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar = "." And Not text.Text.IndexOf(".") Then
            e.Handled = True
        ElseIf e.KeyChar = "." Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    '''<summary>Método validar nombres</summary>
    '''<param name="e">Evento de tipeo de tecla</param>
    Public Shared Sub DefaultName(e As KeyPressEventArgs)
        If Char.IsLetterOrDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "'" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "-" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "_" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "/" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "." Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    '''<summary>Método validar descripciones</summary>
    '''<param name="e">Evento de tipeo de tecla</param>
    Public Shared Sub DefaultDescription(e As KeyPressEventArgs)
        If Char.IsLetterOrDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "'" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "-" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "_" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "/" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "/" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "(" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = ")" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "*" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = ":" Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "." Then
            e.Handled = False
        ElseIf e.KeyChar.ToString = "@" Then
            e.Handled = False
        ElseIf e.KeyChar = Chr(34) Then ' valida "
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

End Class
