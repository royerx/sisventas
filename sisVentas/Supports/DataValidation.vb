﻿Imports System.ComponentModel.DataAnnotations

''' <summary>
''' Clase Diseño de Validacio de Datos: 
''' Crea una lista de errores validados por la libreria data annotation
''' </summary>
Public Class DataValidation

    'validador de la libreria
    Private ReadOnly context As ValidationContext

    'estado de la validacion
    ReadOnly Property Valid As Boolean

    'lista de validaciones con error
    ReadOnly Property Results As List(Of ValidationResult)

    ''' <summary> Constructor de la clase que asigna los parametros de la clase</summary>
    ''' <param name="instance">Objeto a validar en este caso los modelos de vistas</param>
    Public Sub New(instance As Object)
        context = New ValidationContext(instance)
        Results = New List(Of ValidationResult)
        Valid = Validator.TryValidateObject(instance, context, Results, True)
    End Sub
End Class
