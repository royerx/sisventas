﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Comprobante de Pago a entidad Entrada: 
''' Mapea los valores recibidos de un modelo de comprobante de pago a una entidad entrada y viceversa
''' </summary>
Public MustInherit Class EntryMapper

    '''<summary>Método para mapear entrada de un modelo de entrada</summary>
    '''<param name="entityModel">Modelo de Entrada</param>
    '''<returns>Entrada</returns>
    Public Shared Function Map(entityModel As EntryModel) As Entry
        If entityModel IsNot Nothing Then
            Dim entry = New Entry With {
                              .Id = entityModel.Id,
                              .Employee = EmployeeMapper.Map(entityModel.Employee),
                              .Edate = entityModel.Edate,
                              .Serie = entityModel.Serie,
                              .Correlative = entityModel.Correlative,
                              .DocumentType = entityModel.DocumentType,
                              .State = entityModel.State,
                              .Detail = entityModel.Detail,
                              .Supplier = PersonMapper.Map(entityModel.Supplier),
                              .Total = entityModel.Total}
            If entityModel.Employee IsNot Nothing Then
                entry.EmployeeId = entityModel.Employee.Id
            End If
            If entityModel.Supplier IsNot Nothing Then
                entry.SupplierId = entityModel.Supplier.Id
            End If
            If entityModel.Appliances IsNot Nothing Then
                Dim appliances = New List(Of Appliance)
                For Each item In entityModel.Appliances
                    appliances.Add(ApplianceMapper.Map(item))
                Next
                entry.Appliances = appliances
            End If
            Return entry
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de entrada de una entrada</summary>
    '''<param name="entity">Entrada</param>
    '''<returns>Modelo de Entrada</returns>
    Public Shared Function Map(entity As Entry) As EntryModel
        If entity IsNot Nothing Then
            Dim entryModel = New EntryModel With {
                                      .Id = entity.Id,
                                      .Employee = EmployeeMapper.Map(entity.Employee),
                                      .Edate = entity.Edate,
                                      .Serie = entity.Serie,
                                      .Correlative = entity.Correlative,
                                      .DocumentType = entity.DocumentType,
                                      .State = entity.State,
                                      .Detail = entity.Detail,
                                      .Supplier = PersonMapper.Map(entity.Supplier),
                                      .Total = entity.Total,
                                      .Igv = entity.Igv}
            If entity.Appliances IsNot Nothing Then
                Dim appliances = New List(Of ApplianceModel)
                For Each item In entity.Appliances
                    appliances.Add(ApplianceMapper.Map(item))
                Next
                entryModel.Appliances = appliances
            End If
            Return entryModel
        Else
            Return Nothing
        End If

    End Function
End Class
