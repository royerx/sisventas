﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Persona a entidad Persona: 
''' Mapea los valores recibidos de un Modelo de Persona a una entidad Persona
''' </summary>
Public MustInherit Class PersonMapper

    '''<summary>Método para mapear persona de un modelo de persona</summary>
    '''<param name="entityModel">Modelo de Persona</param>
    '''<returns>Persona</returns>
    Public Shared Function Map(entityModel As PersonModel) As Person
        If entityModel IsNot Nothing Then
            Return New Person With {
                             .Id = entityModel.Id,
                             .Name = entityModel.Name,
                             .LastName = entityModel.LastName,
                             .DocumentNumber = entityModel.DocumentNumber,
                             .Address = entityModel.Address,
                             .Phone = entityModel.Phone,
                             .Email = entityModel.Email}
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de persona de una persona</summary>
    '''<param name="entity">Persona</param>
    '''<returns>Modelo de Persona</returns>
    Public Shared Function Map(entity As Person) As PersonModel
        If entity IsNot Nothing Then
            Return New PersonModel With {
                                   .Id = entity.Id,
                                   .Name = entity.Name,
                                   .LastName = entity.LastName,
                                   .DocumentNumber = entity.DocumentNumber,
                                   .Address = entity.Address,
                                   .Phone = entity.Phone,
                                   .Email = entity.Email,
                                   .FullName = entity.GetFullName()}
        Else
            Return Nothing
        End If

    End Function
End Class
