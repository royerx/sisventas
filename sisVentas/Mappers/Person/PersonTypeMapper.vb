﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo Tipo de Persona a entidad Tipo de Persona: 
''' Mapea los valores recibidos de un Modelo de Tipo de Persona a una entidad Tipo de Persona
''' </summary>
Public MustInherit Class PersonTypeMapper

    '''<summary>Método para mapear tipo de persona de un modelo de tipo de persona</summary>
    '''<param name="entityModel">Modelo de Tipo de Persona</param>
    '''<returns>Tipo de Persona</returns>
    Public Shared Function Map(entityModel As PersonTypeModel) As PersonType
        If entityModel IsNot Nothing Then
            Return New PersonType With {
                                .Id = entityModel.Id,
                                .Type = entityModel.Type}
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de tipo de persona de un tipo de persona</summary>
    '''<param name="entity">Tipo de Persona</param>
    '''<returns>Modelo de Tipo de Persona</returns>
    Public Shared Function Map(entity As PersonType) As PersonTypeModel
        If entity IsNot Nothing Then
            Return New PersonTypeModel With {
                                .Id = entity.Id,
                                .Type = entity.Type}
        Else
            Return Nothing
        End If

    End Function
End Class
