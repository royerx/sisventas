﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Detalle de Nota de Pedido a entidad Nota de Pedido: 
''' Mapea los valores recibidos de un modelo de detalle de nota de pedido a una entidad de detalle de nota de pedido y viceversa
''' </summary>
Public MustInherit Class OrderDetailMapper

    '''<summary>Método para mapear detalle de nota de pedido de un modelo de detalle de nota de pedido</summary>
    '''<param name="entityModel">Modelo de Detalle de Nota de Pedido</param>
    '''<returns>Detalle de Nota de Pedido</returns>
    Public Shared Function Map(entityModel As OrderDetailModel) As OrderDetail
        If entityModel IsNot Nothing Then
            Dim orderDetail = New OrderDetail With {
                                   .Id = entityModel.Id,
                                   .ApplianceDescription = ApplianceDescriptionMapper.Map(entityModel.ApplianceDescription),
                                   .Appliance = ApplianceMapper.Map(entityModel.Appliance),
                                   .Price = entityModel.Price,
                                   .Quantity = entityModel.Quantity}
            If entityModel.Appliance IsNot Nothing Then
                orderDetail.ApplianceId = entityModel.Appliance.Id
                If entityModel.Appliance.Order IsNot Nothing Then
                    orderDetail.Appliance.Order = OrderMapper.Map(entityModel.Appliance.Order)
                End If
            End If
                Return orderDetail
        Else
                Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de detalle de nota de pedido de una detalle de nota de pedido</summary>
    '''<param name="entity">Detalle de Nota de Pedido</param>
    '''<returns>Modelo de Detalle de Nota de Pedido</returns>
    Public Shared Function Map(entity As OrderDetail) As OrderDetailModel
        If entity IsNot Nothing Then
            Dim orderDetailModel = New OrderDetailModel With {
                                        .Id = entity.Id,
                                        .ApplianceDescription = ApplianceDescriptionMapper.Map(entity.ApplianceDescription),
                                        .Appliance = ApplianceMapper.Map(entity.Appliance),
                                        .Price = entity.Price,
                                        .Quantity = entity.Quantity,
                                        .SubTotal = entity.SubTotal}
            If entity.Appliance IsNot Nothing Then
                If entity.Appliance.Order IsNot Nothing Then
                    orderDetailModel.Appliance.Order = OrderMapper.Map(entity.Appliance.Order)
                End If
            End If
            Return orderDetailModel
        Else
            Return Nothing
        End If

    End Function
End Class
