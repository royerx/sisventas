﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Comprobante de Pago a entidad Comprobante de Pago: 
''' Mapea los valores recibidos de un modelo de comprobante de pago a una entidad comprobante de pago y viceversa
''' </summary>
Public MustInherit Class VoucherMapper

    '''<summary>Método para mapear comprobante de pago de un modelo de comprobante de pago</summary>
    '''<param name="entityModel">Modelo de Comprobante de Pago</param>
    '''<returns>Comprobante de Pago</returns>
    Public Shared Function Map(entityModel As VoucherModel) As Voucher
        If entityModel IsNot Nothing Then
            Dim voucher = New Voucher With {
                              .Id = entityModel.Id,
                              .Employee = EmployeeMapper.Map(entityModel.Employee),
                              .Vdate = entityModel.Vdate,
                              .Serie = entityModel.Serie,
                              .Correlative = entityModel.Correlative,
                              .Type = entityModel.Type,
                              .State = entityModel.State,
                              .Detail = entityModel.Detail,
                              .VoucherType = entityModel.VoucherType,
                              .Order = OrderMapper.Map(entityModel.Order),
                              .TechnicalReport = TechnicalReportMapper.Map(entityModel.TechnicalReport)}
            If entityModel.Employee IsNot Nothing Then
                voucher.EmployeeId = entityModel.Employee.Id
            End If
            Return voucher
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de comprobante de pago de un comprobante de pago</summary>
    '''<param name="entity">Comprobante de Pago</param>
    '''<returns>Modelo de Comprobante de Pago</returns>
    Public Shared Function Map(entity As Voucher) As VoucherModel
        If entity IsNot Nothing Then
            Dim voucherModel = New VoucherModel With {
                                      .Id = entity.Id,
                                      .Employee = EmployeeMapper.Map(entity.Employee),
                                      .Vdate = entity.Vdate,
                                      .Serie = entity.Serie,
                                      .Correlative = entity.Correlative,
                                      .Type = entity.Type,
                                      .State = entity.State,
                                      .Detail = entity.Detail,
                                      .VoucherType = entity.VoucherType,
                                      .Order = OrderMapper.Map(entity.Order),
                                      .TechnicalReport = TechnicalReportMapper.Map(entity.TechnicalReport),
                                      .Total = entity.Total,
                                      .Igv = entity.Igv}
            Return voucherModel
        Else
            Return Nothing
        End If

    End Function
End Class
