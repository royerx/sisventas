﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Nota de Pedido a entidad Nota de Pedido: 
''' Mapea los valores recibidos de un modelo de nota de pedido a una entidad nota de pedido y viceversa
''' </summary>
Public MustInherit Class OrderMapper

    '''<summary>Método para mapear nota de pedido de un modelo de nota de pedido</summary>
    '''<param name="entityModel">Modelo de Nota de Pedido</param>
    '''<returns>Nota de Pedido</returns>
    Public Shared Function Map(entityModel As OrderModel) As Order
        If entityModel IsNot Nothing Then
            Dim order = New Order With {
                              .Id = entityModel.Id,
                              .Client = PersonMapper.Map(entityModel.Client),
                              .Employee = EmployeeMapper.Map(entityModel.Employee),
                              .Ndate = entityModel.Ndate,
                              .Detail = entityModel.Detail}
            If entityModel.Client IsNot Nothing Then
                order.ClientId = entityModel.Client.Id
            End If
            If entityModel.Employee IsNot Nothing Then
                order.EmployeeId = entityModel.Employee.Id
            End If
            If entityModel.OrderDetails IsNot Nothing Then
                Dim orderDetails = New List(Of OrderDetail)
                For Each item In entityModel.OrderDetails
                    orderDetails.Add(OrderDetailMapper.Map(item))
                Next
                order.OrderDetails = orderDetails
            End If
            If entityModel.Vouchers IsNot Nothing Then
                Dim vouchers = New List(Of Voucher)
                For Each item In entityModel.Vouchers
                    vouchers.Add(VoucherMapper.Map(item))
                Next
                order.Vouchers = vouchers
            End If
            Return order
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de nota de pedido de una nota de pedido</summary>
    '''<param name="entity">Nota de Pedido</param>
    '''<returns>Modelo de Nota de Pedido</returns>
    Public Shared Function Map(entity As Order) As OrderModel
        If entity IsNot Nothing Then
            Dim orderModel = New OrderModel With {
                                              .Id = entity.Id,
                                              .Client = PersonMapper.Map(entity.Client),
                                              .Employee = EmployeeMapper.Map(entity.Employee),
                                              .Ndate = entity.Ndate,
                                              .Detail = entity.Detail}
            If entity.OrderDetails IsNot Nothing Then
                Dim orderDetails = New List(Of OrderDetailModel)
                For Each item In entity.OrderDetails
                    orderDetails.Add(OrderDetailMapper.Map(item))
                Next
                orderModel.OrderDetails = orderDetails
            End If
            If entity.Vouchers IsNot Nothing Then
                Dim vouchers = New List(Of VoucherModel)
                For Each item In entity.Vouchers
                    vouchers.Add(VoucherMapper.Map(item))
                Next
                orderModel.Vouchers = vouchers
            End If
            orderModel.Total = entity.Total
            Return orderModel
        Else
            Return Nothing
        End If

    End Function
End Class
