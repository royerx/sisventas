﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Cotización a entidad Cotización: 
''' Mapea los valores recibidos de un modelo de cotizacion a una entidad cotizacion y viceversa
''' </summary>
Public MustInherit Class QuotedMapper

    '''<summary>Método para mapear cotizacion de un modelo de cotizacion</summary>
    '''<param name="entityModel">Modelo de Cotización</param>
    '''<returns>Cotización</returns>
    Public Shared Function Map(entityModel As QuotedModel) As Quoted
        If entityModel IsNot Nothing Then
            Dim quoted = New Quoted With {
                              .Id = entityModel.Id,
                              .Employee = EmployeeMapper.Map(entityModel.Employee),
                              .Qdate = entityModel.Qdate
                              }
            If entityModel.OrderDetails IsNot Nothing Then
                Dim orderDetails = New List(Of OrderDetail)
                For Each item In entityModel.OrderDetails
                    orderDetails.Add(OrderDetailMapper.Map(item))
                Next
                quoted.OrderDetails = orderDetails
            End If
            Return quoted
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de cotizacion de una cotizacion</summary>
    '''<param name="entity">Cotización</param>
    '''<returns>Modelo de Cotización</returns>
    Public Shared Function Map(entity As Quoted) As QuotedModel
        If entity IsNot Nothing Then
            Dim quotedModel = New QuotedModel With {
                                              .Id = entity.Id,
                                              .Employee = EmployeeMapper.Map(entity.Employee),
                                              .Qdate = entity.Qdate}
            If entity.OrderDetails IsNot Nothing Then
                Dim orderDetails = New List(Of OrderDetailModel)
                For Each item In entity.OrderDetails
                    orderDetails.Add(OrderDetailMapper.Map(item))
                Next
                quotedModel.OrderDetails = orderDetails
                quotedModel.Total = entity.Total
            End If
            Return quotedModel
        Else
            Return Nothing
        End If

    End Function
End Class
