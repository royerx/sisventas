﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Ficha de Recepción a entidad Ficha de Recepción: 
''' Mapea los valores recibidos de un modelo de ficha de recepción a una entidad ficha de recepción y viceversa
''' </summary>
Public MustInherit Class ReceptionSheetMapper

    '''<summary>Método para mapear ficha de recepción de un modelo de ficha de recepción</summary>
    '''<param name="entityModel">Modelo de Ficha de Recepción</param>
    '''<returns>Ficha de Recepción</returns>
    Public Shared Function Map(entityModel As ReceptionSheetModel) As ReceptionSheet
        If entityModel IsNot Nothing Then
            Dim receptionSheet = New ReceptionSheet With {
                              .Id = entityModel.Id,
                              .TechnicalReport = TechnicalReportMapper.Map(entityModel.TechnicalReport),
                              .Client = PersonMapper.Map(entityModel.Client),
                              .Rdate = entityModel.Rdate,
                              .Detail = entityModel.Detail}
            If entityModel.TechnicalReport IsNot Nothing Then
                receptionSheet.TechnicalReportId = entityModel.TechnicalReport.Id
            End If
            If entityModel.Client IsNot Nothing Then
                receptionSheet.ClientId = entityModel.Client.Id
            End If
            Return receptionSheet
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de ficha de recepción de una ficha de recepción</summary>
    '''<param name="entity">Ficha de Recepción</param>
    '''<returns>Modelo de Ficha de Recepción</returns>
    Public Shared Function Map(entity As ReceptionSheet) As ReceptionSheetModel
        If entity IsNot Nothing Then
            Dim receptionSheetModel = New ReceptionSheetModel With {
                                              .Id = entity.Id,
                                              .TechnicalReport = TechnicalReportMapper.Map(entity.TechnicalReport),
                                              .Client = PersonMapper.Map(entity.Client),
                                              .Rdate = entity.Rdate,
                                              .Detail = entity.Detail}
            Return receptionSheetModel
        Else
            Return Nothing
        End If

    End Function
End Class
