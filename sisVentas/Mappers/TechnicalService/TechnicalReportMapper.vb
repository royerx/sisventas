﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Reporte Técnico a entidad Reporte Técnico: 
''' Mapea los valores recibidos de un modelo de reporte técnico a una entidad reporte técnico y viceversa
''' </summary>
Public MustInherit Class TechnicalReportMapper

    '''<summary>Método para mapear reporte técnico de un modelo de reporte técnico</summary>
    '''<param name="entityModel">Modelo de Reporte Técnico</param>
    '''<returns>Reporte Técnico</returns>
    Public Shared Function Map(entityModel As TechnicalReportModel) As TechnicalReport
        If entityModel IsNot Nothing Then
            Dim technicalReport = New TechnicalReport With {
                              .Id = entityModel.Id,
                              .Client = PersonMapper.Map(entityModel.Client),
                              .Employee = EmployeeMapper.Map(entityModel.Employee),
                              .Appliance = ApplianceMapper.Map(entityModel.Appliance),
                              .EntryDate = entityModel.EntryDate,
                              .DeliveryDate = entityModel.DeliveryDate,
                              .Failure = entityModel.Failure,
                              .Diagnosis = entityModel.Diagnosis,
                              .Service = entityModel.Service,
                              .Price = entityModel.Price,
                              .Observation = entityModel.Observation}
            If entityModel.Client IsNot Nothing Then
                technicalReport.ClientId = entityModel.Client.Id
            End If
            If entityModel.Employee IsNot Nothing Then
                technicalReport.EmployeeId = entityModel.Employee.Id
            End If
            If entityModel.Appliance IsNot Nothing Then
                technicalReport.ApplianceId = entityModel.Appliance.Id
            End If
            If entityModel.Vouchers IsNot Nothing Then
                Dim vouchers = New List(Of Voucher)
                For Each item In entityModel.Vouchers
                    vouchers.Add(VoucherMapper.Map(item))
                Next
                technicalReport.Vouchers = vouchers
            End If
            Return technicalReport
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de reporte técnico de una reporte técnico</summary>
    '''<param name="entity">Reporte Técnico</param>
    '''<returns>Modelo de Reporte Técnico</returns>
    Public Shared Function Map(entity As TechnicalReport) As TechnicalReportModel
        If entity IsNot Nothing Then
            Dim technicalReportModel = New TechnicalReportModel With {
                                              .Id = entity.Id,
                                              .Client = PersonMapper.Map(entity.Client),
                                              .Employee = EmployeeMapper.Map(entity.Employee),
                                              .Appliance = ApplianceMapper.Map(entity.Appliance),
                                              .EntryDate = entity.EntryDate,
                                              .DeliveryDate = entity.DeliveryDate,
                                              .Failure = entity.Failure,
                                              .Diagnosis = entity.Diagnosis,
                                              .Service = entity.Service,
                                              .Price = entity.Price,
                                              .Observation = entity.Observation}
            If entity.Vouchers IsNot Nothing Then
                Dim vouchers = New List(Of VoucherModel)
                For Each item In entity.Vouchers
                    vouchers.Add(VoucherMapper.Map(item))
                Next
                technicalReportModel.Vouchers = vouchers
            End If
            technicalReportModel.Appliance.TechnicalReport = New TechnicalReportModel With {
                .Id = entity.Id
                }
            Return technicalReportModel
        Else
            Return Nothing
        End If

    End Function
End Class
