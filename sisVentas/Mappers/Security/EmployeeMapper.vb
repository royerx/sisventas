﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Empleado a entidad Empleado: 
''' Mapea los valores recibidos de un Modelo de Empleado a una entidad Empleado
''' </summary>
Public MustInherit Class EmployeeMapper

    '''<summary>Método para mapear persona de un modelo de empleado</summary>
    '''<param name="entityModel">Modelo de Empleado</param>
    '''<returns>Empleado</returns>
    Public Shared Function Map(entityModel As EmployeeModel) As Employee
        If entityModel IsNot Nothing Then
            Dim employee = New Employee With {
                                .Id = entityModel.Id,
                                .Person = PersonMapper.Map(entityModel.Person),
                                .Login = entityModel.Login,
                                .Pass = entityModel.Pass}
            If entityModel.Roles IsNot Nothing Then
                Dim roles = New List(Of Role)
                For Each item In entityModel.Roles
                    roles.Add(RoleMapper.Map(item))
                Next
                employee.Roles = roles
            End If
            Return employee
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de empleado de un Empleado</summary>
    '''<param name="entity">Empleado</param>
    '''<returns>Modelo de Empleado</returns>
    Public Shared Function Map(entity As Employee) As EmployeeModel
        If entity IsNot Nothing Then
            Dim employeeModel = New EmployeeModel With {
                                .Id = entity.Id,
                                .Login = entity.Login,
                                .Pass = entity.Pass}
            If entity.Person IsNot Nothing Then
                employeeModel.Person = PersonMapper.Map(entity.Person)
                employeeModel.FullName = entity.Person.GetFullName()
                employeeModel.LastName = entity.Person.LastName
                employeeModel.Name = entity.Person.Name
                employeeModel.DocumentNumber = entity.Person.DocumentNumber
                employeeModel.Address = entity.Person.Address
                employeeModel.Phone = entity.Person.Phone
                employeeModel.Email = entity.Person.Email
            End If
            If entity.Roles IsNot Nothing Then
                Dim roles = New List(Of RoleModel)
                For Each item In entity.Roles
                    roles.Add(RoleMapper.Map(item))
                Next
                employeeModel.Roles = roles
            End If
            Return employeeModel
        Else
            Return Nothing
        End If

    End Function
End Class
