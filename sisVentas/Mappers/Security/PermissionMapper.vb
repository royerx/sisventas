﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Permiso a entidad Permiso: 
''' Mapea los valores recibidos de un Modelo de Permiso a una entidad Permiso
''' </summary>
Public MustInherit Class PermissionMapper

    '''<summary>Método para mapear permiso de un modelo de permiso</summary>
    '''<param name="entityModel">Modelo de Permiso</param>
    '''<returns>Permiso</returns>
    Public Shared Function Map(entityModel As PermissionModel) As Permission
        If entityModel IsNot Nothing Then
            Dim permission = New Permission With {
                                .Id = entityModel.Id,
                                .Name = entityModel.Name,
                                .Slug = entityModel.Slug,
                                .Description = entityModel.Description}

            If entityModel.Roles IsNot Nothing Then
                Dim roles = New List(Of Role)
                For Each item In entityModel.Roles
                    roles.Add(RoleMapper.Map(item))
                Next
                permission.Roles = roles
            End If

            Return permission
        Else
            Return Nothing
        End If


    End Function

    '''<summary>Método para mapear modelo de permiso de un permiso</summary>
    '''<param name="entity">Permiso</param>
    '''<returns>Modelo de Permiso</returns>
    Public Shared Function Map(entity As Permission) As PermissionModel
        If entity IsNot Nothing Then
            Dim permissionModel = New PermissionModel With {
                                .Id = entity.Id,
                                .Name = entity.Name,
                                .Slug = entity.Slug,
                                .Description = entity.Description}

            If entity.Roles IsNot Nothing Then
                Dim roles = New List(Of RoleModel)
                For Each item In entity.Roles
                    roles.Add(RoleMapper.Map(item))
                Next
                permissionModel.Roles = roles
            End If

            Return permissionModel
        Else
            Return Nothing
        End If

    End Function
End Class
