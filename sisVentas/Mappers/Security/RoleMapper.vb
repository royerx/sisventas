﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Rol a entidad Rol: 
''' Mapea los valores recibidos de un Modelo de Rol a una entidad Rol
''' </summary>
Public MustInherit Class RoleMapper

    '''<summary>Método para mapear rol de un modelo de rol</summary>
    '''<param name="entityModel">Modelo de Rol</param>
    '''<returns>Rol</returns>
    Public Shared Function Map(entityModel As RoleModel) As Role
        If entityModel IsNot Nothing Then
            Dim role = New Role With {
                                .Id = entityModel.Id,
                                .Name = entityModel.Name,
                                .Description = entityModel.Description}

            If entityModel.Permissions IsNot Nothing Then
                Dim permissions = New List(Of Permission)
                For Each item In entityModel.Permissions
                    Dim permission = PermissionMapper.Map(item)
                    permissions.Add(permission)
                Next
                role.Permissions = permissions
            End If

            If entityModel.Employees IsNot Nothing Then
                Dim employees = New List(Of Employee)
                For Each item In entityModel.Employees
                    Dim employee = EmployeeMapper.Map(item)
                    employees.Add(employee)
                Next
                role.Employees = employees
            End If

            Return role
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de rol de un Rol</summary>
    '''<param name="entity">Role</param>
    '''<returns>Modelo de Rol</returns>
    Public Shared Function Map(entity As Role) As RoleModel
        If entity IsNot Nothing Then
            Dim roleModel = New RoleModel With {
                                .Id = entity.Id,
                                .Name = entity.Name,
                                .Description = entity.Description}

            If entity.Permissions IsNot Nothing Then
                Dim permissions = New List(Of PermissionModel)
                For Each item In entity.Permissions
                    Dim permission = PermissionMapper.Map(item)
                    permissions.Add(permission)
                Next
                roleModel.Permissions = permissions
            End If

            If entity.Employees IsNot Nothing Then
                Dim employees = New List(Of EmployeeModel)
                For Each item In entity.Employees
                    Dim employee = EmployeeMapper.Map(item)
                    employees.Add(employee)
                Next
                roleModel.Employees = employees
            End If

            Return roleModel
        Else
            Return Nothing
        End If

    End Function
End Class
