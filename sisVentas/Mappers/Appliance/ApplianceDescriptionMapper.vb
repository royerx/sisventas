﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Equipo de Cómputo a entidad Equipo de Cómputo: 
''' Mapea los valores recibidos de un modelo de equipo de cómputo a una entidad Equipo de Cómputo y viceversa
''' </summary>
Public MustInherit Class ApplianceDescriptionMapper

    '''<summary>Método para mapear equipo de cómputo de un modelo de equipo de cómputo</summary>
    '''<param name="entityModel">Modelo de Equipo de Cómputo</param>
    '''<returns>Equipo de Cómputo</returns>
    Public Shared Function Map(entityModel As ApplianceDescriptionModel) As ApplianceDescription
        If entityModel IsNot Nothing Then
            Return New ApplianceDescription With {
                                           .Id = entityModel.Id,
                                           .CategoryId = entityModel.Category.Id,
                                           .BrandId = entityModel.Brand.Id,
                                           .ApplianceName = entityModel.ApplianceName,
                                           .Model = entityModel.Model,
                                           .PartNumber = entityModel.PartNumber,
                                           .Price = entityModel.Price,
                                           .Stock = entityModel.Stock,
                                           .Detail = entityModel.Detail,
                                           .Category = CategoryMapper.Map(entityModel.Category),
                                           .Brand = BrandMapper.Map(entityModel.Brand)}
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de equipo de cómputo de un equipo de cómputo</summary>
    '''<param name="entity">Equipo de Cómputo</param>
    '''<returns>Modelo de Equipo de Cómputo</returns>
    Public Shared Function Map(entity As ApplianceDescription) As ApplianceDescriptionModel
        If entity IsNot Nothing Then
            Return New ApplianceDescriptionModel With {
                                                .Id = entity.Id,
                                                .ApplianceName = entity.ApplianceName,
                                                .Model = entity.Model,
                                                .PartNumber = entity.PartNumber,
                                                .Price = entity.Price,
                                                .Stock = entity.Stock,
                                                .Detail = entity.Detail,
                                                .Category = CategoryMapper.Map(entity.Category),
                                                .Brand = BrandMapper.Map(entity.Brand)}
        Else
            Return Nothing
        End If

    End Function
End Class
