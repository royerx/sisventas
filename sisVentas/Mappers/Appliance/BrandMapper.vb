﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Marca a entidad Marca: 
''' Mapea los valores recibidos de un modelo de marca a una entidad Marca y viceversa
''' </summary>
Public MustInherit Class BrandMapper

    '''<summary>Método para mapear marca de un modelo de marca</summary>
    '''<param name="entityModel">Modelo de Marca</param>
    '''<returns>Marca</returns>
    Public Shared Function Map(entityModel As BrandModel) As Brand
        If entityModel IsNot Nothing Then
            Return New Brand With {
                            .Id = entityModel.Id,
                            .Name = entityModel.Name,
                            .Detail = entityModel.Detail}
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de marca de una marca</summary>
    '''<param name="entity">Marca</param>
    '''<returns>Modelo de Marca</returns>
    Public Shared Function Map(entity As Brand) As BrandModel
        If entity IsNot Nothing Then
            Return New BrandModel With {
                                 .Id = entity.Id,
                                 .Name = entity.Name,
                                 .Detail = entity.Detail}
        Else
            Return Nothing
        End If

    End Function
End Class
