﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Detalle de Equipo de Cómputo a entidad Detalle de Equipo de Cómputo: 
''' Mapea los valores recibidos de un modelo de detalle de equipo de cómputo a una entidad Detalle de Equipo de Cómputo y viceversa
''' </summary>
Public MustInherit Class ApplianceMapper

    '''<summary>Método para mapear detalle equipo de cómputo de un modelo de detalle de equipo de cómputo</summary>
    '''<param name="entityModel">Modelo de Detalle de Equipo de Cómputo</param>
    '''<returns>Detalle de Equipo de Cómputo</returns>
    Public Shared Function Map(entityModel As ApplianceModel) As Appliance
        If entityModel IsNot Nothing Then
            Dim appliance = New Appliance With {
                                .Id = entityModel.Id,
                                .ApplianceDescription = ApplianceDescriptionMapper.Map(entityModel.ApplianceDescription),
                                .Serie = entityModel.Serie,
                                .State = entityModel.State,
                                .Entry = EntryMapper.Map(entityModel.Entry),
                                .BuyPrice = entityModel.BuyPrice}
            If entityModel.ApplianceDescription IsNot Nothing Then
                appliance.ApplianceDescriptionId = entityModel.ApplianceDescription.Id
            End If
            If entityModel.Entry IsNot Nothing Then
                appliance.EntryId = entityModel.Entry.Id
            End If
            Return appliance
        Else
            Return Nothing
        End If

    End Function
    '''<summary>Método para mapear modelo de detalle de equipo de cómputo de un detalle de equipo de cómputo</summary>
    '''<param name="entity">Detalle de Equipo de Cómputo</param>
    '''<returns>Modelo de Detalle de Equipo de Cómputo</returns>
    Public Shared Function Map(entity As Appliance) As ApplianceModel
        If entity IsNot Nothing Then
            Return New ApplianceModel With {
                                     .Id = entity.Id,
                                     .ApplianceDescription = ApplianceDescriptionMapper.Map(entity.ApplianceDescription),
                                     .Serie = entity.Serie,
                                     .State = entity.State,
                                     .Entry = EntryMapper.Map(entity.Entry),
                                     .BuyPrice = entity.BuyPrice}
        Else
            Return Nothing
        End If

    End Function
End Class
