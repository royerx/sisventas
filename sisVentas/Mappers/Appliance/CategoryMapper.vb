﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador Modelo de Categoria a entidad Categoria: 
''' Mapea los valores recibidos de un Modelo de Categoria a una entidad Categoria y viceversa
''' </summary>
Public MustInherit Class CategoryMapper

    '''<summary>Método para mapear categoria de un modelo de categoria</summary>
    '''<param name="entityModel">Modelo de Categoria</param>
    '''<returns>Categoria</returns>
    Public Shared Function Map(entityModel As CategoryModel) As Category
        If entityModel IsNot Nothing Then
            Return New Category With {
                                .Id = entityModel.Id,
                                .Name = entityModel.Name,
                                .Detail = entityModel.Detail}
        Else
            Return Nothing
        End If

    End Function

    '''<summary>Método para mapear modelo de categoria de una categoria</summary>
    '''<param name="entity">Categoria</param>
    '''<returns>Modelo de Categoria</returns>
    Public Shared Function Map(entity As Category) As CategoryModel
        If entity IsNot Nothing Then
            Return New CategoryModel With {
                                .Id = entity.Id,
                                .Name = entity.Name,
                                .Detail = entity.Detail}
        Else
            Return Nothing
        End If

    End Function
End Class
