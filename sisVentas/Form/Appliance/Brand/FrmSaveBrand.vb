﻿Imports System.Data.SqlClient
Imports Transitions
''' <summary>
''' Clase Formulario Guardar Marca: 
''' Formulario que permite editar o modificar una marca
''' </summary>
Public Class FrmSaveBrand

    'controlador del modelo de marca
    Private ReadOnly controller As New BrandController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveBrand = Nothing

    'Modelo de marca a ser agregada o modificada
    Public Property EntityModel As BrandModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveBrand
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveBrand
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateBrand.Visible = False
        lblValidateDetail.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Name") Then
                    lblValidateBrand.Text = item.ErrorMessage
                    lblValidateBrand.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtName.Text = ""
        txtDetail.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la marca seleccionada</summary>
    Private Sub FrmSaveBrand_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel.Id Then
            txtName.Text = EntityModel.Name
            txtDetail.Text = EntityModel.Detail
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar marca</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Name = txtName.Text.ToUpper
            EntityModel.Detail = txtDetail.Text.ToUpper

            If ValidateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Marca", "Marca modificada correctamente")
                        Desktop.FrmShowBrand.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Marca", "La marca NO pudo ser modificada")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Marca", "Marca registrada correctamente")
                        Desktop.FrmShowBrand.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Marca", "La marca NO pudo ser registrada")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Marca", txtName.Text & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Marca", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Marca", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSaveBrand_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 30
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    Private Sub FrmSaveBrand_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtName.Focus()
    End Sub
End Class