﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Categoria: 
''' Formulario que permite editar o modificar una categoria
''' </summary>
Public Class FrmSaveCategory

    'controlador del modelo de categoria
    Private ReadOnly controller As New CategoryController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveCategory = Nothing

    'Modelo de categoria a ser agregada o modificada
    Public Property EntityModel As CategoryModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveCategory
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveCategory
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateName.Visible = False
        lblValidateDetail.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Name") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtName.Text = ""
        txtDetail.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la categoria seleccionada</summary>
    Private Sub FrmSaveCategory_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel.Id Then
            txtName.Text = EntityModel.Name
            txtDetail.Text = EntityModel.Detail
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar categoria</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Name = txtName.Text.ToUpper
            EntityModel.Detail = txtDetail.Text.ToUpper

            If validateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Categoría", "Categoría modificada correctamente")
                        Desktop.FrmShowCategory.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Categoría", "La categoría NO pudo ser modificada")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Categoría", "Categoría registrada correctamente")
                        Desktop.FrmShowCategory.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Categoría", "La categoría NO pudo ser registrada")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Categoría", txtName.Text & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Categoría", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Categoría", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 30
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub
End Class