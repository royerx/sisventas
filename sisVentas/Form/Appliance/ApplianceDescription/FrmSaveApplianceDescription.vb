﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Descripción de Equipo de Cómputo: 
''' Formulario que permite editar o modificar una Descripción de Equipo de Cómputo
''' </summary>
Public Class FrmSaveApplianceDescription

    'controlador del modelo de Descripción de Equipo de Cómputo
    Private ReadOnly controller As New ApplianceDescriptionController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveApplianceDescription = Nothing

    'Modelo de Descripción de Equipo de Cómputo a ser agregado o modificado
    Public Property EntityModel As ApplianceDescriptionModel

    'Formulario de origen
    Public Property Form As String

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveApplianceDescription
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveApplianceDescription
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateCategory.Visible = False
        lblValidateBrand.Visible = False
        lblValidateName.Visible = False
        lblValidateModel.Visible = False
        lblValidatePartNumber.Visible = False
        lblValidateDetail.Visible = False
        lblValidatePrice.Visible = False
        lblValidateStock.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "ApplianceName") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "Model") Then
                    lblValidateModel.Text = item.ErrorMessage
                    lblValidateModel.Visible = True
                    txtModel.Focus()
                End If
                If (item.MemberNames.First = "PartNumber") Then
                    lblValidatePartNumber.Text = item.ErrorMessage
                    lblValidatePartNumber.Visible = True
                    txtPartNumber.Focus()
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtPrice.Focus()
                End If
                If (item.MemberNames.First = "Stock") Then
                    lblValidateStock.Text = item.ErrorMessage
                    lblValidateStock.Visible = True
                    txtStock.Focus()
                End If
                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        If cbxCategory.Text = "" Then
            lblValidateCategory.Text = "Campo Requerido"
            lblValidateCategory.Visible = True
            cbxCategory.Focus()
            Return False
        ElseIf cbxCategory.SelectedIndex = -1 Then
            Dim frm As New FrmConfirm(AlertResource.INFO, "Agregar Categoría", "La categoría " & cbxCategory.Text & " no esta registrada ¿Deseas Agregar la Categoría?", AlertResource.QUESTION)
            Dim result = frm.ShowDialog
            If result = DialogResult.OK Then
                FrmSaveCategory.getInstancia.lblTitle.Text = "NUEVA CATEGORÍA"
                FrmSaveCategory.getInstancia.EntityModel = New CategoryModel
                FrmSaveCategory.getInstancia.txtName.Text = cbxCategory.Text
                FrmSaveCategory.getInstancia.ShowDialog()
                cbxCategory.DataSource = controller.GetCategories()
            End If
            Return False
        End If

        If cbxBrand.Text = "" Then
            lblValidateBrand.Text = "Campo Requerido"
            lblValidateBrand.Visible = True
            cbxBrand.Focus()
            Return False
        ElseIf cbxBrand.SelectedIndex = -1 Then
            Dim frm As New FrmConfirm(AlertResource.INFO, "Agregar Marca", "La Marca " & cbxBrand.Text & " no esta registrada ¿Deseas Agregar la Marca?", AlertResource.QUESTION)
            Dim result = frm.ShowDialog
            If result = DialogResult.OK Then
                FrmSaveBrand.getInstancia.lblTitle.Text = "NUEVA MARCA"
                FrmSaveBrand.getInstancia.EntityModel = New BrandModel
                FrmSaveBrand.getInstancia.txtName.Text = cbxBrand.Text
                FrmSaveBrand.getInstancia.ShowDialog()
                cbxBrand.DataSource = controller.GetBrands()
            End If
            Return False
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        cbxCategory.SelectedIndex = -1
        cbxBrand.SelectedIndex = -1
        txtName.Text = ""
        txtModel.Text = ""
        txtPartNumber.Text = ""
        txtPrice.Text = ""
        txtStock.Text = ""
        txtDetail.Text = ""
    End Sub

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub FillTxt()
        If EntityModel.Id Then
            cbxCategory.SelectedItem = EntityModel.Category
            cbxBrand.SelectedItem = EntityModel.Brand
            txtName.Text = EntityModel.ApplianceName
            txtModel.Text = EntityModel.Model
            txtPartNumber.Text = EntityModel.PartNumber
            txtPrice.Text = EntityModel.Price
            txtStock.Text = EntityModel.Stock
            txtDetail.Text = EntityModel.Detail
            lblStock.Visible = True
            txtStock.Visible = True
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la Descripción de Equipo de Cómputo seleccionado</summary>
    Private Sub FrmSaveApplianceDescription_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        cbxCategory.DataSource = controller.GetCategories()
        cbxBrand.DataSource = controller.GetBrands()
        cbxCategory.SelectedIndex = -1
        cbxBrand.SelectedIndex = -1
        lblStock.Visible = False
        txtStock.Visible = False
        txtStock.Enabled = False
        FillTxt()
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Descripción de Equipo de Cómputo</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Category = cbxCategory.SelectedItem
            EntityModel.Brand = cbxBrand.SelectedItem
            EntityModel.ApplianceName = txtName.Text.ToUpper
            EntityModel.Model = txtModel.Text.ToUpper
            EntityModel.PartNumber = txtPartNumber.Text.ToUpper
            EntityModel.Price = Val(txtPrice.Text)
            EntityModel.Stock = Val(txtStock.Text)
            EntityModel.Detail = txtDetail.Text.ToUpper

            If validateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Equipo", "Equipo modificado correctamente")
                        FrmShowApplianceDescription.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Equipo", "El Equipo NO pudo ser modificado")
                    End If

                Else
                    Dim id = controller.Add(EntityModel)

                    If id Then
                        Select Case Form
                            Case "FrmSaveTechnicalReport"
                                FrmSaveTechnicalReport.getInstancia.txtCategoryBrand.Text = EntityModel.Category.Name & " " & EntityModel.Brand.Name
                                FrmSaveTechnicalReport.getInstancia.txtModel.Focus()
                                FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Equipo", "Equipo registrado correctamente Agregar series")
                                ClearTxt()
                                Me.Close()
                            Case Else
                                FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Equipo", "Equipo registrado correctamente Agregar series")
                                ClearTxt()
                                Me.Close()

                                FrmSaveAppliance.getInstancia.lblTitle.Text = "SERIES DE EQUIPO"
                                FrmSaveAppliance.getInstancia.ApplianceDescriptionModel = controller.GetById(id)
                                FrmSaveAppliance.getInstancia.EntityModel = New ApplianceModel
                                FrmSaveAppliance.getInstancia.ShowDialog()
                        End Select


                        Desktop.FrmShowApplianceDescription.getInstancia.GetAll("")
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Equipo", "El Equipo NO pudo ser registrado")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    Dim er = err.Message.IndexOf("(")
                    Dim tam = err.Message.Length - er
                    Dim msg = err.Message.Substring(er + 1, tam - 3)
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", msg & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para nombre del equipo de computo</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para validar caja de texto para modelo</summary>
    Private Sub txtModel_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtModel.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar caja de texto para numero de parte</summary>
    Private Sub txtPartNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPartNumber.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 30
    End Sub

    '''<summary>Método para validar caja de texto para precio</summary>
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        FormDesing.OnlyDecimal(e, sender)
        sender.MaxLength = 18
    End Sub

    '''<summary>Método para validar caja de texto para stock</summary>
    Private Sub txtStock_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStock.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 18
    End Sub

    '''<summary>Método para validar caja de texto para correo electrónico</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

End Class