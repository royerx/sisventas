﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Detalle de Equipo de Cómputo: 
''' Formulario que permite guardar los datos de un detalle de equipo de computo y listar los que pertenecen a una descripción
''' </summary>
Public Class FrmSaveAppliance

    'controlador del modelo de detalle de equipo de computo
    Private ReadOnly controller As New ApplianceController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveAppliance = Nothing

    'Modelo de descripcion de equipo de computo a ser utilizado
    Public Property ApplianceDescriptionModel As ApplianceDescriptionModel

    'Modelo de Detalle de equipo de computo
    Public Property EntityModel As ApplianceModel

    'Controla el boton de cancelar
    Private cancel As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveAppliance
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveAppliance
        End If
        Return _formulario
    End Function

    '''<summary>Método para llenar el datagridview con los detalles de equipos de cómputo filtrados</summary>
    Public Sub GetAll(search As String)
        Try
            lstSeries.DataSource = controller.GetAll(search, ApplianceDescriptionModel.Id)
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los detalles de equipos de cómputo filtrados guardada en memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            lstSeries.DataSource = controller.GetAllMemory(search, ApplianceDescriptionModel.Id)
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateTxt() As Boolean
        lblValidateSerie.Visible = False
        lblValidatePrice.Visible = False
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Serie") Then
                    lblValidateSerie.Text = item.ErrorMessage
                    lblValidateSerie.Visible = True
                    txtSerie.Focus()
                End If
                If (item.MemberNames.First = "State") Then
                    lblValidateSerie.Text = item.ErrorMessage
                    lblValidateSerie.Visible = True
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtPrice.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function


    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel.Id = 0
        EntityModel.Serie = Nothing
        EntityModel.State = Nothing
        txtSerie.Text = ""
        txtPrice.Text = ""
        FormDesing.Placeholder(txtSerie, "Serie...")
        chkActive.Checked = False
    End Sub

    '''<summary>Método para mostrar los datos del detalle de Equipo de Cómputo seleccionados</summary>
    Private Sub FrmSaveAppliance_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        FormDesing.Placeholder(txtSerie, "Serie...")
        btnAdd.Visible = Auth.Can(Values.ApplianceCreate)
        btnEdit.Visible = Auth.Can(Values.ApplianceDetail)
        btnDelete.Visible = Auth.Can(Values.ApplianceDelete)
        toolTip.SetToolTip(btnAdd, "Agregar serie")
        toolTip.SetToolTip(btnDelete, "Eliminar serie")
        toolTip.SetToolTip(btnEdit, "Editar serie")
        chkActive.Visible = False
    End Sub

    '''<summary>Método para filtrar por numero de serie</summary>
    Private Sub txtSerie_TextChanged(sender As Object, e As EventArgs) Handles txtSerie.TextChanged
        If EntityModel.Id = 0 Then
            If txtSerie.Text = "" Or txtSerie.Text = "Serie..." Then
                GetAllMemory("")
            Else
                GetAllMemory(txtSerie.Text)
            End If
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        FrmShowApplianceDescription.getInstancia.GetAll("")
        Dispose()
    End Sub

    '''<summary>Método que habilita la edicion del detalle de equipo de computo</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click


        txtSerie.Text = lstSeries.SelectedItem.Serie
        chkActive.Checked = lstSeries.SelectedItem.State
        txtPrice.Text = lstSeries.SelectedItem.BuyPrice
        EntityModel = lstSeries.SelectedItem
        btnDelete.Visible = False
        chkActive.Visible = True
        btnAdd.Image = My.Resources.save
        toolTip.SetToolTip(btnAdd, "Guardar serie")
        btnEdit.Image = My.Resources.close
        toolTip.SetToolTip(btnEdit, "Cancelar")
        If cancel Then
            btnDelete.Visible = True
            chkActive.Visible = False
            btnAdd.Image = My.Resources.add
            btnEdit.Image = My.Resources.edit
            toolTip.SetToolTip(btnEdit, "Editar Serie")
            toolTip.SetToolTip(btnAdd, "Agregar serie")
            ClearTxt()
            GetAll("")
            cancel = False
        Else
            cancel = True
        End If
    End Sub

    'Placeholder para la caja de texto
    Private Sub txtSerie_GotFocus(sender As Object, e As EventArgs) Handles txtSerie.GotFocus
        FormDesing.Placeholder(sender, "Serie...")
    End Sub

    Private Sub txtSerie_LostFocus(sender As Object, e As EventArgs) Handles txtSerie.LostFocus
        FormDesing.Placeholder(sender, "Serie...")
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Detalles de Descripción de Equipo de Cómputo</summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            If txtSerie.Text = "Serie..." Then
                EntityModel.Serie = ""
            Else
                EntityModel.Serie = txtSerie.Text
            End If

            EntityModel.ApplianceDescription = ApplianceDescriptionModel

            If EntityModel.Id Then
                EntityModel.State = chkActive.Checked
            Else
                EntityModel.State = True
            End If
            EntityModel.BuyPrice = Val(txtPrice.Text)
            If ValidateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Equipo", "Equipo modificado correctamente")
                        GetAll("")
                        ClearTxt()
                        btnDelete.Visible = True
                        chkActive.Visible = False
                        btnAdd.Image = My.Resources.add
                        cancel = False
                        btnEdit.Image = My.Resources.edit
                        toolTip.SetToolTip(btnAdd, "Agregar serie")
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Equipo", "El Equipo NO pudo ser modificado")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Equipo", "Equipo registrado correctamente")
                        GetAll("")
                        ClearTxt()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Equipo", "El Equipo NO pudo ser registrado")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    Dim er = err.Message.IndexOf("(")
                    Dim tam = err.Message.Length - er
                    Dim msg = err.Message.Substring(er + 1, tam - 3)
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", msg & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", ex.Message)
        End Try
    End Sub

    '''<summary>Método para eliminar un detalle de equipo de cómputo</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Equipo", "¿Realmente desea eliminar la Serie " & lstSeries.SelectedItem.Serie & "?", AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then

                If controller.Delete(lstSeries.SelectedItem.Id) Then
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Equipo Eliminado", "Serie " & lstSeries.SelectedItem.Serie & " eliminada correctamente", AlertResource.SUCCESS)
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", "La Serie " & lstSeries.SelectedItem.Serie & " no fue eliminada")
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Equipo", lstSeries.SelectedItem.Serie & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", ex.Message)
        End Try
        GetAll("")
    End Sub

    '''<summary>Método para mostrar el formulario para escanear codigo de barras y/o QR</summary>
    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click
        FrmScan.getInstancia.Form = Me.Name
        FrmScan.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar datos de la base de datos</summary>
    Private Sub txtSerie_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSerie.KeyDown
        If e.KeyData = Keys.Enter Then
            If txtSerie.Text = "" Or txtSerie.Text = "Serie..." Then
                GetAll("")
            Else
                GetAll(txtSerie.Text)
            End If
        End If
    End Sub

    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        sender.MaxLength = 18
        FormDesing.OnlyDecimal(e, sender)
    End Sub
End Class