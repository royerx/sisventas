﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Equipos de Cómputo: 
''' Formulario que muestra las opciones de mantenimiento de equipos de cómputo
''' </summary>
Public Class FrmShowApplianceDescription

    'controlador del modelo de equipo de cómputo
    Private ReadOnly controller As New ApplianceDescriptionController()

    'lista de modelo de equipo de cómputo con filtros
    Private dt As New List(Of ApplianceDescriptionModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowApplianceDescription = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowApplianceDescription
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowApplianceDescription
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista de equipos de cómputo cuando carga el formulario</summary>
    Private Sub FrmShowApplianceDescription_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.ApplianceDescriptionCreate)
        btnEdit.Visible = Auth.Can(Values.ApplianceDescriptionDetail)
        btnDelete.Visible = Auth.Can(Values.ApplianceDescriptionDelete)
        btnCategory.Visible = Auth.Can(Values.CategoryList)
        btnBrand.Visible = Auth.Can(Values.BrandList)
        btnSerie.Visible = Auth.Can(Values.ApplianceList)
    End Sub
    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con los equipos de cómputo filtrados</summary>
    Public Sub GetAll(search As String)
        Try
            dt = controller.GetAll(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Category").HeaderText = "CATEGORÍA"
                dtgList.Columns.Item("Brand").HeaderText = "MARCA"
                dtgList.Columns.Item("ApplianceName").HeaderText = "EQUIPO"
                dtgList.Columns.Item("Model").HeaderText = "MODELO"
                dtgList.Columns.Item("PartNumber").HeaderText = "NÚMERO DE PARTE"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Stock").HeaderText = "STOCK"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLE"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los equipos de cómputo filtrados de memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            dt = controller.GetAllMemory(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Category").HeaderText = "CATEGORÍA"
                dtgList.Columns.Item("Brand").HeaderText = "MARCA"
                dtgList.Columns.Item("ApplianceName").HeaderText = "EQUIPO"
                dtgList.Columns.Item("Model").HeaderText = "MODELO"
                dtgList.Columns.Item("PartNumber").HeaderText = "NÚMERO DE PARTE"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Stock").HeaderText = "STOCK"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLE"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar equipo de cómputo</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSaveApplianceDescription.getInstancia.lblTitle.Text = "AGREGAR EQUIPO"
        FrmSaveApplianceDescription.getInstancia.EntityModel = New ApplianceDescriptionModel
        FrmSaveApplianceDescription.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar equipo de cómputo</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSaveApplianceDescription.getInstancia.lblTitle.Text = "EDITAR EQUIPO"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveApplianceDescription.getInstancia.EntityModel = entityModel
            FrmSaveApplianceDescription.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Equipo", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar un equipo de cómputo o una seleccion de equipos de cómputo</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar el equipo " & dtgList.CurrentRow.Cells.Item("ApplianceName").Value & "?"
            Else
                msg = "¿Realmente desea eliminar los equipos seleccionados?"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Equipo", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", "El Equipo " & row.Cells("ApplianceName").Value & " no fue eliminado")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Equipos Eliminados", "Equipos eliminados correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Equipo Eliminado", "Equipo " & dtgList.CurrentRow.Cells.Item("ApplianceName").Value & " eliminado correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", "El Equipo " & dtgList.CurrentRow.Cells.Item("ApplianceName").Value & " no fue eliminado")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Equipo", "Uno o varios de los registros tiene elementos asignados y no se puedo eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Equipo", dtgList.CurrentRow.Cells.Item("ApplianceName").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Equipo", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("")
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrar equipos de cómputo escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("")
        Else
            GetAllMemory(txtSearch.Text)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de categorias</summary>
    Private Sub btnCategory_Click(sender As Object, e As EventArgs) Handles btnCategory.Click
        frmPrincipal.lblTitle.Text = "CATEGORÍAS"
        frmPrincipal.FormPanel(FrmShowCategory.getInstancia)
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de marcas</summary>
    Private Sub btnBrand_Click(sender As Object, e As EventArgs) Handles btnBrand.Click
        frmPrincipal.lblTitle.Text = "MARCAS"
        frmPrincipal.FormPanel(FrmShowBrand.getInstancia)
    End Sub

    '''<summary>Método para mostrar el formulario de series de equipos</summary>
    Private Sub btnSerie_Click(sender As Object, e As EventArgs) Handles btnSerie.Click
        FrmSaveAppliance.getInstancia.lblTitle.Text = "SERIES DE EQUIPO"
        Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
        FrmSaveAppliance.getInstancia.ApplianceDescriptionModel = entityModel
        FrmSaveAppliance.getInstancia.EntityModel = New ApplianceModel
        FrmSaveAppliance.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("")
            Else
                GetAll(txtSearch.Text)
            End If
        End If
    End Sub
End Class