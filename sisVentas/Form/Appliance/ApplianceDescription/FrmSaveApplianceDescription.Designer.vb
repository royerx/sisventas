﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveApplianceDescription
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.lblValidateDetail = New System.Windows.Forms.Label()
        Me.lblValidatePartNumber = New System.Windows.Forms.Label()
        Me.lblValidateModel = New System.Windows.Forms.Label()
        Me.cbxCategory = New System.Windows.Forms.ComboBox()
        Me.cbxBrand = New System.Windows.Forms.ComboBox()
        Me.lblValidateStock = New System.Windows.Forms.Label()
        Me.lblValidatePrice = New System.Windows.Forms.Label()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtStock = New System.Windows.Forms.TextBox()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblStock = New System.Windows.Forms.Label()
        Me.lblValidateName = New System.Windows.Forms.Label()
        Me.lblValidateBrand = New System.Windows.Forms.Label()
        Me.lblValidateCategory = New System.Windows.Forms.Label()
        Me.lblCategory = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblDetail = New System.Windows.Forms.Label()
        Me.lblBrand = New System.Windows.Forms.Label()
        Me.txtDetail = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtModel = New System.Windows.Forms.TextBox()
        Me.lblPartNumber = New System.Windows.Forms.Label()
        Me.lblModel = New System.Windows.Forms.Label()
        Me.txtPartNumber = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBody.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(702, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "EQUIPOS"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(733, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(711, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.lblValidateDetail)
        Me.pnlBody.Controls.Add(Me.lblValidatePartNumber)
        Me.pnlBody.Controls.Add(Me.lblValidateModel)
        Me.pnlBody.Controls.Add(Me.cbxCategory)
        Me.pnlBody.Controls.Add(Me.cbxBrand)
        Me.pnlBody.Controls.Add(Me.lblValidateStock)
        Me.pnlBody.Controls.Add(Me.lblValidatePrice)
        Me.pnlBody.Controls.Add(Me.txtPrice)
        Me.pnlBody.Controls.Add(Me.txtStock)
        Me.pnlBody.Controls.Add(Me.lblPrice)
        Me.pnlBody.Controls.Add(Me.lblStock)
        Me.pnlBody.Controls.Add(Me.lblValidateName)
        Me.pnlBody.Controls.Add(Me.lblValidateBrand)
        Me.pnlBody.Controls.Add(Me.lblValidateCategory)
        Me.pnlBody.Controls.Add(Me.lblCategory)
        Me.pnlBody.Controls.Add(Me.txtName)
        Me.pnlBody.Controls.Add(Me.lblDetail)
        Me.pnlBody.Controls.Add(Me.lblBrand)
        Me.pnlBody.Controls.Add(Me.txtDetail)
        Me.pnlBody.Controls.Add(Me.lblName)
        Me.pnlBody.Controls.Add(Me.txtModel)
        Me.pnlBody.Controls.Add(Me.lblPartNumber)
        Me.pnlBody.Controls.Add(Me.lblModel)
        Me.pnlBody.Controls.Add(Me.txtPartNumber)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(733, 416)
        Me.pnlBody.TabIndex = 46
        '
        'lblValidateDetail
        '
        Me.lblValidateDetail.AutoSize = True
        Me.lblValidateDetail.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDetail.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDetail.Location = New System.Drawing.Point(397, 200)
        Me.lblValidateDetail.Name = "lblValidateDetail"
        Me.lblValidateDetail.Size = New System.Drawing.Size(83, 15)
        Me.lblValidateDetail.TabIndex = 81
        Me.lblValidateDetail.Text = "Ingrese Detalle"
        Me.lblValidateDetail.Visible = False
        '
        'lblValidatePartNumber
        '
        Me.lblValidatePartNumber.AutoSize = True
        Me.lblValidatePartNumber.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePartNumber.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePartNumber.Location = New System.Drawing.Point(36, 330)
        Me.lblValidatePartNumber.Name = "lblValidatePartNumber"
        Me.lblValidatePartNumber.Size = New System.Drawing.Size(134, 15)
        Me.lblValidatePartNumber.TabIndex = 80
        Me.lblValidatePartNumber.Text = "Ingrese Numero de Parte"
        Me.lblValidatePartNumber.Visible = False
        '
        'lblValidateModel
        '
        Me.lblValidateModel.AutoSize = True
        Me.lblValidateModel.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateModel.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateModel.Location = New System.Drawing.Point(36, 266)
        Me.lblValidateModel.Name = "lblValidateModel"
        Me.lblValidateModel.Size = New System.Drawing.Size(88, 15)
        Me.lblValidateModel.TabIndex = 79
        Me.lblValidateModel.Text = "Ingrese Modelo"
        Me.lblValidateModel.Visible = False
        '
        'cbxCategory
        '
        Me.cbxCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxCategory.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxCategory.FormattingEnabled = True
        Me.cbxCategory.Location = New System.Drawing.Point(31, 43)
        Me.cbxCategory.Name = "cbxCategory"
        Me.cbxCategory.Size = New System.Drawing.Size(300, 27)
        Me.cbxCategory.TabIndex = 58
        '
        'cbxBrand
        '
        Me.cbxBrand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxBrand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxBrand.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxBrand.FormattingEnabled = True
        Me.cbxBrand.Location = New System.Drawing.Point(31, 104)
        Me.cbxBrand.Name = "cbxBrand"
        Me.cbxBrand.Size = New System.Drawing.Size(300, 27)
        Me.cbxBrand.TabIndex = 59
        '
        'lblValidateStock
        '
        Me.lblValidateStock.AutoSize = True
        Me.lblValidateStock.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateStock.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateStock.Location = New System.Drawing.Point(393, 328)
        Me.lblValidateStock.Name = "lblValidateStock"
        Me.lblValidateStock.Size = New System.Drawing.Size(74, 15)
        Me.lblValidateStock.TabIndex = 78
        Me.lblValidateStock.Text = "Ingrese Stock"
        Me.lblValidateStock.Visible = False
        '
        'lblValidatePrice
        '
        Me.lblValidatePrice.AutoSize = True
        Me.lblValidatePrice.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePrice.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePrice.Location = New System.Drawing.Point(393, 263)
        Me.lblValidatePrice.Name = "lblValidatePrice"
        Me.lblValidatePrice.Size = New System.Drawing.Size(78, 15)
        Me.lblValidatePrice.TabIndex = 77
        Me.lblValidatePrice.Text = "Ingrese Precio"
        Me.lblValidatePrice.Visible = False
        '
        'txtPrice
        '
        Me.txtPrice.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrice.Location = New System.Drawing.Point(392, 239)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(300, 24)
        Me.txtPrice.TabIndex = 64
        '
        'txtStock
        '
        Me.txtStock.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStock.Location = New System.Drawing.Point(392, 303)
        Me.txtStock.Name = "txtStock"
        Me.txtStock.Size = New System.Drawing.Size(300, 24)
        Me.txtStock.TabIndex = 65
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrice.Location = New System.Drawing.Point(392, 215)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(55, 19)
        Me.lblPrice.TabIndex = 75
        Me.lblPrice.Text = "Precio:"
        '
        'lblStock
        '
        Me.lblStock.AutoSize = True
        Me.lblStock.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStock.Location = New System.Drawing.Point(392, 281)
        Me.lblStock.Name = "lblStock"
        Me.lblStock.Size = New System.Drawing.Size(50, 19)
        Me.lblStock.TabIndex = 76
        Me.lblStock.Text = "Stock:"
        '
        'lblValidateName
        '
        Me.lblValidateName.AutoSize = True
        Me.lblValidateName.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateName.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateName.Location = New System.Drawing.Point(36, 198)
        Me.lblValidateName.Name = "lblValidateName"
        Me.lblValidateName.Size = New System.Drawing.Size(88, 15)
        Me.lblValidateName.TabIndex = 74
        Me.lblValidateName.Text = "Ingrese Nombre"
        Me.lblValidateName.Visible = False
        '
        'lblValidateBrand
        '
        Me.lblValidateBrand.AutoSize = True
        Me.lblValidateBrand.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateBrand.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateBrand.Location = New System.Drawing.Point(36, 132)
        Me.lblValidateBrand.Name = "lblValidateBrand"
        Me.lblValidateBrand.Size = New System.Drawing.Size(81, 15)
        Me.lblValidateBrand.TabIndex = 73
        Me.lblValidateBrand.Text = "Ingrese Marca"
        Me.lblValidateBrand.Visible = False
        '
        'lblValidateCategory
        '
        Me.lblValidateCategory.AutoSize = True
        Me.lblValidateCategory.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateCategory.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateCategory.Location = New System.Drawing.Point(36, 67)
        Me.lblValidateCategory.Name = "lblValidateCategory"
        Me.lblValidateCategory.Size = New System.Drawing.Size(99, 15)
        Me.lblValidateCategory.TabIndex = 72
        Me.lblValidateCategory.Text = "Ingrese Categoria"
        Me.lblValidateCategory.Visible = False
        '
        'lblCategory
        '
        Me.lblCategory.AutoSize = True
        Me.lblCategory.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(35, 19)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(83, 19)
        Me.lblCategory.TabIndex = 66
        Me.lblCategory.Text = "Categoria:"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(31, 174)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(300, 24)
        Me.txtName.TabIndex = 60
        '
        'lblDetail
        '
        Me.lblDetail.AutoSize = True
        Me.lblDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(396, 19)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(67, 19)
        Me.lblDetail.TabIndex = 71
        Me.lblDetail.Text = "Detalles:"
        '
        'lblBrand
        '
        Me.lblBrand.AutoSize = True
        Me.lblBrand.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBrand.Location = New System.Drawing.Point(31, 84)
        Me.lblBrand.Name = "lblBrand"
        Me.lblBrand.Size = New System.Drawing.Size(59, 19)
        Me.lblBrand.TabIndex = 67
        Me.lblBrand.Text = "Marca:"
        '
        'txtDetail
        '
        Me.txtDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetail.Location = New System.Drawing.Point(396, 43)
        Me.txtDetail.Multiline = True
        Me.txtDetail.Name = "txtDetail"
        Me.txtDetail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDetail.Size = New System.Drawing.Size(300, 155)
        Me.txtDetail.TabIndex = 63
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(31, 150)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 19)
        Me.lblName.TabIndex = 68
        Me.lblName.Text = "Nombre:"
        '
        'txtModel
        '
        Me.txtModel.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModel.Location = New System.Drawing.Point(31, 239)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(300, 24)
        Me.txtModel.TabIndex = 61
        '
        'lblPartNumber
        '
        Me.lblPartNumber.AutoSize = True
        Me.lblPartNumber.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartNumber.Location = New System.Drawing.Point(31, 279)
        Me.lblPartNumber.Name = "lblPartNumber"
        Me.lblPartNumber.Size = New System.Drawing.Size(130, 19)
        Me.lblPartNumber.TabIndex = 70
        Me.lblPartNumber.Text = "Número de Parte:"
        '
        'lblModel
        '
        Me.lblModel.AutoSize = True
        Me.lblModel.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModel.Location = New System.Drawing.Point(31, 215)
        Me.lblModel.Name = "lblModel"
        Me.lblModel.Size = New System.Drawing.Size(66, 19)
        Me.lblModel.TabIndex = 69
        Me.lblModel.Text = "Modelo:"
        '
        'txtPartNumber
        '
        Me.txtPartNumber.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPartNumber.Location = New System.Drawing.Point(31, 303)
        Me.txtPartNumber.Name = "txtPartNumber"
        Me.txtPartNumber.Size = New System.Drawing.Size(300, 24)
        Me.txtPartNumber.TabIndex = 62
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(209, 358)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 45)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(369, 358)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 45)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'FrmSaveApplianceDescription
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(733, 447)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveApplianceDescription"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents cbxCategory As ComboBox
    Friend WithEvents cbxBrand As ComboBox
    Friend WithEvents lblValidateStock As Label
    Friend WithEvents lblValidatePrice As Label
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtStock As TextBox
    Friend WithEvents lblPrice As Label
    Friend WithEvents lblStock As Label
    Friend WithEvents lblValidateName As Label
    Friend WithEvents lblValidateBrand As Label
    Friend WithEvents lblValidateCategory As Label
    Friend WithEvents lblCategory As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents lblDetail As Label
    Friend WithEvents lblBrand As Label
    Friend WithEvents txtDetail As TextBox
    Friend WithEvents lblName As Label
    Friend WithEvents txtModel As TextBox
    Friend WithEvents lblPartNumber As Label
    Friend WithEvents lblModel As Label
    Friend WithEvents txtPartNumber As TextBox
    Friend WithEvents lblValidateDetail As Label
    Friend WithEvents lblValidatePartNumber As Label
    Friend WithEvents lblValidateModel As Label
End Class
