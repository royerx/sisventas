﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Detalle de Equipo de Cómputo: 
''' Formulario que permite guardar los datos de un detalle de equipo de computo y listar los que pertenecen a una descripción
''' </summary>
Public Class FrmListAppliance

    'controlador del modelo de detalle de equipo de computo
    Private ReadOnly controller As New ApplianceDescriptionController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmListAppliance = Nothing

    'lista de modelo de equipo de cómputo con filtros
    Private dt As New List(Of ApplianceDescriptionModel)

    'Formulario de origen
    Public Property Form As String

    'Modelo de descripcion de equipo de computo a ser utilizado
    Public Property ApplianceDescriptionModel As ApplianceDescriptionModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmListAppliance
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmListAppliance
        End If
        Return _formulario
    End Function

    '''<summary>Método para llenar el datagridview con los detalles de equipos de cómputo filtrados</summary>
    Public Sub GetAll(search As String)
        Try
            dt = controller.GetAll(search)

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Category").HeaderText = "CATEGORÍA"
                dtgList.Columns.Item("Brand").HeaderText = "MARCA"
                dtgList.Columns.Item("ApplianceName").HeaderText = "EQUIPO"
                dtgList.Columns.Item("Model").HeaderText = "MODELO"
                dtgList.Columns.Item("PartNumber").HeaderText = "NÚMERO DE PARTE"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Stock").HeaderText = "STOCK"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLE"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los detalles de equipos de cómputo filtrados en memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            dt = controller.GetAllMemory(search)

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Category").HeaderText = "CATEGORÍA"
                dtgList.Columns.Item("Brand").HeaderText = "MARCA"
                dtgList.Columns.Item("ApplianceName").HeaderText = "EQUIPO"
                dtgList.Columns.Item("Model").HeaderText = "MODELO"
                dtgList.Columns.Item("PartNumber").HeaderText = "NÚMERO DE PARTE"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Stock").HeaderText = "STOCK"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLE"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar los datos del detalle de Equipo de Cómputo seleccionados</summary>
    Private Sub FrmListAppliance_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        FormDesing.Placeholder(txtSearch, "Buscar...")
        toolTip.SetToolTip(btnAdd, "Seleccionar Equipo")
    End Sub

    '''<summary>Método para filtrar</summary>
    Private Sub txtSerie_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("")
        Else
            GetAllMemory(txtSearch.Text)
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        FrmShowQuoted.getInstancia.Save = False
        FrmShowQuoted.getInstancia.Cancel = False
        FrmShowQuoted.getInstancia.SaveControls()
        Dispose()
    End Sub

    'Placeholder para la caja de texto
    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para agregar Detalles de Descripción de Equipo de Cómputo</summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            Select Case Form
                Case "FrmShowQuoted"
                    FrmShowQuoted.getInstancia.SetAppliance(controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value))
                    FrmShowQuoted.getInstancia.Save = True
                    FrmShowQuoted.getInstancia.Cancel = True
                    FrmShowQuoted.getInstancia.SaveControls()
                    Close()
                Case "FrmSaveEntry"
                    FrmSaveEntry.getInstancia.SetAppliance(controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value))
                    Close()
            End Select
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Equipo", ex.Message)
        End Try
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyData = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("")
            Else
                GetAll(txtSearch.Text)
            End If
        End If
    End Sub
End Class