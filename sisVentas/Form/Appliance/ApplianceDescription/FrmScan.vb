﻿Imports AForge.Video.DirectShow
Imports Transitions
Imports ZXing

''' <summary>
''' Clase Formulario Escanear Codigo de Barras/QR: 
''' Formulario que permite escanear un codigo de barras o un qr y mostrar en la caja de texto de equipo
''' </summary>
Public Class FrmScan

    'Listado de camaras
    Private webcams As FilterInfoCollection

    'Captura de video
    Private source As VideoCaptureDevice

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmScan = Nothing

    'Formulario de origen
    Public Property Form As String

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmScan
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmScan
        End If
        Return _formulario
    End Function

    '''<summary>Método inicializar la camara web</summary>
    Private Sub FrmScan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 1.0R)
        t.run()

        'Activacion de la camara y mostrar video
        webcams = New FilterInfoCollection(FilterCategory.VideoInputDevice)
        tmrClock.Enabled = True
        tmrClock.Start()
        source = New VideoCaptureDevice(webcams(0).MonikerString)
        vspSource.VideoSource = source
        vspSource.Start()
    End Sub

    '''<summary>Método para escanear el codigo de barras y/o QR</summary>
    Private Sub tmrClock_Tick(sender As Object, e As EventArgs) Handles tmrClock.Tick
        If vspSource.GetCurrentVideoFrame() IsNot Nothing Then
            Dim img As New Bitmap(vspSource.GetCurrentVideoFrame)
            Dim DECODER = New BarcodeReader
            If DECODER.Decode(img) IsNot Nothing Then
                Select Case Form
                    Case "FrmSaveAppliance"
                        FrmSaveAppliance.getInstancia.txtSerie.Text = DECODER.Decode(img).ToString
                    Case "FrmSaveOrder"
                        FrmSaveOrder.getInstancia.txtAppliance.Text = DECODER.Decode(img).ToString
                    Case "FrmSaveEntry"
                        FrmSaveEntry.getInstancia.txtAppliance.Text = DECODER.Decode(img).ToString
                End Select
                tmrClock.Enabled = False
                vspSource.Stop()
                Close()
            End If
            img.Dispose()
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        tmrClock.Enabled = False
        vspSource.Stop()
        Close()
    End Sub
End Class