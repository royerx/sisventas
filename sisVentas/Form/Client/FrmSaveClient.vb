﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Cliente: 
''' Formulario que permite editar o modificar un Cliente
''' </summary>
Public Class FrmSaveClient

    'controlador del modelo de Cliente
    Private ReadOnly controller As New ClientController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveClient = Nothing

    'Modelo de Cliente a ser agregado o modificado
    Public Property EntityModel As PersonModel

    'Variable que determina si se trata de una insercion o no
    Public Property Save As Boolean

    'Formulario de origen
    Public Property Form As String

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveClient
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveClient
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateLastName.Visible = False
        lblValidateName.Visible = False
        lblValidateDocument.Visible = False
        lblValidateAddress.Visible = False
        lblValidatePhone.Visible = False
        lblValidateEmail.Visible = False
    End Sub

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub EnableTxt(state As Boolean)
        txtLastName.Enabled = state
        txtName.Enabled = state
        txtAddress.Enabled = state
        txtPhone.Enabled = state
        txtEmail.Enabled = state
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "LastName") Then
                    lblValidateLastName.Text = item.ErrorMessage
                    lblValidateLastName.Visible = True
                    txtLastName.Focus()
                End If
                If (item.MemberNames.First = "Name") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "DocumentNumber") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "Address") Then
                    lblValidateAddress.Text = item.ErrorMessage
                    lblValidateAddress.Visible = True
                    txtAddress.Focus()
                End If
                If (item.MemberNames.First = "Phone") Then
                    lblValidatePhone.Text = item.ErrorMessage
                    lblValidatePhone.Visible = True
                    txtPhone.Focus()
                End If
                If (item.MemberNames.First = "Email") Then
                    lblValidateEmail.Text = item.ErrorMessage
                    lblValidateEmail.Visible = True
                    txtEmail.Focus()
                End If
            Next
        End If
        If Not chkBussines.Checked And txtLastName.Text = "" Then
            lblValidateLastName.Text = "Campo Requerido"
            lblValidateLastName.Visible = True
            txtLastName.Focus()
            Return False
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = New PersonModel
        txtLastName.Text = ""
        txtName.Text = ""
        txtDocument.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtEmail.Text = ""
    End Sub

    '''<summary>Método para mostrar elementos dependiendo si es cliente natural o juridico</summary>
    Public Sub ShowControls()
        If chkBussines.Checked Then
            lblLastName.Visible = False
            txtLastName.Visible = False
            txtLastName.Text = ""
            lblDocument.Text = "RUC"
        Else
            lblLastName.Visible = True
            txtLastName.Visible = True
            lblDocument.Text = "DNI"
        End If
    End Sub

    '''<summary>Método para validar los controles cuando la persona ya se encuentra registrado como persona</summary>
    Public Sub ValidateClient()
        If Not chkBussines.Checked Then
            If txtDocument.Text.Length = 8 Then
                EnableTxt(True)
                Dim person = controller.GetByDocument(txtDocument.Text)
                If person IsNot Nothing Then
                    EntityModel = person
                    txtLastName.Text = EntityModel.LastName
                    txtName.Text = EntityModel.Name
                    txtAddress.Text = EntityModel.Address
                    txtPhone.Text = EntityModel.Phone
                    txtEmail.Text = EntityModel.Email
                    EnableTxt(False)
                Else
                    txtLastName.Text = ""
                    txtName.Text = ""
                    txtAddress.Text = ""
                    txtPhone.Text = ""
                    txtEmail.Text = ""
                    EnableTxt(True)
                End If
            Else
                EnableTxt(False)
            End If
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del Cliente seleccionado</summary>
    Private Sub FrmSaveClient_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel IsNot Nothing Then
            If EntityModel.Id Then
                chkBussines.Visible = False
            End If
            If EntityModel.LastName = "" And EntityModel.Id Then
                chkBussines.Checked = True
                ShowControls()
            End If
            txtLastName.Text = EntityModel.LastName
            txtName.Text = EntityModel.Name
            txtDocument.Text = EntityModel.DocumentNumber
            txtAddress.Text = EntityModel.Address
            txtPhone.Text = EntityModel.Phone
            txtEmail.Text = EntityModel.Email
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Cliente</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.LastName = txtLastName.Text.ToUpper
            EntityModel.Name = txtName.Text.ToUpper
            EntityModel.DocumentNumber = txtDocument.Text.ToUpper
            EntityModel.Address = txtAddress.Text.ToUpper
            EntityModel.Phone = txtPhone.Text.ToUpper
            EntityModel.Email = txtEmail.Text.ToLower


            If validateTxt() Then

                If Not Save Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Cliente", "Cliente modificado correctamente")
                        Desktop.FrmShowClient.getInstancia.GetAll("")
                        Select Case Form
                            Case "FrmSaveOrder"
                                FrmSaveOrder.getInstancia.txtDocument.Text = ""
                                FrmSaveOrder.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveOrder.getInstancia.txtAppliance.Focus()
                            Case "FrmSaveTechnicalReport"
                                FrmSaveTechnicalReport.getInstancia.txtDocument.Text = ""
                                FrmSaveTechnicalReport.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveTechnicalReport.getInstancia.txtAppliance.Focus()
                            Case "FrmSaveReceptionSheet"
                                FrmSaveReceptionSheet.getInstancia.txtDocument.Text = ""
                                FrmSaveReceptionSheet.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveReceptionSheet.getInstancia.txtDate.Focus()
                        End Select
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Cliente", "El Cliente NO pudo ser modificado")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Cliente", "Cliente registrado correctamente")
                        Desktop.FrmShowClient.getInstancia.GetAll("")
                        Select Case Form
                            Case "FrmSaveOrder"
                                FrmSaveOrder.getInstancia.txtDocument.Text = ""
                                FrmSaveOrder.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveOrder.getInstancia.txtAppliance.Focus()
                            Case "FrmSaveTechnicalReport"
                                FrmSaveTechnicalReport.getInstancia.txtDocument.Text = ""
                                FrmSaveTechnicalReport.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveTechnicalReport.getInstancia.txtAppliance.Focus()
                            Case "FrmSaveReceptionSheet"
                                FrmSaveReceptionSheet.getInstancia.txtDocument.Text = ""
                                FrmSaveReceptionSheet.getInstancia.txtDocument.Text = EntityModel.DocumentNumber
                                FrmSaveReceptionSheet.getInstancia.txtDate.Focus()
                        End Select
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Cliente", "El Cliente NO pudo ser registrado")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Cliente", txtDocument.Text & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Cliente", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Cliente", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        EnableTxt(True)
        HideLblValidate()
    End Sub

    '''<summary>Método para seleccionar y mostrar controles si se trata de una empresa o no</summary>
    Private Sub chkBussines_CheckedChanged(sender As Object, e As EventArgs) Handles chkBussines.CheckedChanged
        If EntityModel.Id = 0 Then
            If chkBussines.Checked And Form Is Nothing Then
                EnableTxt(True)
                ClearTxt()
            Else
                EnableTxt(False)
                If Form Is Nothing Then
                    ClearTxt()
                End If
            End If
            txtDocument.Focus()
            ShowControls()
        End If
    End Sub

    '''<summary>Método para validar caja de texto para apellidos</summary>
    Private Sub txtLastName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtLastName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para validar caja de texto para numero de documento</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
        If chkBussines.Checked Then
            sender.MaxLength = 11
        Else
            sender.MaxLength = 8
        End If
    End Sub

    '''<summary>Método para validar caja de texto para dirección</summary>
    Private Sub txtAddress_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAddress.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar caja de texto para telefono</summary>
    Private Sub txtPhone_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPhone.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 11
    End Sub

    '''<summary>Método para validar caja de texto para correo electrónico</summary>
    Private Sub txtEmail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtEmail.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 100
    End Sub

    Private Sub FrmSaveClient_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtDocument.Focus()
    End Sub

    '''<summary>Método para validar una persona ya registrada</summary>
    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        If Save And sender.text.length = 8 Then
            ValidateClient()
        Else
            EnableTxt(True)
        End If
    End Sub
End Class