﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Entradas: 
''' Formulario que muestra las opciones de mantenimiento de entrada
''' </summary>
Public Class FrmShowEntry

    'controlador del modelo de entrada
    Private ReadOnly controller As New EntryController()

    'lista de modelo de entrada con filtros
    Private dt As New List(Of EntryModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowEntry = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowEntry
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowEntry
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista de entradas cuando carga el formulario</summary>
    Private Sub FrmShowEntry_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.EntryCreate)
        btnEdit.Visible = Auth.Can(Values.EntryDetail)
        btnDelete.Visible = Auth.Can(Values.EntryDelete)
        btnShow.Visible = Auth.Can(Values.EntryDetail)
        rbtDate.Checked = True
    End Sub
    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con entradas filtradas</summary>
    Public Sub GetAll(search As String, beginDate As Date, endDate As Date)
        Try
            If rbtDate.Checked Then
                dt = controller.GetAll(search, beginDate, endDate)
            End If
            If rbtDates.Checked Then
                dt = controller.GetAll(search, beginDate, endDate)
            End If
            If rbtTotal.Checked Then
                dt = controller.GetAll(search)
            End If

            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("VoucherNumber").HeaderText = "NÚMERO"
                dtgList.Columns.Item("Supplier").HeaderText = "PROVEEDOR"
                dtgList.Columns.Item("Employee").HeaderText = "EMPLEADO"
                dtgList.Columns.Item("Edate").HeaderText = "FECHA"
                dtgList.Columns.Item("DocumentType").HeaderText = "TIPO"
                dtgList.Columns.Item("Igv").HeaderText = "IGV"
                dtgList.Columns.Item("Total").HeaderText = "TOTAL"
                dtgList.Columns.Item("State").HeaderText = "ESTADO"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLES"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Serie").Visible = False
                dtgList.Columns.Item("Correlative").Visible = False
                dtgList.Columns.Item("Appliances").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los comprobantes de pago filtrados en memoria</summary>
    Public Sub GetAllMemory(search As String, beginDate As Date, endDate As Date)
        Try
            If rbtDate.Checked Then
                dt = controller.GetAllMemory(search, beginDate, Nothing)
            End If
            If rbtDates.Checked Then
                dt = controller.GetAllMemory(search, beginDate, endDate)
            End If
            If rbtTotal.Checked Then
                dt = controller.GetAllMemory(search)
            End If
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("VoucherNumber").HeaderText = "NÚMERO"
                dtgList.Columns.Item("Supplier").HeaderText = "PROVEEDOR"
                dtgList.Columns.Item("Employee").HeaderText = "EMPLEADO"
                dtgList.Columns.Item("Edate").HeaderText = "FECHA"
                dtgList.Columns.Item("DocumentType").HeaderText = "TIPO"
                dtgList.Columns.Item("Igv").HeaderText = "IGV"
                dtgList.Columns.Item("Total").HeaderText = "TOTAL"
                dtgList.Columns.Item("State").HeaderText = "ESTADO"
                dtgList.Columns.Item("Detail").HeaderText = "DETALLES"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Serie").Visible = False
                dtgList.Columns.Item("Correlative").Visible = False
                dtgList.Columns.Item("Appliances").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar una entrada</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSaveEntry.getInstancia.lblTitle.Text = "NUEVA COMPRA"
        FrmSaveEntry.getInstancia.EntityModel = New EntryModel
        FrmSaveEntry.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar una entrada</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSaveEntry.getInstancia.lblTitle.Text = "EDITAR COMPRA"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveEntry.getInstancia.EntityModel = entityModel
            FrmSaveEntry.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Compra", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar una entrada o una seleccion de entradas</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar La Compra N° " & dtgList.CurrentRow.Cells.Item("VoucherNumber").Value & "?"
            Else
                msg = "¿Realmente desea eliminar las compras"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Compra", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Compra", "La Compra N° " & row.Cells("VoucherNumber").Value & " no fue eliminada")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Compras Eliminadas", "Compras eliminadas correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Eliminar Compra", "Compra N° " & dtgList.CurrentRow.Cells.Item("VoucherNumber").Value & " eliminada correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Compra", "La Compra N° " & dtgList.CurrentRow.Cells.Item("VoucherNumber").Value & " no fue eliminada")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Compra", "Uno o varios de los registros tiene elementos asignados y no se pudo eliminar, primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Compra", "Compra N° " & dtgList.CurrentRow.Cells.Item("VoucherNumber").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Compra", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Compra", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrar compras escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("", txtBeginDate.Value, txtEndDate.Value)
        Else
            GetAllMemory(txtSearch.Text, txtBeginDate.Value, txtEndDate.Value)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para mostrar el formulario de comprobante de pago</summary>
    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        Try
            FrmSaveEntry.getInstancia.lblTitle.Text = "MOSTRAR COMPRA"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveEntry.getInstancia.EntityModel = entityModel
            FrmSaveEntry.getInstancia.detail = True
            FrmSaveEntry.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Mostrar Compra", ex.Message)
        End Try
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("", txtBeginDate.Value, txtEndDate.Value)
            Else
                GetAll(txtSearch.Text, txtBeginDate.Value, txtEndDate.Value)
            End If
        End If
    End Sub

    '''<summary>Carga los datos filtrados por una fecha dada</summary>
    Private Sub rbtDate_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDate.CheckedChanged
        lblLat.Visible = False
        txtEndDate.Visible = False
        lblBegin.Visible = True
        txtBeginDate.Visible = True
        txtBeginDate.Value = Now
        GetAll("", txtBeginDate.Value, Nothing)
    End Sub

    '''<summary>Carga los datos filtrados en un rango de fechas</summary>
    Private Sub rbtDates_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDates.CheckedChanged
        lblLat.Visible = True
        txtEndDate.Visible = True
        lblBegin.Visible = True
        txtBeginDate.Visible = True
        txtBeginDate.Value = Now.AddMonths(-1)
        txtEndDate.Value = Now
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    '''<summary>Carga todos los datos de las compras</summary>
    Private Sub rbtTotal_CheckedChanged(sender As Object, e As EventArgs) Handles rbtTotal.CheckedChanged
        lblLat.Visible = False
        txtEndDate.Visible = False
        lblBegin.Visible = False
        txtBeginDate.Visible = False
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub txtBeginDate_ValueChanged(sender As Object, e As EventArgs) Handles txtBeginDate.ValueChanged
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub txtEndDate_ValueChanged(sender As Object, e As EventArgs) Handles txtEndDate.ValueChanged
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub btnSupplier_Click(sender As Object, e As EventArgs) Handles btnSupplier.Click
        frmPrincipal.lblTitle.Text = "PROVEEDORES"
        frmPrincipal.FormPanel(FrmShowSupplier.getInstancia)
    End Sub
End Class