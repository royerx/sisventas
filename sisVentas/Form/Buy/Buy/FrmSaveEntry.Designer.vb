﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveEntry
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.btnSearchAppliance = New System.Windows.Forms.Button()
        Me.txtAppliance = New System.Windows.Forms.TextBox()
        Me.lblValidateAppliance = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtDetail = New System.Windows.Forms.TextBox()
        Me.btnScan = New System.Windows.Forms.Button()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtApplianceSerie = New System.Windows.Forms.TextBox()
        Me.lblValidatePrice = New System.Windows.Forms.Label()
        Me.lblValidateApplianceSerie = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblAppliance = New System.Windows.Forms.Label()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtSupplier = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.cbxState = New System.Windows.Forms.ComboBox()
        Me.lblValidateState = New System.Windows.Forms.Label()
        Me.txtCorrelative = New System.Windows.Forms.TextBox()
        Me.lblValidateCorrelative = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblState = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxType = New System.Windows.Forms.ComboBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblSupplier = New System.Windows.Forms.Label()
        Me.dtgList = New System.Windows.Forms.DataGridView()
        Me.txtDocument = New System.Windows.Forms.TextBox()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.DateTimePicker()
        Me.lblValidateType = New System.Windows.Forms.Label()
        Me.lblValidateDate = New System.Windows.Forms.Label()
        Me.lblValidateDetail = New System.Windows.Forms.Label()
        Me.lblValidateDocument = New System.Windows.Forms.Label()
        Me.lblDocument = New System.Windows.Forms.Label()
        Me.lblDetail = New System.Windows.Forms.Label()
        Me.lblValidateSerie = New System.Windows.Forms.Label()
        Me.lblNUmber = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBody.SuspendLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(895, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "COMPRAS"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(926, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(904, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.btnSearchAppliance)
        Me.pnlBody.Controls.Add(Me.txtAppliance)
        Me.pnlBody.Controls.Add(Me.lblValidateAppliance)
        Me.pnlBody.Controls.Add(Me.Label3)
        Me.pnlBody.Controls.Add(Me.btnSearch)
        Me.pnlBody.Controls.Add(Me.txtDetail)
        Me.pnlBody.Controls.Add(Me.btnScan)
        Me.pnlBody.Controls.Add(Me.txtPrice)
        Me.pnlBody.Controls.Add(Me.txtApplianceSerie)
        Me.pnlBody.Controls.Add(Me.lblValidatePrice)
        Me.pnlBody.Controls.Add(Me.lblValidateApplianceSerie)
        Me.pnlBody.Controls.Add(Me.lblPrice)
        Me.pnlBody.Controls.Add(Me.lblAppliance)
        Me.pnlBody.Controls.Add(Me.btnEdit)
        Me.pnlBody.Controls.Add(Me.btnDelete)
        Me.pnlBody.Controls.Add(Me.btnAdd)
        Me.pnlBody.Controls.Add(Me.txtSupplier)
        Me.pnlBody.Controls.Add(Me.txtAddress)
        Me.pnlBody.Controls.Add(Me.cbxState)
        Me.pnlBody.Controls.Add(Me.lblValidateState)
        Me.pnlBody.Controls.Add(Me.txtCorrelative)
        Me.pnlBody.Controls.Add(Me.lblValidateCorrelative)
        Me.pnlBody.Controls.Add(Me.lblAddress)
        Me.pnlBody.Controls.Add(Me.lblState)
        Me.pnlBody.Controls.Add(Me.Label1)
        Me.pnlBody.Controls.Add(Me.cbxType)
        Me.pnlBody.Controls.Add(Me.lblTotal)
        Me.pnlBody.Controls.Add(Me.lblSupplier)
        Me.pnlBody.Controls.Add(Me.dtgList)
        Me.pnlBody.Controls.Add(Me.txtDocument)
        Me.pnlBody.Controls.Add(Me.txtSerie)
        Me.pnlBody.Controls.Add(Me.txtDate)
        Me.pnlBody.Controls.Add(Me.lblValidateType)
        Me.pnlBody.Controls.Add(Me.lblValidateDate)
        Me.pnlBody.Controls.Add(Me.lblValidateDetail)
        Me.pnlBody.Controls.Add(Me.lblValidateDocument)
        Me.pnlBody.Controls.Add(Me.lblDocument)
        Me.pnlBody.Controls.Add(Me.lblDetail)
        Me.pnlBody.Controls.Add(Me.lblValidateSerie)
        Me.pnlBody.Controls.Add(Me.lblNUmber)
        Me.pnlBody.Controls.Add(Me.lblType)
        Me.pnlBody.Controls.Add(Me.lblDate)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(926, 451)
        Me.pnlBody.TabIndex = 46
        '
        'btnSearchAppliance
        '
        Me.btnSearchAppliance.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSearchAppliance.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSearchAppliance.FlatAppearance.BorderSize = 0
        Me.btnSearchAppliance.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSearchAppliance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearchAppliance.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearchAppliance.ForeColor = System.Drawing.Color.White
        Me.btnSearchAppliance.Image = Global.sisVentas.Desktop.My.Resources.Resources.searchAppliance
        Me.btnSearchAppliance.Location = New System.Drawing.Point(874, 21)
        Me.btnSearchAppliance.Name = "btnSearchAppliance"
        Me.btnSearchAppliance.Size = New System.Drawing.Size(30, 30)
        Me.btnSearchAppliance.TabIndex = 162
        Me.btnSearchAppliance.UseVisualStyleBackColor = False
        '
        'txtAppliance
        '
        Me.txtAppliance.Enabled = False
        Me.txtAppliance.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppliance.Location = New System.Drawing.Point(337, 24)
        Me.txtAppliance.Name = "txtAppliance"
        Me.txtAppliance.Size = New System.Drawing.Size(525, 25)
        Me.txtAppliance.TabIndex = 159
        '
        'lblValidateAppliance
        '
        Me.lblValidateAppliance.AutoSize = True
        Me.lblValidateAppliance.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateAppliance.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateAppliance.Location = New System.Drawing.Point(340, 48)
        Me.lblValidateAppliance.Name = "lblValidateAppliance"
        Me.lblValidateAppliance.Size = New System.Drawing.Size(81, 15)
        Me.lblValidateAppliance.TabIndex = 161
        Me.lblValidateAppliance.Text = "Ingrese Equipo"
        Me.lblValidateAppliance.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(333, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 19)
        Me.Label3.TabIndex = 160
        Me.Label3.Text = "Equipo:"
        '
        'btnSearch
        '
        Me.btnSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSearch.FlatAppearance.BorderSize = 0
        Me.btnSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSearch.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.ForeColor = System.Drawing.Color.White
        Me.btnSearch.Image = Global.sisVentas.Desktop.My.Resources.Resources.search
        Me.btnSearch.Location = New System.Drawing.Point(284, 23)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(30, 30)
        Me.btnSearch.TabIndex = 158
        Me.btnSearch.UseVisualStyleBackColor = False
        '
        'txtDetail
        '
        Me.txtDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetail.Location = New System.Drawing.Point(14, 359)
        Me.txtDetail.Name = "txtDetail"
        Me.txtDetail.Size = New System.Drawing.Size(300, 24)
        Me.txtDetail.TabIndex = 6
        '
        'btnScan
        '
        Me.btnScan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnScan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnScan.FlatAppearance.BorderSize = 0
        Me.btnScan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScan.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScan.ForeColor = System.Drawing.Color.White
        Me.btnScan.Image = Global.sisVentas.Desktop.My.Resources.Resources.scan
        Me.btnScan.Location = New System.Drawing.Point(620, 76)
        Me.btnScan.Name = "btnScan"
        Me.btnScan.Size = New System.Drawing.Size(41, 30)
        Me.btnScan.TabIndex = 148
        Me.btnScan.UseVisualStyleBackColor = False
        '
        'txtPrice
        '
        Me.txtPrice.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrice.Location = New System.Drawing.Point(667, 79)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(123, 25)
        Me.txtPrice.TabIndex = 140
        '
        'txtApplianceSerie
        '
        Me.txtApplianceSerie.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplianceSerie.Location = New System.Drawing.Point(337, 79)
        Me.txtApplianceSerie.Name = "txtApplianceSerie"
        Me.txtApplianceSerie.Size = New System.Drawing.Size(277, 25)
        Me.txtApplianceSerie.TabIndex = 139
        '
        'lblValidatePrice
        '
        Me.lblValidatePrice.AutoSize = True
        Me.lblValidatePrice.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePrice.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePrice.Location = New System.Drawing.Point(672, 102)
        Me.lblValidatePrice.Name = "lblValidatePrice"
        Me.lblValidatePrice.Size = New System.Drawing.Size(78, 15)
        Me.lblValidatePrice.TabIndex = 147
        Me.lblValidatePrice.Text = "Ingrese Precio"
        Me.lblValidatePrice.Visible = False
        '
        'lblValidateApplianceSerie
        '
        Me.lblValidateApplianceSerie.AutoSize = True
        Me.lblValidateApplianceSerie.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateApplianceSerie.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateApplianceSerie.Location = New System.Drawing.Point(340, 102)
        Me.lblValidateApplianceSerie.Name = "lblValidateApplianceSerie"
        Me.lblValidateApplianceSerie.Size = New System.Drawing.Size(81, 15)
        Me.lblValidateApplianceSerie.TabIndex = 146
        Me.lblValidateApplianceSerie.Text = "Ingrese Equipo"
        Me.lblValidateApplianceSerie.Visible = False
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrice.Location = New System.Drawing.Point(686, 59)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(55, 19)
        Me.lblPrice.TabIndex = 145
        Me.lblPrice.Text = "Precio:"
        '
        'lblAppliance
        '
        Me.lblAppliance.AutoSize = True
        Me.lblAppliance.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliance.Location = New System.Drawing.Point(333, 59)
        Me.lblAppliance.Name = "lblAppliance"
        Me.lblAppliance.Size = New System.Drawing.Size(45, 19)
        Me.lblAppliance.TabIndex = 144
        Me.lblAppliance.Text = "Serie:"
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Image = Global.sisVentas.Desktop.My.Resources.Resources.edit
        Me.btnEdit.Location = New System.Drawing.Point(832, 76)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(30, 30)
        Me.btnEdit.TabIndex = 143
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Image = Global.sisVentas.Desktop.My.Resources.Resources.minus
        Me.btnDelete.Location = New System.Drawing.Point(868, 76)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(30, 30)
        Me.btnDelete.TabIndex = 142
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Image = Global.sisVentas.Desktop.My.Resources.Resources.add
        Me.btnAdd.Location = New System.Drawing.Point(796, 76)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 30)
        Me.btnAdd.TabIndex = 141
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'txtSupplier
        '
        Me.txtSupplier.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupplier.Location = New System.Drawing.Point(14, 80)
        Me.txtSupplier.Name = "txtSupplier"
        Me.txtSupplier.Size = New System.Drawing.Size(300, 24)
        Me.txtSupplier.TabIndex = 138
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(14, 133)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(300, 24)
        Me.txtAddress.TabIndex = 137
        '
        'cbxState
        '
        Me.cbxState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxState.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxState.FormattingEnabled = True
        Me.cbxState.Location = New System.Drawing.Point(392, 398)
        Me.cbxState.Name = "cbxState"
        Me.cbxState.Size = New System.Drawing.Size(164, 27)
        Me.cbxState.TabIndex = 132
        '
        'lblValidateState
        '
        Me.lblValidateState.AutoSize = True
        Me.lblValidateState.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateState.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateState.Location = New System.Drawing.Point(395, 423)
        Me.lblValidateState.Name = "lblValidateState"
        Me.lblValidateState.Size = New System.Drawing.Size(81, 15)
        Me.lblValidateState.TabIndex = 136
        Me.lblValidateState.Text = "Ingrese Estado"
        Me.lblValidateState.Visible = False
        '
        'txtCorrelative
        '
        Me.txtCorrelative.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCorrelative.Location = New System.Drawing.Point(143, 244)
        Me.txtCorrelative.Name = "txtCorrelative"
        Me.txtCorrelative.Size = New System.Drawing.Size(171, 24)
        Me.txtCorrelative.TabIndex = 129
        '
        'lblValidateCorrelative
        '
        Me.lblValidateCorrelative.AutoSize = True
        Me.lblValidateCorrelative.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateCorrelative.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateCorrelative.Location = New System.Drawing.Point(144, 266)
        Me.lblValidateCorrelative.Name = "lblValidateCorrelative"
        Me.lblValidateCorrelative.Size = New System.Drawing.Size(103, 15)
        Me.lblValidateCorrelative.TabIndex = 135
        Me.lblValidateCorrelative.Text = "Ingrese Correlativo"
        Me.lblValidateCorrelative.Visible = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(14, 110)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(84, 20)
        Me.lblAddress.TabIndex = 133
        Me.lblAddress.Text = "Dirección:"
        '
        'lblState
        '
        Me.lblState.AutoSize = True
        Me.lblState.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(326, 401)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(60, 19)
        Me.lblState.TabIndex = 131
        Me.lblState.Text = "Estado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(120, 244)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 23)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "-"
        '
        'cbxType
        '
        Me.cbxType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxType.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.FormattingEnabled = True
        Me.cbxType.Location = New System.Drawing.Point(14, 185)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Size = New System.Drawing.Size(300, 27)
        Me.cbxType.TabIndex = 128
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(562, 401)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(322, 24)
        Me.lblTotal.TabIndex = 127
        Me.lblTotal.Text = "TOTAL"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSupplier
        '
        Me.lblSupplier.AutoSize = True
        Me.lblSupplier.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupplier.Location = New System.Drawing.Point(14, 61)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(92, 20)
        Me.lblSupplier.TabIndex = 120
        Me.lblSupplier.Text = "Proveedor:"
        '
        'dtgList
        '
        Me.dtgList.AllowUserToAddRows = False
        Me.dtgList.AllowUserToDeleteRows = False
        Me.dtgList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dtgList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dtgList.ColumnHeadersHeight = 30
        Me.dtgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgList.EnableHeadersVisualStyles = False
        Me.dtgList.Location = New System.Drawing.Point(330, 120)
        Me.dtgList.Name = "dtgList"
        Me.dtgList.ReadOnly = True
        Me.dtgList.RowHeadersVisible = False
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.WhiteSmoke
        Me.dtgList.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dtgList.RowTemplate.Height = 24
        Me.dtgList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgList.Size = New System.Drawing.Size(583, 263)
        Me.dtgList.TabIndex = 0
        '
        'txtDocument
        '
        Me.txtDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocument.Location = New System.Drawing.Point(14, 26)
        Me.txtDocument.Name = "txtDocument"
        Me.txtDocument.Size = New System.Drawing.Size(264, 24)
        Me.txtDocument.TabIndex = 4
        '
        'txtSerie
        '
        Me.txtSerie.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.Location = New System.Drawing.Point(14, 244)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(102, 24)
        Me.txtSerie.TabIndex = 3
        '
        'txtDate
        '
        Me.txtDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(14, 299)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(300, 26)
        Me.txtDate.TabIndex = 5
        '
        'lblValidateType
        '
        Me.lblValidateType.AutoSize = True
        Me.lblValidateType.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateType.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateType.Location = New System.Drawing.Point(14, 209)
        Me.lblValidateType.Name = "lblValidateType"
        Me.lblValidateType.Size = New System.Drawing.Size(167, 15)
        Me.lblValidateType.TabIndex = 80
        Me.lblValidateType.Text = "Ingrese Numero de Documento"
        Me.lblValidateType.Visible = False
        '
        'lblValidateDate
        '
        Me.lblValidateDate.AutoSize = True
        Me.lblValidateDate.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDate.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDate.Location = New System.Drawing.Point(14, 323)
        Me.lblValidateDate.Name = "lblValidateDate"
        Me.lblValidateDate.Size = New System.Drawing.Size(78, 15)
        Me.lblValidateDate.TabIndex = 79
        Me.lblValidateDate.Text = "Ingrese Fecha"
        Me.lblValidateDate.Visible = False
        '
        'lblValidateDetail
        '
        Me.lblValidateDetail.AutoSize = True
        Me.lblValidateDetail.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDetail.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDetail.Location = New System.Drawing.Point(15, 381)
        Me.lblValidateDetail.Name = "lblValidateDetail"
        Me.lblValidateDetail.Size = New System.Drawing.Size(83, 15)
        Me.lblValidateDetail.TabIndex = 78
        Me.lblValidateDetail.Text = "Ingrese Detalle"
        Me.lblValidateDetail.Visible = False
        '
        'lblValidateDocument
        '
        Me.lblValidateDocument.AutoSize = True
        Me.lblValidateDocument.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDocument.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDocument.Location = New System.Drawing.Point(14, 48)
        Me.lblValidateDocument.Name = "lblValidateDocument"
        Me.lblValidateDocument.Size = New System.Drawing.Size(167, 15)
        Me.lblValidateDocument.TabIndex = 77
        Me.lblValidateDocument.Text = "Ingrese Numero de Documento"
        Me.lblValidateDocument.Visible = False
        '
        'lblDocument
        '
        Me.lblDocument.AutoSize = True
        Me.lblDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(14, 4)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(176, 19)
        Me.lblDocument.TabIndex = 75
        Me.lblDocument.Text = "Número de Documento:"
        '
        'lblDetail
        '
        Me.lblDetail.AutoSize = True
        Me.lblDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(14, 337)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(62, 19)
        Me.lblDetail.TabIndex = 76
        Me.lblDetail.Text = "Detalle:"
        '
        'lblValidateSerie
        '
        Me.lblValidateSerie.AutoSize = True
        Me.lblValidateSerie.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateSerie.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateSerie.Location = New System.Drawing.Point(14, 266)
        Me.lblValidateSerie.Name = "lblValidateSerie"
        Me.lblValidateSerie.Size = New System.Drawing.Size(71, 15)
        Me.lblValidateSerie.TabIndex = 74
        Me.lblValidateSerie.Text = "Ingrese Serie"
        Me.lblValidateSerie.Visible = False
        '
        'lblNUmber
        '
        Me.lblNUmber.AutoSize = True
        Me.lblNUmber.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNUmber.Location = New System.Drawing.Point(14, 222)
        Me.lblNUmber.Name = "lblNUmber"
        Me.lblNUmber.Size = New System.Drawing.Size(166, 19)
        Me.lblNUmber.TabIndex = 68
        Me.lblNUmber.Text = "Número Comprobante"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(14, 165)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(166, 19)
        Me.lblType.TabIndex = 70
        Me.lblType.Text = "Tipo de Comprobante:"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(14, 278)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(53, 19)
        Me.lblDate.TabIndex = 69
        Me.lblDate.Text = "Fecha"
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(27, 397)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 45)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(187, 397)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 45)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'FrmSaveEntry
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(926, 482)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveEntry"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents lblValidateDocument As Label
    Friend WithEvents txtDocument As TextBox
    Friend WithEvents lblDocument As Label
    Friend WithEvents lblValidateSerie As Label
    Friend WithEvents txtSerie As TextBox
    Friend WithEvents lblNUmber As Label
    Friend WithEvents lblType As Label
    Friend WithEvents lblDate As Label
    Friend WithEvents lblValidateType As Label
    Friend WithEvents lblValidateDate As Label
    Friend WithEvents txtDate As DateTimePicker
    Friend WithEvents lblValidateDetail As Label
    Friend WithEvents txtDetail As TextBox
    Friend WithEvents lblDetail As Label
    Friend WithEvents dtgList As DataGridView
    Friend WithEvents lblSupplier As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents toolTip As ToolTip
    Friend WithEvents lblAddress As Label
    Friend WithEvents cbxState As ComboBox
    Friend WithEvents lblState As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCorrelative As TextBox
    Friend WithEvents cbxType As ComboBox
    Friend WithEvents lblValidateCorrelative As Label
    Friend WithEvents lblValidateState As Label
    Friend WithEvents txtSupplier As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents btnScan As Button
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtApplianceSerie As TextBox
    Friend WithEvents lblValidatePrice As Label
    Friend WithEvents lblValidateApplianceSerie As Label
    Friend WithEvents lblPrice As Label
    Friend WithEvents lblAppliance As Label
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnSearch As Button
    Friend WithEvents txtAppliance As TextBox
    Friend WithEvents lblValidateAppliance As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnSearchAppliance As Button
End Class
