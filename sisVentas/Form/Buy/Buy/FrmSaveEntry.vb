﻿Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Entrada: 
''' Formulario que permite editar o modificar una Entrada
''' </summary>
Public Class FrmSaveEntry

    'controlador del modelo de Entrada
    Private ReadOnly controller As New EntryController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveEntry = Nothing

    'Entrada a ser agregada o modificada
    Public Property EntityModel As EntryModel

    'Equipo que se le agrega, edita o elimina en la entrada
    Private appliance As ApplianceModel

    'Descripción de Equipo que se le agrega, edita o elimina en la entrada
    Private applianceDescription As ApplianceDescriptionModel

    'Lista de equipos que se le agrega, edita o elimina en la entrada
    Private appliances As New List(Of ApplianceModel)

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Edit As Boolean = False

    'Permite mostrar los controles para editar o guardar los elementos
    Public Property detail As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveEntry
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveEntry
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateDocument.Visible = False
        lblValidateType.Visible = False
        lblValidateSerie.Visible = False
        lblValidateCorrelative.Visible = False
        lblValidateDate.Visible = False
        lblValidateDetail.Visible = False
        lblValidateState.Visible = False
        lblValidateApplianceSerie.Visible = False
        lblValidateApplianceSerie.Visible = False
        lblValidatePrice.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateEntryTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Supplier") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "Edate") Then
                    lblValidateDate.Text = item.ErrorMessage
                    lblValidateDate.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "DocumentType") Then
                    lblValidateType.Text = item.ErrorMessage
                    lblValidateType.Visible = True
                    cbxType.Focus()
                End If
                If (item.MemberNames.First = "Serie") Then
                    lblValidateSerie.Text = item.ErrorMessage
                    lblValidateSerie.Visible = True
                    txtSerie.Focus()
                End If
                If (item.MemberNames.First = "Correlative") Then
                    lblValidateCorrelative.Text = item.ErrorMessage
                    lblValidateCorrelative.Visible = True
                    txtCorrelative.Focus()
                End If
                If (item.MemberNames.First = "State") Then
                    lblValidateState.Text = item.ErrorMessage
                    lblValidateState.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "Employee") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Comprobante de Pago", "Datos del empleado incorrectos inicie sesion nuevamente")
                End If

                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateApplianceTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(appliance)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "ApplianceDescription") Then
                    lblValidateAppliance.Text = item.ErrorMessage
                    lblValidateAppliance.Visible = True
                    btnSearchAppliance.Focus()
                End If
                If (item.MemberNames.First = "Serie") Then
                    lblValidateApplianceSerie.Text = item.ErrorMessage
                    lblValidateApplianceSerie.Visible = True
                    txtSerie.Focus()
                End If
                If (item.MemberNames.First = "State") Then
                    lblValidateSerie.Text = item.ErrorMessage
                    lblValidateSerie.Visible = True
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtPrice.Focus()
                End If
            Next
        End If
        If (txtAppliance.Text.Length = 0) Then
            lblValidateAppliance.Text = "Seleccionar Descripción"
            lblValidateAppliance.Visible = True
            btnSearchAppliance.Focus()
            Return False
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        appliance = Nothing
        appliances = Nothing
        txtApplianceSerie.Text = ""
        txtAppliance.Text = ""
        txtPrice.Text = ""
        txtDocument.Text = ""
        cbxType.SelectedIndex = 0
        txtSerie.Text = ""
        txtCorrelative.Text = ""
        txtDate.Text = ""
        txtDetail.Text = ""
        cbxState.SelectedIndex = 0
        txtSupplier.Text = ""
        txtAddress.Text = ""
    End Sub

    '''<summary>Método para resetear los valores principales del formulario para agregar equipo</summary>
    Private Sub ClearApplianceTxt()
        appliance = New ApplianceModel
        txtApplianceSerie.Text = ""
    End Sub

    '''<summary>Método para llenar el datagridview con los equipos de la entrada</summary>
    Public Sub GetAllDetails()
        Try
            dtgList.DataSource = Nothing
            dtgList.Columns.Clear()
            If EntityModel.Appliances IsNot Nothing Then

                dtgList.Columns.Add("Item", "ITEM")
                Dim cont = appliances.Max(Function(item) item.Id)
                For Each item In EntityModel.Appliances
                    If item.Id = 0 Then
                        cont += 1
                        item.Id = cont
                    End If
                Next
                dtgList.DataSource = EntityModel.Appliances

                dtgList.Columns.Item("Item").Width = 40
                dtgList.Columns.Item("ApplianceDescription").HeaderText = "DETALLE"
                dtgList.Columns.Item("ApplianceDescription").Width = 320
                dtgList.Columns.Item("Serie").HeaderText = "SERIE"
                dtgList.Columns.Item("BuyPrice").HeaderText = "PRECIO"
                dtgList.Columns.Item("BuyPrice").Width = 80

                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Order").Visible = False
                dtgList.Columns.Item("State").Visible = False
                dtgList.Columns.Item("TechnicalReport").Visible = False
                dtgList.Columns.Item("Entry").Visible = False
                If EntityModel.Total = 0 Then
                    lblTotal.Text = "TOTAL = S/. " & EntryMapper.Map(EntityModel).Total
                Else
                    lblTotal.Text = "TOTAL = S/. " & EntityModel.Total

                End If

                cont = 0
                For Each row In dtgList.Rows
                    cont += 1
                    row.Cells("Item").Value = cont
                Next
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método habilitar controles para guardar entrada</summary>
    Public Sub EnableTxt(state As Boolean)
        cbxType.Enabled = state
        txtSerie.Enabled = state
        txtCorrelative.Enabled = state
        txtDate.Enabled = state
        txtDetail.Enabled = state
        txtApplianceSerie.Enabled = state
        txtPrice.Enabled = state
        btnScan.Enabled = state
        btnAdd.Enabled = state
        btnEdit.Enabled = state
        btnDelete.Enabled = state
        btnSearchAppliance.Enabled = state
        txtSupplier.Enabled = False
        txtAddress.Enabled = False
    End Sub

    '''<summary>Método habilitar controles para guardar estado</summary>
    Public Sub EnableStateTxt(state As Boolean)
        lblState.Visible = state
        cbxState.Visible = state
    End Sub

    '''<summary>Método para agregar un proveedor</summary>
    Public Sub SetSupplier()
        If txtDocument.Text.Length = 8 Or txtDocument.Text.Length = 11 Then
            EntityModel.Supplier = controller.GetSupplierByDocument(txtDocument.Text)
            If EntityModel.Supplier IsNot Nothing Then
                txtSupplier.Text = EntityModel.Supplier.FullName
                txtAddress.Text = EntityModel.Supplier.Address
                EnableTxt(True)
                cbxType.Focus()
            Else
                FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO PROVEEDOR"
                FrmSaveClient.getInstancia.Form = Me.Name
                FrmSaveClient.getInstancia.EntityModel = New PersonModel
                FrmSaveClient.getInstancia.Save = True
                FrmSaveClient.getInstancia.chkBussines.Visible = False
                FrmSaveClient.getInstancia.txtDocument.Enabled = False
                If txtDocument.Text.Length = 1 Then
                    FrmSaveClient.getInstancia.chkBussines.Checked = True
                End If
                FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                FrmSaveClient.getInstancia.Show()
            End If
        Else
            txtSupplier.Text = ""
            txtAddress.Text = ""
            EnableTxt(False)
        End If

    End Sub


    '''<summary>Método rellenar los datos de la descripcion del equipo de cómputo</summary>
    '''<param name="applianceDescription">Equipo de cómputo a ser agregado</param>
    Public Sub SetAppliance(applianceDescription As ApplianceDescriptionModel)
        Me.applianceDescription = applianceDescription
        txtAppliance.Text = applianceDescription.Category.Name & " " & applianceDescription.Brand.Name & " " & applianceDescription.ApplianceName & " " & applianceDescription.Model
    End Sub

    '''<summary>Método para mostrar u ocultar controles dependiendo si se trata de guardado o no</summary>
    Public Sub SaveControls()
        If Edit Then
            btnDelete.Visible = False
            btnAdd.Image = My.Resources.save
            toolTip.SetToolTip(btnAdd, "Guardar")
            btnEdit.Image = My.Resources.close
            toolTip.SetToolTip(btnEdit, "Cancelar")
        Else
            btnDelete.Visible = True
            btnAdd.Image = My.Resources.add
            toolTip.SetToolTip(btnAdd, "Agregar Equipo")
            btnEdit.Image = My.Resources.edit
            toolTip.SetToolTip(btnEdit, "Editar Equipo")
        End If

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la entrada</summary>
    Private Sub FrmSaveEntry_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()

        cbxType.DataSource = My.Settings.SupplirDocument
        cbxState.DataSource = My.Settings.VoucherState

        EnableTxt(False)
        EnableStateTxt(False)


        If EntityModel.Id Then
            If EntityModel.Supplier IsNot Nothing Then
                txtDocument.Text = EntityModel.Supplier.DocumentNumber
                txtSupplier.Text = EntityModel.Supplier.FullName
                txtAddress.Text = EntityModel.Supplier.Address
            End If
            cbxType.Text = EntityModel.DocumentType
            txtSerie.Text = EntityModel.Serie
            txtCorrelative.Text = EntityModel.Correlative
            txtDate.Value = EntityModel.Edate
            txtDetail.Text = EntityModel.Detail
            cbxState.Text = EntityModel.State
            appliances = EntityModel.Appliances
            EnableTxt(True)
            EnableStateTxt(True)
            If detail Then
                EnableTxt(False)
                txtDocument.Enabled = False
                txtDetail.Enabled = False
                cbxState.Enabled = False
                btnSave.Visible = False
                btnCancel.Visible = False
            End If
        End If

        'Redimencionar el datagridview
        dtgList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dtgList.DefaultCellStyle.WrapMode = DataGridViewTriState.True
        appliance = New ApplianceModel
        applianceDescription = New ApplianceDescriptionModel
        GetAllDetails()

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar entrada</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Employee = EmployeeMapper.Map(ActiveUser.Employee)
            EntityModel.DocumentType = cbxType.Text
            EntityModel.Serie = txtSerie.Text
            EntityModel.Correlative = txtCorrelative.Text
            EntityModel.Edate = txtDate.Value
            EntityModel.Detail = txtDetail.Text
            EntityModel.State = cbxState.Text
            EntityModel.Appliances = appliances

            If validateEntryTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Compra", "Compra modificada correctamente")
                        ClearTxt()
                        FrmShowEntry.getInstancia.GetAll("", FrmShowEntry.getInstancia.txtBeginDate.Value, FrmShowEntry.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Compra", "Compra registrada correctamente")
                        ClearTxt()
                        FrmShowEntry.getInstancia.GetAll("", FrmShowEntry.getInstancia.txtBeginDate.Value, FrmShowEntry.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Compra", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSaveEntry_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
        Dispose()
    End Sub

    '''<summary>Método para validar caja de texto para detlle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    Private Sub FrmSaveEntry_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtDocument.Focus()
    End Sub

    '''<summary>Método para validar caja de texto para estado de la entrada</summary>
    Private Sub cbxState_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxState.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 15
    End Sub

    '''<summary>Método para validar caja de texto para tipo de entrada</summary>
    Private Sub cbxType_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxType.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 120
    End Sub

    '''<summary>Método para validar caja de texto para el correlativo del numero de comprobante de pago</summary>
    Private Sub txtCorrelative_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCorrelative.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 8
    End Sub

    '''<summary>Método para validar caja de texto para serie del numero de comprobante de pago</summary>
    Private Sub txtSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSerie.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 4
    End Sub

    '''<summary>Método para validar caja de texto para validar numero de documento del provedor</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
    End Sub

    '''<summary>Método para validar caja de texto para precio</summary>
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        FormDesing.OnlyDecimal(e, sender)
        sender.MaxLength = 18
    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged
        txtSerie.Focus()
    End Sub

    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        SetSupplier()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        SetSupplier()
    End Sub


    '''<summary>Método para buscar una descripcion de un equipo de cómputo</summary>
    Private Sub btnSearchAppliance_Click(sender As Object, e As EventArgs) Handles btnSearchAppliance.Click
        FrmListAppliance.getInstancia.Form = Me.Name
        FrmListAppliance.getInstancia.ShowDialog()
    End Sub


    '''<summary>Método para mostrar el escaneador de qr</summary>
    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click
        FrmScan.getInstancia.Form = Me.Name
        FrmScan.getInstancia.ShowDialog()
    End Sub


    '''<summary>Método para agregar un nuevo equipo a la compra</summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        appliance.ApplianceDescription = applianceDescription
        appliance.Serie = txtApplianceSerie.Text
        appliance.State = True
        appliance.BuyPrice = Val(txtPrice.Text)

        If ValidateApplianceTxt() Then
            If appliance.Id Then
                If EntityModel.Id Then
                    appliance.Entry = EntityModel
                    controller.UpdateAppliance(appliance)
                End If
                appliances.RemoveAll(Function(detail) detail.Id = appliance.Id)
                appliances.Add(appliance)
                EntityModel.Appliances = appliances
            Else
                appliances.Add(appliance)
                EntityModel.Appliances = appliances
            End If
            Edit = False
            Cancel = False
            SaveControls()
            ClearApplianceTxt()
            GetAllDetails()
        End If
    End Sub


    '''<summary>Método para editar un equipo en la compra</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If Cancel Then
            Edit = False
            ClearApplianceTxt()
            applianceDescription = New ApplianceDescriptionModel
            txtAppliance.Text = ""
            SaveControls()
            GetAllDetails()
            Cancel = False
        Else
            If dtgList.CurrentRow IsNot Nothing Then
                appliance = New ApplianceModel With {
                                    .Id = dtgList.CurrentRow.Cells.Item("Id").Value,
                                    .ApplianceDescription = dtgList.CurrentRow.Cells.Item("ApplianceDescription").Value,
                                    .Serie = dtgList.CurrentRow.Cells.Item("Serie").Value,
                                    .BuyPrice = dtgList.CurrentRow.Cells.Item("BuyPrice").Value,
                                    .Entry = dtgList.CurrentRow.Cells.Item("Entry").Value,
                                    .State = dtgList.CurrentRow.Cells.Item("State").Value
                }
                Edit = True
                SetAppliance(appliance.ApplianceDescription)
                txtPrice.Text = appliance.BuyPrice
                txtApplianceSerie.Text = appliance.Serie
                SaveControls()
                Cancel = True
            End If
        End If
    End Sub


    '''<summary>Método para eliminar un equipo de la compra</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Item", "¿Esta seguro de eliminar el item?", AlertResource.QUESTION)
        Dim result = frm.ShowDialog
        If result = DialogResult.OK Then
            If EntityModel.Id Then
                controller.DeleteAppliance(dtgList.CurrentRow.Cells.Item("Id").Value)
                appliances.RemoveAll(Function(detail) detail.Id = dtgList.CurrentRow.Cells.Item("Id").Value)
            Else
                appliances.RemoveAll(Function(detail) detail.Id = dtgList.CurrentRow.Cells.Item("Id").Value)
            End If
            EntityModel.Appliances = appliances
            GetAllDetails()
        End If
    End Sub

End Class