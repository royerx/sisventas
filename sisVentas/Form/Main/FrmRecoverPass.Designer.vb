﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRecoverPass
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.lblValidateUser = New System.Windows.Forms.Label()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.btnLogin = New System.Windows.Forms.Button()
        Me.lblGetPass = New System.Windows.Forms.Label()
        Me.lblBack = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.img = New System.Windows.Forms.PictureBox()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtUser
        '
        Me.txtUser.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUser.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser.ForeColor = System.Drawing.Color.DimGray
        Me.txtUser.Location = New System.Drawing.Point(23, 127)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(348, 18)
        Me.txtUser.TabIndex = 12
        Me.txtUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.DarkGray
        Me.LineShape1.BorderWidth = 3
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 23
        Me.LineShape1.X2 = 370
        Me.LineShape1.Y1 = 152
        Me.LineShape1.Y2 = 150
        '
        'lblValidateUser
        '
        Me.lblValidateUser.AutoSize = True
        Me.lblValidateUser.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateUser.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateUser.Location = New System.Drawing.Point(20, 154)
        Me.lblValidateUser.Name = "lblValidateUser"
        Me.lblValidateUser.Size = New System.Drawing.Size(83, 15)
        Me.lblValidateUser.TabIndex = 48
        Me.lblValidateUser.Text = "Ingrese Usuario"
        Me.lblValidateUser.Visible = False
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(388, 331)
        Me.ShapeContainer2.TabIndex = 46
        Me.ShapeContainer2.TabStop = False
        '
        'btnLogin
        '
        Me.btnLogin.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnLogin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogin.FlatAppearance.BorderSize = 0
        Me.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.White
        Me.btnLogin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogin.Location = New System.Drawing.Point(90, 175)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(200, 30)
        Me.btnLogin.TabIndex = 51
        Me.btnLogin.Text = "Enviar"
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'lblGetPass
        '
        Me.lblGetPass.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblGetPass.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGetPass.ForeColor = System.Drawing.Color.DimGray
        Me.lblGetPass.Location = New System.Drawing.Point(23, 212)
        Me.lblGetPass.Name = "lblGetPass"
        Me.lblGetPass.Size = New System.Drawing.Size(348, 81)
        Me.lblGetPass.TabIndex = 52
        Me.lblGetPass.Text = "Recuperar contraseña"
        Me.lblGetPass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBack
        '
        Me.lblBack.BackColor = System.Drawing.Color.Transparent
        Me.lblBack.Cursor = System.Windows.Forms.Cursors.Hand
        Me.lblBack.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBack.ForeColor = System.Drawing.Color.DimGray
        Me.lblBack.Location = New System.Drawing.Point(170, 300)
        Me.lblBack.Name = "lblBack"
        Me.lblBack.Size = New System.Drawing.Size(62, 28)
        Me.lblBack.TabIndex = 54
        Me.lblBack.Text = "Volver"
        Me.lblBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Control
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_cerrar2
        Me.btnClose.Location = New System.Drawing.Point(369, 6)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 53
        Me.btnClose.TabStop = False
        '
        'img
        '
        Me.img.Image = Global.sisVentas.Desktop.My.Resources.Resources.lock
        Me.img.Location = New System.Drawing.Point(153, 12)
        Me.img.Name = "img"
        Me.img.Size = New System.Drawing.Size(100, 100)
        Me.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img.TabIndex = 0
        Me.img.TabStop = False
        '
        'FrmRecoverPass
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(388, 331)
        Me.Controls.Add(Me.lblBack)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblGetPass)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.img)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.lblValidateUser)
        Me.Controls.Add(Me.ShapeContainer2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmRecoverPass"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents img As PictureBox
    Friend WithEvents LineShape1 As PowerPacks.LineShape
    Friend WithEvents txtUser As TextBox
    Friend WithEvents lblValidateUser As Label
    Friend WithEvents ShapeContainer2 As PowerPacks.ShapeContainer
    Friend WithEvents btnLogin As Button
    Friend WithEvents lblGetPass As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents lblBack As Label
End Class
