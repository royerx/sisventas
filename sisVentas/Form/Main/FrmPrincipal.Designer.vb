﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.pnlMenuVertical = New System.Windows.Forms.Panel()
        Me.pnlSelected = New System.Windows.Forms.Panel()
        Me.pnlfLeftMenu = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.pnlLogo = New System.Windows.Forms.Panel()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.pnlUser = New System.Windows.Forms.Panel()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.lblRole = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.lblHour = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tmrDate = New System.Windows.Forms.Timer(Me.components)
        Me.imgMaximizar = New System.Windows.Forms.PictureBox()
        Me.imgRestaurar = New System.Windows.Forms.PictureBox()
        Me.imgMinimizar = New System.Windows.Forms.PictureBox()
        Me.imgClose = New System.Windows.Forms.PictureBox()
        Me.imgMenu = New System.Windows.Forms.PictureBox()
        Me.RoundPictureBox1 = New sisVentas.Desktop.RoundPictureBox()
        Me.btnAppliance = New System.Windows.Forms.Button()
        Me.btnSale = New System.Windows.Forms.Button()
        Me.btnServices = New System.Windows.Forms.Button()
        Me.btnClient = New System.Windows.Forms.Button()
        Me.btnBuy = New System.Windows.Forms.Button()
        Me.btnEmployye = New System.Windows.Forms.Button()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.btnManagement = New System.Windows.Forms.Button()
        Me.btnLogout = New System.Windows.Forms.Button()
        Me.imgLogo = New System.Windows.Forms.PictureBox()
        Me.imgFrase = New System.Windows.Forms.PictureBox()
        Me.pnlMenuVertical.SuspendLayout()
        Me.pnlfLeftMenu.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlLogo.SuspendLayout()
        Me.pnlTitle.SuspendLayout()
        Me.pnlUser.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.imgMaximizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgRestaurar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgMinimizar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RoundPictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgFrase, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMenuVertical
        '
        Me.pnlMenuVertical.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlMenuVertical.Controls.Add(Me.pnlSelected)
        Me.pnlMenuVertical.Controls.Add(Me.pnlfLeftMenu)
        Me.pnlMenuVertical.Controls.Add(Me.pnlLogo)
        Me.pnlMenuVertical.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlMenuVertical.Location = New System.Drawing.Point(0, 0)
        Me.pnlMenuVertical.Name = "pnlMenuVertical"
        Me.pnlMenuVertical.Size = New System.Drawing.Size(250, 650)
        Me.pnlMenuVertical.TabIndex = 0
        '
        'pnlSelected
        '
        Me.pnlSelected.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlSelected.Location = New System.Drawing.Point(1, 110)
        Me.pnlSelected.Name = "pnlSelected"
        Me.pnlSelected.Size = New System.Drawing.Size(5, 45)
        Me.pnlSelected.TabIndex = 10
        '
        'pnlfLeftMenu
        '
        Me.pnlfLeftMenu.Controls.Add(Me.btnAppliance)
        Me.pnlfLeftMenu.Controls.Add(Me.btnSale)
        Me.pnlfLeftMenu.Controls.Add(Me.btnServices)
        Me.pnlfLeftMenu.Controls.Add(Me.btnClient)
        Me.pnlfLeftMenu.Controls.Add(Me.btnBuy)
        Me.pnlfLeftMenu.Controls.Add(Me.btnEmployye)
        Me.pnlfLeftMenu.Controls.Add(Me.btnReport)
        Me.pnlfLeftMenu.Controls.Add(Me.btnManagement)
        Me.pnlfLeftMenu.Controls.Add(Me.Panel3)
        Me.pnlfLeftMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlfLeftMenu.Location = New System.Drawing.Point(0, 110)
        Me.pnlfLeftMenu.Name = "pnlfLeftMenu"
        Me.pnlfLeftMenu.Size = New System.Drawing.Size(250, 540)
        Me.pnlfLeftMenu.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnLogout)
        Me.Panel3.Location = New System.Drawing.Point(3, 411)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(244, 129)
        Me.Panel3.TabIndex = 11
        '
        'pnlLogo
        '
        Me.pnlLogo.Controls.Add(Me.imgLogo)
        Me.pnlLogo.Controls.Add(Me.imgFrase)
        Me.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlLogo.Location = New System.Drawing.Point(0, 0)
        Me.pnlLogo.Name = "pnlLogo"
        Me.pnlLogo.Size = New System.Drawing.Size(250, 110)
        Me.pnlLogo.TabIndex = 0
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Controls.Add(Me.imgMaximizar)
        Me.pnlTitle.Controls.Add(Me.imgRestaurar)
        Me.pnlTitle.Controls.Add(Me.imgMinimizar)
        Me.pnlTitle.Controls.Add(Me.imgClose)
        Me.pnlTitle.Controls.Add(Me.imgMenu)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(1050, 50)
        Me.pnlTitle.TabIndex = 1
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(50, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(903, 26)
        Me.lblTitle.TabIndex = 6
        Me.lblTitle.Text = "INICIO"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlBody
        '
        Me.pnlBody.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlBody.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlBody.Location = New System.Drawing.Point(0, 48)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(1050, 519)
        Me.pnlBody.TabIndex = 2
        '
        'pnlUser
        '
        Me.pnlUser.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlUser.Controls.Add(Me.RoundPictureBox1)
        Me.pnlUser.Controls.Add(Me.lblUser)
        Me.pnlUser.Controls.Add(Me.lblRole)
        Me.pnlUser.Controls.Add(Me.lblDate)
        Me.pnlUser.Controls.Add(Me.lblHour)
        Me.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlUser.Location = New System.Drawing.Point(0, 567)
        Me.pnlUser.Name = "pnlUser"
        Me.pnlUser.Size = New System.Drawing.Size(1050, 83)
        Me.pnlUser.TabIndex = 3
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblUser.Location = New System.Drawing.Point(80, 41)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(63, 20)
        Me.lblUser.TabIndex = 17
        Me.lblUser.Text = "Usuario"
        '
        'lblRole
        '
        Me.lblRole.AutoSize = True
        Me.lblRole.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRole.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblRole.Location = New System.Drawing.Point(81, 15)
        Me.lblRole.Name = "lblRole"
        Me.lblRole.Size = New System.Drawing.Size(31, 18)
        Me.lblRole.TabIndex = 15
        Me.lblRole.Text = "Rol"
        '
        'lblDate
        '
        Me.lblDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDate.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblDate.Location = New System.Drawing.Point(649, 51)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(271, 23)
        Me.lblDate.TabIndex = 14
        Me.lblDate.Text = "Fecha"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHour
        '
        Me.lblHour.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHour.Font = New System.Drawing.Font("Century Gothic", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHour.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.lblHour.Location = New System.Drawing.Point(741, 9)
        Me.lblHour.Name = "lblHour"
        Me.lblHour.Size = New System.Drawing.Size(181, 40)
        Me.lblHour.TabIndex = 13
        Me.lblHour.Text = "HORA"
        Me.lblHour.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.pnlMenuVertical)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1300, 650)
        Me.Panel1.TabIndex = 4
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.pnlTitle)
        Me.Panel2.Controls.Add(Me.pnlBody)
        Me.Panel2.Controls.Add(Me.pnlUser)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(250, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1050, 650)
        Me.Panel2.TabIndex = 4
        '
        'tmrDate
        '
        Me.tmrDate.Enabled = True
        '
        'imgMaximizar
        '
        Me.imgMaximizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imgMaximizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgMaximizar.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_maximizar
        Me.imgMaximizar.Location = New System.Drawing.Point(985, 7)
        Me.imgMaximizar.Name = "imgMaximizar"
        Me.imgMaximizar.Size = New System.Drawing.Size(20, 20)
        Me.imgMaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgMaximizar.TabIndex = 4
        Me.imgMaximizar.TabStop = False
        '
        'imgRestaurar
        '
        Me.imgRestaurar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imgRestaurar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgRestaurar.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_restaurar
        Me.imgRestaurar.Location = New System.Drawing.Point(985, 7)
        Me.imgRestaurar.Name = "imgRestaurar"
        Me.imgRestaurar.Size = New System.Drawing.Size(20, 20)
        Me.imgRestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgRestaurar.TabIndex = 3
        Me.imgRestaurar.TabStop = False
        Me.imgRestaurar.Visible = False
        '
        'imgMinimizar
        '
        Me.imgMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imgMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgMinimizar.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_minimizar
        Me.imgMinimizar.Location = New System.Drawing.Point(959, 7)
        Me.imgMinimizar.Name = "imgMinimizar"
        Me.imgMinimizar.Size = New System.Drawing.Size(20, 20)
        Me.imgMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgMinimizar.TabIndex = 2
        Me.imgMinimizar.TabStop = False
        '
        'imgClose
        '
        Me.imgClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.imgClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_cerrar2
        Me.imgClose.Location = New System.Drawing.Point(1013, 7)
        Me.imgClose.Name = "imgClose"
        Me.imgClose.Size = New System.Drawing.Size(20, 20)
        Me.imgClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgClose.TabIndex = 1
        Me.imgClose.TabStop = False
        '
        'imgMenu
        '
        Me.imgMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgMenu.Image = Global.sisVentas.Desktop.My.Resources.Resources.Mobile_Menu_Icon
        Me.imgMenu.Location = New System.Drawing.Point(9, 7)
        Me.imgMenu.Name = "imgMenu"
        Me.imgMenu.Size = New System.Drawing.Size(35, 35)
        Me.imgMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgMenu.TabIndex = 0
        Me.imgMenu.TabStop = False
        '
        'RoundPictureBox1
        '
        Me.RoundPictureBox1.Image = Global.sisVentas.Desktop.My.Resources.Resources.categorias
        Me.RoundPictureBox1.Location = New System.Drawing.Point(9, 7)
        Me.RoundPictureBox1.Name = "RoundPictureBox1"
        Me.RoundPictureBox1.Size = New System.Drawing.Size(65, 67)
        Me.RoundPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.RoundPictureBox1.TabIndex = 0
        Me.RoundPictureBox1.TabStop = False
        '
        'btnAppliance
        '
        Me.btnAppliance.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAppliance.FlatAppearance.BorderSize = 0
        Me.btnAppliance.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnAppliance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAppliance.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAppliance.ForeColor = System.Drawing.Color.White
        Me.btnAppliance.Image = Global.sisVentas.Desktop.My.Resources.Resources.producto
        Me.btnAppliance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAppliance.Location = New System.Drawing.Point(3, 3)
        Me.btnAppliance.Name = "btnAppliance"
        Me.btnAppliance.Size = New System.Drawing.Size(250, 45)
        Me.btnAppliance.TabIndex = 2
        Me.btnAppliance.Text = "Equipos"
        Me.btnAppliance.UseVisualStyleBackColor = True
        '
        'btnSale
        '
        Me.btnSale.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSale.FlatAppearance.BorderSize = 0
        Me.btnSale.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSale.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSale.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSale.ForeColor = System.Drawing.Color.White
        Me.btnSale.Image = Global.sisVentas.Desktop.My.Resources.Resources.venta
        Me.btnSale.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSale.Location = New System.Drawing.Point(3, 54)
        Me.btnSale.Name = "btnSale"
        Me.btnSale.Size = New System.Drawing.Size(250, 45)
        Me.btnSale.TabIndex = 3
        Me.btnSale.Text = "Ventas"
        Me.btnSale.UseVisualStyleBackColor = True
        '
        'btnServices
        '
        Me.btnServices.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnServices.FlatAppearance.BorderSize = 0
        Me.btnServices.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnServices.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnServices.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnServices.ForeColor = System.Drawing.Color.White
        Me.btnServices.Image = Global.sisVentas.Desktop.My.Resources.Resources.servicios
        Me.btnServices.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnServices.Location = New System.Drawing.Point(3, 105)
        Me.btnServices.Name = "btnServices"
        Me.btnServices.Size = New System.Drawing.Size(250, 45)
        Me.btnServices.TabIndex = 4
        Me.btnServices.Text = "Servicios"
        Me.btnServices.UseVisualStyleBackColor = True
        '
        'btnClient
        '
        Me.btnClient.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClient.FlatAppearance.BorderSize = 0
        Me.btnClient.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClient.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClient.ForeColor = System.Drawing.Color.White
        Me.btnClient.Image = Global.sisVentas.Desktop.My.Resources.Resources.clientes
        Me.btnClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClient.Location = New System.Drawing.Point(3, 156)
        Me.btnClient.Name = "btnClient"
        Me.btnClient.Size = New System.Drawing.Size(250, 45)
        Me.btnClient.TabIndex = 5
        Me.btnClient.Text = "Clientes"
        Me.btnClient.UseVisualStyleBackColor = True
        '
        'btnBuy
        '
        Me.btnBuy.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBuy.FlatAppearance.BorderSize = 0
        Me.btnBuy.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnBuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuy.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuy.ForeColor = System.Drawing.Color.White
        Me.btnBuy.Image = Global.sisVentas.Desktop.My.Resources.Resources.compras
        Me.btnBuy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuy.Location = New System.Drawing.Point(3, 207)
        Me.btnBuy.Name = "btnBuy"
        Me.btnBuy.Size = New System.Drawing.Size(250, 45)
        Me.btnBuy.TabIndex = 6
        Me.btnBuy.Text = "Compras"
        Me.btnBuy.UseVisualStyleBackColor = True
        '
        'btnEmployye
        '
        Me.btnEmployye.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEmployye.FlatAppearance.BorderSize = 0
        Me.btnEmployye.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnEmployye.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployye.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployye.ForeColor = System.Drawing.Color.White
        Me.btnEmployye.Image = Global.sisVentas.Desktop.My.Resources.Resources.empleados
        Me.btnEmployye.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEmployye.Location = New System.Drawing.Point(3, 258)
        Me.btnEmployye.Name = "btnEmployye"
        Me.btnEmployye.Size = New System.Drawing.Size(250, 45)
        Me.btnEmployye.TabIndex = 7
        Me.btnEmployye.Text = "Empleados"
        Me.btnEmployye.UseVisualStyleBackColor = True
        '
        'btnReport
        '
        Me.btnReport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnReport.FlatAppearance.BorderSize = 0
        Me.btnReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReport.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReport.ForeColor = System.Drawing.Color.White
        Me.btnReport.Image = Global.sisVentas.Desktop.My.Resources.Resources.reportes
        Me.btnReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReport.Location = New System.Drawing.Point(3, 309)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(250, 45)
        Me.btnReport.TabIndex = 8
        Me.btnReport.Text = "Reportes"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'btnManagement
        '
        Me.btnManagement.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnManagement.FlatAppearance.BorderSize = 0
        Me.btnManagement.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManagement.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnManagement.ForeColor = System.Drawing.Color.White
        Me.btnManagement.Image = Global.sisVentas.Desktop.My.Resources.Resources.mantenimiento
        Me.btnManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnManagement.Location = New System.Drawing.Point(3, 360)
        Me.btnManagement.Name = "btnManagement"
        Me.btnManagement.Size = New System.Drawing.Size(250, 45)
        Me.btnManagement.TabIndex = 10
        Me.btnManagement.Text = "Mantenimiento"
        Me.btnManagement.UseVisualStyleBackColor = True
        '
        'btnLogout
        '
        Me.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLogout.FlatAppearance.BorderSize = 0
        Me.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.ForeColor = System.Drawing.Color.White
        Me.btnLogout.Image = Global.sisVentas.Desktop.My.Resources.Resources.power_off
        Me.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogout.Location = New System.Drawing.Point(-3, 80)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.Size = New System.Drawing.Size(250, 45)
        Me.btnLogout.TabIndex = 11
        Me.btnLogout.Text = "Cerrar Sesion"
        Me.btnLogout.UseVisualStyleBackColor = True
        '
        'imgLogo
        '
        Me.imgLogo.Image = Global.sisVentas.Desktop.My.Resources.Resources.ostik
        Me.imgLogo.Location = New System.Drawing.Point(3, 3)
        Me.imgLogo.Name = "imgLogo"
        Me.imgLogo.Size = New System.Drawing.Size(60, 49)
        Me.imgLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgLogo.TabIndex = 1
        Me.imgLogo.TabStop = False
        Me.imgLogo.Visible = False
        '
        'imgFrase
        '
        Me.imgFrase.Image = Global.sisVentas.Desktop.My.Resources.Resources.SISTEMAVENTAS
        Me.imgFrase.Location = New System.Drawing.Point(3, 3)
        Me.imgFrase.Name = "imgFrase"
        Me.imgFrase.Size = New System.Drawing.Size(240, 62)
        Me.imgFrase.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgFrase.TabIndex = 0
        Me.imgFrase.TabStop = False
        '
        'frmPrincipal
        '
        Me.ClientSize = New System.Drawing.Size(1300, 650)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmPrincipal"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmPrincipal"
        Me.pnlMenuVertical.ResumeLayout(False)
        Me.pnlfLeftMenu.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.pnlLogo.ResumeLayout(False)
        Me.pnlTitle.ResumeLayout(False)
        Me.pnlUser.ResumeLayout(False)
        Me.pnlUser.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.imgMaximizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgRestaurar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgMinimizar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RoundPictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgFrase, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlMenuVertical As Panel
    Friend WithEvents imgFrase As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents imgMenu As PictureBox
    Friend WithEvents pnlBody As Panel
    Friend WithEvents imgLogo As PictureBox
    Friend WithEvents imgMinimizar As PictureBox
    Friend WithEvents imgClose As PictureBox
    Friend WithEvents imgMaximizar As PictureBox
    Friend WithEvents imgRestaurar As PictureBox
    Friend WithEvents btnAppliance As Button
    Friend WithEvents btnEmployye As Button
    Friend WithEvents btnBuy As Button
    Friend WithEvents btnClient As Button
    Friend WithEvents btnServices As Button
    Friend WithEvents btnSale As Button
    Friend WithEvents btnReport As Button
    Friend WithEvents btnManagement As Button
    Friend WithEvents pnlUser As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents lblUser As Label
    Friend WithEvents lblRole As Label
    Friend WithEvents lblDate As Label
    Friend WithEvents lblHour As Label
    Friend WithEvents RoundPictureBox1 As RoundPictureBox
    Friend WithEvents pnlfLeftMenu As FlowLayoutPanel
    Friend WithEvents pnlLogo As Panel
    Friend WithEvents pnlSelected As Panel
    Friend WithEvents lblTitle As Label
    Friend WithEvents tmrDate As Timer
    Friend WithEvents Panel3 As Panel
    Friend WithEvents btnLogout As Button
End Class
