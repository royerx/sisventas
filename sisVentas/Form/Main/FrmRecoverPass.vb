﻿Imports sisVentas.CrossCutting
Imports Transitions
''' <summary>
''' Clase Formulario Recuperar Contraseña: 
''' Formulario que permite recuperar contraseña mediante el envio de correo
''' </summary>
Public Class FrmRecoverPass

    'controlador del modelo de Autorización
    Private ReadOnly controller As New Auth

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateUser.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateTxt(email As String) As Boolean
        HideLblValidate()
        If email.Length = 0 Then
            lblValidateUser.Visible = True
            lblValidateUser.Text = "Ingrese un correo electrónico"
            Return False
        Else
            Return True
        End If
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        txtUser.Text = ""
    End Sub

    '''<summary>Método para iniciar el formulario</summary>
    Private Sub FrmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.8R)
        t.run()
        FormDesing.Placeholder(txtUser, "Ingrese Correo Electrónico ...")
        Me.ActiveControl = img
    End Sub

    'Metodos para el placeholder de usuario y contraseña
    Private Sub txtUser_GotFocus(sender As Object, e As EventArgs) Handles txtUser.GotFocus
        FormDesing.Placeholder(sender, "Ingrese Correo Electrónico ...")
    End Sub

    Private Sub txtUser_LostFocus(sender As Object, e As EventArgs) Handles txtUser.LostFocus
        FormDesing.Placeholder(sender, "Ingrese Correo Electrónico ...")
    End Sub

    '''<summary>Método para recuperar contraseña</summary>
    Private Sub lblBack_Click(sender As Object, e As EventArgs) Handles lblBack.Click
        Me.Close()
        FrmLogin.Show()
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        FrmLogin.Close()
    End Sub

    'Metodos para hover de recuperacion de cotraseña
    Private Sub lblBack_MouseHover(sender As Object, e As EventArgs) Handles lblBack.MouseHover
        sender.ForeColor = Color.FromArgb(34, 187, 51) 'color verde
    End Sub

    Private Sub lblBack_MouseLeave(sender As Object, e As EventArgs) Handles lblBack.MouseLeave
        sender.ForeColor = Color.FromArgb(105, 105, 105) ' color gris
    End Sub

    '''<summary>Método para iniciar sesion</summary>
    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim email = IIf(txtUser.Text = "Ingrese Correo Electrónico ...", "", txtUser.Text)
        lblGetPass.Text = "Enviando correo electrónico ..."
        If ValidateTxt(email) Then
            lblGetPass.Text = controller.GetPassByEmail(email)
        Else
            lblGetPass.Text = "Recuperar contraseña"
        End If
    End Sub

End Class