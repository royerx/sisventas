﻿Imports Transitions

''' <summary>
''' Clase Formulario Seleccionar Rol: 
''' Formulario que permite seleccionar un rol
''' </summary>
Public Class FrmSelectRole

    'controlador del modelo de login
    Private ReadOnly controller As New LoginController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSelectRole = Nothing

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSelectRole
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSelectRole
        End If
        Return _formulario
    End Function

    Private Sub FrmSelectRole_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
    End Sub

    '''<summary>Método que selecciona el rol</summary>
    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        controller.SelectedRole(lstRoles.SelectedItem)
        Dispose()
    End Sub
End Class