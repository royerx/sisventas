﻿Imports System.Runtime.InteropServices
Imports sisVentas.CrossCutting
Imports Transitions
''' <summary>
''' Clase Formulario Principal: 
''' Formulario que muestra el menu de la aplicacion
''' </summary>
Public Class frmPrincipal

    'importacion de los dll que permiten mover el formulario con el mouse
    <DllImport("User32.DLL", EntryPoint:="ReleaseCapture")> Private Shared Sub ReleaseCapture()
    End Sub
    <DllImport("user32.DLL", EntryPoint:="SendMessage")> Private Shared Sub SendMessage(ByVal hWnd As System.IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Integer)
    End Sub

    'controlador del modelo de marca
    Private ReadOnly controller As New EmployeeController()

    '''<summary>Método para agregar un formulario al pane principal</summary>
    '''<param name="frmChild">Formulario a ser agregado al panel</param>
    Public Sub FormPanel(frmChild As Object)
        If (pnlBody.Controls.Count > 0) Then
            pnlBody.Controls.RemoveAt(0)
        End If
        Dim fh As Form = TryCast(frmChild, Form)
        fh.TopLevel = False
        fh.FormBorderStyle = Windows.Forms.BorderStyle.None
        fh.Dock = DockStyle.Fill
        pnlBody.Controls.Add(fh)
        pnlBody.Tag = fh
        fh.Show()
    End Sub

    '''<summary>Método para ubicar el indicador de activo de item de menu</summary>
    '''<param name="btn">Item de menu en el que se ubicara el indicador</param>
    Public Sub ShowPnlSelected(btn As Button)
        pnlSelected.Visible = False
        If btn IsNot Nothing Then
            pnlSelected.Location = New System.Drawing.Point(0, btn.Location.Y + pnlLogo.Height)
            pnlSelected.Visible = True
        End If
    End Sub

    '''<summary>Método para el efecto de colapso del menu principal</summary>
    Private Sub imgMenu_Click(sender As Object, e As EventArgs) Handles imgMenu.Click
        If pnlMenuVertical.Width = 250 Then
            pnlMenuVertical.Width = 65
            imgFrase.Visible = False
            imgLogo.Visible = True
        Else
            pnlMenuVertical.Width = 250
            imgFrase.Visible = True
            imgLogo.Visible = False
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub imgClose_Click(sender As Object, e As EventArgs) Handles imgClose.Click
        FrmLogin.Close()
    End Sub

    '''<summary>Método para minimizar el formulario</summary>
    Private Sub imgMinimizar_Click(sender As Object, e As EventArgs) Handles imgMinimizar.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    '''<summary>Método para capturar el movimiento del mouse y desplazar el formulario</summary>
    Private Sub pnlTitle_MouseDown(sender As Object, e As MouseEventArgs) Handles pnlTitle.MouseDown
        ReleaseCapture()
        SendMessage(Me.Handle, &H112&, &HF012&, 0)
    End Sub

    '''<summary>Método para maximizar el formulario</summary>
    Private Sub imgMaximizar_Click(sender As Object, e As EventArgs) Handles imgMaximizar.Click
        Me.WindowState = FormWindowState.Maximized
        imgRestaurar.Visible = True
        imgMaximizar.Visible = False
    End Sub

    '''<summary>Método para restaurar el tamaño el formulario</summary>
    Private Sub imgRestaurar_Click(sender As Object, e As EventArgs) Handles imgRestaurar.Click
        Me.WindowState = FormWindowState.Normal
        imgRestaurar.Visible = False
        imgMaximizar.Visible = True
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de equipos</summary>
    Private Sub btnAppliance_Click(sender As Object, e As EventArgs) Handles btnAppliance.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "EQUIPOS"
        FormPanel(FrmShowApplianceDescription.getInstancia)
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de clientes</summary>
    Private Sub btnClient_Click(sender As Object, e As EventArgs) Handles btnClient.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "CLIENTES"
        FrmShowClient.getInstancia.Type = Values.CLIENT
        FormPanel(FrmShowClient.getInstancia)
    End Sub

    '''<summary>Método para deseleccionar la seleccion del item de menu y validar permisos</summary>
    Private Sub FrmPrincipal_Load(sender As Object, e As EventArgs) Handles Me.Load
        pnlSelected.Visible = False
        Dim t As New Transition(New TransitionType_EaseInEaseOut(800))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If ActiveUser.Employee IsNot Nothing Then
            lblRole.Text = ActiveUser.SelectedRole.Name
            lblUser.Text = ActiveUser.Employee.Person.Name & " " & ActiveUser.Employee.Person.LastName
            btnAppliance.Visible = Auth.Can(Values.ApplianceDescriptionList)
            btnSale.Visible = Auth.Can(Values.SaleList)
            btnServices.Visible = Auth.Can(Values.ServiceList)
            btnClient.Visible = Auth.Can(Values.ClientList)
            btnBuy.Visible = Auth.Can(Values.BuyList)
            btnEmployye.Visible = Auth.Can(Values.EmployeeList)
            btnReport.Visible = Auth.Can(Values.ReportList)
            btnManagement.Visible = Auth.Can(Values.ManagementList)
        Else
            FrmMessage.ShowMessage(AlertResource.ERRO, "Acceso Al Sistema", "No ha iniciado sesion en el sistema")
            Close()
        End If
    End Sub

    '''<summary>Método para mostrar el formulario de menu de mantenimiento</summary>
    Private Sub btnManagement_Click(sender As Object, e As EventArgs) Handles btnManagement.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "MANTENIMIENTO"
        FormPanel(FrmMenuManagement.getInstancia)
    End Sub

    Private Sub btnSale_Click(sender As Object, e As EventArgs) Handles btnSale.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "VENTAS"
        FormPanel(FrmMenuSale.getInstancia)
    End Sub

    '''<summary>Método para mostrar reloj y fecha</summary>
    Private Sub tmrDate_Tick(sender As Object, e As EventArgs) Handles tmrDate.Tick
        lblHour.Text = DateTime.Now.ToString("hh:mm:ss ")
        lblDate.Text = DateTime.Now.ToLongDateString()
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de empleados</summary>
    Private Sub btnEmployye_Click(sender As Object, e As EventArgs) Handles btnEmployye.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "EMPLEADOS"
        FormPanel(FrmShowEmployee.getInstancia)
    End Sub

    Private Sub btnServices_Click(sender As Object, e As EventArgs) Handles btnServices.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "REPORTES TÉCNICOS"
        FormPanel(FrmShowTechnicalReport.getInstancia)
    End Sub

    Private Sub btnBuy_Click(sender As Object, e As EventArgs) Handles btnBuy.Click
        ShowPnlSelected(sender)
        lblTitle.Text = "COMPRAS"
        FormPanel(FrmShowEntry.getInstancia)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Dim frm As New FrmConfirm(AlertResource.ERRO, "Salir del Sistema", "Estas seguro de salir del sistema", AlertResource.QUESTION)
        Dim result = frm.ShowDialog
        If result = DialogResult.OK Then
            Me.Close()
            ActiveUser.Employee.Login = ""
            ActiveUser.Employee.Pass = ""
            ActiveUser.Employee.Id = 0
            ActiveUser.SelectedRole.Name = ""
            ActiveUser.SelectedRole.Id = 0
            FrmLogin.txtUser.Text = "USUARIO"
            FrmLogin.txtPass.Text = "CONTRASEÑA"
            FrmLogin.txtUser.Focus()
            FrmLogout.ShowDialog()
        End If

    End Sub
End Class
