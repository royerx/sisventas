﻿Imports sisVentas.CrossCutting
Imports Transitions
''' <summary>
''' Clase Formulario Login: 
''' Formulario que permite iniciar sesion
''' </summary>
Public Class FrmLogin

    'controlador del modelo de Login
    Private ReadOnly controller As New LoginController()

    'Modelo de login a ser utilizado
    Private EntityModel As New LoginModel

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateUser.Visible = False
        lblValidatePass.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Login") Then
                    lblValidateUser.Text = item.ErrorMessage
                    lblValidateUser.Visible = True
                    txtUser.Focus()
                    Return False
                End If
                If (item.MemberNames.First = "Pass") Then
                    lblValidatePass.Text = item.ErrorMessage
                    lblValidatePass.Visible = True
                    txtPass.Focus()
                    Return False
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtUser.Text = ""
        txtPass.Text = ""
    End Sub

    '''<summary>Método para iniciar el formulario</summary>
    Private Sub FrmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.8R)
        t.run()
        FormDesing.Placeholder(txtUser, "USUARIO")
        FormDesing.Placeholder(txtPass, "CONTRASEÑA")
        Me.ActiveControl = img
    End Sub

    'Metodos para el placeholder de usuario y contraseña
    Private Sub txtUser_GotFocus(sender As Object, e As EventArgs) Handles txtUser.GotFocus
        FormDesing.Placeholder(sender, "USUARIO")
    End Sub

    Private Sub txtUser_LostFocus(sender As Object, e As EventArgs) Handles txtUser.LostFocus
        FormDesing.Placeholder(sender, "USUARIO")
    End Sub

    Private Sub txtPass_GotFocus(sender As Object, e As EventArgs) Handles txtPass.GotFocus
        FormDesing.Placeholder(sender, "CONTRASEÑA")
    End Sub

    Private Sub txtPass_LostFocus(sender As Object, e As EventArgs) Handles txtPass.LostFocus
        FormDesing.Placeholder(sender, "CONTRASEÑA")
    End Sub

    '''<summary>Método para recuperar contraseña</summary>
    Private Sub lblForgot_Click(sender As Object, e As EventArgs) Handles lblForgot.Click
        FrmRecoverPass.Show()
        Me.Hide()
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Close()
    End Sub

    'Metodos para hover de recuperacion de cotraseña
    Private Sub lblForgot_MouseHover(sender As Object, e As EventArgs) Handles lblForgot.MouseHover
        lblForgot.ForeColor = Color.FromArgb(34, 187, 51) 'color verde
    End Sub

    Private Sub lblForgot_MouseLeave(sender As Object, e As EventArgs) Handles lblForgot.MouseLeave
        lblForgot.ForeColor = Color.FromArgb(105, 105, 105) ' color gris
    End Sub

    '''<summary>Método para iniciar sesion</summary>
    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        EntityModel.Login = IIf(txtUser.Text = "USUARIO", "", txtUser.Text)
        EntityModel.Pass = IIf(txtPass.Text = "CONTRASEÑA", "", txtPass.Text)
        If ValidateTxt() Then
            If controller.Login(EntityModel.Login, EntityModel.Pass) Then
                If (ActiveUser.Employee.Roles.count > 1) Then
                    FrmSelectRole.getInstancia.lstRoles.DataSource = ActiveUser.Employee.Roles
                    Me.Hide()
                    FrmSelectRole.getInstancia.ShowDialog()
                    FrmLoadScreen.ShowDialog()
                Else
                    Me.Hide()
                    controller.SelectedRole(ActiveUser.Employee.Roles(0))
                    FrmLoadScreen.ShowDialog()
                End If
            Else
                MsgBox("No entro")
            End If
        End If
    End Sub
End Class