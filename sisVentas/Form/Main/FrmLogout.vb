﻿Imports sisVentas.CrossCutting
Imports Transitions
''' <summary>
''' Clase Formulario de Carga: 
''' Formulario que permite mostrar una ventana de carga
''' </summary>
Public Class FrmLogout

    Private Sub FrmLoadScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.8R)
        t.run()
        progress.Value = 0
        tmrShow.Start()
    End Sub

    Private Sub show_Tick(sender As Object, e As EventArgs) Handles tmrShow.Tick
        progress.Value += 1
        progress.Text = progress.Value
        If progress.Value = 100 Then
            tmrShow.Stop()
            Me.Close()
            FrmLogin.Show()
        End If
    End Sub
End Class