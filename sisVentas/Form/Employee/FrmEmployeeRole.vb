﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Rol de empleado: 
''' Formulario que permite asignar un rol a un empleado
''' </summary>
Public Class FrmEmployeeRole

    'controlador del modelo de empleado
    Private ReadOnly controller As New EmployeeController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmEmployeeRole = Nothing

    'Modelo de empleado a ser agregado o modificado
    Public Property EntityModel As EmployeeModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmEmployeeRole
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmEmployeeRole
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateRol.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        If (cbxRol.SelectedIndex = -1) Then
            lblValidateRol.Text = "Seleccionar un Rol Válido"
            lblValidateRol.Visible = True
            cbxRol.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        cbxRol.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los roles que pueden ser asignados</summary>
    Private Sub FrmEmployeeRole_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        cbxRol.DataSource = controller.GetRoles()
        cbxRol.SelectedIndex = -1
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar un empleado y asignar su rol</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try

            If validateTxt() Then

                Dim roles = New List(Of RoleModel)
                roles.Add(cbxRol.SelectedItem)
                EntityModel.Roles = roles

                If controller.Add(EntityModel) Then
                    FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Empleado", "Empleado registrado correctamente")
                    FrmShowEmployee.getInstancia.GetAll("")
                    ClearTxt()
                    Me.Close()
                    FrmSaveEmployee.getInstancia.Close()
                Else
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Empleado", "Empleado NO pudo ser registrado")
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    Dim er = err.Message.IndexOf("(")
                    Dim tam = err.Message.Length - er
                    Dim msg = err.Message.Substring(er + 1, tam - 3)
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", msg & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", err.Message, AlertResource.INFO)
                End If
            Next
            Dispose()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", ex.Message)
            Dispose()
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub
    '''<summary>Método para validar caja de texto para rol</summary>
    Private Sub txtType_KeyPress(sender As Object, e As KeyPressEventArgs)
        FormDesing.DefaultName(e)
        sender.MaxLength = 30
    End Sub

End Class