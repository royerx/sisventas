﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Empleados: 
''' Formulario que muestra las opciones de mantenimiento de Empleados
''' </summary>
Public Class FrmShowEmployee

    'controlador del modelo de Empleado
    Private ReadOnly controller As New EmployeeController()

    'lista de modelo de Empleados con filtros
    Private dt As List(Of EmployeeModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowEmployee = Nothing

    Public Property Type

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowEmployee
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowEmployee
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista los Empleados cuando carga el formulario y validar permisos</summary>
    Private Sub FrmShowEmployee_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.EmployeeCreate)
        btnEdit.Visible = Auth.Can(Values.EmployeeDetail)
        btnDelete.Visible = Auth.Can(Values.EmployeeDelete)
    End Sub

    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con los Empleados filtrados</summary>
    Public Sub GetAll(search As String)
        Try
            dt = controller.GetAll(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Login").HeaderText = "USUARIO"
                dtgList.Columns.Item("FullName").HeaderText = "NOMBRE"
                dtgList.Columns.Item("DocumentNumber").HeaderText = "DOCUMENTO"
                dtgList.Columns.Item("Address").HeaderText = "DIRECCIÓN"
                dtgList.Columns.Item("Phone").HeaderText = "TELEFONO"
                dtgList.Columns.Item("Email").HeaderText = "CORREO"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Person").Visible = False
                dtgList.Columns.Item("Name").Visible = False
                dtgList.Columns.Item("LastName").Visible = False
                dtgList.Columns.Item("Roles").Visible = False
                dtgList.Columns.Item("Pass").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los Empleados filtrados en memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            dt = controller.GetAllMemory(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Login").HeaderText = "USUARIO"
                dtgList.Columns.Item("FullName").HeaderText = "NOMBRE"
                dtgList.Columns.Item("DocumentNumber").HeaderText = "DOCUMENTO"
                dtgList.Columns.Item("Address").HeaderText = "DIRECCIÓN"
                dtgList.Columns.Item("Phone").HeaderText = "TELEFONO"
                dtgList.Columns.Item("Email").HeaderText = "CORREO"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Person").Visible = False
                dtgList.Columns.Item("Name").Visible = False
                dtgList.Columns.Item("LastName").Visible = False
                dtgList.Columns.Item("Roles").Visible = False
                dtgList.Columns.Item("Pass").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar Empleado</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSaveEmployee.getInstancia.lblTitle.Text = "NUEVO EMPLEADO"
        FrmSaveEmployee.getInstancia.EntityModel = New EmployeeModel
        FrmSaveEmployee.getInstancia.save = True
        FrmSaveEmployee.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar Empleado</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSaveEmployee.getInstancia.lblTitle.Text = "EDITAR EMPLEADO"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveEmployee.getInstancia.EntityModel = entityModel
            FrmSaveEmployee.getInstancia.save = False
            FrmSaveEmployee.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Empleado", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar un Empleado o una seleccion de Empleados</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar el Empleado " & dtgList.CurrentRow.Cells.Item("Name").Value & "?"
            Else
                msg = "¿Realmente desea eliminar los Empleados seleccionadas?"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Empleado", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey, Values.ClientId) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Empleado", "El Empleado " & row.Cells("Name").Value & " no fue eliminado")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Empleados Eliminados", "Registros eliminados correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value, ClientId) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Empleado Eliminado", "Empleado " & dtgList.CurrentRow.Cells.Item("Name").Value & " eliminado correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Empleado", "El Empleado " & dtgList.CurrentRow.Cells.Item("Name").Value & " no fue eliminado")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Empleado", "Uno o varios de los registros tiene elementos asignados y no se puedo eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Empleado", dtgList.CurrentRow.Cells.Item("Name").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Empleado", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Empleado", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("")
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrar Empleados escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("")
        Else
            GetAllMemory(txtSearch.Text)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para mostrar los roles de un empleado seleccionado</summary>
    Private Sub btnRoles_Click(sender As Object, e As EventArgs) Handles btnRoles.Click
        Try
            FrmEmployeeRoles.getInstancia.lblTitle.Text = "ROLES DEL EMPLEADO"
            Dim entity = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmEmployeeRoles.getInstancia.EntityModel = entity
            FrmEmployeeRoles.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar rol", ex.Message)
        End Try
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("")
            Else
                GetAll(txtSearch.Text)
            End If
        End If
    End Sub
End Class