﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveEmployee
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.lblLastName = New System.Windows.Forms.Label()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.lblValidatePass = New System.Windows.Forms.Label()
        Me.lblPass = New System.Windows.Forms.Label()
        Me.txtLogin = New System.Windows.Forms.TextBox()
        Me.lblValidateLogin = New System.Windows.Forms.Label()
        Me.lblLogin = New System.Windows.Forms.Label()
        Me.txtDocument = New System.Windows.Forms.TextBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblValidateDocument = New System.Windows.Forms.Label()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.lblValidateLastName = New System.Windows.Forms.Label()
        Me.lblValidateEmail = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblValidatePhone = New System.Windows.Forms.Label()
        Me.lblDocument = New System.Windows.Forms.Label()
        Me.lblValidateAddress = New System.Windows.Forms.Label()
        Me.lblValidateName = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBody.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(12, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(329, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "PERSONA"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(376, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(347, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'lblLastName
        '
        Me.lblLastName.AutoSize = True
        Me.lblLastName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(36, 110)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(74, 19)
        Me.lblLastName.TabIndex = 29
        Me.lblLastName.Text = "Apellidos:"
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.txtName)
        Me.pnlBody.Controls.Add(Me.lblValidateName)
        Me.pnlBody.Controls.Add(Me.txtPass)
        Me.pnlBody.Controls.Add(Me.lblValidatePass)
        Me.pnlBody.Controls.Add(Me.lblPass)
        Me.pnlBody.Controls.Add(Me.txtLogin)
        Me.pnlBody.Controls.Add(Me.lblValidateLogin)
        Me.pnlBody.Controls.Add(Me.lblLogin)
        Me.pnlBody.Controls.Add(Me.txtDocument)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Controls.Add(Me.lblValidateDocument)
        Me.pnlBody.Controls.Add(Me.txtLastName)
        Me.pnlBody.Controls.Add(Me.txtEmail)
        Me.pnlBody.Controls.Add(Me.lblValidateLastName)
        Me.pnlBody.Controls.Add(Me.lblValidateEmail)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.txtPhone)
        Me.pnlBody.Controls.Add(Me.txtAddress)
        Me.pnlBody.Controls.Add(Me.lblLastName)
        Me.pnlBody.Controls.Add(Me.lblName)
        Me.pnlBody.Controls.Add(Me.lblEmail)
        Me.pnlBody.Controls.Add(Me.lblValidatePhone)
        Me.pnlBody.Controls.Add(Me.lblDocument)
        Me.pnlBody.Controls.Add(Me.lblValidateAddress)
        Me.pnlBody.Controls.Add(Me.lblAddress)
        Me.pnlBody.Controls.Add(Me.lblPhone)
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(376, 491)
        Me.pnlBody.TabIndex = 45
        '
        'txtPass
        '
        Me.txtPass.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPass.Location = New System.Drawing.Point(37, 395)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(300, 24)
        Me.txtPass.TabIndex = 8
        '
        'lblValidatePass
        '
        Me.lblValidatePass.AutoSize = True
        Me.lblValidatePass.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePass.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePass.Location = New System.Drawing.Point(41, 416)
        Me.lblValidatePass.Name = "lblValidatePass"
        Me.lblValidatePass.Size = New System.Drawing.Size(103, 15)
        Me.lblValidatePass.TabIndex = 71
        Me.lblValidatePass.Text = "Ingrese Contaseña"
        Me.lblValidatePass.Visible = False
        '
        'lblPass
        '
        Me.lblPass.AutoSize = True
        Me.lblPass.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPass.Location = New System.Drawing.Point(36, 376)
        Me.lblPass.Name = "lblPass"
        Me.lblPass.Size = New System.Drawing.Size(76, 19)
        Me.lblPass.TabIndex = 70
        Me.lblPass.Text = "Password:"
        '
        'txtLogin
        '
        Me.txtLogin.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLogin.Location = New System.Drawing.Point(37, 342)
        Me.txtLogin.Name = "txtLogin"
        Me.txtLogin.Size = New System.Drawing.Size(300, 24)
        Me.txtLogin.TabIndex = 7
        '
        'lblValidateLogin
        '
        Me.lblValidateLogin.AutoSize = True
        Me.lblValidateLogin.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateLogin.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateLogin.Location = New System.Drawing.Point(42, 363)
        Me.lblValidateLogin.Name = "lblValidateLogin"
        Me.lblValidateLogin.Size = New System.Drawing.Size(74, 15)
        Me.lblValidateLogin.TabIndex = 68
        Me.lblValidateLogin.Text = "Ingrese Login"
        Me.lblValidateLogin.Visible = False
        '
        'lblLogin
        '
        Me.lblLogin.AutoSize = True
        Me.lblLogin.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogin.Location = New System.Drawing.Point(36, 323)
        Me.lblLogin.Name = "lblLogin"
        Me.lblLogin.Size = New System.Drawing.Size(49, 19)
        Me.lblLogin.TabIndex = 67
        Me.lblLogin.Text = "Login:"
        '
        'txtDocument
        '
        Me.txtDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocument.Location = New System.Drawing.Point(37, 25)
        Me.txtDocument.Name = "txtDocument"
        Me.txtDocument.Size = New System.Drawing.Size(300, 24)
        Me.txtDocument.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(196, 437)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 49)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'lblValidateDocument
        '
        Me.lblValidateDocument.AutoSize = True
        Me.lblValidateDocument.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDocument.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDocument.Location = New System.Drawing.Point(41, 46)
        Me.lblValidateDocument.Name = "lblValidateDocument"
        Me.lblValidateDocument.Size = New System.Drawing.Size(64, 15)
        Me.lblValidateDocument.TabIndex = 62
        Me.lblValidateDocument.Text = "Ingrese DNI"
        Me.lblValidateDocument.Visible = False
        '
        'txtLastName
        '
        Me.txtLastName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.Location = New System.Drawing.Point(37, 129)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(300, 24)
        Me.txtLastName.TabIndex = 3
        '
        'txtEmail
        '
        Me.txtEmail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.Location = New System.Drawing.Point(37, 289)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(300, 24)
        Me.txtEmail.TabIndex = 6
        '
        'lblValidateLastName
        '
        Me.lblValidateLastName.AutoSize = True
        Me.lblValidateLastName.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateLastName.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateLastName.Location = New System.Drawing.Point(34, 150)
        Me.lblValidateLastName.Name = "lblValidateLastName"
        Me.lblValidateLastName.Size = New System.Drawing.Size(92, 15)
        Me.lblValidateLastName.TabIndex = 47
        Me.lblValidateLastName.Text = "Ingrese Apellidos"
        Me.lblValidateLastName.Visible = False
        '
        'lblValidateEmail
        '
        Me.lblValidateEmail.AutoSize = True
        Me.lblValidateEmail.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateEmail.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateEmail.Location = New System.Drawing.Point(42, 310)
        Me.lblValidateEmail.Name = "lblValidateEmail"
        Me.lblValidateEmail.Size = New System.Drawing.Size(82, 15)
        Me.lblValidateEmail.TabIndex = 65
        Me.lblValidateEmail.Text = "Ingrese Correo"
        Me.lblValidateEmail.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(36, 437)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 49)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'txtPhone
        '
        Me.txtPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(37, 236)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(300, 24)
        Me.txtPhone.TabIndex = 5
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(37, 183)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(300, 24)
        Me.txtAddress.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(37, 77)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(300, 24)
        Me.txtName.TabIndex = 2
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(36, 58)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(73, 19)
        Me.lblName.TabIndex = 56
        Me.lblName.Text = "Nombres:"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(36, 270)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(56, 19)
        Me.lblEmail.TabIndex = 60
        Me.lblEmail.Text = "e-mail:"
        '
        'lblValidatePhone
        '
        Me.lblValidatePhone.AutoSize = True
        Me.lblValidatePhone.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePhone.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePhone.Location = New System.Drawing.Point(42, 257)
        Me.lblValidatePhone.Name = "lblValidatePhone"
        Me.lblValidatePhone.Size = New System.Drawing.Size(91, 15)
        Me.lblValidatePhone.TabIndex = 64
        Me.lblValidatePhone.Text = "Ingrese Telefono"
        Me.lblValidatePhone.Visible = False
        '
        'lblDocument
        '
        Me.lblDocument.AutoSize = True
        Me.lblDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(36, 6)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(36, 19)
        Me.lblDocument.TabIndex = 57
        Me.lblDocument.Text = "DNI:"
        '
        'lblValidateAddress
        '
        Me.lblValidateAddress.AutoSize = True
        Me.lblValidateAddress.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateAddress.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateAddress.Location = New System.Drawing.Point(42, 204)
        Me.lblValidateAddress.Name = "lblValidateAddress"
        Me.lblValidateAddress.Size = New System.Drawing.Size(93, 15)
        Me.lblValidateAddress.TabIndex = 63
        Me.lblValidateAddress.Text = "Ingrese Dirección"
        Me.lblValidateAddress.Visible = False
        '
        'lblValidateName
        '
        Me.lblValidateName.AutoSize = True
        Me.lblValidateName.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateName.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateName.Location = New System.Drawing.Point(42, 98)
        Me.lblValidateName.Name = "lblValidateName"
        Me.lblValidateName.Size = New System.Drawing.Size(92, 15)
        Me.lblValidateName.TabIndex = 61
        Me.lblValidateName.Text = "Ingrese Nombres"
        Me.lblValidateName.Visible = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(36, 164)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(78, 19)
        Me.lblAddress.TabIndex = 58
        Me.lblAddress.Text = "Dirección:"
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(36, 217)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(70, 19)
        Me.lblPhone.TabIndex = 59
        Me.lblPhone.Text = "Telefono:"
        '
        'FrmSaveEmployee
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(376, 534)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveEmployee"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents lblLastName As Label
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents pnlBody As Panel
    Friend WithEvents txtLastName As TextBox
    Friend WithEvents lblValidateLastName As Label
    Friend WithEvents txtPass As TextBox
    Friend WithEvents lblValidatePass As Label
    Friend WithEvents lblPass As Label
    Friend WithEvents txtLogin As TextBox
    Friend WithEvents lblValidateLogin As Label
    Friend WithEvents lblLogin As Label
    Friend WithEvents txtDocument As TextBox
    Friend WithEvents lblValidateDocument As Label
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents lblValidateEmail As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents lblName As Label
    Friend WithEvents lblEmail As Label
    Friend WithEvents lblValidatePhone As Label
    Friend WithEvents lblDocument As Label
    Friend WithEvents lblValidateAddress As Label
    Friend WithEvents lblValidateName As Label
    Friend WithEvents lblAddress As Label
    Friend WithEvents lblPhone As Label
End Class
