﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Empleado: 
''' Formulario que permite editar o modificar un Empleado
''' </summary>
Public Class FrmSaveEmployee

    'controlador del modelo de Empleado
    Private ReadOnly controller As New EmployeeController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveEmployee = Nothing

    'Modelo de Empleado a ser agregado o modificado
    Public Property EntityModel As EmployeeModel

    'Variable que determina si se trata de una insercion o no
    Public Property save As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveEmployee
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveEmployee
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateLastName.Visible = False
        lblValidateName.Visible = False
        lblValidateDocument.Visible = False
        lblValidateAddress.Visible = False
        lblValidatePhone.Visible = False
        lblValidateEmail.Visible = False
        lblValidateLogin.Visible = False
        lblValidatePass.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "LastName") Then
                    lblValidateLastName.Text = item.ErrorMessage
                    lblValidateLastName.Visible = True
                    txtLastName.Focus()
                End If
                If (item.MemberNames.First = "Name") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "DocumentNumber") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "Address") Then
                    lblValidateAddress.Text = item.ErrorMessage
                    lblValidateAddress.Visible = True
                    txtAddress.Focus()
                End If
                If (item.MemberNames.First = "Phone") Then
                    lblValidatePhone.Text = item.ErrorMessage
                    lblValidatePhone.Visible = True
                    txtPhone.Focus()
                End If
                If (item.MemberNames.First = "Email") Then
                    lblValidateEmail.Text = item.ErrorMessage
                    lblValidateEmail.Visible = True
                    txtEmail.Focus()
                End If
                If (item.MemberNames.First = "Login") Then
                    lblValidateLogin.Text = item.ErrorMessage
                    lblValidateLogin.Visible = True
                    txtLogin.Focus()
                End If
                If (item.MemberNames.First = "Pass") Then
                    lblValidatePass.Text = item.ErrorMessage
                    lblValidatePass.Visible = True
                    txtPass.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtLastName.Text = ""
        txtName.Text = ""
        txtDocument.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtEmail.Text = ""
        txtLogin.Text = ""
        txtPass.Text = ""
    End Sub

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub EnableTxt(state As Boolean)
        txtLastName.Enabled = state
        txtName.Enabled = state
        txtAddress.Enabled = state
        txtPhone.Enabled = state
        txtEmail.Enabled = state
    End Sub

    '''<summary>Método para validar un empleado ya registrado como persona y sus controles</summary>
    Public Sub ValidateEmployee()
        If txtDocument.Text.Length = 8 Then
            EnableTxt(True)
            Dim person = controller.GetByDocument(txtDocument.Text)
            If person IsNot Nothing Then
                EntityModel.Person = person
                txtLastName.Text = EntityModel.Person.LastName
                txtName.Text = EntityModel.Person.Name
                txtAddress.Text = EntityModel.Person.Address
                txtPhone.Text = EntityModel.Person.Phone
                txtEmail.Text = EntityModel.Person.Email
                EnableTxt(False)
            Else
                txtLastName.Text = ""
                txtName.Text = ""
                txtAddress.Text = ""
                txtPhone.Text = ""
                txtEmail.Text = ""
                EnableTxt(True)
            End If
        Else
            EnableTxt(False)
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del Empleado seleccionado</summary>
    Private Sub FrmSaveEmployee_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel IsNot Nothing Then
            txtLastName.Text = EntityModel.LastName
            txtName.Text = EntityModel.Name
            txtDocument.Text = EntityModel.DocumentNumber
            txtAddress.Text = EntityModel.Address
            txtPhone.Text = EntityModel.Phone
            txtEmail.Text = EntityModel.Email
            txtLogin.Text = EntityModel.Login
            txtPass.Text = EntityModel.Pass
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        'Dispose()

    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Empleado</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.LastName = txtLastName.Text.ToUpper
            EntityModel.Name = txtName.Text.ToUpper
            EntityModel.DocumentNumber = txtDocument.Text.ToUpper
            EntityModel.Address = txtAddress.Text.ToUpper
            EntityModel.Phone = txtPhone.Text.ToUpper
            EntityModel.Email = txtEmail.Text.ToLower
            EntityModel.Login = txtLogin.Text.ToLower
            EntityModel.Pass = txtPass.Text

            EntityModel.Person = New PersonModel With {
                                    .LastName = txtLastName.Text.ToUpper,
                                    .Name = txtName.Text.ToUpper,
                                    .DocumentNumber = txtDocument.Text.ToUpper,
                                    .Address = txtAddress.Text.ToUpper,
                                    .Phone = txtPhone.Text.ToUpper,
                                    .Email = txtEmail.Text.ToUpper}

            If validateTxt() Then

                If Not save Then
                    EntityModel.Person.Id = EntityModel.Id
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Empleado", "Empleado modificado correctamente")
                        Desktop.FrmShowEmployee.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Empleado", "El Empleado NO pudo ser modificado")
                    End If

                Else
                    FrmEmployeeRole.getInstancia.EntityModel = EntityModel
                    FrmEmployeeRole.getInstancia.Show()
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    Dim er = err.Message.IndexOf("(")
                    Dim tam = err.Message.Length - er
                    Dim msg = err.Message.Substring(er + 1, tam - 3)
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", msg & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Empleado", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para apellidos</summary>
    Private Sub txtLastName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtLastName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para validar caja de texto para numero de documento</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 8
    End Sub

    '''<summary>Método para validar caja de texto para dirección</summary>
    Private Sub txtAddress_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAddress.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar caja de texto para telefono</summary>
    Private Sub txtPhone_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPhone.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 11
    End Sub

    '''<summary>Método para validar caja de texto para correo electrónico</summary>
    Private Sub txtEmail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtEmail.KeyPress
        FormDesing.DefaultDescription(e)
        sender.MaxLength = 100
    End Sub

    '''<summary>Método para autogenerar un nombre de uruario</summary>
    Private Sub txtLogin_GotFocus(sender As Object, e As EventArgs) Handles txtLogin.GotFocus, txtPass.GotFocus
        If save Then
            If txtName.Text.Length > 0 And txtLastName.Text.Length > 0 Then
                Dim user = txtName.Text.Split(" ")
                txtLogin.Text = user(0).ToLower & txtLastName.Text.Substring(0, 1).ToLower
            End If
        End If

    End Sub

    '''<summary>Método para verificar si existe el empleado registrado como persona</summary>
    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        If save And sender.text.length = 8 Then
            ValidateEmployee()
        Else
            EnableTxt(True)
        End If
    End Sub
End Class