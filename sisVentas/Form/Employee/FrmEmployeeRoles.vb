﻿Imports Transitions

''' <summary>
''' Clase Formulario Guardar Rol: 
''' Formulario que permite editar o modificar un rol
''' </summary>
Public Class FrmEmployeeRoles

    'controlador del modelo de rol
    Private ReadOnly controller As New EmployeeController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmEmployeeRoles = Nothing

    'Modelo de Rol a ser agregado o modificado
    Public Property EntityModel As EmployeeModel

    'lista de checkbox de permisos
    Private chkList As List(Of CheckBox)

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmEmployeeRoles
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmEmployeeRoles
        End If
        Return _formulario
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtName.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del rol seleccionado</summary>
    Private Sub FrmEmployeeRoles_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel IsNot Nothing Then

            FormDesing.Placeholder(txtName, "Buscar...")

            chkList = New List(Of CheckBox)

            For Each item In controller.GetRoles()
                Dim checkBox = New CheckBox With {
                    .Text = item.Name,
                    .Name = item.Id,
                    .Size = New Size(240, 20),
                    .Cursor = Cursors.Hand
                }
                toolTip.SetToolTip(checkBox, item.Description)
                For Each r In EntityModel.Roles
                    If r.Id = item.Id Then
                        checkBox.Font = New Font("Century Gothic", 9.2!, FontStyle.Bold)
                        checkBox.Checked = True
                    End If
                Next
                chkList.Add(checkBox)
            Next
            AddCheckBox(chkList)
        End If

    End Sub

    '''<summary>Método para agregar los checks en el contenedor</summary>
    Public Sub AddCheckBox(list As List(Of CheckBox))
        pnlRoles.Controls.Clear()
        For Each item In list
            pnlRoles.Controls.Add(item)
        Next
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar rol</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim roleIds = New List(Of Integer)
            For Each control As CheckBox In pnlRoles.Controls
                If control.Checked Then
                    roleIds.Add(control.Name)
                End If
            Next
            If controller.AddRoles(EntityModel.Id, roleIds) >= 0 Then
                FrmMessage.ShowMessage(AlertResource.SUCCESS, "Roles del Empleado", "Roles registrados correctamente")
                Me.Close()
            Else
                FrmMessage.ShowMessage(AlertResource.ERRO, "Roles del Empleado", "Los Roles NO se registraron")
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Roles del Empleado", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
    End Sub

    '''<summary>Método para filtrar roles por nombre</summary>
    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged

        If chkList IsNot Nothing Then
            If txtName.Text = "" Or txtName.Text = "Buscar..." Then
                AddCheckBox(chkList)
            Else
                Dim query = chkList.FindAll(Function(check) check.Text.ToLower.Contains(txtName.Text.ToLower))
                AddCheckBox(query)
            End If
        End If

    End Sub

    'Placeholder para la caja de texto
    Private Sub txtName_GotFocus(sender As Object, e As EventArgs) Handles txtName.GotFocus
        FormDesing.Placeholder(txtName, "Buscar...")
    End Sub

    Private Sub txtName_LostFocus(sender As Object, e As EventArgs) Handles txtName.LostFocus
        FormDesing.Placeholder(txtName, "Buscar...")
    End Sub
End Class