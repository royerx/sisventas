﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTechnicalReportReport
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.viewer = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.TechnicalReportModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PersonModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ApplianceModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBody.SuspendLayout()
        CType(Me.TechnicalReportModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PersonModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ApplianceModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(768, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "COMPROBANTE DE PAGO"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(804, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(777, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.viewer)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(804, 579)
        Me.pnlBody.TabIndex = 46
        '
        'viewer
        '
        ReportDataSource1.Name = "TechnicalReport"
        ReportDataSource1.Value = Me.TechnicalReportModelBindingSource
        ReportDataSource2.Name = "Client"
        ReportDataSource2.Value = Me.PersonModelBindingSource
        ReportDataSource3.Name = "Appliance"
        ReportDataSource3.Value = Me.ApplianceModelBindingSource
        Me.viewer.LocalReport.DataSources.Add(ReportDataSource1)
        Me.viewer.LocalReport.DataSources.Add(ReportDataSource2)
        Me.viewer.LocalReport.DataSources.Add(ReportDataSource3)
        Me.viewer.LocalReport.ReportEmbeddedResource = "sisVentas.Desktop.RptTechnicalReport.rdlc"
        Me.viewer.Location = New System.Drawing.Point(0, 0)
        Me.viewer.Name = "viewer"
        Me.viewer.ServerReport.BearerToken = Nothing
        Me.viewer.Size = New System.Drawing.Size(802, 577)
        Me.viewer.TabIndex = 0
        '
        'TechnicalReportModelBindingSource
        '
        Me.TechnicalReportModelBindingSource.DataSource = GetType(sisVentas.Desktop.TechnicalReportModel)
        '
        'PersonModelBindingSource
        '
        Me.PersonModelBindingSource.DataSource = GetType(sisVentas.Desktop.PersonModel)
        '
        'ApplianceModelBindingSource
        '
        Me.ApplianceModelBindingSource.DataSource = GetType(sisVentas.Desktop.ApplianceModel)
        '
        'FrmTechnicalReportReport
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(804, 610)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmTechnicalReportReport"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBody.ResumeLayout(False)
        CType(Me.TechnicalReportModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PersonModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ApplianceModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents viewer As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents TechnicalReportModelBindingSource As BindingSource
    Friend WithEvents PersonModelBindingSource As BindingSource
    Friend WithEvents ApplianceModelBindingSource As BindingSource
End Class
