﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Generar Reporte de Cotización: 
''' Formulario que permite Visualizar la impresion de una cotización
''' </summary>
Public Class FrmQuotedReport

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmQuotedReport = Nothing

    'Modelo de Cotizacion a ser mostrada
    Public Property EntityModel As QuotedModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmQuotedReport
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmQuotedReport
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    Private Sub GetReport()
        QuotedModelBindingSource.DataSource = EntityModel
        PersonModelBindingSource.DataSource = EntityModel.Employee.Person
        OrderDetailModelBindingSource.DataSource = EntityModel.OrderDetails
        Me.viewer.RefreshReport()
    End Sub

    '''<summary>Método para mostrar los datos de la Cotización</summary>
    Private Sub FrmSaveTechnicalReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        GetReport()
    End Sub

    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Dispose()
    End Sub

End Class