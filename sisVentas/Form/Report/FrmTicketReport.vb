﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Generar Ticket: 
''' Formulario que permite Visualizar la impresion de un ticket
''' </summary>
Public Class FrmTicketReport

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmTicketReport = Nothing

    'Modelo de Nota de Pedido a ser mostrada
    Public Property EntityModel As OrderModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmTicketReport
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmTicketReport
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    Private Sub GetReport()
        OrderModelBindingSource.DataSource = EntityModel
        Me.viewer.RefreshReport()
    End Sub

    '''<summary>Método para mostrar los datos de la Cotización</summary>
    Private Sub FrmSaveTechnicalReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        GetReport()
    End Sub

    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Dispose()
    End Sub

End Class