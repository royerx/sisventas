﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Generar Reporte de Comprobante de Pago
''' Formulario que permite visualizar reporte de comprobante de pago
''' </summary>
Public Class FrmVoucherReport

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmVoucherReport = Nothing

    'Modelo de Comprobante de Pago a ser mostrado
    Public Property EntityModel As VoucherModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmVoucherReport
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmVoucherReport
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    Private Sub GetReport()
        VoucherModelBindingSource.DataSource = EntityModel
        If EntityModel.Order IsNot Nothing Then
            OrderModelBindingSource.DataSource = EntityModel.Order
            PersonModelBindingSource.DataSource = EntityModel.Order.Client
            For Each item In EntityModel.Order.OrderDetails
                item.Order = EntityModel.Order
            Next
            OrderDetailModelBindingSource.DataSource = EntityModel.Order.OrderDetails
        End If
        If EntityModel.TechnicalReport IsNot Nothing Then
            TechnicalReportModelBindingSource.DataSource = EntityModel.TechnicalReport
            PersonModelBindingSource.DataSource = EntityModel.TechnicalReport.Client
        End If

        Me.viewer.RefreshReport()
    End Sub

    '''<summary>Método para mostrar los datos del comprobante de pago</summary>
    Private Sub FrmSaveTechnicalReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        GetReport()
    End Sub

    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Dispose()
    End Sub

End Class