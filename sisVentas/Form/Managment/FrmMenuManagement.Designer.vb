﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMenuManagement
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnlPersonType = New System.Windows.Forms.Panel()
        Me.pnlHeadPersonType = New System.Windows.Forms.Panel()
        Me.lblHeaderPersonType = New System.Windows.Forms.Label()
        Me.pnlRole = New System.Windows.Forms.Panel()
        Me.pnlHeadRole = New System.Windows.Forms.Panel()
        Me.lblHeaderRole = New System.Windows.Forms.Label()
        Me.pnlBody = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnlPermission = New System.Windows.Forms.Panel()
        Me.pnlHeadPermission = New System.Windows.Forms.Panel()
        Me.lblHeaderPermission = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.imgPersonType = New System.Windows.Forms.PictureBox()
        Me.imgRole = New System.Windows.Forms.PictureBox()
        Me.imgPermission = New System.Windows.Forms.PictureBox()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlPersonType.SuspendLayout()
        Me.pnlHeadPersonType.SuspendLayout()
        Me.pnlRole.SuspendLayout()
        Me.pnlHeadRole.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        Me.pnlPermission.SuspendLayout()
        Me.pnlHeadPermission.SuspendLayout()
        Me.pnlTitle.SuspendLayout()
        CType(Me.imgPersonType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgRole, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgPermission, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlPersonType
        '
        Me.pnlPersonType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPersonType.Controls.Add(Me.imgPersonType)
        Me.pnlPersonType.Controls.Add(Me.pnlHeadPersonType)
        Me.pnlPersonType.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlPersonType.Location = New System.Drawing.Point(23, 23)
        Me.pnlPersonType.Name = "pnlPersonType"
        Me.pnlPersonType.Size = New System.Drawing.Size(200, 250)
        Me.pnlPersonType.TabIndex = 1
        '
        'pnlHeadPersonType
        '
        Me.pnlHeadPersonType.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadPersonType.Controls.Add(Me.lblHeaderPersonType)
        Me.pnlHeadPersonType.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadPersonType.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadPersonType.Name = "pnlHeadPersonType"
        Me.pnlHeadPersonType.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadPersonType.TabIndex = 0
        '
        'lblHeaderPersonType
        '
        Me.lblHeaderPersonType.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderPersonType.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderPersonType.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderPersonType.Name = "lblHeaderPersonType"
        Me.lblHeaderPersonType.Size = New System.Drawing.Size(190, 22)
        Me.lblHeaderPersonType.TabIndex = 0
        Me.lblHeaderPersonType.Text = "Tipo de Persona"
        Me.lblHeaderPersonType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlRole
        '
        Me.pnlRole.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlRole.Controls.Add(Me.imgRole)
        Me.pnlRole.Controls.Add(Me.pnlHeadRole)
        Me.pnlRole.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlRole.Location = New System.Drawing.Point(229, 23)
        Me.pnlRole.Name = "pnlRole"
        Me.pnlRole.Size = New System.Drawing.Size(200, 250)
        Me.pnlRole.TabIndex = 2
        '
        'pnlHeadRole
        '
        Me.pnlHeadRole.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadRole.Controls.Add(Me.lblHeaderRole)
        Me.pnlHeadRole.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadRole.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadRole.Name = "pnlHeadRole"
        Me.pnlHeadRole.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadRole.TabIndex = 0
        '
        'lblHeaderRole
        '
        Me.lblHeaderRole.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderRole.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderRole.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderRole.Name = "lblHeaderRole"
        Me.lblHeaderRole.Size = New System.Drawing.Size(190, 20)
        Me.lblHeaderRole.TabIndex = 0
        Me.lblHeaderRole.Text = "Roles"
        Me.lblHeaderRole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlBody
        '
        Me.pnlBody.Controls.Add(Me.pnlPersonType)
        Me.pnlBody.Controls.Add(Me.pnlRole)
        Me.pnlBody.Controls.Add(Me.pnlPermission)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 25)
        Me.pnlBody.Margin = New System.Windows.Forms.Padding(20)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Padding = New System.Windows.Forms.Padding(20)
        Me.pnlBody.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlBody.Size = New System.Drawing.Size(1050, 575)
        Me.pnlBody.TabIndex = 7
        '
        'pnlPermission
        '
        Me.pnlPermission.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPermission.Controls.Add(Me.imgPermission)
        Me.pnlPermission.Controls.Add(Me.pnlHeadPermission)
        Me.pnlPermission.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlPermission.Location = New System.Drawing.Point(435, 23)
        Me.pnlPermission.Name = "pnlPermission"
        Me.pnlPermission.Size = New System.Drawing.Size(200, 250)
        Me.pnlPermission.TabIndex = 3
        '
        'pnlHeadPermission
        '
        Me.pnlHeadPermission.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadPermission.Controls.Add(Me.lblHeaderPermission)
        Me.pnlHeadPermission.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadPermission.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadPermission.Name = "pnlHeadPermission"
        Me.pnlHeadPermission.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadPermission.TabIndex = 0
        '
        'lblHeaderPermission
        '
        Me.lblHeaderPermission.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderPermission.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderPermission.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderPermission.Name = "lblHeaderPermission"
        Me.lblHeaderPermission.Size = New System.Drawing.Size(190, 20)
        Me.lblHeaderPermission.TabIndex = 0
        Me.lblHeaderPermission.Text = "Permisos"
        Me.lblHeaderPermission.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.SystemColors.Control
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(1050, 25)
        Me.pnlTitle.TabIndex = 22
        '
        'imgPersonType
        '
        Me.imgPersonType.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgPersonType.Image = Global.sisVentas.Desktop.My.Resources.Resources.clienteIndividual
        Me.imgPersonType.Location = New System.Drawing.Point(4, 55)
        Me.imgPersonType.Name = "imgPersonType"
        Me.imgPersonType.Size = New System.Drawing.Size(190, 190)
        Me.imgPersonType.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgPersonType.TabIndex = 1
        Me.imgPersonType.TabStop = False
        '
        'imgRole
        '
        Me.imgRole.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgRole.Image = Global.sisVentas.Desktop.My.Resources.Resources.role
        Me.imgRole.Location = New System.Drawing.Point(4, 55)
        Me.imgRole.Name = "imgRole"
        Me.imgRole.Size = New System.Drawing.Size(190, 190)
        Me.imgRole.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgRole.TabIndex = 1
        Me.imgRole.TabStop = False
        '
        'imgPermission
        '
        Me.imgPermission.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgPermission.Image = Global.sisVentas.Desktop.My.Resources.Resources.permission
        Me.imgPermission.Location = New System.Drawing.Point(4, 55)
        Me.imgPermission.Name = "imgPermission"
        Me.imgPermission.Size = New System.Drawing.Size(190, 190)
        Me.imgPermission.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgPermission.TabIndex = 1
        Me.imgPermission.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Control
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_cerrar2
        Me.btnClose.Location = New System.Drawing.Point(12, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'FrmMenuManagement
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1050, 600)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmMenuManagement"
        Me.Text = "frmEquipo"
        Me.pnlPersonType.ResumeLayout(False)
        Me.pnlHeadPersonType.ResumeLayout(False)
        Me.pnlRole.ResumeLayout(False)
        Me.pnlHeadRole.ResumeLayout(False)
        Me.pnlBody.ResumeLayout(False)
        Me.pnlPermission.ResumeLayout(False)
        Me.pnlHeadPermission.ResumeLayout(False)
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.imgPersonType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgRole, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgPermission, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlPersonType As Panel
    Friend WithEvents pnlHeadPersonType As Panel
    Friend WithEvents imgPersonType As PictureBox
    Friend WithEvents lblHeaderPersonType As Label
    Friend WithEvents pnlRole As Panel
    Friend WithEvents imgRole As PictureBox
    Friend WithEvents pnlHeadRole As Panel
    Friend WithEvents lblHeaderRole As Label
    Friend WithEvents pnlBody As FlowLayoutPanel
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlPermission As Panel
    Friend WithEvents imgPermission As PictureBox
    Friend WithEvents pnlHeadPermission As Panel
    Friend WithEvents lblHeaderPermission As Label
End Class
