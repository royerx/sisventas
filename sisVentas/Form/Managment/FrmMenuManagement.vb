﻿Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Menu de Mantenimiento: 
''' Formulario que muestra el menu del modulo de mantenimiento
''' </summary>
Public Class FrmMenuManagement

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmMenuManagement = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmMenuManagement
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmMenuManagement
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de tipos de persona</summary>
    Private Sub imgPersonType_Click(sender As Object, e As EventArgs) Handles imgPersonType.Click
        frmPrincipal.lblTitle.Text = "TIPOS DE PERSONAS"
        frmPrincipal.FormPanel(FrmShowPersonType.getInstancia)
    End Sub

    '''<summary>Método para validar permisos</summary>
    Private Sub FrmMenuManagement_Load(sender As Object, e As EventArgs) Handles Me.Load
        pnlPersonType.Visible = Auth.Can(Values.PersonTypeList)
        pnlRole.Visible = Auth.Can(Values.RoleList)
        pnlPermission.Visible = Auth.Can(Values.PermissionList)
    End Sub

    '''<summary>Método para mostrar formulario de mantenimiento de permisos</summary>
    Private Sub imgPermission_Click(sender As Object, e As EventArgs) Handles imgPermission.Click
        frmPrincipal.lblTitle.Text = "PERMISOS"
        frmPrincipal.FormPanel(FrmShowPermission.getInstancia)
    End Sub

    '''<summary>Método para mostrar formulario de mantenimiento de roles</summary>
    Private Sub imgRole_Click(sender As Object, e As EventArgs) Handles imgRole.Click
        frmPrincipal.lblTitle.Text = "ROLES"
        frmPrincipal.FormPanel(FrmShowRole.getInstancia)
    End Sub
End Class