﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Tipos de Persona: 
''' Formulario que muestra las opciones de mantenimiento de tipo de persona
''' </summary>
Public Class FrmShowPersonType

    'controlador del modelo de categoria
    Private ReadOnly controller As New PersonTypeController()

    'lista de modelo de tipo de persona con filtros
    Private dt As List(Of PersonTypeModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowPersonType = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowPersonType
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowPersonType
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista los tipos de persona cuando carga el formulario y validar permisos</summary>
    Private Sub FrmShowPersonType_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.PersonTypeCreate)
        btnEdit.Visible = Auth.Can(Values.PersonTypeDetail)
        btnDelete.Visible = Auth.Can(Values.PersonTypeDelete)
    End Sub

    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con los tipos de persona filtradas</summary>
    Public Sub GetAll(search As String)
        Try
            dt = controller.GetAll(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Type").HeaderText = "TIPO DE PERSONA"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los tipos de persona filtradas en memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            dt = controller.GetAllMemory(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Type").HeaderText = "TIPO DE PERSONA"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar categoria</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSavePersonType.getInstancia.lblTitle.Text = "NUEVO TIPO DE PERSONA"
        FrmSavePersonType.getInstancia.EntityModel = New PersonTypeModel
        FrmSavePersonType.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar categoria</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSavePersonType.getInstancia.lblTitle.Text = "EDITAR TIPO DE PERSONA"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSavePersonType.getInstancia.EntityModel = entityModel
            FrmSavePersonType.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Tipo de Persona", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar una categoria o una seleccion de tipo de persona</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar el tipo de persona " & dtgList.CurrentRow.Cells.Item("Type").Value & "?"
            Else
                msg = "¿Realmente desea eliminar los tipos de persona seleccionados?"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Tipo de Persona", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Tipo de Persona", "El Tipo de Persona " & row.Cells("Type").Value & " no fue eliminada")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Tipos de Persona Eliminadas", "Registros eliminados correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Categoría Tipo de Pesona", "Tipo de Persona " & dtgList.CurrentRow.Cells.Item("Type").Value & " eliminada correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Tipo de Persona", "El Tipo de Persona " & dtgList.CurrentRow.Cells.Item("Type").Value & " no fue eliminada")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Tipo de Persona", "Uno o varios de los registros tiene elementos asignados y no se puedo, eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Tipo de Persona", dtgList.CurrentRow.Cells.Item("Type").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Tipo de Persona", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Tipo de Persona", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("")
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrar tipos de persona escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("")
        Else
            GetAllMemory(txtSearch.Text)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para mostrar el formulario de mantenimiento de equipos de cómputo</summary>
    Private Sub btnAppliance_Click(sender As Object, e As EventArgs)
        frmPrincipal.FormPanel(FrmShowApplianceDescription.getInstancia)
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("")
            Else
                GetAll(txtSearch.Text)
            End If
        End If
    End Sub
End Class