﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Rol: 
''' Formulario que permite editar o modificar un rol
''' </summary>
Public Class FrmSaveRole

    'controlador del modelo de rol
    Private ReadOnly controller As New RoleController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveRole = Nothing

    'Modelo de Rol a ser agregado o modificado
    Public Property EntityModel As RoleModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveRole
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveRole
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateName.Visible = False
        lblValidateDescription.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Name") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "Description") Then
                    lblValidateDescription.Text = item.ErrorMessage
                    lblValidateDescription.Visible = True
                    txtDescription.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtName.Text = ""
        txtDescription.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del rol seleccionado</summary>
    Private Sub FrmSaveRole_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        If EntityModel IsNot Nothing Then
            txtName.Text = EntityModel.Name
            txtDescription.Text = EntityModel.Description
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar rol</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try

            EntityModel.Name = txtName.Text.ToUpper
            EntityModel.Description = txtDescription.Text

            If validateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Rol", "Rol modificado correctamente")
                        Desktop.FrmShowRole.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Rol", "El Rol NO pudo ser modificado")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Rol", "Rol registrado correctamente")
                        Desktop.FrmShowRole.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Rol", "El Rol NO pudo ser registrado")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Rol", txtName.Text & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Rol", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Rol", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescription.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub
End Class