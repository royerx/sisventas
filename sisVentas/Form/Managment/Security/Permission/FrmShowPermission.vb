﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Permisos: 
''' Formulario que muestra las opciones de mantenimiento de permisos
''' </summary>
Public Class FrmShowPermission

    'controlador del modelo de permiso
    Private ReadOnly controller As New PermissionController()

    'lista de modelo de permisos con filtros
    Private dt As List(Of PermissionModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowPermission = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowPermission
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowPermission
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista los permisos cuando carga el formulario y validar permisos</summary>
    Private Sub FrmShowPermission_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.PermissionCreate)
        btnEdit.Visible = Auth.Can(Values.PermissionDetail)
        btnDelete.Visible = Auth.Can(Values.PermissionDelete)
    End Sub

    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con los permisos filtrados</summary>
    Public Sub GetAll(search As String)
        Try
            dt = controller.GetAll(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Name").HeaderText = "NOMBRE"
                dtgList.Columns.Item("Slug").HeaderText = "SLUG"
                dtgList.Columns.Item("Description").HeaderText = "DESCRIPCIÓN"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Roles").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los permisos filtrados en memoria</summary>
    Public Sub GetAllMemory(search As String)
        Try
            dt = controller.GetAllMemory(search)
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Name").HeaderText = "NOMBRE"
                dtgList.Columns.Item("Slug").HeaderText = "SLUG"
                dtgList.Columns.Item("Description").HeaderText = "DESCRIPCIÓN"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Roles").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar permiso</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSavePermission.getInstancia.lblTitle.Text = "NUEVO PERMISO"
        FrmSavePermission.getInstancia.EntityModel = New PermissionModel
        FrmSavePermission.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar permiso</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSavePermission.getInstancia.lblTitle.Text = "EDITAR PERMISO"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSavePermission.getInstancia.EntityModel = entityModel
            FrmSavePermission.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Permiso", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar un permiso o una seleccion de permisos</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar el permiso " & dtgList.CurrentRow.Cells.Item("Name").Value & "?"
            Else
                msg = "¿Realmente desea eliminar los permisos seleccionados?"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Permiso", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Permiso", "El Permiso " & row.Cells("Name").Value & " no fue eliminado")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Permisos Eliminados", "Registros eliminados correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Permiso Eliminado", "Permiso " & dtgList.CurrentRow.Cells.Item("Name").Value & " eliminado correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Permiso", "El Permiso " & dtgList.CurrentRow.Cells.Item("Name").Value & " no fue eliminado")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Permiso", "Uno o varios de los registros tiene elementos asignados y no se puedo eliminar, primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Permiso", dtgList.CurrentRow.Cells.Item("Name").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Permiso", err.Message & ":" & err.Number, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Permiso", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("")
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrar permisos escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("")
        Else
            GetAllMemory(txtSearch.Text)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("")
            Else
                GetAll(txtSearch.Text)
            End If
        End If
    End Sub
End Class