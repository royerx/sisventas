﻿Imports System.Data.SqlClient
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Permiso: 
''' Formulario que permite editar o modificar un permiso
''' </summary>
Public Class FrmSavePermission

    'controlador del modelo de permiso
    Private ReadOnly controller As New PermissionController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSavePermission = Nothing

    'Modelo de permiso a ser agregado o modificado
    Public Property EntityModel As PermissionModel

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSavePermission
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSavePermission
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateName.Visible = False
        lblValidateTable.Visible = False
        lblValidateAction.Visible = False
        lblValidateDescription.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Name") Then
                    lblValidateName.Text = item.ErrorMessage
                    lblValidateName.Visible = True
                    txtName.Focus()
                End If
                If (item.MemberNames.First = "slug") Then
                    lblValidateTable.Text = item.ErrorMessage
                    lblValidateTable.Visible = True
                    cbxTable.Focus()
                End If
                If (item.MemberNames.First = "Description") Then
                    lblValidateDescription.Text = item.ErrorMessage
                    lblValidateDescription.Visible = True
                    txtDescription.Focus()
                End If
            Next
            If cbxTable.Text = "" Then
                lblValidateTable.Text = "Ingrese o Seleccione una Tabla/Modulo"
                lblValidateTable.Visible = True
                cbxTable.Focus()
            End If
            If cbxAction.Text = "" Then
                lblValidateAction.Text = "Ingrese o Seleccione una Acción"
                lblValidateAction.Visible = True
                cbxAction.Focus()
            End If
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtName.Text = ""
        cbxTable.SelectedIndex = -1
        cbxAction.SelectedIndex = -1
        txtDescription.Text = ""
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del permiso seleccionado</summary>
    Private Sub FrmSavePermission_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()
        cbxTable.DataSource = controller.Tables()
        If EntityModel IsNot Nothing Then
            txtName.Text = EntityModel.Name
            txtDescription.Text = EntityModel.Description
            If (EntityModel.Slug IsNot Nothing) Then
                Dim slug = EntityModel.Slug.Split(".")
                cbxTable.Text = slug(0)
                cbxAction.Text = slug(1)
            Else
                cbxTable.SelectedIndex = -1
                cbxAction.SelectedIndex = -1
            End If
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar permiso</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try

            EntityModel.Name = txtName.Text
            EntityModel.Slug = cbxTable.Text.ToLower & "." & cbxAction.Text.ToLower
            EntityModel.Description = txtDescription.Text

            If validateTxt() Then

                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Permiso", "Permiso modificado correctamente")
                        Desktop.FrmShowPermission.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Modificar Permiso", "El Permiso NO pudo ser modificado")
                    End If

                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Permiso", "Permiso registrado correctamente")
                        Desktop.FrmShowPermission.getInstancia.GetAll("")
                        ClearTxt()
                        Me.Close()
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Permiso", "El Permiso NO pudo ser registrado")
                    End If
                End If
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 2627 Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Permiso", cbxTable.Text.ToLower & "." & cbxAction.Text.ToLower & " ya se encuentra registrado en la base de datos", AlertResource.INFO)
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Permiso", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Permiso", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
    End Sub

    '''<summary>Método para validar caja de texto para nombre</summary>
    Private Sub txtName_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtName.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDescription.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para validar slug de permiso</summary>
    Private Sub cbxTable_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxTable.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 50
    End Sub

    '''<summary>Método para validar slug de permiso</summary>
    Private Sub cbxAction_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxAction.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 50
    End Sub
End Class