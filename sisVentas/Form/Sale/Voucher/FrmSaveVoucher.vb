﻿Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Comprobante de Pago: 
''' Formulario que permite editar o modificar un Comprobante de Pago
''' </summary>
Public Class FrmSaveVoucher

    'controlador del modelo de Comprobante de Pago
    Private ReadOnly controller As New VoucherController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveVoucher = Nothing

    'Modelo de Comprobante de Pago a ser agregado o modificado
    Public Property EntityModel As VoucherModel

    'Nota de pedido que se le agrega, edita o elimina en el comprobante de pago
    Private order As OrderModel

    'Reporte técnico que se le agrega, edita o elimina en el comprobante de pago
    Private technicalReport

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Edit As Boolean = False

    'Permite mostrar los controles para editar o guardar los elementos
    Public Property detail As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveVoucher
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveVoucher
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateOrder.Visible = False
        lblValidateType.Visible = False
        lblValidateSerie.Visible = False
        lblValidateCorrelative.Visible = False
        lblValidateDate.Visible = False
        lblValidateDetail.Visible = False
        lblValidateState.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateOrderTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Vdate") Then
                    lblValidateDate.Text = item.ErrorMessage
                    lblValidateDate.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "Type") Then
                    lblValidateType.Text = item.ErrorMessage
                    lblValidateType.Visible = True
                    cbxType.Focus()
                End If
                If (item.MemberNames.First = "Serie") Then
                    lblValidateSerie.Text = item.ErrorMessage
                    lblValidateSerie.Visible = True
                    txtSerie.Focus()
                End If
                If (item.MemberNames.First = "Correlative") Then
                    lblValidateCorrelative.Text = item.ErrorMessage
                    lblValidateCorrelative.Visible = True
                    txtCorrelative.Focus()
                End If
                If (item.MemberNames.First = "State") Then
                    lblValidateState.Text = item.ErrorMessage
                    lblValidateState.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "Employee") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Comprobante de Pago", "Datos del empleado incorrectos inicie sesion nuevamente")
                End If

                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        If rbtSale.Checked Then
            If order Is Nothing Then
                FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Comprobante de Pago", "El Comprobante de Pago necesita ingresar el ticket de nota de pedido")
                txtOrderId.Focus()
            End If
        ElseIf rbtService.Checked Then
            If technicalReport Is Nothing Then
                FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Comprobante de Pago", "El Comprobante de Pago necesita ingresar el ticket de reporte técnico")
                txtOrderId.Focus()
            End If
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        order = Nothing
        technicalReport = Nothing
        txtOrderId.Text = ""
        cbxType.SelectedIndex = 0
        txtSerie.Text = ""
        txtCorrelative.Text = ""
        txtDate.Text = ""
        txtDetail.Text = ""
        cbxState.SelectedIndex = 0
        txtClient.Text = ""
        txtAddress.Text = ""
        txtDocument.Text = ""
    End Sub

    '''<summary>Método para llenar el datagridview con los detalles de la nota de pedido del comprobante de pago</summary>
    Public Sub GetAllDetails()
        Try
            dtgList.DataSource = Nothing
            dtgList.Columns.Clear()
            If order IsNot Nothing Then
                If order.Detail IsNot Nothing Then
                    dtgList.Columns.Add("Item", "ITEM")
                    dtgList.DataSource = order.OrderDetails

                    dtgList.Columns.Item("Item").Width = 40
                    dtgList.Columns.Item("Appliance").HeaderText = "DETALLE"
                    dtgList.Columns.Item("Appliance").Width = 282
                    dtgList.Columns.Item("Quantity").HeaderText = "CANTIDAD"
                    dtgList.Columns.Item("Quantity").Width = 80
                    dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                    dtgList.Columns.Item("Price").Width = 80
                    dtgList.Columns.Item("SubTotal").HeaderText = "SUB TOTAL"
                    dtgList.Columns.Item("SubTotal").Width = 80

                    dtgList.ColumnHeadersVisible = True
                    dtgList.Columns.Item("Id").Visible = False
                    dtgList.Columns.Item("Order").Visible = False
                    dtgList.Columns.Item("ApplianceDescription").Visible = False
                    dtgList.Columns.Item("Detail").Visible = False
                    lblTotal.Text = "TOTAL = S/. " & OrderMapper.Map(order).Total

                    Dim cont = 0
                    For Each row In dtgList.Rows
                        cont += 1
                        row.Cells("Item").Value = cont
                    Next
                Else
                    dtgList.DataSource = Nothing
                End If
            ElseIf technicalReport IsNot Nothing Then
                Dim list = New List(Of TechnicalReportModel)
                list.Add(technicalReport)
                dtgList.Columns.Add("Item", "ITEM")
                dtgList.DataSource = list

                dtgList.Columns.Item("Item").Width = 40
                dtgList.Columns.Item("Service").HeaderText = "DETALLE"
                dtgList.Columns.Item("Service").Width = 282
                dtgList.Columns.Item("Quantity").HeaderText = "CANTIDAD"
                dtgList.Columns.Item("Quantity").Width = 80
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Price").Width = 80
                dtgList.Columns.Item("SubTotal").HeaderText = "SUB TOTAL"
                dtgList.Columns.Item("SubTotal").Width = 80

                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Appliance").Visible = False
                dtgList.Columns.Item("Failure").Visible = False
                dtgList.Columns.Item("Diagnosis").Visible = False
                dtgList.Columns.Item("Observation").Visible = False
                dtgList.Columns.Item("Vouchers").Visible = False
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Client").Visible = False
                dtgList.Columns.Item("EntryDate").Visible = False
                dtgList.Columns.Item("DeliveryDate").Visible = False
                dtgList.Columns.Item("Employee").Visible = False

                dtgList.ColumnHeadersVisible = True
                lblTotal.Text = "TOTAL = S/. " & technicalReport.Price

                Dim cont = 0
                For Each row In dtgList.Rows
                    cont += 1
                    row.Cells("Item").Value = cont
                Next
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método habilitar controles para guardar comprobante de pago</summary>
    Public Sub EnableTxt(state As Boolean)
        cbxType.Enabled = state
        txtSerie.Enabled = state
        txtCorrelative.Enabled = state
        txtDate.Enabled = state
        txtDetail.Enabled = state
        txtClient.Enabled = False
        txtDocument.Enabled = False
        txtAddress.Enabled = False
    End Sub

    '''<summary>Método habilitar controles para guardar estado</summary>
    Public Sub EnableStateTxt(state As Boolean)
        lblState.Visible = state
        cbxState.Visible = state
    End Sub

    Public Sub SetDocumentNumber(document As String)
        Select Case document
            Case 8
                cbxState.Text = "BOLETA"
            Case 11
                cbxState.Text = "FACTURA"
        End Select
        If EntityModel.Id = 0 Then
            Dim number = controller.GetNumber(cbxType.Text)
            txtSerie.Text = number.Item("Serie")
            txtCorrelative.Text = number.Item("Correlative")
        End If
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos del Comprobante de Pago</summary>
    Private Sub FrmSaveVoucher_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()

        cbxType.DataSource = My.Settings.VoucherType
        cbxState.DataSource = My.Settings.VoucherState

        EnableTxt(False)
        EnableStateTxt(False)
        If EntityModel.Id = 0 Then
            rbtSale.Checked = True
        End If


        If EntityModel.Id Then
            btnPrint.Visible = True
            If EntityModel.Order IsNot Nothing Then
                txtOrderId.Text = EntityModel.Order.Id
                txtClient.Text = EntityModel.Order.Client.FullName
                txtAddress.Text = EntityModel.Order.Client.Address
                txtDocument.Text = EntityModel.Order.Client.DocumentNumber
                order = EntityModel.Order
            ElseIf EntityModel.TechnicalReport IsNot Nothing Then
                txtOrderId.Text = EntityModel.TechnicalReport.Id
                txtClient.Text = EntityModel.TechnicalReport.Client.FullName
                txtAddress.Text = EntityModel.TechnicalReport.Client.Address
                txtDocument.Text = EntityModel.TechnicalReport.Client.DocumentNumber
                technicalReport = EntityModel.TechnicalReport
            End If
            cbxType.Text = EntityModel.Type
            txtSerie.Text = EntityModel.Serie
            txtCorrelative.Text = EntityModel.Correlative
            txtDate.Value = EntityModel.Vdate
            txtDetail.Text = EntityModel.Detail
            cbxState.Text = EntityModel.State
            EnableTxt(True)
            EnableStateTxt(True)
            If EntityModel.VoucherType = "Order" Then
                rbtSale.Checked = True
            End If
            If EntityModel.VoucherType = "TechnicalReport" Then
                rbtService.Checked = True
            End If
            If detail Then
                EnableTxt(False)
                txtDocument.Enabled = False
                txtDetail.Enabled = False
                cbxState.Enabled = False
                btnSave.Visible = False
                btnCancel.Visible = False
            End If
        Else
            btnPrint.Visible = False
        End If
        'Bloquear cajas de texto y rellenar total
        lblTotal.Text = "TOTAL = S/. " & VoucherMapper.Map(EntityModel).Total

        'Redimencionar el datagridview
        dtgList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dtgList.DefaultCellStyle.WrapMode = DataGridViewTriState.True
        GetAllDetails()

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Comprobante de pago</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Employee = EmployeeMapper.Map(ActiveUser.Employee)
            EntityModel.Type = cbxType.Text
            EntityModel.Serie = txtSerie.Text
            EntityModel.Correlative = txtCorrelative.Text
            EntityModel.Vdate = txtDate.Value
            EntityModel.Detail = txtDetail.Text
            EntityModel.State = cbxState.Text
            EntityModel.Order = order
            EntityModel.TechnicalReport = technicalReport
            EntityModel.Total = VoucherMapper.Map(EntityModel).Total
            Dim entity = New VoucherModel
            If validateOrderTxt() Then
                entity = EntityModel
                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Comprobante de Pago", "Comprobante de Pago modificado correctamente")
                        ClearTxt()
                        FrmShowVoucher.getInstancia.GetAll("", FrmShowVoucher.getInstancia.txtBeginDate.Value, FrmShowVoucher.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Comprobante de Pago", "Comprobante de Pago registrado correctamente")
                        ClearTxt()
                        FrmShowVoucher.getInstancia.GetAll("", FrmShowVoucher.getInstancia.txtBeginDate.Value, FrmShowVoucher.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                End If
            End If
            FrmVoucherReport.getInstancia.lblTitle.Text = "IMPRIMIR COMPROBANTE DE PAGO"
            FrmVoucherReport.getInstancia.EntityModel = entity
            FrmVoucherReport.getInstancia.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Comprobante de Pago", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
        Dispose()
    End Sub

    '''<summary>Método para validar caja de texto para detlle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    Private Sub FrmSaveOrder_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtOrderId.Focus()
    End Sub

    '''<summary>Método para validar caja de texto para estado del comprobante de pago</summary>
    Private Sub cbxState_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxState.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 15
    End Sub

    '''<summary>Método para validar caja de texto para tipo de comprobante de pago</summary>
    Private Sub cbxType_KeyPress(sender As Object, e As KeyPressEventArgs) Handles cbxType.KeyPress
        FormDesing.DefaultName(e)
        sender.MaxLength = 10
    End Sub

    '''<summary>Método para validar caja de texto para el correlativo del numero de comprobante de pago</summary>
    Private Sub txtCorrelative_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCorrelative.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 8
    End Sub

    '''<summary>Método para validar caja de texto para serie del numero de comprobante de pago</summary>
    Private Sub txtSerie_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSerie.KeyPress
        FormDesing.OnlyNumbers(e)
        sender.MaxLength = 4
    End Sub

    '''<summary>Método para validar caja de texto para numero de nota de pedido</summary>
    Private Sub txtOrderId_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtOrderId.KeyPress
        FormDesing.OnlyNumbers(e)
    End Sub

    '''<summary>Método para cargar datos de nota de pedido para comprobante de pago</summary>
    Private Sub txtOrderId_KeyDown(sender As Object, e As KeyEventArgs) Handles txtOrderId.KeyDown
        If e.KeyCode = Keys.Enter Then
            If rbtSale.Checked Then
                order = controller.GetOrderById(sender.Text)
                If order IsNot Nothing Then
                    EnableTxt(True)
                    txtClient.Text = order.Client.FullName
                    txtAddress.Text = order.Client.Address
                    txtDocument.Text = order.Client.DocumentNumber
                    GetAllDetails()
                    SetDocumentNumber(order.Client.DocumentNumber)
                End If
            ElseIf rbtService.Checked Then
                technicalReport = controller.GetTechnicalReportById(sender.Text)
                If technicalReport IsNot Nothing Then
                    EnableTxt(True)
                    txtClient.Text = technicalReport.Client.FullName
                    txtAddress.Text = technicalReport.Client.Address
                    txtDocument.Text = technicalReport.Client.DocumentNumber
                    GetAllDetails()
                    SetDocumentNumber(technicalReport.Client.DocumentNumber)
                End If
            End If

        Else
            EnableTxt(False)
            txtClient.Text = ""
            txtAddress.Text = ""
            txtDocument.Text = ""
            dtgList.DataSource = Nothing
        End If
    End Sub

    Private Sub cbxType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxType.SelectedIndexChanged
        If order IsNot Nothing Then
            SetDocumentNumber(order.Client.DocumentNumber)
        ElseIf technicalReport IsNot Nothing Then
            SetDocumentNumber(technicalReport.Client.DocumentNumber)
        End If

    End Sub

    Private Sub rbtSale_CheckedChanged(sender As Object, e As EventArgs) Handles rbtSale.CheckedChanged
        If rbtSale.Checked And EntityModel.Id = 0 Then
            ClearTxt()
            EnableTxt(False)
            EntityModel = New VoucherModel
            GetAllDetails()
        End If
        EntityModel.VoucherType = "Order"
    End Sub

    Private Sub rbtService_CheckedChanged(sender As Object, e As EventArgs) Handles rbtService.CheckedChanged
        If rbtService.Checked And EntityModel.Id = 0 Then
            ClearTxt()
            EnableTxt(False)
            EntityModel = New VoucherModel
            GetAllDetails()
        End If
        EntityModel.VoucherType = "TechnicalReport"
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        FrmVoucherReport.getInstancia.lblTitle.Text = "IMPRIMIR COMPROBANTE DE PAGO"
        FrmVoucherReport.getInstancia.EntityModel = EntityModel
        FrmVoucherReport.getInstancia.ShowDialog()
    End Sub
End Class