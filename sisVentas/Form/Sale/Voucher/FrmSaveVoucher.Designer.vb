﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveVoucher
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.rbtService = New System.Windows.Forms.RadioButton()
        Me.rbtSale = New System.Windows.Forms.RadioButton()
        Me.txtDocument = New System.Windows.Forms.TextBox()
        Me.txtClient = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.cbxState = New System.Windows.Forms.ComboBox()
        Me.lblValidateState = New System.Windows.Forms.Label()
        Me.txtCorrelative = New System.Windows.Forms.TextBox()
        Me.lblValidateCorrelative = New System.Windows.Forms.Label()
        Me.lblDocument = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblState = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxType = New System.Windows.Forms.ComboBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblClient = New System.Windows.Forms.Label()
        Me.dtgList = New System.Windows.Forms.DataGridView()
        Me.txtOrderId = New System.Windows.Forms.TextBox()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.DateTimePicker()
        Me.lblValidateType = New System.Windows.Forms.Label()
        Me.lblValidateDate = New System.Windows.Forms.Label()
        Me.lblValidateDetail = New System.Windows.Forms.Label()
        Me.lblValidateOrder = New System.Windows.Forms.Label()
        Me.txtDetail = New System.Windows.Forms.TextBox()
        Me.lblOrder = New System.Windows.Forms.Label()
        Me.lblDetail = New System.Windows.Forms.Label()
        Me.lblValidateSerie = New System.Windows.Forms.Label()
        Me.lblNUmber = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlTitle.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(869, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "COMPROBANTE DE PAGO"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(900, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.btnPrint)
        Me.pnlBody.Controls.Add(Me.rbtService)
        Me.pnlBody.Controls.Add(Me.rbtSale)
        Me.pnlBody.Controls.Add(Me.txtDocument)
        Me.pnlBody.Controls.Add(Me.txtClient)
        Me.pnlBody.Controls.Add(Me.txtAddress)
        Me.pnlBody.Controls.Add(Me.cbxState)
        Me.pnlBody.Controls.Add(Me.lblValidateState)
        Me.pnlBody.Controls.Add(Me.txtCorrelative)
        Me.pnlBody.Controls.Add(Me.lblValidateCorrelative)
        Me.pnlBody.Controls.Add(Me.lblDocument)
        Me.pnlBody.Controls.Add(Me.lblAddress)
        Me.pnlBody.Controls.Add(Me.lblState)
        Me.pnlBody.Controls.Add(Me.Label1)
        Me.pnlBody.Controls.Add(Me.cbxType)
        Me.pnlBody.Controls.Add(Me.lblTotal)
        Me.pnlBody.Controls.Add(Me.lblClient)
        Me.pnlBody.Controls.Add(Me.dtgList)
        Me.pnlBody.Controls.Add(Me.txtOrderId)
        Me.pnlBody.Controls.Add(Me.txtSerie)
        Me.pnlBody.Controls.Add(Me.txtDate)
        Me.pnlBody.Controls.Add(Me.lblValidateType)
        Me.pnlBody.Controls.Add(Me.lblValidateDate)
        Me.pnlBody.Controls.Add(Me.lblValidateDetail)
        Me.pnlBody.Controls.Add(Me.lblValidateOrder)
        Me.pnlBody.Controls.Add(Me.txtDetail)
        Me.pnlBody.Controls.Add(Me.lblOrder)
        Me.pnlBody.Controls.Add(Me.lblDetail)
        Me.pnlBody.Controls.Add(Me.lblValidateSerie)
        Me.pnlBody.Controls.Add(Me.lblNUmber)
        Me.pnlBody.Controls.Add(Me.lblType)
        Me.pnlBody.Controls.Add(Me.lblDate)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(900, 409)
        Me.pnlBody.TabIndex = 46
        '
        'rbtService
        '
        Me.rbtService.AutoSize = True
        Me.rbtService.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtService.Location = New System.Drawing.Point(229, 7)
        Me.rbtService.Name = "rbtService"
        Me.rbtService.Size = New System.Drawing.Size(85, 22)
        Me.rbtService.TabIndex = 141
        Me.rbtService.TabStop = True
        Me.rbtService.Text = "Servicio"
        Me.rbtService.UseVisualStyleBackColor = True
        '
        'rbtSale
        '
        Me.rbtSale.AutoSize = True
        Me.rbtSale.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSale.Location = New System.Drawing.Point(153, 7)
        Me.rbtSale.Name = "rbtSale"
        Me.rbtSale.Size = New System.Drawing.Size(70, 22)
        Me.rbtSale.TabIndex = 140
        Me.rbtSale.TabStop = True
        Me.rbtSale.Text = "Venta"
        Me.rbtSale.UseVisualStyleBackColor = True
        '
        'txtDocument
        '
        Me.txtDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocument.Location = New System.Drawing.Point(736, 33)
        Me.txtDocument.Name = "txtDocument"
        Me.txtDocument.Size = New System.Drawing.Size(148, 24)
        Me.txtDocument.TabIndex = 139
        '
        'txtClient
        '
        Me.txtClient.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClient.Location = New System.Drawing.Point(414, 5)
        Me.txtClient.Name = "txtClient"
        Me.txtClient.Size = New System.Drawing.Size(300, 24)
        Me.txtClient.TabIndex = 138
        '
        'txtAddress
        '
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(414, 33)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(300, 24)
        Me.txtAddress.TabIndex = 137
        '
        'cbxState
        '
        Me.cbxState.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxState.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxState.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxState.FormattingEnabled = True
        Me.cbxState.Location = New System.Drawing.Point(392, 363)
        Me.cbxState.Name = "cbxState"
        Me.cbxState.Size = New System.Drawing.Size(164, 27)
        Me.cbxState.TabIndex = 132
        '
        'lblValidateState
        '
        Me.lblValidateState.AutoSize = True
        Me.lblValidateState.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateState.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateState.Location = New System.Drawing.Point(395, 388)
        Me.lblValidateState.Name = "lblValidateState"
        Me.lblValidateState.Size = New System.Drawing.Size(81, 15)
        Me.lblValidateState.TabIndex = 136
        Me.lblValidateState.Text = "Ingrese Estado"
        Me.lblValidateState.Visible = False
        '
        'txtCorrelative
        '
        Me.txtCorrelative.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCorrelative.Location = New System.Drawing.Point(143, 171)
        Me.txtCorrelative.Name = "txtCorrelative"
        Me.txtCorrelative.Size = New System.Drawing.Size(171, 24)
        Me.txtCorrelative.TabIndex = 129
        '
        'lblValidateCorrelative
        '
        Me.lblValidateCorrelative.AutoSize = True
        Me.lblValidateCorrelative.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateCorrelative.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateCorrelative.Location = New System.Drawing.Point(144, 193)
        Me.lblValidateCorrelative.Name = "lblValidateCorrelative"
        Me.lblValidateCorrelative.Size = New System.Drawing.Size(103, 15)
        Me.lblValidateCorrelative.TabIndex = 135
        Me.lblValidateCorrelative.Text = "Ingrese Correlativo"
        Me.lblValidateCorrelative.Visible = False
        '
        'lblDocument
        '
        Me.lblDocument.AutoSize = True
        Me.lblDocument.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(732, 7)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(130, 20)
        Me.lblDocument.TabIndex = 134
        Me.lblDocument.Text = "Número de Doc."
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(328, 33)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(80, 20)
        Me.lblAddress.TabIndex = 133
        Me.lblAddress.Text = "Dirección"
        '
        'lblState
        '
        Me.lblState.AutoSize = True
        Me.lblState.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(326, 366)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(60, 19)
        Me.lblState.TabIndex = 131
        Me.lblState.Text = "Estado:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(120, 171)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 23)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "-"
        '
        'cbxType
        '
        Me.cbxType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxType.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxType.FormattingEnabled = True
        Me.cbxType.Location = New System.Drawing.Point(14, 109)
        Me.cbxType.Name = "cbxType"
        Me.cbxType.Size = New System.Drawing.Size(300, 27)
        Me.cbxType.TabIndex = 128
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(562, 366)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(322, 24)
        Me.lblTotal.TabIndex = 127
        Me.lblTotal.Text = "TOTAL"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblClient
        '
        Me.lblClient.AutoSize = True
        Me.lblClient.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClient.Location = New System.Drawing.Point(328, 4)
        Me.lblClient.Name = "lblClient"
        Me.lblClient.Size = New System.Drawing.Size(61, 20)
        Me.lblClient.TabIndex = 120
        Me.lblClient.Text = "Cliente"
        '
        'dtgList
        '
        Me.dtgList.AllowUserToAddRows = False
        Me.dtgList.AllowUserToDeleteRows = False
        Me.dtgList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dtgList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dtgList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgList.ColumnHeadersHeight = 30
        Me.dtgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgList.EnableHeadersVisualStyles = False
        Me.dtgList.Location = New System.Drawing.Point(320, 63)
        Me.dtgList.Name = "dtgList"
        Me.dtgList.ReadOnly = True
        Me.dtgList.RowHeadersVisible = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.WhiteSmoke
        Me.dtgList.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dtgList.RowTemplate.Height = 24
        Me.dtgList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgList.Size = New System.Drawing.Size(564, 287)
        Me.dtgList.TabIndex = 0
        '
        'txtOrderId
        '
        Me.txtOrderId.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderId.Location = New System.Drawing.Point(14, 53)
        Me.txtOrderId.Name = "txtOrderId"
        Me.txtOrderId.Size = New System.Drawing.Size(300, 24)
        Me.txtOrderId.TabIndex = 4
        '
        'txtSerie
        '
        Me.txtSerie.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.Location = New System.Drawing.Point(14, 171)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(102, 24)
        Me.txtSerie.TabIndex = 3
        '
        'txtDate
        '
        Me.txtDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(14, 228)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(300, 26)
        Me.txtDate.TabIndex = 5
        '
        'lblValidateType
        '
        Me.lblValidateType.AutoSize = True
        Me.lblValidateType.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateType.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateType.Location = New System.Drawing.Point(14, 133)
        Me.lblValidateType.Name = "lblValidateType"
        Me.lblValidateType.Size = New System.Drawing.Size(167, 15)
        Me.lblValidateType.TabIndex = 80
        Me.lblValidateType.Text = "Ingrese Numero de Documento"
        Me.lblValidateType.Visible = False
        '
        'lblValidateDate
        '
        Me.lblValidateDate.AutoSize = True
        Me.lblValidateDate.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDate.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDate.Location = New System.Drawing.Point(14, 252)
        Me.lblValidateDate.Name = "lblValidateDate"
        Me.lblValidateDate.Size = New System.Drawing.Size(78, 15)
        Me.lblValidateDate.TabIndex = 79
        Me.lblValidateDate.Text = "Ingrese Fecha"
        Me.lblValidateDate.Visible = False
        '
        'lblValidateDetail
        '
        Me.lblValidateDetail.AutoSize = True
        Me.lblValidateDetail.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDetail.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDetail.Location = New System.Drawing.Point(15, 315)
        Me.lblValidateDetail.Name = "lblValidateDetail"
        Me.lblValidateDetail.Size = New System.Drawing.Size(83, 15)
        Me.lblValidateDetail.TabIndex = 78
        Me.lblValidateDetail.Text = "Ingrese Detalle"
        Me.lblValidateDetail.Visible = False
        '
        'lblValidateOrder
        '
        Me.lblValidateOrder.AutoSize = True
        Me.lblValidateOrder.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateOrder.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateOrder.Location = New System.Drawing.Point(14, 75)
        Me.lblValidateOrder.Name = "lblValidateOrder"
        Me.lblValidateOrder.Size = New System.Drawing.Size(136, 15)
        Me.lblValidateOrder.TabIndex = 77
        Me.lblValidateOrder.Text = "Ingrese Numero de Order"
        Me.lblValidateOrder.Visible = False
        '
        'txtDetail
        '
        Me.txtDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetail.Location = New System.Drawing.Point(14, 290)
        Me.txtDetail.Name = "txtDetail"
        Me.txtDetail.Size = New System.Drawing.Size(300, 24)
        Me.txtDetail.TabIndex = 6
        '
        'lblOrder
        '
        Me.lblOrder.AutoSize = True
        Me.lblOrder.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrder.Location = New System.Drawing.Point(14, 31)
        Me.lblOrder.Name = "lblOrder"
        Me.lblOrder.Size = New System.Drawing.Size(51, 19)
        Me.lblOrder.TabIndex = 75
        Me.lblOrder.Text = "Ticket:"
        '
        'lblDetail
        '
        Me.lblDetail.AutoSize = True
        Me.lblDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(14, 268)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(62, 19)
        Me.lblDetail.TabIndex = 76
        Me.lblDetail.Text = "Detalle:"
        '
        'lblValidateSerie
        '
        Me.lblValidateSerie.AutoSize = True
        Me.lblValidateSerie.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateSerie.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateSerie.Location = New System.Drawing.Point(14, 193)
        Me.lblValidateSerie.Name = "lblValidateSerie"
        Me.lblValidateSerie.Size = New System.Drawing.Size(71, 15)
        Me.lblValidateSerie.TabIndex = 74
        Me.lblValidateSerie.Text = "Ingrese Serie"
        Me.lblValidateSerie.Visible = False
        '
        'lblNUmber
        '
        Me.lblNUmber.AutoSize = True
        Me.lblNUmber.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNUmber.Location = New System.Drawing.Point(14, 149)
        Me.lblNUmber.Name = "lblNUmber"
        Me.lblNUmber.Size = New System.Drawing.Size(166, 19)
        Me.lblNUmber.TabIndex = 68
        Me.lblNUmber.Text = "Número Comprobante"
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblType.Location = New System.Drawing.Point(14, 89)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(166, 19)
        Me.lblType.TabIndex = 70
        Me.lblType.Text = "Tipo de Comprobante:"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(14, 207)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(53, 19)
        Me.lblDate.TabIndex = 69
        Me.lblDate.Text = "Fecha"
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.White
        Me.btnPrint.Image = Global.sisVentas.Desktop.My.Resources.Resources.print
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(71, 3)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(59, 45)
        Me.btnPrint.TabIndex = 142
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = False
        Me.btnPrint.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(14, 345)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 45)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(174, 345)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 45)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(878, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'FrmSaveVoucher
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(900, 440)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveVoucher"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents lblValidateOrder As Label
    Friend WithEvents txtOrderId As TextBox
    Friend WithEvents lblOrder As Label
    Friend WithEvents lblValidateSerie As Label
    Friend WithEvents txtSerie As TextBox
    Friend WithEvents lblNUmber As Label
    Friend WithEvents lblType As Label
    Friend WithEvents lblDate As Label
    Friend WithEvents lblValidateType As Label
    Friend WithEvents lblValidateDate As Label
    Friend WithEvents txtDate As DateTimePicker
    Friend WithEvents lblValidateDetail As Label
    Friend WithEvents txtDetail As TextBox
    Friend WithEvents lblDetail As Label
    Friend WithEvents dtgList As DataGridView
    Friend WithEvents lblClient As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents toolTip As ToolTip
    Friend WithEvents lblDocument As Label
    Friend WithEvents lblAddress As Label
    Friend WithEvents cbxState As ComboBox
    Friend WithEvents lblState As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCorrelative As TextBox
    Friend WithEvents cbxType As ComboBox
    Friend WithEvents lblValidateCorrelative As Label
    Friend WithEvents lblValidateState As Label
    Friend WithEvents txtDocument As TextBox
    Friend WithEvents txtClient As TextBox
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents rbtService As RadioButton
    Friend WithEvents rbtSale As RadioButton
    Friend WithEvents btnPrint As Button
End Class
