﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Nota de Pedido: 
''' Formulario que permite editar o modificar una Nota de Pedido
''' </summary>
Public Class FrmSaveOrder

    'controlador del modelo de Nota de Pedido
    Private ReadOnly controller As New OrderController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveOrder = Nothing

    'Modelo de Nota de Pedido a ser agregada o modificada
    Public Property EntityModel As OrderModel

    'lista de modelo de detalles de nota de pedido 
    Private dt As New List(Of OrderDetailModel)

    'Detalle de nota de pedido que se le agrega, edita o elimina en la nota de pedido
    Private orderDetail As OrderDetailModel

    'Descripcion del equipo de cómputo a agregar en el detalle de nota de pedido
    Private appliance As ApplianceModel

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Edit As Boolean = False

    'Permite mostrar los controles para editar o guardar los elementos
    Public Property detail As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveOrder
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveOrder
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidateOrder()
        lblValidateDocument.Visible = False
        lblValidateName.Visible = False
        lblValidateAddress.Visible = False
        lblValidateDate.Visible = False
        lblValidateDetail.Visible = False
    End Sub

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidateOrderDetail()
        lblValidateAppliance.Visible = False
        lblValidatePrice.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateOrderTxt() As Boolean
        HideLblValidateOrder()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Client") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "Employee") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Nota de Pedido", "Datos del empleado incorrectos inicie sesion nuevamente")
                End If
                If (item.MemberNames.First = "Ndate") Then
                    lblValidateDate.Text = item.ErrorMessage
                    lblValidateDate.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
                If (item.MemberNames.First = "OrderDetails") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Nota de Pedido", "La nota de pedido no cuenta con equipos debe de agregarlos")
                    txtDocument.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateOrderDetailTxt() As Boolean
        HideLblValidateOrderDetail()
        Dim validate = New DataValidation(orderDetail)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Appliance") Then
                    lblValidateAppliance.Text = item.ErrorMessage
                    lblValidateAppliance.Visible = True
                    txtAppliance.Focus()
                End If
                If (item.MemberNames.First = "Quantity") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "VALIDAR DATOS", "Elemento no agregado Intente de nuevo")
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtPrice.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearOrderTxt()
        EntityModel = Nothing
        txtDocument.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtDate.Text = ""
        txtDetail.Text = ""
    End Sub

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearOrderDetailTxt()
        orderDetail = New OrderDetailModel
        txtAppliance.Text = ""
        txtPrice.Text = ""
    End Sub

    '''<summary>Método rellenar los datos de la descripcion del equipo de cómputo</summary>
    '''<param name="appliance">Equipo de cómputo a ser agregado</param>
    Public Sub SetAppliance(appliance As ApplianceModel)
        Me.appliance = appliance
        txtAppliance.Text = appliance.Serie
        txtPrice.Text = appliance.ApplianceDescription.Price
    End Sub

    '''<summary>Método para mostrar u ocultar controles dependiendo si se trata de guardado o no</summary>
    Public Sub SaveControls()
        If Edit Then
            btnDelete.Visible = False
            btnAdd.Image = My.Resources.save
            toolTip.SetToolTip(btnAdd, "Guardar")
            btnEdit.Image = My.Resources.close
            toolTip.SetToolTip(btnEdit, "Cancelar")
        Else
            btnDelete.Visible = True
            btnAdd.Image = My.Resources.add
            toolTip.SetToolTip(btnAdd, "Agregar Equipo")
            btnEdit.Image = My.Resources.edit
            toolTip.SetToolTip(btnEdit, "Editar Equipo")
        End If

    End Sub

    '''<summary>Método para llenar el datagridview con los detalles de la nota de pedido de la nota de pedido</summary>
    Public Sub GetAllDetails()
        Try
            dtgList.DataSource = Nothing
            dtgList.Columns.Clear()
            If EntityModel.OrderDetails IsNot Nothing Then

                dtgList.Columns.Add("Item", "ITEM")
                Dim cont = dt.Max(Function(item) item.Id)
                For Each item In EntityModel.OrderDetails
                    If item.Id = 0 Then
                        cont += 1
                        item.Id = cont
                    End If
                Next

                dtgList.DataSource = EntityModel.OrderDetails

                dtgList.Columns.Item("Item").Width = 40
                dtgList.Columns.Item("Appliance").HeaderText = "DETALLE"
                dtgList.Columns.Item("Appliance").Width = 282
                dtgList.Columns.Item("Quantity").HeaderText = "CANTIDAD"
                dtgList.Columns.Item("Quantity").Width = 80
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("Price").Width = 80
                dtgList.Columns.Item("SubTotal").HeaderText = "SUB TOTAL"
                dtgList.Columns.Item("SubTotal").Width = 80

                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Order").Visible = False
                dtgList.Columns.Item("ApplianceDescription").Visible = False
                dtgList.Columns.Item("Detail").Visible = False
                lblTotal.Text = "TOTAL = S/. " & OrderMapper.Map(EntityModel).Total

                cont = 0
                For Each row In dtgList.Rows
                    cont += 1
                    row.Cells("Item").Value = cont
                Next

            Else
                dtgList.DataSource = EntityModel.OrderDetails
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método habilitar controles para agregar detalles</summary>
    Public Sub EnableDetailTxt(state As Boolean)
        txtAppliance.Enabled = state
        txtPrice.Enabled = state
        btnScan.Visible = state
        btnAdd.Visible = state
        btnEdit.Visible = state
        btnDelete.Visible = state
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la Nota de Pedido</summary>
    Private Sub FrmSaveOrder_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()

        'Bloquear cajas de texto y rellenar total
        txtName.Enabled = False
        txtAddress.Enabled = False
        lblTotal.Text = "TOTAL = S/. " & OrderMapper.Map(EntityModel).Total

        'Agregar tooltips a los botones
        toolTip.SetToolTip(btnAdd, "Agregar Equipo")
        toolTip.SetToolTip(btnEdit, "Editar Equipo")
        toolTip.SetToolTip(btnDelete, "Eliminar Equipo")

        'Inicializar variables para la entidad
        dt = New List(Of OrderDetailModel)
        orderDetail = New OrderDetailModel

        'Redimencionar el datagridview
        dtgList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dtgList.DefaultCellStyle.WrapMode = DataGridViewTriState.True

        EnableDetailTxt(False)

        If EntityModel.Id Then
            btnPrint.Visible = True
            txtDocument.Text = EntityModel.Client.DocumentNumber
            txtName.Text = EntityModel.Client.FullName
            txtAddress.Text = EntityModel.Client.Address
            txtDate.Value = EntityModel.Ndate
            txtDetail.Text = EntityModel.Detail
            dt = EntityModel.OrderDetails
            If detail Then
                EnableDetailTxt(False)
                txtDocument.Enabled = False
                txtDetail.Enabled = False
                btnSave.Visible = False
                btnCancel.Visible = False
            Else
                EnableDetailTxt(True)
            End If
        Else
            btnPrint.Visible = False
        End If

        GetAllDetails()

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Nota de Pedido</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Employee = EmployeeMapper.Map(ActiveUser.Employee)
            EntityModel.Ndate = txtDate.Value
            EntityModel.Detail = txtDetail.Text
            If txtName.Text = "" Then
                EntityModel.Client = Nothing
            End If
            Dim entity = New OrderModel
            If validateOrderTxt() Then
                entity = EntityModel
                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Nota de Pedido", "Nota de Pedido modificada correctamente")
                        ClearOrderTxt()
                        ClearOrderDetailTxt()
                        FrmShowOrder.getInstancia.GetAll("", FrmShowOrder.getInstancia.txtBeginDate.Value, FrmShowOrder.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                Else
                    entity.Id = controller.Add(EntityModel)
                    If entity.Id Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Nota de Pedido", "Nota de Pedido registrada correctamente")
                        ClearOrderTxt()
                        ClearOrderDetailTxt()
                        FrmShowOrder.getInstancia.GetAll("", FrmShowOrder.getInstancia.txtBeginDate.Value, FrmShowOrder.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                End If
            End If
            FrmTicketReport.getInstancia.lblTitle.Text = "IMPRIMIR NUMERO DE NOTA DE PEDIDO"
            FrmTicketReport.getInstancia.EntityModel = entity
            FrmTicketReport.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Nota de Pedido", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearOrderTxt()
        ClearOrderDetailTxt()
        HideLblValidateOrder()
        HideLblValidateOrderDetail()
        Dispose()
    End Sub

    '''<summary>Método para validar caja de texto para numero de documento</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
        If rbtDni.Checked Then
            sender.MaxLength = 8
        End If
        If rbtRuc.Checked Then
            sender.MaxLength = 11
        End If
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDetail.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para agregar equipo a una nota de detalle</summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If appliance IsNot Nothing Then
            orderDetail.Appliance = appliance
            orderDetail.Appliance.Order = EntityModel
            orderDetail.ApplianceDescription = appliance.ApplianceDescription
        End If
        orderDetail.Quantity = 1
        orderDetail.Price = Val(txtPrice.Text)
        'orderDetail.SubTotal = OrderDetailMapper.Map(orderDetail).SubTotal

        If ValidateOrderDetailTxt() Then
            If orderDetail.Id Then
                dt.RemoveAll(Function(detail) detail.Id = orderDetail.Id)
                dt = controller.AddAppliance(EntityModel.OrderDetails, orderDetail)
                EntityModel.OrderDetails = dt
                txtAppliance.Enabled = True
            Else
                dt = controller.AddAppliance(EntityModel.OrderDetails, orderDetail)
                EntityModel.OrderDetails = dt
            End If
            Edit = False
            Cancel = False
            SaveControls()
            ClearOrderDetailTxt()
            GetAllDetails()
        End If
    End Sub

    '''<summary>Método para editar un equipo de la nota de pedido</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If Cancel Then
            Edit = False
            SaveControls()
            ClearOrderDetailTxt()
            GetAllDetails()
            Cancel = False
        Else
            If dtgList.CurrentRow IsNot Nothing Then
                orderDetail = New OrderDetailModel With {
                                    .Id = dtgList.CurrentRow.Cells.Item("Id").Value,
                                    .ApplianceDescription = dtgList.CurrentRow.Cells.Item("ApplianceDescription").Value,
                                    .Appliance = dtgList.CurrentRow.Cells.Item("Appliance").Value,
                                    .Price = dtgList.CurrentRow.Cells.Item("Price").Value,
                                    .Quantity = dtgList.CurrentRow.Cells.Item("Quantity").Value
                }
                Edit = True
                txtAppliance.Enabled = False
                SetAppliance(orderDetail.Appliance)
                txtPrice.Text = orderDetail.Price
                SaveControls()
                Cancel = True
            End If
        End If
    End Sub

    '''<summary>Método para eliminar un equipo de una nota de pedido</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Item", "¿Esta seguro de eliminar el/los items?", AlertResource.QUESTION)
        Dim result = frm.ShowDialog

        If result = DialogResult.OK Then
            dt.RemoveAll(Function(detail) detail.Id = dtgList.CurrentRow.Cells.Item("Id").Value)
            EntityModel.OrderDetails = dt
            GetAllDetails()
        End If
    End Sub

    '''<summary>Método para validar precio</summary>
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        FormDesing.OnlyDecimal(e, sender)
        sender.MaxLength = 18
    End Sub

    '''<summary>Método para validar cantidad</summary>
    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs)
        FormDesing.OnlyNumbers(e)
    End Sub

    '''<summary>Método para validar cliente</summary>
    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        If rbtDni.Checked Then
            If sender.Text.Length = 8 Then
                EntityModel.Client = controller.GetClientByDocument(sender.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    EnableDetailTxt(True)
                    txtAppliance.Focus()
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Show()
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                EnableDetailTxt(False)
            End If
        End If
        If rbtRuc.Checked Then
            If sender.Text.Length = 11 Then
                EntityModel.Client = controller.GetClientByDocument(sender.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    EnableDetailTxt(True)
                    txtAppliance.Focus()
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Checked = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Show()
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                EnableDetailTxt(False)
            End If
        End If
    End Sub

    '''<summary>Método para verificar si es dni</summary>
    Private Sub rbtDni_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDni.CheckedChanged
        If rbtDni.Checked Then
            lblDocument.Text = "DNI:"
            txtDocument.MaxLength = 8
        End If
        txtDocument.Focus()
    End Sub

    '''<summary>Método para verificar si es ruc</summary>
    Private Sub rbtRuc_CheckedChanged(sender As Object, e As EventArgs) Handles rbtRuc.CheckedChanged
        If rbtRuc.Checked Then
            lblDocument.Text = "RUC:"
            txtDocument.MaxLength = 11
        End If
        txtDocument.Focus()
    End Sub

    Private Sub FrmSaveOrder_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtDocument.Focus()
    End Sub

    '''<summary>Método para abrir el escaneador de series</summary>
    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click
        FrmScan.getInstancia.Form = Me.Name
        FrmScan.getInstancia.ShowDialog()
    End Sub
    '''<summary>Método para validar serie</summary>
    Private Sub txtAppliance_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAppliance.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para buscar la serie escrita</summary>
    Private Sub txtAppliance_TextChanged(sender As Object, e As EventArgs) Handles txtAppliance.TextChanged
        If Not Edit Then
            appliance = controller.GetBySerieMemory(sender.Text)
        End If

        If appliance IsNot Nothing Then
            txtPrice.Text = appliance.ApplianceDescription.Price
            txtPrice.Focus()
        Else
            txtPrice.Text = ""
        End If
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        FrmTicketReport.getInstancia.lblTitle.Text = "IMPRIMIR NUMERO DE NOTA DE PEDIDO"
        FrmTicketReport.getInstancia.EntityModel = EntityModel
        FrmTicketReport.getInstancia.ShowDialog()
    End Sub
End Class