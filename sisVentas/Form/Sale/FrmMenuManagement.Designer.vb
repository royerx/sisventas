﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMenuSale
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.pnlQuoted = New System.Windows.Forms.Panel()
        Me.imgQuoted = New System.Windows.Forms.PictureBox()
        Me.pnlHeadQuoted = New System.Windows.Forms.Panel()
        Me.lblHeaderQuoted = New System.Windows.Forms.Label()
        Me.pnlOrder = New System.Windows.Forms.Panel()
        Me.imgOrder = New System.Windows.Forms.PictureBox()
        Me.pnlHeadOrder = New System.Windows.Forms.Panel()
        Me.lblHeaderOrder = New System.Windows.Forms.Label()
        Me.pnlBody = New System.Windows.Forms.FlowLayoutPanel()
        Me.pnlVoucher = New System.Windows.Forms.Panel()
        Me.imgVoucher = New System.Windows.Forms.PictureBox()
        Me.pnlHeadVoucher = New System.Windows.Forms.Panel()
        Me.lblHeaderVoucher = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlQuoted.SuspendLayout()
        CType(Me.imgQuoted, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeadQuoted.SuspendLayout()
        Me.pnlOrder.SuspendLayout()
        CType(Me.imgOrder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeadOrder.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        Me.pnlVoucher.SuspendLayout()
        CType(Me.imgVoucher, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlHeadVoucher.SuspendLayout()
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlQuoted
        '
        Me.pnlQuoted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlQuoted.Controls.Add(Me.imgQuoted)
        Me.pnlQuoted.Controls.Add(Me.pnlHeadQuoted)
        Me.pnlQuoted.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlQuoted.Location = New System.Drawing.Point(23, 23)
        Me.pnlQuoted.Name = "pnlQuoted"
        Me.pnlQuoted.Size = New System.Drawing.Size(200, 250)
        Me.pnlQuoted.TabIndex = 1
        '
        'imgQuoted
        '
        Me.imgQuoted.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgQuoted.Image = Global.sisVentas.Desktop.My.Resources.Resources.quotation
        Me.imgQuoted.Location = New System.Drawing.Point(4, 55)
        Me.imgQuoted.Name = "imgQuoted"
        Me.imgQuoted.Size = New System.Drawing.Size(190, 190)
        Me.imgQuoted.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgQuoted.TabIndex = 1
        Me.imgQuoted.TabStop = False
        '
        'pnlHeadQuoted
        '
        Me.pnlHeadQuoted.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadQuoted.Controls.Add(Me.lblHeaderQuoted)
        Me.pnlHeadQuoted.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadQuoted.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadQuoted.Name = "pnlHeadQuoted"
        Me.pnlHeadQuoted.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadQuoted.TabIndex = 0
        '
        'lblHeaderQuoted
        '
        Me.lblHeaderQuoted.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderQuoted.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderQuoted.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderQuoted.Name = "lblHeaderQuoted"
        Me.lblHeaderQuoted.Size = New System.Drawing.Size(190, 22)
        Me.lblHeaderQuoted.TabIndex = 0
        Me.lblHeaderQuoted.Text = "Cotización"
        Me.lblHeaderQuoted.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlOrder
        '
        Me.pnlOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOrder.Controls.Add(Me.imgOrder)
        Me.pnlOrder.Controls.Add(Me.pnlHeadOrder)
        Me.pnlOrder.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlOrder.Location = New System.Drawing.Point(229, 23)
        Me.pnlOrder.Name = "pnlOrder"
        Me.pnlOrder.Size = New System.Drawing.Size(200, 250)
        Me.pnlOrder.TabIndex = 2
        '
        'imgOrder
        '
        Me.imgOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgOrder.Image = Global.sisVentas.Desktop.My.Resources.Resources.supermarket
        Me.imgOrder.Location = New System.Drawing.Point(4, 55)
        Me.imgOrder.Name = "imgOrder"
        Me.imgOrder.Size = New System.Drawing.Size(190, 190)
        Me.imgOrder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgOrder.TabIndex = 1
        Me.imgOrder.TabStop = False
        '
        'pnlHeadOrder
        '
        Me.pnlHeadOrder.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadOrder.Controls.Add(Me.lblHeaderOrder)
        Me.pnlHeadOrder.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadOrder.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadOrder.Name = "pnlHeadOrder"
        Me.pnlHeadOrder.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadOrder.TabIndex = 0
        '
        'lblHeaderOrder
        '
        Me.lblHeaderOrder.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderOrder.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderOrder.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderOrder.Name = "lblHeaderOrder"
        Me.lblHeaderOrder.Size = New System.Drawing.Size(190, 20)
        Me.lblHeaderOrder.TabIndex = 0
        Me.lblHeaderOrder.Text = "Nota de Pedido"
        Me.lblHeaderOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlBody
        '
        Me.pnlBody.Controls.Add(Me.pnlQuoted)
        Me.pnlBody.Controls.Add(Me.pnlOrder)
        Me.pnlBody.Controls.Add(Me.pnlVoucher)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 25)
        Me.pnlBody.Margin = New System.Windows.Forms.Padding(20)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Padding = New System.Windows.Forms.Padding(20)
        Me.pnlBody.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.pnlBody.Size = New System.Drawing.Size(1050, 575)
        Me.pnlBody.TabIndex = 7
        '
        'pnlVoucher
        '
        Me.pnlVoucher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlVoucher.Controls.Add(Me.imgVoucher)
        Me.pnlVoucher.Controls.Add(Me.pnlHeadVoucher)
        Me.pnlVoucher.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlVoucher.Location = New System.Drawing.Point(435, 23)
        Me.pnlVoucher.Name = "pnlVoucher"
        Me.pnlVoucher.Size = New System.Drawing.Size(200, 250)
        Me.pnlVoucher.TabIndex = 3
        '
        'imgVoucher
        '
        Me.imgVoucher.Cursor = System.Windows.Forms.Cursors.Hand
        Me.imgVoucher.Image = Global.sisVentas.Desktop.My.Resources.Resources.money
        Me.imgVoucher.Location = New System.Drawing.Point(4, 55)
        Me.imgVoucher.Name = "imgVoucher"
        Me.imgVoucher.Size = New System.Drawing.Size(190, 190)
        Me.imgVoucher.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgVoucher.TabIndex = 1
        Me.imgVoucher.TabStop = False
        '
        'pnlHeadVoucher
        '
        Me.pnlHeadVoucher.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.pnlHeadVoucher.Controls.Add(Me.lblHeaderVoucher)
        Me.pnlHeadVoucher.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeadVoucher.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeadVoucher.Name = "pnlHeadVoucher"
        Me.pnlHeadVoucher.Size = New System.Drawing.Size(198, 50)
        Me.pnlHeadVoucher.TabIndex = 0
        '
        'lblHeaderVoucher
        '
        Me.lblHeaderVoucher.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHeaderVoucher.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblHeaderVoucher.Location = New System.Drawing.Point(4, 14)
        Me.lblHeaderVoucher.Name = "lblHeaderVoucher"
        Me.lblHeaderVoucher.Size = New System.Drawing.Size(190, 20)
        Me.lblHeaderVoucher.TabIndex = 0
        Me.lblHeaderVoucher.Text = "Comprobante"
        Me.lblHeaderVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.SystemColors.Control
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(1050, 25)
        Me.pnlTitle.TabIndex = 22
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.Control
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.icon_cerrar2
        Me.btnClose.Location = New System.Drawing.Point(12, 5)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'FrmMenuSale
        '
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1050, 600)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmMenuSale"
        Me.Text = "frmEquipo"
        Me.pnlQuoted.ResumeLayout(False)
        CType(Me.imgQuoted, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeadQuoted.ResumeLayout(False)
        Me.pnlOrder.ResumeLayout(False)
        CType(Me.imgOrder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeadOrder.ResumeLayout(False)
        Me.pnlBody.ResumeLayout(False)
        Me.pnlVoucher.ResumeLayout(False)
        CType(Me.imgVoucher, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlHeadVoucher.ResumeLayout(False)
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlQuoted As Panel
    Friend WithEvents pnlHeadQuoted As Panel
    Friend WithEvents imgQuoted As PictureBox
    Friend WithEvents lblHeaderQuoted As Label
    Friend WithEvents pnlOrder As Panel
    Friend WithEvents imgOrder As PictureBox
    Friend WithEvents pnlHeadOrder As Panel
    Friend WithEvents lblHeaderOrder As Label
    Friend WithEvents pnlBody As FlowLayoutPanel
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlVoucher As Panel
    Friend WithEvents imgVoucher As PictureBox
    Friend WithEvents pnlHeadVoucher As Panel
    Friend WithEvents lblHeaderVoucher As Label
End Class
