﻿Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Menu de Ventas: 
''' Formulario que muestra el menu del modulo de Ventas
''' </summary>
Public Class FrmMenuSale

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmMenuSale = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmMenuSale
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmMenuSale
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método para mostrar el formulario de Cotización</summary>
    Private Sub imgQuoted_Click(sender As Object, e As EventArgs) Handles imgQuoted.Click
        frmPrincipal.lblTitle.Text = "COTIZACIÓN"
        Dim entityModel = New QuotedModel With {
            .Employee = EmployeeMapper.Map(ActiveUser.Employee),
            .Qdate = Now
        }
        FrmShowQuoted.getInstancia.EntityModel = entityModel
        frmPrincipal.FormPanel(FrmShowQuoted.getInstancia)
    End Sub

    '''<summary>Método para validar permisos</summary>
    Private Sub FrmMenuManagement_Load(sender As Object, e As EventArgs) Handles Me.Load
        pnlQuoted.Visible = Auth.Can(Values.QuotedShow)
        pnlOrder.Visible = Auth.Can(Values.OrderList)
        pnlVoucher.Visible = Auth.Can(Values.VoucherList)
    End Sub

    '''<summary>Método para mostrar formulario de mantenimiento de comprobantes de pago</summary>
    Private Sub imgVoucher_Click(sender As Object, e As EventArgs) Handles imgVoucher.Click
        frmPrincipal.lblTitle.Text = "COMPROBANTES DE PAGO"
        frmPrincipal.FormPanel(FrmShowVoucher.getInstancia)
    End Sub

    '''<summary>Método para mostrar formulario de mantenimiento de Notas de Pedido</summary>
    Private Sub imgOrder_Click(sender As Object, e As EventArgs) Handles imgOrder.Click
        frmPrincipal.lblTitle.Text = "NOTA DE PEDIDO"
        frmPrincipal.FormPanel(FrmShowOrder.getInstancia)
    End Sub
End Class