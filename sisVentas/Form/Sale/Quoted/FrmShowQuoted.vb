﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Realizar Cotizacion: 
''' Formulario que muestra las opciones de realizacion de cotización
''' </summary>
Public Class FrmShowQuoted

    'lista de modelo de detalles de nota de pedido 
    Private dt As New List(Of OrderDetailModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowQuoted = Nothing

    'Modelo de nota de pedido a ser utilizada
    Public Property EntityModel As QuotedModel

    'Detalle de nota de pedido que se le agrega, edita o elimina en la cotizacion
    Private orderDetail As OrderDetailModel

    'Descripcion del equipo de cómputo a agregar en el detalle de nota de pedido
    Private appliance As ApplianceDescriptionModel

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Save As Boolean = False

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowQuoted
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowQuoted
        End If
        Return _formulario
    End Function

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function ValidateTxt() As Boolean
        lblValidateAppliance.Visible = False
        lblValidateQuantity.Visible = False
        lblValidatePrice.Visible = False
        Dim validate = New DataValidation(orderDetail)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "ApplianceDescription") Then
                    lblValidateAppliance.Text = item.ErrorMessage
                    lblValidateAppliance.Visible = True
                    txtAppliance.Focus()
                End If
                If (item.MemberNames.First = "Quantity") Then
                    lblValidateQuantity.Text = item.ErrorMessage
                    lblValidateQuantity.Visible = True
                    txtQuantity.Focus()
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtQuantity.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function


    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        orderDetail = New OrderDetailModel
        txtAppliance.Text = ""
        txtPrice.Text = ""
        txtQuantity.Text = ""
    End Sub

    '''<summary>Método rellenar los datos de la descripcion del equipo de cómputo</summary>
    '''<param name="appliance">Equipo de cómputo a ser agregado</param>
    Public Sub SetAppliance(appliance As ApplianceDescriptionModel)
        Me.appliance = appliance
        txtAppliance.Text = appliance.Category.Name & " " & appliance.Brand.Name & " " & appliance.ApplianceName & " " & appliance.Model
        txtQuantity.Text = 1
        txtPrice.Text = appliance.Price
    End Sub

    '''<summary>Método para mostrar u ocultar controles dependiendo si se trata de guardado o no</summary>
    Public Sub SaveControls()
        If Save Then
            btnDelete.Visible = False
            btnAdd.Image = My.Resources.save
            toolTip.SetToolTip(btnAdd, "Guardar")
            btnEdit.Image = My.Resources.close
            toolTip.SetToolTip(btnEdit, "Cancelar")
        Else
            btnDelete.Visible = True
            btnAdd.Image = My.Resources.add
            toolTip.SetToolTip(btnAdd, "Agregar Equipo")
            btnEdit.Image = My.Resources.edit
            toolTip.SetToolTip(btnEdit, "Editar Equipo")
        End If

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método que inicializa las variables cuando carga el formulario</summary>
    Private Sub FrmShowQuoted_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtAppliance, "Buscar...")

        'Bloquear cajas de texto y rellenar total
        txtAppliance.Enabled = False
        lblTotal.Text = "TOTAL = S/. " & QuotedMapper.Map(EntityModel).Total

        'Agregar tooltips a los botones
        toolTip.SetToolTip(btnAdd, "Agregar Equipo")
        toolTip.SetToolTip(btnEdit, "Editar Equipo")
        toolTip.SetToolTip(btnDelete, "Eliminar Equipo")

        'Inicializar variables para la entidad
        dt = New List(Of OrderDetailModel)
        orderDetail = New OrderDetailModel

        'Redimencionar el datagridview
        dtgList.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dtgList.DefaultCellStyle.WrapMode = DataGridViewTriState.True

        GetAll()
    End Sub

    '''<summary>Método para llenar el datagridview con los detalles de la nota de pedido de la cotizacion</summary>
    Public Sub GetAll()
        Try
            dtgList.DataSource = Nothing
            dtgList.Columns.Item("Eliminar").Visible = False
            dtgList.Columns.Item("Eliminar").Width = 80
            If dtgList.Columns("Item") IsNot Nothing Then
                dtgList.Columns.Remove("Item")
            End If

            If EntityModel.OrderDetails IsNot Nothing Then
                dtgList.Columns.Add("Item", "ITEM")

                Dim cont = 0
                For Each item In EntityModel.OrderDetails
                    cont += 1
                    item.Id = cont
                Next

                dtgList.DataSource = EntityModel.OrderDetails
                If dtgList.Rows.Count > 0 Then
                    btnPrint.Visible = True
                Else
                    btnPrint.Visible = False
                End If
                dtgList.Columns.Item("Item").Width = 40
                dtgList.Columns.Item("ApplianceDescription").HeaderText = "DETALLE"
                dtgList.Columns.Item("ApplianceDescription").Width = 650
                dtgList.Columns.Item("Quantity").HeaderText = "CANTIDAD"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("SubTotal").HeaderText = "SUB TOTAL"

                cont = 0
                For Each row In dtgList.Rows
                    cont += 1
                    row.Cells("Item").Value = cont
                Next

                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Id").Visible = False
                dtgList.Columns.Item("Appliance").Visible = False
                dtgList.Columns.Item("Order").Visible = False
                dtgList.Columns.Item("Detail").Visible = False
                lblTotal.Text = "TOTAL = S/. " & QuotedMapper.Map(EntityModel).Total
            Else
                dtgList.DataSource = Nothing
                btnPrint.Visible = False
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub

    '''<summary>Método para agregar o editar un detalle de nota de pedido de la cotizacion</summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click

        If Save Then

            orderDetail.ApplianceDescription = appliance
            orderDetail.Appliance = New ApplianceModel
            orderDetail.Quantity = Val(txtQuantity.Text)
            orderDetail.Price = Val(txtPrice.Text)
            orderDetail.SubTotal = OrderDetailMapper.Map(orderDetail).SubTotal

            If ValidateTxt() Then
                If orderDetail.Id Then
                    dt.RemoveAll(Function(detail) detail.Id = orderDetail.Id)
                    dt.Add(orderDetail)
                    EntityModel.OrderDetails = dt
                Else
                    dt.Add(orderDetail)
                    EntityModel.OrderDetails = dt
                End If
                Save = False
                Cancel = False
                SaveControls()
                ClearTxt()
                GetAll()
            End If
        Else
            FrmListAppliance.getInstancia.Form = Me.Name
            FrmListAppliance.getInstancia.ShowDialog()

        End If
    End Sub

    '''<summary>Método para cargar los datos de un detalle de nota de pedido a ser editado</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If Cancel Then
            Save = False
            SaveControls()
            ClearTxt()
            GetAll()
            Cancel = False
        Else
            If dtgList.CurrentRow IsNot Nothing Then
                orderDetail = New OrderDetailModel With {
                                    .Id = dtgList.CurrentRow.Cells.Item("Id").Value,
                                    .ApplianceDescription = dtgList.CurrentRow.Cells.Item("ApplianceDescription").Value,
                                    .Price = dtgList.CurrentRow.Cells.Item("Price").Value,
                                    .Quantity = dtgList.CurrentRow.Cells.Item("Quantity").Value
                }
                SetAppliance(orderDetail.ApplianceDescription)
                txtQuantity.Text = orderDetail.Quantity
                txtPrice.Text = orderDetail.Price
                Save = True
                SaveControls()
                Cancel = True
            End If
        End If
    End Sub

    '''<summary>Método para eliminar un detalle o una seleccion de detalles</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Item", "¿Esta seguro de eliminar el/los items?", AlertResource.QUESTION)
        Dim result = frm.ShowDialog

        If result = DialogResult.OK Then
            If chkDelete.Checked Then
                Dim onekey As Integer
                For Each row In dtgList.Rows
                    Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                    If cheked Then
                        onekey = Convert.ToInt32(row.Cells("Item").Value)
                        dt.RemoveAll(Function(detail) detail.Id = onekey)
                    End If
                Next
            Else
                dt.RemoveAll(Function(detail) detail.Id = dtgList.CurrentRow.Cells.Item("Id").Value)
            End If
            EntityModel.OrderDetails = dt
            chkDelete.Checked = False
            GetAll()
        End If
    End Sub

    '''<summary>Método para validar precio</summary>
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        FormDesing.OnlyDecimal(e, sender)
        sender.MaxLength = 18
    End Sub

    '''<summary>Método para validar cantidad</summary>
    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtQuantity.KeyPress
        FormDesing.OnlyNumbers(e)
    End Sub

    Private Sub lblList_Click(sender As Object, e As EventArgs) Handles lblList.Click
        EntityModel.OrderDetails.Clear()
        EntityModel = New QuotedModel
        ClearTxt()
        GetAll()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        FrmQuotedReport.getInstancia.lblTitle.Text = "IMPRIMIR COTIZACIÓN"
        FrmQuotedReport.getInstancia.EntityModel = EntityModel
        FrmQuotedReport.getInstancia.ShowDialog()
    End Sub
End Class