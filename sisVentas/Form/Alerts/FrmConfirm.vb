﻿Imports Transitions

''' <summary>
''' Clase Formulario de Confirmacion: 
''' Formulario que muestra un mensaje de confirmacion
''' </summary>
Public Class FrmConfirm

    Property Confirm As Boolean

    '''<summary>Constructor que inyecta los parametros del formulario</summary>
    '''<param name="type">Tipo de mensaje</param>
    '''<param name="title">Titulo del mensaje</param>
    '''<param name="message">Mensaje que se muestra</param>
    '''<param name="icon">Icono a mostrar en el mensaje</param>
    Public Sub New(type As String, title As String, message As String, icon As String)
        InitializeComponent()
        lblMessage.Text = message
        lblTitle.Text = title
        pnlHead.BackColor = AlertResource.Color(type)
        img.Image = AlertResource.Img(icon)
    End Sub

    '''<summary>Constructor que inyecta los parametros del formulario</summary>
    '''<param name="type">Tipo de mensaje</param>
    '''<param name="title">Titulo del mensaje</param>
    '''<param name="message">Mensaje que se muestra</param>
    Public Sub New(type As String, title As String, message As String)
        InitializeComponent()
        lblMessage.Text = message
        lblTitle.Text = title
        pnlHead.BackColor = AlertResource.Color(type)
        img.Image = AlertResource.Img(type)
    End Sub

    '''<summary>Método que implementa las transiciones</summary>
    Private Sub FrmConfirm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(800))
        t.add(Me, "Opacity", 1.0R)
        t.add(img, "Left", 75)
        t.run()
    End Sub

    '''<summary>Método que acepta la accion</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Me.DialogResult = DialogResult.OK
    End Sub
    '''<summary>Método que cancela la accion</summary>
    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

End Class