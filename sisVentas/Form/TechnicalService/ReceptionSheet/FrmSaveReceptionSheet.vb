﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Ficha de Recepción: 
''' Formulario que permite editar o modificar una Ficha de Recepción
''' </summary>
Public Class FrmSaveReceptionSheet

    'controlador del modelo de Ficha de Recepción
    Private ReadOnly controller As New ReceptionSheetController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveReceptionSheet = Nothing

    'Modelo de Ficha de Recepción a ser agregada o modificada
    Public Property EntityModel As ReceptionSheetModel

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Edit As Boolean = False

    'Permite mostrar los controles para editar o guardar los elementos
    Public Property detail As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveReceptionSheet
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveReceptionSheet
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateDocument.Visible = False
        lblValidateTechnicalReport.Visible = False
        lblValidateDate.Visible = False
        lblValidateDetail.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Client") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "TechnicalReport") Then
                    lblValidateTechnicalReport.Text = item.ErrorMessage
                    lblValidateTechnicalReport.Visible = True
                    txtTechnicalReportId.Focus()
                End If
                If (item.MemberNames.First = "Rdate") Then
                    lblValidateDate.Text = item.ErrorMessage
                    lblValidateDate.Visible = True
                    txtDate.Focus()
                End If
                If (item.MemberNames.First = "Detail") Then
                    lblValidateDetail.Text = item.ErrorMessage
                    lblValidateDetail.Visible = True
                    txtDetail.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtDocument.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtDate.Text = ""
        txtService.Text = ""
        txtSerie.Text = ""
        txtTechnicalReportId.Text = ""
        txtApplianceDescription.Text = ""
    End Sub
    '''<summary>Método para resetear asignar cliente</summary>
    Private Sub GetClient()
        If rbtDni.Checked Then
            If txtDocument.Text.Length = 8 Then
                EntityModel.Client = controller.GetClientByDocument(txtDocument.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    If EntityModel.Client.Phone.Length > 0 Then
                        txtPhone.Text = EntityModel.Client.Phone
                        txtSerie.Enabled = True
                        btnGetClient.Enabled = True
                        txtSerie.Focus()
                    Else
                        FrmSaveClient.getInstancia.lblTitle.Text = "EDITAR CLIENTE"
                        FrmSaveClient.getInstancia.EntityModel = EntityModel.Client
                        FrmSaveClient.getInstancia.Save = False
                        FrmSaveClient.getInstancia.chkBussines.Visible = False
                        FrmSaveClient.getInstancia.Form = Me.Name
                        FrmSaveClient.getInstancia.ShowDialog()
                    End If
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.Show()
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                txtPhone.Text = ""
            End If
        End If
        If rbtRuc.Checked Then
            If txtDocument.Text.Length = 11 Then
                EntityModel.Client = controller.GetClientByDocument(txtDocument.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    If EntityModel.Client.Phone.Length > 0 Then
                        txtPhone.Text = EntityModel.Client.Phone
                        txtSerie.Focus()
                    Else
                        FrmSaveClient.getInstancia.lblTitle.Text = "EDITAR CLIENTE"
                        FrmSaveClient.getInstancia.EntityModel = EntityModel.Client
                        FrmSaveClient.getInstancia.Save = False
                        FrmSaveClient.getInstancia.chkBussines.Visible = False
                        FrmSaveClient.getInstancia.Form = Me.Name
                        FrmSaveClient.getInstancia.ShowDialog()
                    End If
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Checked = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.Show()
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                txtPhone.Text = ""
            End If
        End If
    End Sub



    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la Ficha de Recepción</summary>
    Private Sub FrmSaveOrder_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()

        toolTip.SetToolTip(btnGetClient, "Buscar Cliente")

        If EntityModel IsNot Nothing Then
            If EntityModel.Id Then
                btnPrint.Visible = True
            Else
                btnPrint.Visible = False
            End If
            txtDocument.Text = EntityModel.Client.DocumentNumber
            txtName.Text = EntityModel.Client.FullName
            txtAddress.Text = EntityModel.Client.Address
            txtPhone.Text = EntityModel.Client.Phone
            txtSerie.Text = EntityModel.TechnicalReport.Appliance.Serie
            txtTechnicalReportId.Text = EntityModel.TechnicalReport.Id
            txtApplianceDescription.Text = EntityModel.TechnicalReport.Appliance.ApplianceDescription.Category.Name & " " & EntityModel.TechnicalReport.Appliance.ApplianceDescription.Brand.Name & " " & EntityModel.TechnicalReport.Appliance.ApplianceDescription.ApplianceName & " " & EntityModel.TechnicalReport.Appliance.ApplianceDescription.Model
            txtService.Text = EntityModel.TechnicalReport.Service
            txtEntryDate.Value = EntityModel.TechnicalReport.EntryDate
            If EntityModel.Id Then
                txtDelveryDate.Value = EntityModel.TechnicalReport.DeliveryDate
                txtDate.Value = EntityModel.Rdate
                txtDetail.Text = EntityModel.Detail
            End If
            If txtDocument.Text.Length = 8 Then
                rbtDni.Checked = True
            ElseIf txtDocument.Text.Length = 11 Then
                rbtRuc.Checked = True
            End If
        End If

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Ficha de Recepción</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Rdate = txtDate.Value
            EntityModel.Detail = txtDetail.Text
            If txtName.Text = "" Then
                EntityModel.Client = Nothing
            End If
            Dim entity = New ReceptionSheetModel
            If validateTxt() Then
                entity = EntityModel
                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Ficha de Entrega", "Ficha de Entrega modificada correctamente")
                        ClearTxt()
                        FrmShowTechnicalReport.getInstancia.GetAll("", FrmShowTechnicalReport.getInstancia.txtBeginDate.Value, FrmShowTechnicalReport.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                Else
                    If controller.Add(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Ficha de Entrega", "Ficha de Entrega registrada correctamente")
                        ClearTxt()
                        FrmShowTechnicalReport.getInstancia.GetAll("", FrmShowTechnicalReport.getInstancia.txtBeginDate.Value, FrmShowTechnicalReport.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                End If
            End If
            FrmReceptionSheetReport.getInstancia.lblTitle.Text = "IMPRIMIR FICHA DE ENTREGA"
            FrmReceptionSheetReport.getInstancia.EntityModel = entity
            FrmReceptionSheetReport.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Ficha de Entrega", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
        Dispose()
    End Sub

    '''<summary>Método para validar caja de texto para numero de documento</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
        If rbtDni.Checked Then
            sender.MaxLength = 8
        End If
        If rbtRuc.Checked Then
            sender.MaxLength = 11
        End If
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs)
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para validar cliente</summary>
    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        GetClient()
    End Sub

    '''<summary>Método para verificar si es dni</summary>
    Private Sub rbtDni_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDni.CheckedChanged
        If rbtDni.Checked Then
            lblDocument.Text = "DNI:"
            txtDocument.MaxLength = 8
        End If
        txtDocument.Focus()
    End Sub

    '''<summary>Método para verificar si es ruc</summary>
    Private Sub rbtRuc_CheckedChanged(sender As Object, e As EventArgs) Handles rbtRuc.CheckedChanged
        If rbtRuc.Checked Then
            lblDocument.Text = "RUC:"
            txtDocument.MaxLength = 11
        End If
        txtDocument.Focus()
    End Sub

    Private Sub FrmSaveOrder_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtDocument.Focus()
    End Sub

    Private Sub btnGetClient_Click(sender As Object, e As EventArgs) Handles btnGetClient.Click
        GetClient()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        FrmReceptionSheetReport.getInstancia.lblTitle.Text = "IMPRIMIR FICHA DE ENTREGA"
        FrmReceptionSheetReport.getInstancia.EntityModel = EntityModel
        FrmReceptionSheetReport.getInstancia.ShowDialog()
    End Sub
End Class