﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveReceptionSheet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.txtService = New System.Windows.Forms.TextBox()
        Me.lblValidateDetail = New System.Windows.Forms.Label()
        Me.txtDetail = New System.Windows.Forms.TextBox()
        Me.lblDetail = New System.Windows.Forms.Label()
        Me.txtDelveryDate = New System.Windows.Forms.DateTimePicker()
        Me.lblDeliveryDate = New System.Windows.Forms.Label()
        Me.txtEntryDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEntryDate = New System.Windows.Forms.Label()
        Me.txtApplianceDescription = New System.Windows.Forms.TextBox()
        Me.lblApplianceDescription = New System.Windows.Forms.Label()
        Me.txtTechnicalReportId = New System.Windows.Forms.TextBox()
        Me.lblValidateTechnicalReport = New System.Windows.Forms.Label()
        Me.lblTechnicalReport = New System.Windows.Forms.Label()
        Me.lblService = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.rbtRuc = New System.Windows.Forms.RadioButton()
        Me.rbtDni = New System.Windows.Forms.RadioButton()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.lblSerie = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtDocument = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.DateTimePicker()
        Me.lblValidateDocument = New System.Windows.Forms.Label()
        Me.lblValidateDate = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblDocument = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnGetClient = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.pnlTitle.SuspendLayout()
        Me.pnlBody.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(732, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "FICHA DE ENTREGA"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(757, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.btnPrint)
        Me.pnlBody.Controls.Add(Me.txtService)
        Me.pnlBody.Controls.Add(Me.lblValidateDetail)
        Me.pnlBody.Controls.Add(Me.txtDetail)
        Me.pnlBody.Controls.Add(Me.lblDetail)
        Me.pnlBody.Controls.Add(Me.txtDelveryDate)
        Me.pnlBody.Controls.Add(Me.lblDeliveryDate)
        Me.pnlBody.Controls.Add(Me.txtEntryDate)
        Me.pnlBody.Controls.Add(Me.lblEntryDate)
        Me.pnlBody.Controls.Add(Me.btnGetClient)
        Me.pnlBody.Controls.Add(Me.txtApplianceDescription)
        Me.pnlBody.Controls.Add(Me.lblApplianceDescription)
        Me.pnlBody.Controls.Add(Me.txtTechnicalReportId)
        Me.pnlBody.Controls.Add(Me.lblValidateTechnicalReport)
        Me.pnlBody.Controls.Add(Me.lblTechnicalReport)
        Me.pnlBody.Controls.Add(Me.lblService)
        Me.pnlBody.Controls.Add(Me.txtPhone)
        Me.pnlBody.Controls.Add(Me.lblPhone)
        Me.pnlBody.Controls.Add(Me.rbtRuc)
        Me.pnlBody.Controls.Add(Me.rbtDni)
        Me.pnlBody.Controls.Add(Me.txtSerie)
        Me.pnlBody.Controls.Add(Me.lblSerie)
        Me.pnlBody.Controls.Add(Me.txtAddress)
        Me.pnlBody.Controls.Add(Me.txtName)
        Me.pnlBody.Controls.Add(Me.txtDocument)
        Me.pnlBody.Controls.Add(Me.txtDate)
        Me.pnlBody.Controls.Add(Me.lblValidateDocument)
        Me.pnlBody.Controls.Add(Me.lblValidateDate)
        Me.pnlBody.Controls.Add(Me.lblAddress)
        Me.pnlBody.Controls.Add(Me.lblName)
        Me.pnlBody.Controls.Add(Me.lblDocument)
        Me.pnlBody.Controls.Add(Me.lblDate)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(757, 390)
        Me.pnlBody.TabIndex = 46
        '
        'txtService
        '
        Me.txtService.Enabled = False
        Me.txtService.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtService.Location = New System.Drawing.Point(328, 153)
        Me.txtService.Name = "txtService"
        Me.txtService.Size = New System.Drawing.Size(410, 25)
        Me.txtService.TabIndex = 165
        '
        'lblValidateDetail
        '
        Me.lblValidateDetail.AutoSize = True
        Me.lblValidateDetail.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDetail.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDetail.Location = New System.Drawing.Point(329, 297)
        Me.lblValidateDetail.Name = "lblValidateDetail"
        Me.lblValidateDetail.Size = New System.Drawing.Size(83, 15)
        Me.lblValidateDetail.TabIndex = 164
        Me.lblValidateDetail.Text = "Ingrese Detalle"
        Me.lblValidateDetail.Visible = False
        '
        'txtDetail
        '
        Me.txtDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetail.Location = New System.Drawing.Point(328, 272)
        Me.txtDetail.Name = "txtDetail"
        Me.txtDetail.Size = New System.Drawing.Size(406, 24)
        Me.txtDetail.TabIndex = 162
        '
        'lblDetail
        '
        Me.lblDetail.AutoSize = True
        Me.lblDetail.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetail.Location = New System.Drawing.Point(328, 250)
        Me.lblDetail.Name = "lblDetail"
        Me.lblDetail.Size = New System.Drawing.Size(62, 19)
        Me.lblDetail.TabIndex = 163
        Me.lblDetail.Text = "Detalle:"
        '
        'txtDelveryDate
        '
        Me.txtDelveryDate.Enabled = False
        Me.txtDelveryDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDelveryDate.Location = New System.Drawing.Point(538, 208)
        Me.txtDelveryDate.Name = "txtDelveryDate"
        Me.txtDelveryDate.Size = New System.Drawing.Size(196, 26)
        Me.txtDelveryDate.TabIndex = 160
        '
        'lblDeliveryDate
        '
        Me.lblDeliveryDate.AutoSize = True
        Me.lblDeliveryDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeliveryDate.Location = New System.Drawing.Point(538, 187)
        Me.lblDeliveryDate.Name = "lblDeliveryDate"
        Me.lblDeliveryDate.Size = New System.Drawing.Size(138, 19)
        Me.lblDeliveryDate.TabIndex = 161
        Me.lblDeliveryDate.Text = "Fecha de Entrega:"
        '
        'txtEntryDate
        '
        Me.txtEntryDate.Enabled = False
        Me.txtEntryDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntryDate.Location = New System.Drawing.Point(328, 208)
        Me.txtEntryDate.Name = "txtEntryDate"
        Me.txtEntryDate.Size = New System.Drawing.Size(196, 26)
        Me.txtEntryDate.TabIndex = 158
        '
        'lblEntryDate
        '
        Me.lblEntryDate.AutoSize = True
        Me.lblEntryDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntryDate.Location = New System.Drawing.Point(328, 187)
        Me.lblEntryDate.Name = "lblEntryDate"
        Me.lblEntryDate.Size = New System.Drawing.Size(140, 19)
        Me.lblEntryDate.TabIndex = 159
        Me.lblEntryDate.Text = "Fecha de Entrada:"
        '
        'txtApplianceDescription
        '
        Me.txtApplianceDescription.Enabled = False
        Me.txtApplianceDescription.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplianceDescription.Location = New System.Drawing.Point(328, 92)
        Me.txtApplianceDescription.Name = "txtApplianceDescription"
        Me.txtApplianceDescription.Size = New System.Drawing.Size(410, 25)
        Me.txtApplianceDescription.TabIndex = 155
        '
        'lblApplianceDescription
        '
        Me.lblApplianceDescription.AutoSize = True
        Me.lblApplianceDescription.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplianceDescription.Location = New System.Drawing.Point(324, 72)
        Me.lblApplianceDescription.Name = "lblApplianceDescription"
        Me.lblApplianceDescription.Size = New System.Drawing.Size(66, 19)
        Me.lblApplianceDescription.TabIndex = 156
        Me.lblApplianceDescription.Text = "Equipo: "
        '
        'txtTechnicalReportId
        '
        Me.txtTechnicalReportId.Enabled = False
        Me.txtTechnicalReportId.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTechnicalReportId.Location = New System.Drawing.Point(328, 33)
        Me.txtTechnicalReportId.Name = "txtTechnicalReportId"
        Me.txtTechnicalReportId.Size = New System.Drawing.Size(200, 25)
        Me.txtTechnicalReportId.TabIndex = 152
        '
        'lblValidateTechnicalReport
        '
        Me.lblValidateTechnicalReport.AutoSize = True
        Me.lblValidateTechnicalReport.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateTechnicalReport.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateTechnicalReport.Location = New System.Drawing.Point(334, 56)
        Me.lblValidateTechnicalReport.Name = "lblValidateTechnicalReport"
        Me.lblValidateTechnicalReport.Size = New System.Drawing.Size(130, 15)
        Me.lblValidateTechnicalReport.TabIndex = 154
        Me.lblValidateTechnicalReport.Text = "Ingrese Reporte Técnico"
        Me.lblValidateTechnicalReport.Visible = False
        '
        'lblTechnicalReport
        '
        Me.lblTechnicalReport.AutoSize = True
        Me.lblTechnicalReport.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTechnicalReport.Location = New System.Drawing.Point(324, 13)
        Me.lblTechnicalReport.Name = "lblTechnicalReport"
        Me.lblTechnicalReport.Size = New System.Drawing.Size(125, 19)
        Me.lblTechnicalReport.TabIndex = 153
        Me.lblTechnicalReport.Text = "Reporte Técnico:"
        '
        'lblService
        '
        Me.lblService.AutoSize = True
        Me.lblService.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(332, 131)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(65, 19)
        Me.lblService.TabIndex = 143
        Me.lblService.Text = "Servicio:"
        '
        'txtPhone
        '
        Me.txtPhone.Enabled = False
        Me.txtPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(13, 211)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(300, 24)
        Me.txtPhone.TabIndex = 130
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(13, 189)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(70, 19)
        Me.lblPhone.TabIndex = 131
        Me.lblPhone.Text = "Telefono:"
        '
        'rbtRuc
        '
        Me.rbtRuc.AutoSize = True
        Me.rbtRuc.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtRuc.Location = New System.Drawing.Point(257, 3)
        Me.rbtRuc.Name = "rbtRuc"
        Me.rbtRuc.Size = New System.Drawing.Size(57, 22)
        Me.rbtRuc.TabIndex = 1
        Me.rbtRuc.TabStop = True
        Me.rbtRuc.Text = "RUC"
        Me.rbtRuc.UseVisualStyleBackColor = True
        '
        'rbtDni
        '
        Me.rbtDni.AutoSize = True
        Me.rbtDni.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtDni.Location = New System.Drawing.Point(199, 3)
        Me.rbtDni.Name = "rbtDni"
        Me.rbtDni.Size = New System.Drawing.Size(52, 22)
        Me.rbtDni.TabIndex = 0
        Me.rbtDni.TabStop = True
        Me.rbtDni.Text = "DNI"
        Me.rbtDni.UseVisualStyleBackColor = True
        '
        'txtSerie
        '
        Me.txtSerie.Enabled = False
        Me.txtSerie.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.Location = New System.Drawing.Point(538, 33)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(200, 25)
        Me.txtSerie.TabIndex = 7
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerie.Location = New System.Drawing.Point(534, 13)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(45, 19)
        Me.lblSerie.TabIndex = 120
        Me.lblSerie.Text = "Serie:"
        '
        'txtAddress
        '
        Me.txtAddress.Enabled = False
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(14, 153)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(300, 24)
        Me.txtAddress.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Enabled = False
        Me.txtName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(14, 93)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(300, 24)
        Me.txtName.TabIndex = 3
        '
        'txtDocument
        '
        Me.txtDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocument.Location = New System.Drawing.Point(14, 33)
        Me.txtDocument.Name = "txtDocument"
        Me.txtDocument.Size = New System.Drawing.Size(263, 24)
        Me.txtDocument.TabIndex = 2
        '
        'txtDate
        '
        Me.txtDate.Enabled = False
        Me.txtDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(14, 272)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(300, 26)
        Me.txtDate.TabIndex = 5
        '
        'lblValidateDocument
        '
        Me.lblValidateDocument.AutoSize = True
        Me.lblValidateDocument.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDocument.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDocument.Location = New System.Drawing.Point(14, 55)
        Me.lblValidateDocument.Name = "lblValidateDocument"
        Me.lblValidateDocument.Size = New System.Drawing.Size(167, 15)
        Me.lblValidateDocument.TabIndex = 80
        Me.lblValidateDocument.Text = "Ingrese Numero de Documento"
        Me.lblValidateDocument.Visible = False
        '
        'lblValidateDate
        '
        Me.lblValidateDate.AutoSize = True
        Me.lblValidateDate.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDate.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDate.Location = New System.Drawing.Point(14, 296)
        Me.lblValidateDate.Name = "lblValidateDate"
        Me.lblValidateDate.Size = New System.Drawing.Size(78, 15)
        Me.lblValidateDate.TabIndex = 79
        Me.lblValidateDate.Text = "Ingrese Fecha"
        Me.lblValidateDate.Visible = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(14, 131)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(78, 19)
        Me.lblAddress.TabIndex = 75
        Me.lblAddress.Text = "Dirección:"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(14, 71)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 19)
        Me.lblName.TabIndex = 68
        Me.lblName.Text = "Nombre:"
        '
        'lblDocument
        '
        Me.lblDocument.AutoSize = True
        Me.lblDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(14, 11)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(71, 19)
        Me.lblDocument.TabIndex = 70
        Me.lblDocument.Text = "DNI/RUC:"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(14, 251)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(57, 19)
        Me.lblDate.TabIndex = 69
        Me.lblDate.Text = "Fecha:"
        '
        'btnGetClient
        '
        Me.btnGetClient.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnGetClient.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGetClient.Enabled = False
        Me.btnGetClient.FlatAppearance.BorderSize = 0
        Me.btnGetClient.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnGetClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGetClient.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetClient.ForeColor = System.Drawing.Color.White
        Me.btnGetClient.Image = Global.sisVentas.Desktop.My.Resources.Resources.search
        Me.btnGetClient.Location = New System.Drawing.Point(284, 30)
        Me.btnGetClient.Name = "btnGetClient"
        Me.btnGetClient.Size = New System.Drawing.Size(30, 30)
        Me.btnGetClient.TabIndex = 157
        Me.btnGetClient.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(148, 323)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 45)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(308, 323)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 45)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(739, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.White
        Me.btnPrint.Image = Global.sisVentas.Desktop.My.Resources.Resources.print
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(465, 323)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(140, 45)
        Me.btnPrint.TabIndex = 166
        Me.btnPrint.Text = "Imprimir"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = False
        Me.btnPrint.Visible = False
        '
        'FrmSaveReceptionSheet
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(757, 421)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveReceptionSheet"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents lblAddress As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents lblName As Label
    Friend WithEvents lblDocument As Label
    Friend WithEvents lblDate As Label
    Friend WithEvents txtDocument As TextBox
    Friend WithEvents lblValidateDocument As Label
    Friend WithEvents lblValidateDate As Label
    Friend WithEvents txtDate As DateTimePicker
    Friend WithEvents txtSerie As TextBox
    Friend WithEvents lblSerie As Label
    Friend WithEvents rbtDni As RadioButton
    Friend WithEvents rbtRuc As RadioButton
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents lblPhone As Label
    Friend WithEvents lblService As Label
    Friend WithEvents txtApplianceDescription As TextBox
    Friend WithEvents lblApplianceDescription As Label
    Friend WithEvents txtTechnicalReportId As TextBox
    Friend WithEvents lblValidateTechnicalReport As Label
    Friend WithEvents toolTip As ToolTip
    Friend WithEvents btnGetClient As Button
    Friend WithEvents txtDelveryDate As DateTimePicker
    Friend WithEvents lblDeliveryDate As Label
    Friend WithEvents txtEntryDate As DateTimePicker
    Friend WithEvents lblEntryDate As Label
    Friend WithEvents lblTechnicalReport As Label
    Friend WithEvents lblValidateDetail As Label
    Friend WithEvents txtDetail As TextBox
    Friend WithEvents lblDetail As Label
    Friend WithEvents txtService As TextBox
    Friend WithEvents btnPrint As Button
End Class
