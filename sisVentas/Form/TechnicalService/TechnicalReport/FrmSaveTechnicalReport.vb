﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports Transitions

''' <summary>
''' Clase Formulario Guardar Reporte Técnico: 
''' Formulario que permite editar o modificar una Reporte Técnico
''' </summary>
Public Class FrmSaveTechnicalReport

    'controlador del modelo de Reporte Técnico
    Private ReadOnly controller As New TechnicalReportController()

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmSaveTechnicalReport = Nothing

    'Modelo de Reporte Técnico a ser agregada o modificada
    Public Property EntityModel As TechnicalReportModel

    'Descripcion del equipo de cómputo a agregar en el detalle de Reporte Técnico
    Private appliance As ApplianceModel

    'Controla el boton de cancelar
    Public Property Cancel As Boolean

    'Si se trata de guardar o no
    Public Property Edit As Boolean = False

    'Permite mostrar los controles para editar o guardar los elementos
    Public Property detail As Boolean

    '''<summary>Constructor del formulario que inicializa los componente y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmSaveTechnicalReport
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmSaveTechnicalReport
        End If
        Return _formulario
    End Function

    '''<summary>Método para ocultar las etiquetas de validacion del formulario</summary>
    Private Sub HideLblValidate()
        lblValidateDocument.Visible = False
        lblValidateName.Visible = False
        lblValidateAddress.Visible = False
        lblValidatePhone.Visible = False
        lblValidateEntryDate.Visible = False
        lblValidateDeliveryDate.Visible = False
        lblValidateAppliance.Visible = False
        lblValidateService.Visible = False
        lblValidatePrice.Visible = False
        lblValidateFailure.Visible = False
        lblValidateDiagnosis.Visible = False
        lblValidateObservation.Visible = False
    End Sub

    '''<summary>Método para mostrar las etiquetas de validacion del formulario</summary>
    Private Function validateTxt() As Boolean
        HideLblValidate()
        Dim validate = New DataValidation(EntityModel)
        If (Not validate.Valid) Then
            For Each item In validate.Results
                If (item.MemberNames.First = "Client") Then
                    lblValidateDocument.Text = item.ErrorMessage
                    lblValidateDocument.Visible = True
                    txtDocument.Focus()
                End If
                If (item.MemberNames.First = "Appliance") Then
                    lblValidateAppliance.Text = item.ErrorMessage
                    lblValidateAppliance.Visible = True
                    txtAppliance.Focus()
                End If
                If (item.MemberNames.First = "Service") Then
                    lblValidateService.Text = item.ErrorMessage
                    lblValidateService.Visible = True
                    cbxService.Focus()
                End If
                If (item.MemberNames.First = "Price") Then
                    lblValidatePrice.Text = item.ErrorMessage
                    lblValidatePrice.Visible = True
                    txtPrice.Focus()
                End If
                If (item.MemberNames.First = "EntryDate") Then
                    lblValidateEntryDate.Text = item.ErrorMessage
                    lblValidateEntryDate.Visible = True
                    txtEntryDate.Focus()
                End If
                If (item.MemberNames.First = "DeliveryDate") Then
                    lblValidateDeliveryDate.Text = item.ErrorMessage
                    lblValidateDeliveryDate.Visible = True
                    txtDeliveryDate.Focus()
                End If
                If (item.MemberNames.First = "Employee") Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Reporte Técnico", "Datos del empleado incorrectos inicie sesion nuevamente")
                End If
                If (item.MemberNames.First = "Failure") Then
                    lblValidateFailure.Text = item.ErrorMessage
                    lblValidateFailure.Visible = True
                    txtFailure.Focus()
                End If
                If (item.MemberNames.First = "Diagnosis") Then
                    lblValidateDiagnosis.Text = item.ErrorMessage
                    lblValidateDiagnosis.Visible = True
                    txtDiagnosis.Focus()
                End If
                If (item.MemberNames.First = "Observation") Then
                    lblValidateObservation.Text = item.ErrorMessage
                    lblValidateObservation.Visible = True
                    txtObservation.Focus()
                End If
            Next
        End If
        Return validate.Valid
    End Function

    '''<summary>Método para resetear los valores principales del formulario</summary>
    Private Sub ClearTxt()
        EntityModel = Nothing
        txtDocument.Text = ""
        txtName.Text = ""
        txtAddress.Text = ""
        txtPhone.Text = ""
        txtEntryDate.Text = ""
        txtDeliveryDate.Text = ""
        cbxService.Text = ""
        txtAppliance.Text = ""
        txtModel.Text = ""
        txtCategoryBrand.Text = ""
        cbxService.Text = ""
        txtPrice.Text = ""
        txtFailure.Text = ""
        txtDiagnosis.Text = ""
        txtObservation.Text = ""
    End Sub

    '''<summary>Método rellenar los datos de la descripcion del equipo de cómputo</summary>
    '''<param name="appliance">Equipo de cómputo a ser agregado</param>
    Public Sub SetAppliance(appliance As ApplianceModel)
        Me.appliance = appliance
        txtAppliance.Text = appliance.Serie
        txtModel.Text = appliance.ApplianceDescription.Model
        txtCategoryBrand.Text = appliance.ApplianceDescription.Category.Name & " " & appliance.ApplianceDescription.Brand.Name
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dispose()
    End Sub

    '''<summary>Método para mostrar los datos de la Reporte Técnico</summary>
    Private Sub FrmSaveTechnicalReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim t As New Transition(New TransitionType_EaseInEaseOut(500))
        t.add(Me, "Opacity", 0.97R)
        t.run()

        toolTip.SetToolTip(btnEdit, "Edtar Cliente")

        txtDeliveryDate.Value = Now.AddDays(2)

        cbxService.DataSource = controller.GetServices

        If EntityModel.Id Then
            btnPrint.Visible = True
            txtDocument.Text = EntityModel.Client.DocumentNumber
            txtName.Text = EntityModel.Client.FullName
            txtAddress.Text = EntityModel.Client.Address
            txtPhone.Text = EntityModel.Client.Phone
            txtEntryDate.Value = EntityModel.EntryDate
            txtDeliveryDate.Value = EntityModel.DeliveryDate
            txtAppliance.Text = EntityModel.Appliance.Serie
            txtModel.Text = EntityModel.Appliance.ApplianceDescription.Model
            txtCategoryBrand.Text = EntityModel.Appliance.ApplianceDescription.Category.Name & " " & EntityModel.Appliance.ApplianceDescription.Brand.Name
            cbxService.Text = EntityModel.Service
            txtPrice.Text = EntityModel.Price
            txtFailure.Text = EntityModel.Failure
            txtDiagnosis.Text = EntityModel.Diagnosis
            txtObservation.Text = EntityModel.Observation
            txtAppliance.Enabled = True
            txtModel.Enabled = False
            txtEntryDate.Enabled = True
            txtDeliveryDate.Enabled = True
            btnEdit.Enabled = True
        Else
            btnPrint.Visible = False
        End If

    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Dispose()
    End Sub

    '''<summary>Método para guardar los cambios ya sea agragar o editar Reporte Técnico</summary>
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            EntityModel.Employee = EmployeeMapper.Map(ActiveUser.Employee)
            EntityModel.Service = cbxService.Text
            EntityModel.Price = Val(txtPrice.Text)
            EntityModel.EntryDate = txtEntryDate.Value
            EntityModel.DeliveryDate = txtDeliveryDate.Value
            EntityModel.Failure = txtFailure.Text
            EntityModel.Diagnosis = txtDiagnosis.Text
            EntityModel.Observation = txtObservation.Text
            If appliance IsNot Nothing Then
                EntityModel.Appliance = appliance
            End If
            If txtName.Text = "" Then
                EntityModel.Client = Nothing
            End If
            Dim entity = New TechnicalReportModel
            If validateTxt() Then
                entity = EntityModel
                If EntityModel.Id Then
                    If controller.Update(EntityModel) Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Modificar Reporte Técnico", "Reporte Técnico modificado correctamente")
                        ClearTxt()
                        FrmShowTechnicalReport.getInstancia.GetAll("", FrmShowTechnicalReport.getInstancia.txtBeginDate.Value, FrmShowTechnicalReport.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                Else
                    entity.Id = controller.Add(EntityModel)
                    If entity.Id Then
                        FrmMessage.ShowMessage(AlertResource.SUCCESS, "Agregar Reporte Técnico", "Reporte Técnico registrado correctamente")
                        ClearTxt()
                        FrmShowTechnicalReport.getInstancia.GetAll("", FrmShowTechnicalReport.getInstancia.txtBeginDate.Value, FrmShowTechnicalReport.getInstancia.txtEndDate.Value)
                        Me.Close()
                    End If
                End If
            End If
            FrmTechnicalReportReport.getInstancia.lblTitle.Text = "IMPRIMIR REPORTE TÉCNICO"
            FrmTechnicalReportReport.getInstancia.EntityModel = entity
            FrmTechnicalReportReport.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Guardar Reporte Técnico", ex.Message)
        End Try
    End Sub

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub FrmSave_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ClearTxt()
        HideLblValidate()
        Dispose()
    End Sub

    '''<summary>Método para validar caja de texto para numero de documento</summary>
    Private Sub txtDocument_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocument.KeyPress
        FormDesing.OnlyNumbers(e)
        If rbtDni.Checked Then
            sender.MaxLength = 8
        End If
        If rbtRuc.Checked Then
            sender.MaxLength = 11
        End If
    End Sub

    '''<summary>Método para validar caja de texto para detalle</summary>
    Private Sub txtDetail_KeyPress(sender As Object, e As KeyPressEventArgs)
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para validar precio</summary>
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        FormDesing.OnlyDecimal(e, sender)
        sender.MaxLength = 18
    End Sub

    '''<summary>Método para validar cantidad</summary>
    Private Sub txtQuantity_KeyPress(sender As Object, e As KeyPressEventArgs)
        FormDesing.OnlyNumbers(e)
    End Sub

    '''<summary>Método para validar cliente</summary>
    Private Sub txtDocument_TextChanged(sender As Object, e As EventArgs) Handles txtDocument.TextChanged
        If rbtDni.Checked Then
            If sender.Text.Length = 8 Then
                EntityModel.Client = controller.GetClientByDocument(sender.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    If EntityModel.Client.Phone.Length > 0 Then
                        txtPhone.Text = EntityModel.Client.Phone
                        txtAppliance.Enabled = True
                        btnEdit.Enabled = True
                        txtAppliance.Focus()
                    Else
                        FrmSaveClient.getInstancia.lblTitle.Text = "EDITAR CLIENTE"
                        FrmSaveClient.getInstancia.EntityModel = EntityModel.Client
                        FrmSaveClient.getInstancia.Save = False
                        FrmSaveClient.getInstancia.chkBussines.Visible = False
                        FrmSaveClient.getInstancia.Form = Me.Name
                        txtAppliance.Enabled = False
                        btnEdit.Enabled = False
                        FrmSaveClient.getInstancia.ShowDialog()
                    End If
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.Show()
                    txtAppliance.Enabled = False
                    btnEdit.Enabled = False
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                txtPhone.Text = ""
                txtAppliance.Enabled = False
                btnEdit.Enabled = False
            End If
        End If
        If rbtRuc.Checked Then
            If sender.Text.Length = 11 Then
                EntityModel.Client = controller.GetClientByDocument(sender.Text)
                If EntityModel.Client IsNot Nothing Then
                    txtName.Text = EntityModel.Client.FullName
                    txtAddress.Text = EntityModel.Client.Address
                    If EntityModel.Client.Phone.Length > 0 Then
                        txtPhone.Text = EntityModel.Client.Phone
                        txtAppliance.Enabled = True
                        btnEdit.Enabled = True
                        txtAppliance.Focus()
                    Else
                        FrmSaveClient.getInstancia.lblTitle.Text = "EDITAR CLIENTE"
                        FrmSaveClient.getInstancia.EntityModel = EntityModel.Client
                        FrmSaveClient.getInstancia.Save = False
                        FrmSaveClient.getInstancia.chkBussines.Visible = False
                        FrmSaveClient.getInstancia.Form = Me.Name
                        txtAppliance.Enabled = False
                        btnEdit.Enabled = False
                        FrmSaveClient.getInstancia.ShowDialog()
                    End If
                Else
                    FrmSaveClient.getInstancia.lblTitle.Text = "NUEVO CLIENTE"
                    FrmSaveClient.getInstancia.Form = Me.Name
                    FrmSaveClient.getInstancia.EntityModel = New PersonModel
                    FrmSaveClient.getInstancia.Save = True
                    FrmSaveClient.getInstancia.chkBussines.Checked = True
                    FrmSaveClient.getInstancia.chkBussines.Visible = False
                    FrmSaveClient.getInstancia.txtDocument.Enabled = False
                    FrmSaveClient.getInstancia.EntityModel.DocumentNumber = txtDocument.Text
                    FrmSaveClient.getInstancia.Form = Me.Name
                    txtAppliance.Enabled = False
                    btnEdit.Enabled = False
                    FrmSaveClient.getInstancia.Show()
                End If
            Else
                txtName.Text = ""
                txtAddress.Text = ""
                txtPhone.Text = ""
                txtAppliance.Enabled = False
                btnEdit.Enabled = False
            End If
        End If
    End Sub

    '''<summary>Método para verificar si es dni</summary>
    Private Sub rbtDni_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDni.CheckedChanged
        If rbtDni.Checked Then
            lblDocument.Text = "DNI:"
            txtDocument.MaxLength = 8
        End If
        txtDocument.Focus()
    End Sub

    '''<summary>Método para verificar si es ruc</summary>
    Private Sub rbtRuc_CheckedChanged(sender As Object, e As EventArgs) Handles rbtRuc.CheckedChanged
        If rbtRuc.Checked Then
            lblDocument.Text = "RUC:"
            txtDocument.MaxLength = 11
        End If
        txtDocument.Focus()
    End Sub

    Private Sub FrmSaveTechnicalReport_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        txtDocument.Focus()
    End Sub

    '''<summary>Método para abrir el escaneador de series</summary>
    Private Sub btnScan_Click(sender As Object, e As EventArgs) Handles btnScan.Click
        FrmScan.getInstancia.Form = Me.Name
        FrmScan.getInstancia.ShowDialog()
    End Sub
    '''<summary>Método para validar serie</summary>
    Private Sub txtAppliance_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAppliance.KeyPress
        FormDesing.DefaultDescription(e)
    End Sub

    '''<summary>Método para buscar la serie escrita</summary>
    Private Sub txtAppliance_TextChanged(sender As Object, e As EventArgs) Handles txtAppliance.TextChanged

        If sender.Text.Length > 0 Then
            txtModel.Enabled = True
        Else
            txtModel.Enabled = False
        End If

    End Sub

    Private Sub txtAppliance_LostFocus(sender As Object, e As EventArgs) Handles txtAppliance.LostFocus
        appliance = controller.GetBySerie(txtAppliance.Text)
        If appliance IsNot Nothing Then
            txtModel.Text = appliance.ApplianceDescription.Model
            txtCategoryBrand.Text = appliance.ApplianceDescription.Category.Name & " " & appliance.ApplianceDescription.Brand.Name
            txtModel.Enabled = False
            cbxService.Enabled = True
            cbxService.Focus()
        Else
            cbxService.Enabled = False
            txtModel.Text = ""
            txtCategoryBrand.Text = ""
        End If
    End Sub

    Private Sub txtModel_TextChanged(sender As Object, e As EventArgs) Handles txtModel.TextChanged
        If sender.Text.Length > 0 Then
            cbxService.Enabled = True
        Else
            cbxService.Enabled = False
        End If
    End Sub

    Private Sub cbxService_TextChanged(sender As Object, e As EventArgs) Handles cbxService.TextChanged
        If cbxService.Text.Length > 2 Then
            txtPrice.Enabled = True
            txtFailure.Enabled = True
            txtDiagnosis.Enabled = True
            txtObservation.Enabled = True
        Else
            txtPrice.Enabled = False
            txtFailure.Enabled = False
            txtDiagnosis.Enabled = False
            txtObservation.Enabled = False
        End If
    End Sub

    Private Sub cbxService_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxService.SelectedIndexChanged
        Try
            txtPrice.Text = controller.GetServicePrice(cbxService.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtModel_LostFocus(sender As Object, e As EventArgs) Handles txtModel.LostFocus
        Try
            Dim applianceDescription = controller.GetByModel(txtModel.Text)
            If applianceDescription IsNot Nothing And appliance Is Nothing Then
                txtCategoryBrand.Text = applianceDescription.Category.Name & " " & applianceDescription.Brand.Name
                appliance = New ApplianceModel
                appliance.ApplianceDescription = applianceDescription
                appliance.Serie = txtAppliance.Text
                appliance.State = False
                controller.AddAppliance(appliance)
                appliance = controller.GetBySerie(txtAppliance.Text)
            Else
                If appliance Is Nothing Then
                    txtCategoryBrand.Text = ""
                    appliance = Nothing
                    FrmSaveApplianceDescription.getInstancia.Form = Me.Name
                    FrmSaveApplianceDescription.getInstancia.lblTitle.Text = "AGREGAR EQUIPO"
                    FrmSaveApplianceDescription.getInstancia.EntityModel = New ApplianceDescriptionModel
                    FrmSaveApplianceDescription.getInstancia.txtModel.Text = txtModel.Text
                    FrmSaveApplianceDescription.getInstancia.ShowDialog()
                End If

            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Agregar Reporte Técnico", ex.Message)
        End Try
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSaveClient.getInstancia.lblTitle.Text = "EDITAR CLIENTE"
            FrmSaveClient.getInstancia.EntityModel = EntityModel.Client
            FrmSaveClient.getInstancia.Save = False
            FrmSaveClient.getInstancia.chkBussines.Visible = False
            FrmSaveClient.getInstancia.Form = Me.Name
            txtAppliance.Enabled = False
            btnEdit.Enabled = False
            FrmSaveClient.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Cliente", ex.Message)
        End Try
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        FrmTechnicalReportReport.getInstancia.lblTitle.Text = "IMPRIMIR REPORTE TÉCNICO"
        FrmTechnicalReportReport.getInstancia.EntityModel = EntityModel
        FrmTechnicalReportReport.getInstancia.ShowDialog()
    End Sub
End Class