﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSaveTechnicalReport
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTitle = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.pnlBody = New System.Windows.Forms.Panel()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.txtCategoryBrand = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtModel = New System.Windows.Forms.TextBox()
        Me.lblValidateModel = New System.Windows.Forms.Label()
        Me.lblModel = New System.Windows.Forms.Label()
        Me.txtObservation = New System.Windows.Forms.TextBox()
        Me.lblValidateObservation = New System.Windows.Forms.Label()
        Me.lblObservation = New System.Windows.Forms.Label()
        Me.txtDiagnosis = New System.Windows.Forms.TextBox()
        Me.lblValidateDiagnosis = New System.Windows.Forms.Label()
        Me.lblDiagnosis = New System.Windows.Forms.Label()
        Me.cbxService = New System.Windows.Forms.ComboBox()
        Me.lblValidateService = New System.Windows.Forms.Label()
        Me.lblService = New System.Windows.Forms.Label()
        Me.txtDeliveryDate = New System.Windows.Forms.DateTimePicker()
        Me.lblValidateDeliveryDate = New System.Windows.Forms.Label()
        Me.lblDeliveryDate = New System.Windows.Forms.Label()
        Me.lblValidatePhone = New System.Windows.Forms.Label()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.txtFailure = New System.Windows.Forms.TextBox()
        Me.lblValidateFailure = New System.Windows.Forms.Label()
        Me.lblFailure = New System.Windows.Forms.Label()
        Me.rbtRuc = New System.Windows.Forms.RadioButton()
        Me.rbtDni = New System.Windows.Forms.RadioButton()
        Me.btnScan = New System.Windows.Forms.Button()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtAppliance = New System.Windows.Forms.TextBox()
        Me.lblValidatePrice = New System.Windows.Forms.Label()
        Me.lblValidateAppliance = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblAppliance = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtDocument = New System.Windows.Forms.TextBox()
        Me.txtEntryDate = New System.Windows.Forms.DateTimePicker()
        Me.lblValidateDocument = New System.Windows.Forms.Label()
        Me.lblValidateEntryDate = New System.Windows.Forms.Label()
        Me.lblValidateAddress = New System.Windows.Forms.Label()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.lblValidateName = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblDocument = New System.Windows.Forms.Label()
        Me.lblEntryDate = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.pnlTitle.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBody.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.lblTitle.Location = New System.Drawing.Point(3, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(929, 19)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "EQUIPOS"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTitle
        '
        Me.pnlTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.pnlTitle.Controls.Add(Me.btnClose)
        Me.pnlTitle.Controls.Add(Me.lblTitle)
        Me.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTitle.Location = New System.Drawing.Point(0, 0)
        Me.pnlTitle.Name = "pnlTitle"
        Me.pnlTitle.Size = New System.Drawing.Size(960, 31)
        Me.pnlTitle.TabIndex = 21
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.Image = Global.sisVentas.Desktop.My.Resources.Resources.cerrar
        Me.btnClose.Location = New System.Drawing.Point(942, 7)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(15, 15)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.btnClose.TabIndex = 4
        Me.btnClose.TabStop = False
        '
        'pnlBody
        '
        Me.pnlBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBody.Controls.Add(Me.btnPrint)
        Me.pnlBody.Controls.Add(Me.btnEdit)
        Me.pnlBody.Controls.Add(Me.txtCategoryBrand)
        Me.pnlBody.Controls.Add(Me.Label3)
        Me.pnlBody.Controls.Add(Me.txtModel)
        Me.pnlBody.Controls.Add(Me.lblValidateModel)
        Me.pnlBody.Controls.Add(Me.lblModel)
        Me.pnlBody.Controls.Add(Me.txtObservation)
        Me.pnlBody.Controls.Add(Me.lblValidateObservation)
        Me.pnlBody.Controls.Add(Me.lblObservation)
        Me.pnlBody.Controls.Add(Me.txtDiagnosis)
        Me.pnlBody.Controls.Add(Me.lblValidateDiagnosis)
        Me.pnlBody.Controls.Add(Me.lblDiagnosis)
        Me.pnlBody.Controls.Add(Me.cbxService)
        Me.pnlBody.Controls.Add(Me.lblValidateService)
        Me.pnlBody.Controls.Add(Me.lblService)
        Me.pnlBody.Controls.Add(Me.txtDeliveryDate)
        Me.pnlBody.Controls.Add(Me.lblValidateDeliveryDate)
        Me.pnlBody.Controls.Add(Me.lblDeliveryDate)
        Me.pnlBody.Controls.Add(Me.lblValidatePhone)
        Me.pnlBody.Controls.Add(Me.txtPhone)
        Me.pnlBody.Controls.Add(Me.lblPhone)
        Me.pnlBody.Controls.Add(Me.txtFailure)
        Me.pnlBody.Controls.Add(Me.lblValidateFailure)
        Me.pnlBody.Controls.Add(Me.lblFailure)
        Me.pnlBody.Controls.Add(Me.rbtRuc)
        Me.pnlBody.Controls.Add(Me.rbtDni)
        Me.pnlBody.Controls.Add(Me.btnScan)
        Me.pnlBody.Controls.Add(Me.txtPrice)
        Me.pnlBody.Controls.Add(Me.txtAppliance)
        Me.pnlBody.Controls.Add(Me.lblValidatePrice)
        Me.pnlBody.Controls.Add(Me.lblValidateAppliance)
        Me.pnlBody.Controls.Add(Me.lblPrice)
        Me.pnlBody.Controls.Add(Me.lblAppliance)
        Me.pnlBody.Controls.Add(Me.txtAddress)
        Me.pnlBody.Controls.Add(Me.txtName)
        Me.pnlBody.Controls.Add(Me.txtDocument)
        Me.pnlBody.Controls.Add(Me.txtEntryDate)
        Me.pnlBody.Controls.Add(Me.lblValidateDocument)
        Me.pnlBody.Controls.Add(Me.lblValidateEntryDate)
        Me.pnlBody.Controls.Add(Me.lblValidateAddress)
        Me.pnlBody.Controls.Add(Me.lblAddress)
        Me.pnlBody.Controls.Add(Me.lblValidateName)
        Me.pnlBody.Controls.Add(Me.lblName)
        Me.pnlBody.Controls.Add(Me.lblDocument)
        Me.pnlBody.Controls.Add(Me.lblEntryDate)
        Me.pnlBody.Controls.Add(Me.btnSave)
        Me.pnlBody.Controls.Add(Me.btnCancel)
        Me.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBody.Location = New System.Drawing.Point(0, 31)
        Me.pnlBody.Name = "pnlBody"
        Me.pnlBody.Size = New System.Drawing.Size(960, 469)
        Me.pnlBody.TabIndex = 46
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEdit.Enabled = False
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Image = Global.sisVentas.Desktop.My.Resources.Resources.edit
        Me.btnEdit.Location = New System.Drawing.Point(284, 30)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(30, 30)
        Me.btnEdit.TabIndex = 157
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'txtCategoryBrand
        '
        Me.txtCategoryBrand.Enabled = False
        Me.txtCategoryBrand.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCategoryBrand.Location = New System.Drawing.Point(746, 41)
        Me.txtCategoryBrand.Name = "txtCategoryBrand"
        Me.txtCategoryBrand.Size = New System.Drawing.Size(200, 25)
        Me.txtCategoryBrand.TabIndex = 155
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(742, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(135, 19)
        Me.Label3.TabIndex = 156
        Me.Label3.Text = "Categoria/Marca:"
        '
        'txtModel
        '
        Me.txtModel.Enabled = False
        Me.txtModel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModel.Location = New System.Drawing.Point(540, 41)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(200, 25)
        Me.txtModel.TabIndex = 152
        '
        'lblValidateModel
        '
        Me.lblValidateModel.AutoSize = True
        Me.lblValidateModel.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateModel.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateModel.Location = New System.Drawing.Point(546, 64)
        Me.lblValidateModel.Name = "lblValidateModel"
        Me.lblValidateModel.Size = New System.Drawing.Size(88, 15)
        Me.lblValidateModel.TabIndex = 154
        Me.lblValidateModel.Text = "Ingrese Modelo"
        Me.lblValidateModel.Visible = False
        '
        'lblModel
        '
        Me.lblModel.AutoSize = True
        Me.lblModel.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModel.Location = New System.Drawing.Point(536, 21)
        Me.lblModel.Name = "lblModel"
        Me.lblModel.Size = New System.Drawing.Size(66, 19)
        Me.lblModel.TabIndex = 153
        Me.lblModel.Text = "Modelo:"
        '
        'txtObservation
        '
        Me.txtObservation.Enabled = False
        Me.txtObservation.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservation.Location = New System.Drawing.Point(334, 335)
        Me.txtObservation.Multiline = True
        Me.txtObservation.Name = "txtObservation"
        Me.txtObservation.Size = New System.Drawing.Size(612, 60)
        Me.txtObservation.TabIndex = 148
        '
        'lblValidateObservation
        '
        Me.lblValidateObservation.AutoSize = True
        Me.lblValidateObservation.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateObservation.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateObservation.Location = New System.Drawing.Point(344, 392)
        Me.lblValidateObservation.Name = "lblValidateObservation"
        Me.lblValidateObservation.Size = New System.Drawing.Size(111, 15)
        Me.lblValidateObservation.TabIndex = 150
        Me.lblValidateObservation.Text = "Ingrese Observación"
        Me.lblValidateObservation.Visible = False
        '
        'lblObservation
        '
        Me.lblObservation.AutoSize = True
        Me.lblObservation.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservation.Location = New System.Drawing.Point(334, 317)
        Me.lblObservation.Name = "lblObservation"
        Me.lblObservation.Size = New System.Drawing.Size(101, 19)
        Me.lblObservation.TabIndex = 149
        Me.lblObservation.Text = "Observación:"
        '
        'txtDiagnosis
        '
        Me.txtDiagnosis.Enabled = False
        Me.txtDiagnosis.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiagnosis.Location = New System.Drawing.Point(334, 247)
        Me.txtDiagnosis.Multiline = True
        Me.txtDiagnosis.Name = "txtDiagnosis"
        Me.txtDiagnosis.Size = New System.Drawing.Size(612, 60)
        Me.txtDiagnosis.TabIndex = 145
        '
        'lblValidateDiagnosis
        '
        Me.lblValidateDiagnosis.AutoSize = True
        Me.lblValidateDiagnosis.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDiagnosis.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDiagnosis.Location = New System.Drawing.Point(344, 304)
        Me.lblValidateDiagnosis.Name = "lblValidateDiagnosis"
        Me.lblValidateDiagnosis.Size = New System.Drawing.Size(106, 15)
        Me.lblValidateDiagnosis.TabIndex = 147
        Me.lblValidateDiagnosis.Text = "Ingrese Diagnostico"
        Me.lblValidateDiagnosis.Visible = False
        '
        'lblDiagnosis
        '
        Me.lblDiagnosis.AutoSize = True
        Me.lblDiagnosis.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiagnosis.Location = New System.Drawing.Point(334, 227)
        Me.lblDiagnosis.Name = "lblDiagnosis"
        Me.lblDiagnosis.Size = New System.Drawing.Size(94, 19)
        Me.lblDiagnosis.TabIndex = 146
        Me.lblDiagnosis.Text = "Diagnóstico:"
        '
        'cbxService
        '
        Me.cbxService.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbxService.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbxService.Enabled = False
        Me.cbxService.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxService.FormattingEnabled = True
        Me.cbxService.Location = New System.Drawing.Point(334, 100)
        Me.cbxService.Name = "cbxService"
        Me.cbxService.Size = New System.Drawing.Size(406, 27)
        Me.cbxService.TabIndex = 142
        '
        'lblValidateService
        '
        Me.lblValidateService.AutoSize = True
        Me.lblValidateService.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateService.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateService.Location = New System.Drawing.Point(339, 124)
        Me.lblValidateService.Name = "lblValidateService"
        Me.lblValidateService.Size = New System.Drawing.Size(85, 15)
        Me.lblValidateService.TabIndex = 144
        Me.lblValidateService.Text = "Ingrese Servicio"
        Me.lblValidateService.Visible = False
        '
        'lblService
        '
        Me.lblService.AutoSize = True
        Me.lblService.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(338, 79)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(65, 19)
        Me.lblService.TabIndex = 143
        Me.lblService.Text = "Servicio:"
        '
        'txtDeliveryDate
        '
        Me.txtDeliveryDate.Enabled = False
        Me.txtDeliveryDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDeliveryDate.Location = New System.Drawing.Point(14, 332)
        Me.txtDeliveryDate.Name = "txtDeliveryDate"
        Me.txtDeliveryDate.Size = New System.Drawing.Size(300, 26)
        Me.txtDeliveryDate.TabIndex = 133
        '
        'lblValidateDeliveryDate
        '
        Me.lblValidateDeliveryDate.AutoSize = True
        Me.lblValidateDeliveryDate.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDeliveryDate.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDeliveryDate.Location = New System.Drawing.Point(14, 356)
        Me.lblValidateDeliveryDate.Name = "lblValidateDeliveryDate"
        Me.lblValidateDeliveryDate.Size = New System.Drawing.Size(78, 15)
        Me.lblValidateDeliveryDate.TabIndex = 135
        Me.lblValidateDeliveryDate.Text = "Ingrese Fecha"
        Me.lblValidateDeliveryDate.Visible = False
        '
        'lblDeliveryDate
        '
        Me.lblDeliveryDate.AutoSize = True
        Me.lblDeliveryDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeliveryDate.Location = New System.Drawing.Point(14, 311)
        Me.lblDeliveryDate.Name = "lblDeliveryDate"
        Me.lblDeliveryDate.Size = New System.Drawing.Size(138, 19)
        Me.lblDeliveryDate.TabIndex = 134
        Me.lblDeliveryDate.Text = "Fecha de Entrega:"
        '
        'lblValidatePhone
        '
        Me.lblValidatePhone.AutoSize = True
        Me.lblValidatePhone.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePhone.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePhone.Location = New System.Drawing.Point(14, 236)
        Me.lblValidatePhone.Name = "lblValidatePhone"
        Me.lblValidatePhone.Size = New System.Drawing.Size(83, 15)
        Me.lblValidatePhone.TabIndex = 132
        Me.lblValidatePhone.Text = "Ingrese Detalle"
        Me.lblValidatePhone.Visible = False
        '
        'txtPhone
        '
        Me.txtPhone.Enabled = False
        Me.txtPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.Location = New System.Drawing.Point(13, 211)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(300, 24)
        Me.txtPhone.TabIndex = 130
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(13, 189)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(70, 19)
        Me.lblPhone.TabIndex = 131
        Me.lblPhone.Text = "Telefono:"
        '
        'txtFailure
        '
        Me.txtFailure.Enabled = False
        Me.txtFailure.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFailure.Location = New System.Drawing.Point(334, 157)
        Me.txtFailure.Multiline = True
        Me.txtFailure.Name = "txtFailure"
        Me.txtFailure.Size = New System.Drawing.Size(612, 60)
        Me.txtFailure.TabIndex = 127
        '
        'lblValidateFailure
        '
        Me.lblValidateFailure.AutoSize = True
        Me.lblValidateFailure.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateFailure.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateFailure.Location = New System.Drawing.Point(344, 214)
        Me.lblValidateFailure.Name = "lblValidateFailure"
        Me.lblValidateFailure.Size = New System.Drawing.Size(70, 15)
        Me.lblValidateFailure.TabIndex = 129
        Me.lblValidateFailure.Text = "Ingrese Falla"
        Me.lblValidateFailure.Visible = False
        '
        'lblFailure
        '
        Me.lblFailure.AutoSize = True
        Me.lblFailure.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFailure.Location = New System.Drawing.Point(334, 137)
        Me.lblFailure.Name = "lblFailure"
        Me.lblFailure.Size = New System.Drawing.Size(126, 19)
        Me.lblFailure.TabIndex = 128
        Me.lblFailure.Text = "Falla Reportada:"
        '
        'rbtRuc
        '
        Me.rbtRuc.AutoSize = True
        Me.rbtRuc.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtRuc.Location = New System.Drawing.Point(257, 3)
        Me.rbtRuc.Name = "rbtRuc"
        Me.rbtRuc.Size = New System.Drawing.Size(57, 22)
        Me.rbtRuc.TabIndex = 1
        Me.rbtRuc.TabStop = True
        Me.rbtRuc.Text = "RUC"
        Me.rbtRuc.UseVisualStyleBackColor = True
        '
        'rbtDni
        '
        Me.rbtDni.AutoSize = True
        Me.rbtDni.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtDni.Location = New System.Drawing.Point(199, 3)
        Me.rbtDni.Name = "rbtDni"
        Me.rbtDni.Size = New System.Drawing.Size(52, 22)
        Me.rbtDni.TabIndex = 0
        Me.rbtDni.TabStop = True
        Me.rbtDni.Text = "DNI"
        Me.rbtDni.UseVisualStyleBackColor = True
        '
        'btnScan
        '
        Me.btnScan.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnScan.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnScan.FlatAppearance.BorderSize = 0
        Me.btnScan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnScan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnScan.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnScan.ForeColor = System.Drawing.Color.White
        Me.btnScan.Image = Global.sisVentas.Desktop.My.Resources.Resources.scan
        Me.btnScan.Location = New System.Drawing.Point(493, 8)
        Me.btnScan.Name = "btnScan"
        Me.btnScan.Size = New System.Drawing.Size(41, 30)
        Me.btnScan.TabIndex = 126
        Me.btnScan.UseVisualStyleBackColor = False
        '
        'txtPrice
        '
        Me.txtPrice.Enabled = False
        Me.txtPrice.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrice.Location = New System.Drawing.Point(746, 100)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(200, 25)
        Me.txtPrice.TabIndex = 9
        '
        'txtAppliance
        '
        Me.txtAppliance.Enabled = False
        Me.txtAppliance.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppliance.Location = New System.Drawing.Point(334, 41)
        Me.txtAppliance.Name = "txtAppliance"
        Me.txtAppliance.Size = New System.Drawing.Size(200, 25)
        Me.txtAppliance.TabIndex = 7
        '
        'lblValidatePrice
        '
        Me.lblValidatePrice.AutoSize = True
        Me.lblValidatePrice.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidatePrice.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidatePrice.Location = New System.Drawing.Point(751, 124)
        Me.lblValidatePrice.Name = "lblValidatePrice"
        Me.lblValidatePrice.Size = New System.Drawing.Size(78, 15)
        Me.lblValidatePrice.TabIndex = 125
        Me.lblValidatePrice.Text = "Ingrese Precio"
        Me.lblValidatePrice.Visible = False
        '
        'lblValidateAppliance
        '
        Me.lblValidateAppliance.AutoSize = True
        Me.lblValidateAppliance.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateAppliance.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateAppliance.Location = New System.Drawing.Point(337, 64)
        Me.lblValidateAppliance.Name = "lblValidateAppliance"
        Me.lblValidateAppliance.Size = New System.Drawing.Size(71, 15)
        Me.lblValidateAppliance.TabIndex = 123
        Me.lblValidateAppliance.Text = "Ingrese Serie"
        Me.lblValidateAppliance.Visible = False
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrice.Location = New System.Drawing.Point(749, 80)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(55, 19)
        Me.lblPrice.TabIndex = 121
        Me.lblPrice.Text = "Precio:"
        '
        'lblAppliance
        '
        Me.lblAppliance.AutoSize = True
        Me.lblAppliance.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliance.Location = New System.Drawing.Point(330, 21)
        Me.lblAppliance.Name = "lblAppliance"
        Me.lblAppliance.Size = New System.Drawing.Size(45, 19)
        Me.lblAppliance.TabIndex = 120
        Me.lblAppliance.Text = "Serie:"
        '
        'txtAddress
        '
        Me.txtAddress.Enabled = False
        Me.txtAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(14, 153)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(300, 24)
        Me.txtAddress.TabIndex = 4
        '
        'txtName
        '
        Me.txtName.Enabled = False
        Me.txtName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(14, 93)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(300, 24)
        Me.txtName.TabIndex = 3
        '
        'txtDocument
        '
        Me.txtDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocument.Location = New System.Drawing.Point(14, 33)
        Me.txtDocument.Name = "txtDocument"
        Me.txtDocument.Size = New System.Drawing.Size(263, 24)
        Me.txtDocument.TabIndex = 2
        '
        'txtEntryDate
        '
        Me.txtEntryDate.Enabled = False
        Me.txtEntryDate.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEntryDate.Location = New System.Drawing.Point(14, 272)
        Me.txtEntryDate.Name = "txtEntryDate"
        Me.txtEntryDate.Size = New System.Drawing.Size(300, 26)
        Me.txtEntryDate.TabIndex = 5
        '
        'lblValidateDocument
        '
        Me.lblValidateDocument.AutoSize = True
        Me.lblValidateDocument.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateDocument.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateDocument.Location = New System.Drawing.Point(14, 55)
        Me.lblValidateDocument.Name = "lblValidateDocument"
        Me.lblValidateDocument.Size = New System.Drawing.Size(167, 15)
        Me.lblValidateDocument.TabIndex = 80
        Me.lblValidateDocument.Text = "Ingrese Numero de Documento"
        Me.lblValidateDocument.Visible = False
        '
        'lblValidateEntryDate
        '
        Me.lblValidateEntryDate.AutoSize = True
        Me.lblValidateEntryDate.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateEntryDate.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateEntryDate.Location = New System.Drawing.Point(14, 296)
        Me.lblValidateEntryDate.Name = "lblValidateEntryDate"
        Me.lblValidateEntryDate.Size = New System.Drawing.Size(78, 15)
        Me.lblValidateEntryDate.TabIndex = 79
        Me.lblValidateEntryDate.Text = "Ingrese Fecha"
        Me.lblValidateEntryDate.Visible = False
        '
        'lblValidateAddress
        '
        Me.lblValidateAddress.AutoSize = True
        Me.lblValidateAddress.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateAddress.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateAddress.Location = New System.Drawing.Point(14, 175)
        Me.lblValidateAddress.Name = "lblValidateAddress"
        Me.lblValidateAddress.Size = New System.Drawing.Size(93, 15)
        Me.lblValidateAddress.TabIndex = 77
        Me.lblValidateAddress.Text = "Ingrese Dirección"
        Me.lblValidateAddress.Visible = False
        '
        'lblAddress
        '
        Me.lblAddress.AutoSize = True
        Me.lblAddress.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(14, 131)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(78, 19)
        Me.lblAddress.TabIndex = 75
        Me.lblAddress.Text = "Dirección:"
        '
        'lblValidateName
        '
        Me.lblValidateName.AutoSize = True
        Me.lblValidateName.Font = New System.Drawing.Font("Century Gothic", 7.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidateName.ForeColor = System.Drawing.Color.OrangeRed
        Me.lblValidateName.Location = New System.Drawing.Point(14, 115)
        Me.lblValidateName.Name = "lblValidateName"
        Me.lblValidateName.Size = New System.Drawing.Size(88, 15)
        Me.lblValidateName.TabIndex = 74
        Me.lblValidateName.Text = "Ingrese Nombre"
        Me.lblValidateName.Visible = False
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(14, 71)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(68, 19)
        Me.lblName.TabIndex = 68
        Me.lblName.Text = "Nombre:"
        '
        'lblDocument
        '
        Me.lblDocument.AutoSize = True
        Me.lblDocument.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocument.Location = New System.Drawing.Point(14, 11)
        Me.lblDocument.Name = "lblDocument"
        Me.lblDocument.Size = New System.Drawing.Size(71, 19)
        Me.lblDocument.TabIndex = 70
        Me.lblDocument.Text = "DNI/RUC:"
        '
        'lblEntryDate
        '
        Me.lblEntryDate.AutoSize = True
        Me.lblEntryDate.Font = New System.Drawing.Font("Century Gothic", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEntryDate.Location = New System.Drawing.Point(14, 251)
        Me.lblEntryDate.Name = "lblEntryDate"
        Me.lblEntryDate.Size = New System.Drawing.Size(140, 19)
        Me.lblEntryDate.TabIndex = 69
        Me.lblEntryDate.Text = "Fecha de Entrada:"
        '
        'btnSave
        '
        Me.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Image = Global.sisVentas.Desktop.My.Resources.Resources.guardar
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(13, 395)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(140, 45)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Guardar"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.Image = Global.sisVentas.Desktop.My.Resources.Resources.cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(173, 395)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(140, 45)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Cancelar"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Century Gothic", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.White
        Me.btnPrint.Image = Global.sisVentas.Desktop.My.Resources.Resources.print
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(549, 411)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(140, 45)
        Me.btnPrint.TabIndex = 158
        Me.btnPrint.Text = "Imprimir"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = False
        Me.btnPrint.Visible = False
        '
        'FrmSaveTechnicalReport
        '
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(960, 500)
        Me.Controls.Add(Me.pnlBody)
        Me.Controls.Add(Me.pnlTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmSaveTechnicalReport"
        Me.Opacity = 0.2R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEquipo"
        Me.pnlTitle.ResumeLayout(False)
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBody.ResumeLayout(False)
        Me.pnlBody.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents btnClose As PictureBox
    Friend WithEvents pnlTitle As Panel
    Friend WithEvents pnlBody As Panel
    Friend WithEvents toolTip As ToolTip
    Friend WithEvents btnEdit As Button
    Friend WithEvents txtCategoryBrand As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtModel As TextBox
    Friend WithEvents lblValidateModel As Label
    Friend WithEvents lblModel As Label
    Friend WithEvents txtObservation As TextBox
    Friend WithEvents lblValidateObservation As Label
    Friend WithEvents lblObservation As Label
    Friend WithEvents txtDiagnosis As TextBox
    Friend WithEvents lblValidateDiagnosis As Label
    Friend WithEvents lblDiagnosis As Label
    Friend WithEvents cbxService As ComboBox
    Friend WithEvents lblValidateService As Label
    Friend WithEvents lblService As Label
    Friend WithEvents txtDeliveryDate As DateTimePicker
    Friend WithEvents lblValidateDeliveryDate As Label
    Friend WithEvents lblDeliveryDate As Label
    Friend WithEvents lblValidatePhone As Label
    Friend WithEvents txtPhone As TextBox
    Friend WithEvents lblPhone As Label
    Friend WithEvents txtFailure As TextBox
    Friend WithEvents lblValidateFailure As Label
    Friend WithEvents lblFailure As Label
    Friend WithEvents rbtRuc As RadioButton
    Friend WithEvents rbtDni As RadioButton
    Friend WithEvents btnScan As Button
    Friend WithEvents txtPrice As TextBox
    Friend WithEvents txtAppliance As TextBox
    Friend WithEvents lblValidatePrice As Label
    Friend WithEvents lblValidateAppliance As Label
    Friend WithEvents lblPrice As Label
    Friend WithEvents lblAppliance As Label
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtDocument As TextBox
    Friend WithEvents txtEntryDate As DateTimePicker
    Friend WithEvents lblValidateDocument As Label
    Friend WithEvents lblValidateEntryDate As Label
    Friend WithEvents lblValidateAddress As Label
    Friend WithEvents lblAddress As Label
    Friend WithEvents lblValidateName As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblDocument As Label
    Friend WithEvents lblEntryDate As Label
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnPrint As Button
End Class
