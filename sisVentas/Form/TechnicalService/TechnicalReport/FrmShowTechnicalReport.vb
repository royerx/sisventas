﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
''' <summary>
''' Clase Formulario Listar Reportes Técnicos: 
''' Formulario que muestra las opciones de mantenimiento de reporte técnico
''' </summary>
Public Class FrmShowTechnicalReport

    'controlador del modelo de reporte técnico
    Private ReadOnly controller As New TechnicalReportController()

    'lista de modelo de reporte técnico con filtros
    Private dt As New List(Of TechnicalReportModel)

    'instancia del formulario para implementarlo como singleton
    Private Shared _formulario As FrmShowTechnicalReport = Nothing

    '''<summary>Constructor del formulario que inicializa los componentes y permite implemetar el singleton</summary>
    Private Sub New()
        InitializeComponent()
    End Sub

    '''<summary>Método singleton del formulario</summary>
    Public Shared Function getInstancia() As FrmShowTechnicalReport
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New FrmShowTechnicalReport
        End If
        Return _formulario
    End Function

    '''<summary>Método para cerrar el formulario</summary>
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmPrincipal.ShowPnlSelected(Nothing)
        Me.Close()
    End Sub

    '''<summary>Método lista de reportes técnicos cuando carga el formulario</summary>
    Private Sub FrmShowTechnicalReport_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormDesing.Placeholder(txtSearch, "Buscar...")
        btnNew.Visible = Auth.Can(Values.TechnicalReportCreate)
        btnEdit.Visible = Auth.Can(Values.TechnicalReportDetail)
        btnDelete.Visible = Auth.Can(Values.TechnicalReportDelete)
        btnShow.Visible = Auth.Can(Values.TechnicalReportDetail)
        rbtTotal.Checked = True
    End Sub
    Private Sub txtSearch_GotFocus(sender As Object, e As EventArgs) Handles txtSearch.GotFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    Private Sub txtSearch_LostFocus(sender As Object, e As EventArgs) Handles txtSearch.LostFocus
        FormDesing.Placeholder(sender, "Buscar...")
    End Sub

    '''<summary>Método para llenar el datagridview con los reportes técnicos filtrados</summary>
    Public Sub GetAll(search As String, beginDate As Date, endDate As Date)
        Try
            If rbtDate.Checked Then
                dt = controller.GetAll(search, beginDate, endDate)
            End If
            If rbtDates.Checked Then
                dt = controller.GetAll(search, beginDate, endDate)
            End If
            If rbtTotal.Checked Then
                dt = controller.GetAll(search)
            End If

            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Id").HeaderText = "N°"
                dtgList.Columns.Item("Client").HeaderText = "CLIENTE"
                dtgList.Columns.Item("Appliance").HeaderText = "SERIE"
                dtgList.Columns.Item("Service").HeaderText = "SERVICIO"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("EntryDate").HeaderText = "F. INGRESO"
                dtgList.Columns.Item("DeliveryDate").HeaderText = "F. ENTREGA"
                dtgList.Columns.Item("Employee").HeaderText = "EMPLEADO"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Failure").Visible = False
                dtgList.Columns.Item("Diagnosis").Visible = False
                dtgList.Columns.Item("Observation").Visible = False
                dtgList.Columns.Item("Vouchers").Visible = False
                dtgList.Columns.Item("Quantity").Visible = False
                dtgList.Columns.Item("SubTotal").Visible = False
                dtgList.Columns.Item("ApplianceCategory").Visible = False
                dtgList.Columns.Item("ApplianceBrand").Visible = False
                dtgList.Columns.Item("ApplianceModel").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para llenar el datagridview con los reportes técnicos filtrados en memoria</summary>
    Public Sub GetAllMemory(search As String, beginDate As Date, endDate As Date)
        Try
            If rbtDate.Checked Then
                dt = controller.GetAllMemory(search, beginDate, Nothing)
            End If
            If rbtDates.Checked Then
                dt = controller.GetAllMemory(search, beginDate, endDate)
            End If
            If rbtTotal.Checked Then
                dt = controller.GetAllMemory(search)
            End If
            dtgList.Columns.Item("Eliminar").Visible = False

            If dt.Count <> 0 Then

                dtgList.DataSource = dt

                dtgList.Columns.Item("Id").HeaderText = "N°"
                dtgList.Columns.Item("Client").HeaderText = "CLIENTE"
                dtgList.Columns.Item("Appliance").HeaderText = "SERIE"
                dtgList.Columns.Item("Service").HeaderText = "SERVICIO"
                dtgList.Columns.Item("Price").HeaderText = "PRECIO"
                dtgList.Columns.Item("EntryDate").HeaderText = "F. INGRESO"
                dtgList.Columns.Item("DeliveryDate").HeaderText = "F. ENTREGA"
                dtgList.Columns.Item("Employee").HeaderText = "EMPLEADO"

                txtSearch.Enabled = True
                dtgList.ColumnHeadersVisible = True
                dtgList.Columns.Item("Failure").Visible = False
                dtgList.Columns.Item("Diagnosis").Visible = False
                dtgList.Columns.Item("Observation").Visible = False
                dtgList.Columns.Item("Vouchers").Visible = False
                dtgList.Columns.Item("Quantity").Visible = False
                dtgList.Columns.Item("SubTotal").Visible = False
                dtgList.Columns.Item("ApplianceCategory").Visible = False
                dtgList.Columns.Item("ApplianceBrand").Visible = False
                dtgList.Columns.Item("ApplianceModel").Visible = False
            Else
                dtgList.DataSource = Nothing
            End If
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "ERROR", ex.Message)
        End Try
    End Sub

    '''<summary>Método para mostrar el formulario de agragar un reporte técnico</summary>
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        FrmSaveTechnicalReport.getInstancia.lblTitle.Text = "NUEVO REPORTE TÉCNICO"
        FrmSaveTechnicalReport.getInstancia.EntityModel = New TechnicalReportModel
        FrmSaveTechnicalReport.getInstancia.ShowDialog()
    End Sub

    '''<summary>Método para mostrar el formulario de modificar un reporte técnico</summary>
    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Try
            FrmSaveTechnicalReport.getInstancia.lblTitle.Text = "EDITAR REPORTE TÉCNICO"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveTechnicalReport.getInstancia.EntityModel = entityModel
            FrmSaveTechnicalReport.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Reporte Técnico", ex.Message)
        End Try

    End Sub

    '''<summary>Método para eliminar un reporte técnico o una seleccion de reportes técnicos</summary>
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim msg As String
            If Not chkDelete.Checked Then
                msg = "¿Realmente desea eliminar la Reporte Técnico N° " & dtgList.CurrentRow.Cells.Item("Id").Value & "?"
            Else
                msg = "¿Realmente desea eliminar los equipos seleccionados?"
            End If
            Dim frm As New FrmConfirm(AlertResource.ERRO, "Eliminar Reporte Técnico", msg, AlertResource.QUESTION)
            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then
                If chkDelete.Checked Then
                    Dim onekey As Integer
                    For Each row In dtgList.Rows
                        Dim cheked As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If cheked Then

                            onekey = Convert.ToInt32(row.Cells("Id").Value)
                            If controller.Delete(onekey) Then
                            Else
                                FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Reporte Técnico", "El Reporte Técnico N° " & row.Cells("Id").Value & " no fue eliminada")
                            End If
                        End If
                    Next
                    FrmMessage.ShowMessage(AlertResource.WARNING, "Reportes Técnicos Eliminadas", "Reportes Técnicos eliminados correctamente", AlertResource.SUCCESS)
                    chkDelete.Checked = False
                Else
                    If controller.Delete(dtgList.CurrentRow.Cells.Item("Id").Value) Then
                        FrmMessage.ShowMessage(AlertResource.WARNING, "Eliminar Reporte Técnico", "Reporte Técnico N° " & dtgList.CurrentRow.Cells.Item("Id").Value & " eliminada correctamente", AlertResource.SUCCESS)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Reporte Técnico", "El Reporte Técnico N° " & dtgList.CurrentRow.Cells.Item("Id").Value & " no fue eliminada")
                    End If
                End If
            Else
                chkDelete.Checked = False
            End If
        Catch ex As SqlException
            For Each err As SqlError In ex.Errors
                If err.Number = 547 Then
                    If (chkDelete.Checked) Then
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Reporte Técnico", "Uno o varios de los registros tiene elementos asignados y no se puedo eliminar, primero elimine sus asignaciones", AlertResource.INFO)
                    Else
                        FrmMessage.ShowMessage(AlertResource.ERRO, "Emilinar Reporte Técnico", "Reporte Técnico N° " & dtgList.CurrentRow.Cells.Item("Id").Value & " tiene elementos asignados y no se puede eliminar primero elimine sus asignaciones", AlertResource.INFO)
                    End If
                ElseIf (err.Number <> 3621) Then
                    FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Reporte Técnico", err.Message, AlertResource.INFO)
                End If
            Next
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Eliminar Reporte Técnico", ex.Message)
        End Try
        btnEdit.Visible = True
        btnNew.Visible = True
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    '''<summary>Método para activar o desactivar los checkbox de la tabla para eliminar los registros</summary>
    Private Sub chkDelete_CheckedChanged(sender As Object, e As EventArgs) Handles chkDelete.CheckedChanged
        If chkDelete.CheckState = CheckState.Checked Then
            dtgList.Columns.Item("Eliminar").Visible = True
            btnNew.Visible = False
            btnEdit.Visible = False
        Else
            dtgList.Columns.Item("Eliminar").Visible = False
            btnNew.Visible = True
            btnEdit.Visible = True
        End If
    End Sub

    '''<summary>Método para mostrar filtrarreportes t escribiendo en la caja de texto</summary>
    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
            GetAllMemory("", txtBeginDate.Value, txtEndDate.Value)
        Else
            GetAllMemory(txtSearch.Text, txtBeginDate.Value, txtEndDate.Value)
        End If
    End Sub

    '''<summary>Método para poder activar la seleccion de los chekbox del datagridview</summary>
    Private Sub dtgList_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgList.CellContentClick
        If e.ColumnIndex = Me.dtgList.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgList.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        ElseIf e.ColumnIndex = Me.dtgList.Columns.Item("Sheet").Index Then
            Try
                Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
                Dim receptionSheet = controller.GetReceptionSheet(entityModel.Id)
                If receptionSheet Is Nothing Then
                    FrmSaveReceptionSheet.getInstancia.lblTitle.Text = "NUEVA FICHA DE ENTREGA"
                    receptionSheet = New ReceptionSheetModel
                Else
                    FrmSaveReceptionSheet.getInstancia.lblTitle.Text = "EDITAR FICHA DE ENTREGA"
                End If
                receptionSheet.TechnicalReport = entityModel
                receptionSheet.Client = entityModel.Client
                FrmSaveReceptionSheet.getInstancia.EntityModel = receptionSheet
                FrmSaveReceptionSheet.getInstancia.ShowDialog()
            Catch ex As Exception
                FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Reporte Técnico", ex.Message)
            End Try
        End If
    End Sub

    '''<summary>Método para mostrar el formulario de reporte técnico</summary>
    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        Try
            FrmSaveTechnicalReport.getInstancia.lblTitle.Text = "EDITAR REPORTE TÉCNICO"
            Dim entityModel = controller.GetById(dtgList.CurrentRow.Cells.Item("Id").Value)
            FrmSaveTechnicalReport.getInstancia.EntityModel = entityModel
            FrmSaveTechnicalReport.getInstancia.detail = True
            FrmSaveTechnicalReport.getInstancia.ShowDialog()
        Catch ex As Exception
            FrmMessage.ShowMessage(AlertResource.ERRO, "Editar Reporte Técnico", ex.Message)
        End Try
    End Sub

    '''<summary>Método para buscar mediante un enter en la base de datos</summary>
    Private Sub txtSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If txtSearch.Text = "" Or txtSearch.Text = "Buscar..." Then
                GetAll("", txtBeginDate.Value, txtEndDate.Value)
            Else
                GetAll(txtSearch.Text, txtBeginDate.Value, txtEndDate.Value)
            End If
        End If
    End Sub

    '''<summary>Carga los datos filtrados por una fecha dada</summary>
    Private Sub rbtDate_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDate.CheckedChanged
        lblLat.Visible = False
        txtEndDate.Visible = False
        lblBegin.Visible = True
        txtBeginDate.Visible = True
        txtBeginDate.Value = Now
        GetAll("", txtBeginDate.Value, Nothing)
    End Sub

    '''<summary>Carga los datos filtrados en un rango de fechas</summary>
    Private Sub rbtDates_CheckedChanged(sender As Object, e As EventArgs) Handles rbtDates.CheckedChanged
        lblLat.Visible = True
        txtEndDate.Visible = True
        lblBegin.Visible = True
        txtBeginDate.Visible = True
        txtBeginDate.Value = Now.AddMonths(-1)
        txtEndDate.Value = Now
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    '''<summary>Carga todos los datos de los reportes técnicos</summary>
    Private Sub rbtTotal_CheckedChanged(sender As Object, e As EventArgs) Handles rbtTotal.CheckedChanged
        lblLat.Visible = False
        txtEndDate.Visible = False
        lblBegin.Visible = False
        txtBeginDate.Visible = False
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub txtBeginDate_ValueChanged(sender As Object, e As EventArgs) Handles txtBeginDate.ValueChanged
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub txtEndDate_ValueChanged(sender As Object, e As EventArgs) Handles txtEndDate.ValueChanged
        GetAll("", txtBeginDate.Value, txtEndDate.Value)
    End Sub

    Private Sub dtgList_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dtgList.CellPainting
        If e.ColumnIndex >= 0 AndAlso dtgList.Columns(e.ColumnIndex).Name = "Sheet" AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dtgList.Rows(e.RowIndex).Cells("Sheet"), DataGridViewButtonCell)
            e.Graphics.DrawImage(My.Resources.sheet, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            celBoton.ToolTipText = "Ficha de Entrega"
            celBoton.Style.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(53, Byte), Integer))
            e.Handled = True
        End If
    End Sub
End Class