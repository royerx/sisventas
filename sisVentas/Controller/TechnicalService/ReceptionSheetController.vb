﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Modelo de Ficha de Recepción: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class ReceptionSheetController

    'Declaracion del servicio de aplicacion de reporte técnico usada por el controlador
    Private ReadOnly app As AppReceptionSheetService

    Private ReadOnly appPerson As AppPersonService

    Private ReadOnly appTechnicalReport As AppTechnicalReportService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de nota de pedido</summary>
    Public Sub New()
        app = New AppReceptionSheetService()
        appPerson = New AppPersonService()
        appTechnicalReport = New AppTechnicalReportService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de fichas de recepción</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de ficha de recepcion filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ReceptionSheetModel)
        Dim sheets = app.GetAll(search)
        Dim viewModel As New List(Of ReceptionSheetModel)
        For Each item In sheets
            viewModel.Add(ReceptionSheetMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de fichas de recepción en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de ficha de recepción filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of ReceptionSheetModel)
        Dim sheets = app.GetAllMemory(search)
        Dim viewModel As New List(Of ReceptionSheetModel)
        For Each item In sheets
            viewModel.Add(ReceptionSheetMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheetModel)
        Dim sheets = app.GetAll(search, beginDate, endDate)
        Dim viewModel As New List(Of ReceptionSheetModel)
        For Each item In sheets
            viewModel.Add(ReceptionSheetMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheetModel)
        Dim sheets = app.GetAllMemory(search, beginDate, endDate)
        Dim viewModel As New List(Of ReceptionSheetModel)
        For Each item In sheets
            viewModel.Add(ReceptionSheetMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de ficha de recepción por el Id</summary>
    '''<param name="id">Identificador del modelo de ficha de recepción a ser encontrada</param>
    '''<returns>Modelo de ficha de recepción encontrada</returns>
    Public Function GetById(id As Integer) As ReceptionSheetModel
        Dim sheet = app.GetById(id)
        Return ReceptionSheetMapper.Map(sheet)
    End Function

    '''<summary>Método para agregar una ficha de recepción recibe un modelo de ficha de recepción y lo mapea a una entidad de dominio ficha de recepción </summary>
    '''<param name="entityModel">Ficha de Recepción a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As ReceptionSheetModel) As Integer
        Return app.Add(ReceptionSheetMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una ficha de recepción recibe un modelo de ficha de recepción y lo mapea a una entidad de dominio ficha de recepción </summary>
    '''<param name="entityModel">Ficha de recepción a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As ReceptionSheetModel) As Integer
        Return app.Update(ReceptionSheetMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una ficha de recepción</summary>
    '''<param name="id">Identificador de ficha de recepción a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método obtener cliente por numero de documento</summary>
    '''<param name="documentNumber">Número de documento del cliente</param>
    '''<returns>Cliente encontrado</returns>
    Public Function GetClientByDocument(documentNumber As String) As PersonModel
        Dim client = appPerson.GetByDocument(documentNumber)
        Return PersonMapper.Map(client)
    End Function

    '''<summary>Método para obtener un reporte tecnico</summary>
    '''<param name="technicalReportId">Identificador del reporte técnico</param>
    '''<returns>Reporte Técnico encontrado</returns>
    Public Function GetTechnicalReport(technicalReportId As Integer) As TechnicalReportModel
        Dim report = appTechnicalReport.GetById(technicalReportId)
        Return TechnicalReportMapper.Map(report)
    End Function

End Class
