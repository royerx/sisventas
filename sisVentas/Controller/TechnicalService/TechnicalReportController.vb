﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Modelo de Reporte Técnico: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class TechnicalReportController

    'Declaracion del servicio de aplicacion de reporte técnico usada por el controlador
    Private ReadOnly app As AppTechnicalReportService

    Private ReadOnly appPerson As AppPersonService

    Private ReadOnly appAppliance As AppApplianceService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de nota de pedido</summary>
    Public Sub New()
        app = New AppTechnicalReportService()
        appPerson = New AppPersonService()
        appAppliance = New AppApplianceService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de reportes técnicos</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de reporte técnico filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of TechnicalReportModel)
        Dim reports = app.GetAll(search)
        Dim viewModel As New List(Of TechnicalReportModel)
        For Each item In reports
            viewModel.Add(TechnicalReportMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de reporte técnico en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de reporte técnico filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of TechnicalReportModel)
        Dim reports = app.GetAllMemory(search)
        Dim viewModel As New List(Of TechnicalReportModel)
        For Each item In reports
            viewModel.Add(TechnicalReportMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReportModel)
        Dim reports = app.GetAll(search, beginDate, endDate)
        Dim viewModel As New List(Of TechnicalReportModel)
        For Each item In reports
            viewModel.Add(TechnicalReportMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReportModel)
        Dim reports = app.GetAllMemory(search, beginDate, endDate)
        Dim viewModel As New List(Of TechnicalReportModel)
        For Each item In reports
            viewModel.Add(TechnicalReportMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de reporte técnico por el Id</summary>
    '''<param name="id">Identificador del modelo de reporte técnico a ser encontrado</param>
    '''<returns>Modelo de reporte técnico encontrado</returns>
    Public Function GetById(id As Integer) As TechnicalReportModel
        Dim report = app.GetById(id)
        Return TechnicalReportMapper.Map(report)
    End Function

    '''<summary>Método para agregar un reporte técnico recibe un modelo de reporte técnico y lo mapea a una entidad de dominio reporte técnico </summary>
    '''<param name="entityModel">Reporte técnico a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As TechnicalReportModel) As Integer
        Return app.Add(TechnicalReportMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar un reporte técnico recibe un modelo de reporte técnico y lo mapea a una entidad de dominio reporte técnico </summary>
    '''<param name="entityModel">Reporte técnico a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As TechnicalReportModel) As Integer
        Return app.Update(TechnicalReportMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar un reporte técnico</summary>
    '''<param name="id">Identificador de reporte técnico a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método obtener cliente por numero de documento</summary>
    '''<param name="documentNumber">Número de documento del cliente</param>
    '''<returns>Cliente encontrado</returns>
    Public Function GetClientByDocument(documentNumber As String) As PersonModel
        Dim client = appPerson.GetByDocument(documentNumber)
        Return PersonMapper.Map(client)
    End Function

    '''<summary>Método para obtener una lista de modelo de Equipos</summary>
    '''<param name="serie">Serie que va a ser filtrada</param>
    '''<returns>Equipo de cómputo encontrado</returns>
    Public Function GetBySerie(serie As String) As ApplianceModel
        Dim appliance = appAppliance.GetBySerie(serie)
        Return ApplianceMapper.Map(appliance)
    End Function

    '''<summary>Método obtener servicios</summary>
    '''<returns>Lista de Servicios</returns>
    Function GetServices() As List(Of String)
        Return app.GetServices
    End Function

    '''<summary>Método obtener el precio de un servicio</summary>
    '''<param name="service">Nombre del servicio</param>
    '''<returns>Precio</returns>
    Function GetServicePrice(service As String) As Double
        Return app.GetServicePrice(service)
    End Function

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="model">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetByModel(model As String) As ApplianceDescriptionModel
        Return ApplianceDescriptionMapper.Map(appAppliance.GetByModel(model))
    End Function

    '''<summary>Método para agregar un equipo</summary>
    '''<param name="appliance">Equipo a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function AddAppliance(appliance As ApplianceModel) As Integer
        Return appAppliance.Add(ApplianceMapper.Map(appliance))
    End Function

    '''<summary>Método obtener ficha de recepción por reporte técnico</summary>
    '''<param name="technicalReportId">Identificador de reporte técnico</param>
    '''<returns>ficha de recepción</returns>
    Function GetReceptionSheet(technicalReportId As Integer) As ReceptionSheetModel
        Dim sheet = app.GetReceptionSheet(technicalReportId)
        Return ReceptionSheetMapper.Map(sheet)
    End Function

End Class
