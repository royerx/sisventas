﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Modelo de Entrada: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class EntryController

    'Declaracion del servicio de aplicacion de entrada usada por el controlador
    Private ReadOnly app As AppEntryService

    Private ReadOnly appPerson As AppPersonService

    Private ReadOnly appAppliance As AppApplianceService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de entrada</summary>
    Public Sub New()
        app = New AppEntryService()
        appPerson = New AppPersonService()
        appAppliance = New AppApplianceService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de entradas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de entrada filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of EntryModel)
        Dim entries = app.GetAll(search)
        Dim viewModel As New List(Of EntryModel)
        For Each item In entries
            viewModel.Add(EntryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de entrada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de entrada filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of EntryModel)
        Dim entries = app.GetAllMemory(search)
        Dim viewModel As New List(Of EntryModel)
        For Each item In entries
            viewModel.Add(EntryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of EntryModel)
        Dim entries = app.GetAll(search, beginDate, endDate)
        Dim viewModel As New List(Of EntryModel)
        For Each item In entries
            viewModel.Add(EntryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of EntryModel)
        Dim vouchers = app.GetAllMemory(search, beginDate, endDate)
        Dim viewModel As New List(Of EntryModel)
        For Each item In vouchers
            viewModel.Add(EntryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de entrada por el Id</summary>
    '''<param name="id">Identificador del modelo de entrada a ser encontrada</param>
    '''<returns>Modelo de entrada encontrada</returns>
    Public Function GetById(id As Integer) As EntryModel
        Dim entry = app.GetById(id)
        Return EntryMapper.Map(entry)
    End Function

    '''<summary>Método para agregar una nota de pedido recibe un modelo de entrada y lo mapea a una entidad de dominio entrada </summary>
    '''<param name="entityModel">Entrada a ser agregada</param>
    '''<returns>Identificador Generado</returns>
    Public Function Add(entityModel As EntryModel) As Integer
        Return app.Add(EntryMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una entrada recibe un modelo de entrada y lo mapea a una entidad de dominio entrada</summary>
    '''<param name="entityModel">Entrada a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As EntryModel) As Integer
        Return app.Update(EntryMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una entrada</summary>
    '''<param name="id">Identificador de entrada a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método obtener proveedor por numero de documento</summary>
    '''<param name="documentNumber">Número de documento del proveedor</param>
    '''<returns>Proveedor encontrado</returns>
    Public Function GetSupplierByDocument(documentNumber As String) As PersonModel
        Dim supplier = appPerson.GetByDocument(documentNumber)
        Return PersonMapper.Map(supplier)
    End Function

    '''<summary>Método para modificar un modelo de equipo y lo mapea a una entidad de dominio equipo</summary>
    '''<param name="entityModel">equipo a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function UpdateAppliance(entityModel As ApplianceModel) As Integer
        Return appAppliance.Update(ApplianceMapper.Map(entityModel))
    End Function

    '''<summary>Método para eliminar un equipo recibe un modelo de equipo y lo mapea a una entidad de dominio equipo</summary>
    '''<param name="id">Identificador de equipo</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function DeleteAppliance(id As Integer) As Integer
        Return appAppliance.Delete(id)
    End Function

End Class
