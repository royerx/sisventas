﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Modelo de Comprobante de Paog: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class VoucherController

    'Declaracion del servicio de aplicacion de comprobante de pago usada por el controlador
    Private ReadOnly app As AppVoucherService

    Private ReadOnly appOrder As AppOrderService

    Private ReadOnly appTechnicalReport As AppTechnicalReportService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de comprobante de pago</summary>
    Public Sub New()
        app = New AppVoucherService()
        appOrder = New AppOrderService()
        appTechnicalReport = New AppTechnicalReportService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de comprobante de pago</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de comprobante de pago filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of VoucherModel)
        Dim vouchers = app.GetAll(search)
        Dim viewModel As New List(Of VoucherModel)
        For Each item In vouchers
            viewModel.Add(VoucherMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de comprobante de pago en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de comprobante de pago filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of VoucherModel)
        Dim vouchers = app.GetAllMemory(search)
        Dim viewModel As New List(Of VoucherModel)
        For Each item In vouchers
            viewModel.Add(VoucherMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of VoucherModel)
        Dim vouchers = app.GetAll(search, beginDate, endDate)
        Dim viewModel As New List(Of VoucherModel)
        For Each item In vouchers
            viewModel.Add(VoucherMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of VoucherModel)
        Dim vouchers = app.GetAllMemory(search, beginDate, endDate)
        Dim viewModel As New List(Of VoucherModel)
        For Each item In vouchers
            viewModel.Add(VoucherMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de comprobante de pago por el Id</summary>
    '''<param name="id">Identificador del modelo de comprobante de pago a ser encontrada</param>
    '''<returns>Modelo decomprobante de pago encontrado</returns>
    Public Function GetById(id As Integer) As VoucherModel
        Dim voucher = app.GetById(id)
        Return VoucherMapper.Map(voucher)
    End Function

    '''<summary>Método para agregar una nota de pedido recibe un modelo de comprobante de pago y lo mapea a una entidad de dominio comprobante de pago </summary>
    '''<param name="entityModel">Comprobante de Pago a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As VoucherModel) As Integer
        Return app.Add(VoucherMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una nota de pedido recibe un modelo de comprobante de pago y lo mapea a una entidad de dominio comprobante de pago</summary>
    '''<param name="entityModel">Comprobante de pago a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As VoucherModel) As Integer
        Return app.Update(VoucherMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una nota de pedido</summary>
    '''<param name="id">Identificador de nota e pedido a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método obtener nota de pedido por id</summary>
    '''<param name="id">Identificador de nota de pedido</param>
    '''<returns>Nota de Pedido encontrada</returns>
    Public Function GetOrderById(id As Integer) As OrderModel
        Dim order = appOrder.GetById(id)
        Return OrderMapper.Map(order)
    End Function

    '''<summary>Método obtener reporte técnico por id</summary>
    '''<param name="id">Identificador de reporte técnico</param>
    '''<returns>Reporte Técnico encontrado</returns>
    Public Function GetTechnicalReportById(id As Integer) As TechnicalReportModel
        Dim technicalReport = appTechnicalReport.GetById(id)
        Return TechnicalReportMapper.Map(technicalReport)
    End Function

    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String)
        Return app.GetNumber(type)
    End Function

End Class
