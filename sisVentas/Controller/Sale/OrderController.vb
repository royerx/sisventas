﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Modelo de Nota de Pedido: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class OrderController

    'Declaracion del servicio de aplicacion de nota de pedido usada por el controlador
    Private ReadOnly app As AppOrderService

    Private ReadOnly appPerson As AppPersonService

    Private ReadOnly appAppliance As AppApplianceService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de nota de pedido</summary>
    Public Sub New()
        app = New AppOrderService()
        appPerson = New AppPersonService()
        appAppliance = New AppApplianceService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de notas de pedido</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de nota de pedido filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of OrderModel)
        Dim orders = app.GetAll(search)
        Dim viewModel As New List(Of OrderModel)
        For Each item In orders
            viewModel.Add(OrderMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de notas de pedido en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de nota de pedido filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of OrderModel)
        Dim orders = app.GetAllMemory(search)
        Dim viewModel As New List(Of OrderModel)
        For Each item In orders
            viewModel.Add(OrderMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of OrderModel)
        Dim orders = app.GetAll(search, beginDate, endDate)
        Dim viewModel As New List(Of OrderModel)
        For Each item In orders
            viewModel.Add(OrderMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of OrderModel)
        Dim orders = app.GetAllMemory(search, beginDate, endDate)
        Dim viewModel As New List(Of OrderModel)
        For Each item In orders
            viewModel.Add(OrderMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de nota de pedido por el Id</summary>
    '''<param name="id">Identificador del modelo de nota de pedido a ser encontrada</param>
    '''<returns>Modelo de nota de pedido encontrado</returns>
    Public Function GetById(id As Integer) As OrderModel
        Dim order = app.GetById(id)
        Return OrderMapper.Map(order)
    End Function

    '''<summary>Método para agregar una nota de pedido recibe un modelo de nota de pedido y lo mapea a una entidad de dominio nota de pedido </summary>
    '''<param name="entityModel">Nota de pedido a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As OrderModel) As Integer
        Return app.Add(OrderMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una nota de pedido recibe un modelo de nota de pedido y lo mapea a una entidad de dominio nota de pedido </summary>
    '''<param name="entityModel">Nota de pedido a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As OrderModel) As Integer
        Return app.Update(OrderMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una nota de pedido</summary>
    '''<param name="id">Identificador de nota e pedido a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método obtener cliente por numero de documento</summary>
    '''<param name="documentNumber">Número de documento del cliente</param>
    '''<returns>Cliente encontrado</returns>
    Public Function GetClientByDocument(documentNumber As String) As PersonModel
        Dim client = appPerson.GetByDocument(documentNumber)
        Return PersonMapper.Map(client)
    End Function

    '''<summary>Método para obtener una lista de modelo de Equipos en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de nota de pedido filtrada</returns>
    Public Function GetBySerieMemory(search As String) As ApplianceModel
        Dim appliance = appAppliance.GetBySerieMemory(search)
        Return ApplianceMapper.Map(appliance)
    End Function

    '''<summary>Método agregar y verificar duplicado de series</summary>
    '''<param name="orderDetails">Lista de detalles de nota de pedido</param>
    '''<param name="detail">detalle a ser agregago</param>
    '''<returns> si hay repetidos o no</returns>
    Function AddAppliance(orderDetails As List(Of OrderDetailModel), detail As OrderDetailModel) As IEnumerable(Of OrderDetailModel)
        Dim detailsModel = New List(Of OrderDetail)
        If orderDetails IsNot Nothing Then
            For Each item In orderDetails
                detailsModel.Add(OrderDetailMapper.Map(item))
            Next
        End If
        Dim details = app.AddAppliance(detailsModel, OrderDetailMapper.Map(detail))
        Dim viewModel As New List(Of OrderDetailModel)
        For Each item In details
            viewModel.Add(OrderDetailMapper.Map(item))
        Next
        Return viewModel
    End Function

End Class
