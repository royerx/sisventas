﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Equipo de Cómputo: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class ApplianceDescriptionController

    'Declaracion del servicio de aplicacion de descripción equipo de computo usada por el controlador
    Private ReadOnly app As AppApplianceDescriptionService

    Private ReadOnly appCategory As AppCategoryService

    Private ReadOnly appBrand As AppBrandService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de equipo de cómputo</summary>
    Public Sub New()
        app = New AppApplianceDescriptionService()
        appCategory = New AppCategoryService()
        appBrand = New AppBrandService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de equipos de cómputo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de equipos de cómputo filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ApplianceDescriptionModel)
        Dim applianceDescriptions = app.GetAll(search)
        Dim viewModel As New List(Of ApplianceDescriptionModel)
        For Each item In applianceDescriptions
            viewModel.Add(ApplianceDescriptionMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de equipos de cómputo en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de equipos de cómputo filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of ApplianceDescriptionModel)
        Dim applianceDescriptions = app.GetAllMemory(search)
        Dim viewModel As New List(Of ApplianceDescriptionModel)
        For Each item In applianceDescriptions
            viewModel.Add(ApplianceDescriptionMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de equipos de computo por el Id</summary>
    '''<param name="id">Identificador del modelo de equipo de computo a ser encontrada</param>
    '''<returns>Modelo de equipo de computo encontrado</returns>
    Public Function GetById(id As Integer) As ApplianceDescriptionModel
        Dim applianceDescription = app.GetById(id)
        Return ApplianceDescriptionMapper.Map(applianceDescription)
    End Function

    '''<summary>Método para agregar un equipo de computo recibe un modelo de Equipo de computo y lo mapea a una entidad de dominio equipo de computo </summary>
    '''<param name="entityModel">Equipo de computo a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As ApplianceDescriptionModel) As Integer
        Return app.Add(ApplianceDescriptionMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar un equipo de computo recibe un modelo de equipo de computo y lo mapea a una entidad de dominio equipo de computo </summary>
    '''<param name="entityModel">Equipo de Cómputo a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As ApplianceDescriptionModel) As Integer
        Return app.Update(ApplianceDescriptionMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar un equipo de computo</summary>
    '''<param name="id">Identificador de equipo de computo a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Mostrar categorias</summary>
    '''<returns>Lista de categorias</returns>
    Public Function GetCategories() As IEnumerable(Of CategoryModel)
        Dim categories = appCategory.GetAll("")
        Dim viewModel As New List(Of CategoryModel)
        For Each item In categories
            viewModel.Add(CategoryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Mostrar marcas</summary>
    '''<returns>Lista de marcas</returns>
    Public Function GetBrands() As IEnumerable(Of BrandModel)
        Dim brands = appBrand.GetAll("")
        Dim viewModel As New List(Of BrandModel)
        For Each item In brands
            viewModel.Add(BrandMapper.Map(item))
        Next
        Return viewModel
    End Function

End Class
