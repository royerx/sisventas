﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Detalle de Equipo de Cómputo: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class ApplianceController

    'Declaracion del servicio de aplicacion de equipo de computo usada por el controlador
    Private ReadOnly app As AppApplianceService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de equipo de cómputo</summary>
    Public Sub New()
        app = New AppApplianceService()
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, applianceDescriptionId As Integer) As IEnumerable(Of ApplianceModel)
        Dim appliances = app.GetAll(search, applianceDescriptionId)
        Dim viewModel As New List(Of ApplianceModel)
        For Each item In appliances
            viewModel.Add(ApplianceMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guardada en Memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String, applianceDescriptionId As Integer) As IEnumerable(Of ApplianceModel)
        Dim appliances = app.GetAllMemory(search, applianceDescriptionId)
        Dim viewModel As New List(Of ApplianceModel)
        For Each item In appliances
            viewModel.Add(ApplianceMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de detalle de equipos de computo por el Id</summary>
    '''<param name="id">Identificador del modelo de detalle de equipo de computo a ser encontrada</param>
    '''<returns>Modelo de detalle de equipo de computo encontrado</returns>
    Public Function GetById(id As Integer) As ApplianceModel
        Dim appliance = app.GetById(id)
        Return ApplianceMapper.Map(appliance)
    End Function

    '''<summary>Método para agregar un detalle de equipo de computo recibe un modelo de Detalle de Equipo de computo y lo mapea a una entidad de dominio detalle de equipo de computo </summary>
    '''<param name="entityModel">Detalle de Equipo de computo a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As ApplianceModel) As Integer
        Return app.Add(ApplianceMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar un detalle de equipo de computo recibe un modelo de detalle de equipo de computo y lo mapea a una entidad de detalle de dominio equipo de computo </summary>
    '''<param name="entityModel">Detalle de Euipo de Cómputo a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As ApplianceModel) As Integer
        Return app.Update(ApplianceMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar un detalle de equipo de computo</summary>
    '''<param name="id">Identificador de detalle de equipo de computo a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

End Class
