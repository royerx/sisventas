﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Marca: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class BrandController

    'Declaracion del servicio de aplicacion de marca usada por el controlador
    Private ReadOnly app As AppBrandService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de marca</summary>
    Public Sub New()
        app = New AppBrandService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de marcas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de categorias filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of BrandModel)
        Dim brands = app.GetAll(search)
        Dim viewModel As New List(Of BrandModel)
        For Each item In brands
            viewModel.Add(BrandMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de marcas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de categorias filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of BrandModel)
        Dim brands = app.GetAllMemory(search)
        Dim viewModel As New List(Of BrandModel)
        For Each item In brands
            viewModel.Add(BrandMapper.Map(item))
        Next
        Return viewModel
    End Function


    '''<summary>Método para obtener un modelo de marca por el Id</summary>
    '''<param name="id">Identificador del modelo de marca a ser encontrada</param>
    '''<returns>Modelo de categoria encontrado</returns>
    Public Function GetById(id As Integer) As BrandModel
        Dim brand = app.GetById(id)
        Return BrandMapper.Map(brand)
    End Function

    '''<summary>Método para agregar una marca recibe un modelo de marca y lo mapea a una entidad de dominio marca </summary>
    '''<param name="entityModel">Categoria a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As BrandModel) As Integer
        Return app.Add(BrandMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una marca recige un modelo de marca y lo mapea a una entidad de dominio marca </summary>
    '''<param name="entityModel">Categoria a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As BrandModel) As Integer
        Return app.Update(BrandMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una marca</summary>
    '''<param name="id">Identificador de la marca a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function
End Class
