﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Categoria: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class CategoryController

    'Declaracion del servicio de aplicacion de categoria usada por el controlador
    Private ReadOnly app As AppCategoryService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de categoria</summary>
    Public Sub New()
        app = New AppCategoryService()
    End Sub


    '''<summary>Método para obtener una lista de modelo de categorias</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de categorias filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of CategoryModel)
        Dim categories = app.GetAll(search)
        Dim viewModel As New List(Of CategoryModel)
        For Each item In categories
            viewModel.Add(CategoryMapper.Map(item))
        Next
        Return viewModel
    End Function


    '''<summary>Método para obtener una lista de modelo de categorias en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de categorias filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of CategoryModel)
        Dim categories = app.GetAllMemory(search)
        Dim viewModel As New List(Of CategoryModel)
        For Each item In categories
            viewModel.Add(CategoryMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de categoria por el Id</summary>
    '''<param name="id">Identificador del modelo de catgoria a ser encontrada</param>
    '''<returns>Modelo de categoria encontrado</returns>
    Public Function GetById(id As Integer) As CategoryModel
        Dim category = app.GetById(id)
        Return CategoryMapper.Map(category)
    End Function

    '''<summary>Método para agregar una categoria recibe un modelo de categoria y lo mapea a una entidad de dominio categoria </summary>
    '''<param name="entityModel">Categoria a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As CategoryModel) As Integer
        Return app.Add(CategoryMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una categoria recige un modelo de categoria y lo mapea a una entidad de dominio categoria </summary>
    '''<param name="entityModel">Categoria a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As CategoryModel) As Integer
        Return app.Update(CategoryMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una categoria</summary>
    '''<param name="id">Identificador de la categoria a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function
End Class
