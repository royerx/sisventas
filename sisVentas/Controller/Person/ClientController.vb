﻿Imports sisVentas.Application
Imports sisVentas.CrossCutting

''' <summary>
''' Clase Controlador de Modelo de Persona que representa un Cliente: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class ClientController

    'Declaracion del servicio de aplicacion de persona usada por el controlador
    Private ReadOnly app As AppPersonService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de persona</summary>
    Public Sub New()
        app = New AppPersonService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de persona</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de persona filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of PersonModel)
        Dim persons = app.GetAll(search, Values.CLIENT)
        Dim viewModel As New List(Of PersonModel)
        For Each item In persons
            viewModel.Add(PersonMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de persona en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de persona filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of PersonModel)
        Dim persons = app.GetAllMemory(search, Values.CLIENT)
        Dim viewModel As New List(Of PersonModel)
        For Each item In persons
            viewModel.Add(PersonMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de persona por el Id</summary>
    '''<param name="id">Identificador del modelo de persona a ser encontrada</param>
    '''<returns>Modelo de persona encontrado</returns>
    Public Function GetById(id As Integer) As PersonModel
        Dim person = app.GetById(id)
        Return PersonMapper.Map(person)
    End Function

    '''<summary>Método para agregar una persona recibe un modelo de persona y lo mapea a una entidad de dominio persona </summary>
    '''<param name="entityModel">Persona a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As PersonModel) As Integer
        Return app.Add(PersonMapper.Map(entityModel), Values.ClientId)
    End Function

    '''<summary>Método para modificar una persona recige un modelo de persona y lo mapea a una entidad de dominio persona </summary>
    '''<param name="entityModel">Persona a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As PersonModel) As Integer
        Return app.Update(PersonMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una persona</summary>
    '''<param name="personId">Identificador de la persona a ser eliminada</param>
    '''<param name="typeId">Identificador de Tipo de persona</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(personId As Integer, typeId As Integer) As Integer
        Return app.Delete(personId, typeId)
    End Function


    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As PersonModel
        Dim person = app.GetByDocument(document)
        Return PersonMapper.Map(person)
    End Function
End Class
