﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Tipo de Persona: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class PersonTypeController

    'Declaracion del servicio de aplicacion de tipo de persona usada por el controlador
    Private ReadOnly app As AppPersonTypeService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de tipo de persona</summary>
    Public Sub New()
        app = New AppPersonTypeService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de tipos de persona</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de tipos de persona filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of PersonTypeModel)
        Dim personTypes = app.GetAll(search)
        Dim viewModel As New List(Of PersonTypeModel)
        For Each item In personTypes
            viewModel.Add(PersonTypeMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de tipos de persona en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de tipos de persona filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of PersonTypeModel)
        Dim personTypes = app.GetAllMemory(search)
        Dim viewModel As New List(Of PersonTypeModel)
        For Each item In personTypes
            viewModel.Add(PersonTypeMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de tipo de persona por el Id</summary>
    '''<param name="id">Identificador del modelo de tipo de persona a ser encontrada</param>
    '''<returns>Modelo de tipo de persona encontrado</returns>
    Public Function GetById(id As Integer) As PersonTypeModel
        Dim personType = app.GetById(id)
        Return PersonTypeMapper.Map(personType)
    End Function

    '''<summary>Método para agregar una tipo de persona recibe un modelo de tipo de persona y lo mapea a una entidad de dominio tipo de persona </summary>
    '''<param name="entityModel">Tipo de Persona a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As PersonTypeModel) As Integer
        Return app.Add(PersonTypeMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar una tipo de persona recige un modelo de tipo de persona y lo mapea a una entidad de dominio tipo de persona </summary>
    '''<param name="entityModel">Tipo de Persona a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As PersonTypeModel) As Integer
        Return app.Update(PersonTypeMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una tipo de persona</summary>
    '''<param name="id">Identificador de la tipo de persona a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function
End Class
