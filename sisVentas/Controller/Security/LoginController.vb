﻿Imports sisVentas.Application
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Controlador de Logeo: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class LoginController

    'Declaracion del servicio de aplicacion de autorización usada por el controlador
    Private ReadOnly app As AppAuthService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de empleado</summary>
    Public Sub New()
        app = New AppAuthService()
    End Sub

    '''<summary>Método iniciar sesion</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Public Function Login(user As String, pass As String) As Boolean
        Return app.Login(user, pass)
    End Function

    '''<summary>Método inicializar rol escogido</summary>
    '''<param name="role">Rol seleccionado</param>
    Public Sub SelectedRole(role As Role)
        app.SelectedRole(role)
    End Sub
End Class
