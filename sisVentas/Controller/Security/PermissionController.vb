﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Permiso: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class PermissionController

    'Declaracion del servicio de aplicacion de permiso usada por el controlador
    Private ReadOnly app As AppPermissionService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de permiso</summary>
    Public Sub New()
        app = New AppPermissionService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de permiso</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de permiso filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of PermissionModel)
        Dim permissions = app.GetAll(search)
        Dim viewModel As New List(Of PermissionModel)
        For Each item In permissions
            viewModel.Add(PermissionMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de permiso en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de permiso filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of PermissionModel)
        Dim permissions = app.GetAllMemory(search)
        Dim viewModel As New List(Of PermissionModel)
        For Each item In permissions
            viewModel.Add(PermissionMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de permiso por el Id</summary>
    '''<param name="id">Identificador del modelo de permiso a ser encontrado</param>
    '''<returns>Modelo de permiso encontrado</returns>
    Public Function GetById(id As Integer) As PermissionModel
        Dim permission = app.GetById(id)
        Return PermissionMapper.Map(permission)
    End Function

    '''<summary>Método para agregar un permiso recibe un modelo de permiso y lo mapea a una entidad de dominio permiso </summary>
    '''<param name="entityModel">permiso a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As PermissionModel) As Integer
        Return app.Add(PermissionMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar un permiso recige un modelo de permiso y lo mapea a una entidad de dominio permiso </summary>
    '''<param name="entityModel">permiso a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As PermissionModel) As Integer
        Return app.Update(PermissionMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar un permiso</summary>
    '''<param name="id">Identificador de la permiso a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método listar tablas</summary>
    '''<returns>Lista de nombres de tablas de la base de datos</returns>
    Public Function Tables() As IEnumerable(Of String)
        Return app.Tables()
    End Function

End Class
