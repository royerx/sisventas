﻿Imports sisVentas.Application
Imports sisVentas.CrossCutting

''' <summary>
''' Clase Controlador de Modelo de Empleado: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class EmployeeController

    'Declaracion del servicio de aplicacion de empleado usada por el controlador
    Private ReadOnly app As AppEmployeeService

    Private ReadOnly appRole As AppRoleService

    Private ReadOnly appPerson As AppPersonService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de empleado</summary>
    Public Sub New()
        app = New AppEmployeeService()
        appRole = New AppRoleService()
        appPerson = New AppPersonService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de empleado</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de empleado filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of EmployeeModel)
        Dim employees = app.GetAll(search)
        Dim viewModel As New List(Of EmployeeModel)
        For Each item In employees
            viewModel.Add(EmployeeMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de empleado en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de empleado filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of EmployeeModel)
        Dim employees = app.GetAllMemory(search)
        Dim viewModel As New List(Of EmployeeModel)
        For Each item In employees
            viewModel.Add(EmployeeMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de empleado por el Id</summary>
    '''<param name="id">Identificador del modelo de empleado a ser encontrado</param>
    '''<returns>Modelo de empleado encontrado</returns>
    Public Function GetById(id As Integer) As EmployeeModel
        Dim employee = app.GetById(id)
        Return EmployeeMapper.Map(employee)
    End Function

    '''<summary>Método para agregar una empleado recibe un modelo de empleado y lo mapea a una entidad de dominio empleado </summary>
    '''<param name="entityModel">Empleado a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As EmployeeModel) As Integer
        Return app.Add(EmployeeMapper.Map(entityModel), Values.EmployeeId)
    End Function

    '''<summary>Método para modificar una empleado recige un modelo de empleado y lo mapea a una entidad de dominio empleado </summary>
    '''<param name="entityModel">empleado a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As EmployeeModel) As Integer
        Return app.Update(EmployeeMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar una empleado</summary>
    '''<param name="id">Identificador de la empleado a ser eliminada</param>
    '''<param name="typeId">Identificador de tipo de persona</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer, typeId As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Mostrar roles</summary>
    '''<returns>Lista de Roles</returns>
    Public Function GetRoles() As IEnumerable(Of RoleModel)
        Dim roles = appRole.GetAll("")
        Dim viewModel As New List(Of RoleModel)
        For Each item In roles
            viewModel.Add(RoleMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As PersonModel
        Dim person = appPerson.GetByDocument(document)
        Return PersonMapper.Map(person)
    End Function

    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="rolesId">Identificadores de Roles</param>
    '''<returns>Filas afectadas</returns>
    Function AddRoles(employeeId As Integer, rolesId As List(Of Integer)) As Integer
        Return app.AddRoles(employeeId, rolesId)
    End Function

End Class
