﻿Imports sisVentas.Application

''' <summary>
''' Clase Controlador de Modelo de Rol: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class RoleController

    'Declaracion del servicio de aplicacion de rol usada por el controlador
    Private ReadOnly app As AppRoleService

    'Declaracion del servicio de aplicacion de permiso usada por el controlador
    Private ReadOnly appPermission As AppPermissionService

    '''<summary>Constructor de la clase que inyecta un servicio de aplicacion de rol</summary>
    Public Sub New()
        app = New AppRoleService()
        appPermission = New AppPermissionService()
    End Sub

    '''<summary>Método para obtener una lista de modelo de rol</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de rol filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of RoleModel)
        Dim roles = app.GetAll(search)
        Dim viewModel As New List(Of RoleModel)
        For Each item In roles
            viewModel.Add(RoleMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener una lista de modelo de rol en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de modelo de rol filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of RoleModel)
        Dim roles = app.GetAllMemory(search)
        Dim viewModel As New List(Of RoleModel)
        For Each item In roles
            viewModel.Add(RoleMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Método para obtener un modelo de rol por el Id</summary>
    '''<param name="id">Identificador del modelo de rol a ser encontrado</param>
    '''<returns>Modelo de rol encontrado</returns>
    Public Function GetById(id As Integer) As RoleModel
        Dim role = app.GetById(id)
        Return RoleMapper.Map(role)
    End Function

    '''<summary>Método para agregar un rol recibe un modelo de rol y lo mapea a una entidad de dominio rol </summary>
    '''<param name="entityModel">rol a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entityModel As RoleModel) As Integer
        Return app.Add(RoleMapper.Map(entityModel))
    End Function

    '''<summary>Método para modificar un rol recige un modelo de rol y lo mapea a una entidad de dominio rol </summary>
    '''<param name="entityModel">rol a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entityModel As RoleModel) As Integer
        Return app.Update(RoleMapper.Map(entityModel))
    End Function

    '''<summary>Método eliminar un rol</summary>
    '''<param name="id">Identificador de la rol a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return app.Delete(id)
    End Function

    '''<summary>Método para obtener una lista de modelo de permiso</summary>
    '''<returns>Lista de modelo de permiso</returns>
    Public Function GetAllPermissions() As IEnumerable(Of PermissionModel)
        Dim permissions = appPermission.GetAll("")
        Dim viewModel As New List(Of PermissionModel)
        For Each item In permissions
            viewModel.Add(PermissionMapper.Map(item))
        Next
        Return viewModel
    End Function

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="permissionIds">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Public Function SavePermissions(roleId As Integer, permissionIds As IEnumerable(Of Integer)) As Integer
        Return app.SavePermissions(roleId, permissionIds)
    End Function

End Class
