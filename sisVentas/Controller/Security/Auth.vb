﻿Imports sisVentas.Application
''' <summary>
''' Clase Controlador para autorizacion: 
''' Implementa los metodos base para manipular los servicio de aplicación por la capa de presentacion
''' </summary>
Public Class Auth
    '''<summary>Método verificar permiso</summary>
    '''<param name="permission">Slug del permiso a verificar</param>
    '''<returns>Si tiene o no el permiso</returns>
    Public Shared Function Can(permission As String) As Boolean
        Dim app = New AppAuthService
        Return app.Can(permission)
    End Function

    '''<summary>Método para enviar correo de recuperacion de contraseña a un empleado</summary>
    '''<param name="email">Correo Electrónico Empleado</param>
    '''<returns>Mensaje de Cofirmacion</returns>
    Function GetPassByEmail(email As String) As String
        Dim app = New AppAuthService
        Return app.GetPassByEmail(email)
    End Function

End Class
