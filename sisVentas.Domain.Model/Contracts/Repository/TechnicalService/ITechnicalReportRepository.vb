﻿''' <summary>
''' Interfaz para Repositorio de Reporte Técnico: 
''' Define una interfaz para la implementacion del Repositorio de Reporte Técnico
''' </summary>
''' <inheritdoc/>
Public Interface ITechnicalReportRepository
    Inherits IGenericRepository(Of TechnicalReport)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport)

    '''<summary>Método obtener entidad mediante identificador comprobante de pago</summary>
    '''<param name="id">Identificador comprobante de pago</param>
    '''<returns>Reporte Técnico</returns>
    Function GetByVoucherId(id As Integer) As TechnicalReport

    '''<summary>Método obtener servicios</summary>
    '''<returns>Lista de Servicios</returns>
    Function GetServices() As List(Of String)

    '''<summary>Método obtener el precio de un servicio</summary>
    '''<param name="service">Nombre del servicio</param>
    '''<returns>Precio</returns>
    Function GetServicePrice(service As String) As Double

    '''<summary>Método obtener ficha de recepción por reporte técnico</summary>
    '''<param name="technicalReportId">Identificador de reporte técnico</param>
    '''<returns>ficha de recepción</returns>
    Function GetReceptionSheet(technicalReportId As Integer) As ReceptionSheet

End Interface
