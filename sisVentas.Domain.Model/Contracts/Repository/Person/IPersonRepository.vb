﻿''' <summary>
''' Interfaz para Repositorio de Persona: 
''' Define una interfaz para la implementacion del Repositorio de Persona
''' </summary>
''' <inheritdoc/>
Public Interface IPersonRepository
    Inherits IGenericRepository(Of Person)

    '''<summary>Método para obtener una lista de personas mediate el tipo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Overloads Function GetAll(search As String, type As String) As IEnumerable(Of Person)

    '''<summary>Método verificar si una persona tiene tipos asignados</summary>
    '''<param name="id">Id de persona</param>
    '''<returns>Cantidad de registros</returns>
    Overloads Function HasPersonTypes(id As Integer) As Integer

    '''<summary>Método para asignat tipo de persona</summary>
    '''<param name="personId">Identificador de persona</param>
    '''<param name="typeId">Identificador de Tipo de Persona</param>
    '''<returns>Filas afectadas</returns>
    Function AddTypePerson(personId As Integer, typeId As Integer) As Integer

    '''<summary>Método eliminar el tipo de persona asignado</summary>
    '''<param name="typeId">Identificador de Tipo de Persona</param>
    '''<param name="personId">Identificador de persona</param>
    '''<returns>Cantidad e Filas Afectadas</returns>
    Function DeleteTypePerson(personId As Integer, typeId As Integer) As Integer

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As Person

    '''<summary>Método verificar si tiene asignación de tipo de persona</summary>
    '''<param name="personId">Identificador de Persona</param>
    '''<param name="typeId">Identificador Tipo de Persona</param>
    '''<returns>si cuenta o no con el tipo</returns>
    Function HasType(personId As Integer, typeId As Integer) As Boolean

End Interface
