﻿''' <summary>
''' Interfaz para Repositorio de Tipo de Persona: 
''' Define una interfaz para la implementacion del Repositorio de Tipo de Persona
''' </summary>
''' <inheritdoc/>
Public Interface IPersonTypeRepository
    Inherits IGenericRepository(Of PersonType)

End Interface
