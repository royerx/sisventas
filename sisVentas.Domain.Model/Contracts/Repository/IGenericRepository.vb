﻿''' <summary>
''' Interfaz Generica para Repositorios: 
''' Define una interfaz generica para la implementacion de los repositorios 
''' </summary>
''' <typeparam name="Entity">Entidad que va a implementar el repositorio</typeparam>
Public Interface IGenericRepository(Of Entity As Class)

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Function GetAll(search As String) As IEnumerable(Of Entity)

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Function GetById(id As Integer) As Entity

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="entity">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function Add(entity As Entity) As Integer

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="entity">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function Update(entity As Entity) As Integer

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function Delete(id As Integer) As Integer
End Interface
