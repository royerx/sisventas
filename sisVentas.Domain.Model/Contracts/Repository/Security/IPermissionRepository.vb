﻿''' <summary>
''' Interfaz para Repositorio de Permiso: 
''' Define una interfaz para la implementacion del Repositorio de Permiso
''' </summary>
''' <inheritdoc/>
Public Interface IPermissionRepository
    Inherits IGenericRepository(Of Permission)

    '''<summary>Método obtener permisos mediante identificador de rol</summary>
    '''<param name="id">Identificador del Rol</param>
    '''<returns>Lista de Permisos</returns>
    Function GetByRoleId(id As Integer) As IEnumerable(Of Permission)

    '''<summary>Método listar tablas de la base de datos</summary>
    '''<returns>Lista de nombres de tablas</returns>
    Function Tables() As IEnumerable(Of String)

End Interface
