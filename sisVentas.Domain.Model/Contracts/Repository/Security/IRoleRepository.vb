﻿''' <summary>
''' Interfaz para Repositorio de Rol: 
''' Define una interfaz para la implementacion del Repositorio de Rol
''' </summary>
''' <inheritdoc/>
Public Interface IRoleRepository
    Inherits IGenericRepository(Of Role)

    '''<summary>Método listar roles por identificador de empleado</summary>
    '''<param name="id">Identificador del Empleado</param>
    '''<returns>Roles que tiene el empleado</returns>
    Function GetByEmployeeId(id As Integer) As IEnumerable(Of Role)

    '''<summary>Método eliminar permisos de un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<returns>Numero de filas afectadas</returns>
    Function DeletePermissions(roleId As Integer) As Integer

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="ids">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Function SavePermissions(roleId As Integer, ids As IEnumerable(Of Integer)) As Integer

End Interface
