﻿''' <summary>
''' Interfaz para Repositorio de Empleado: 
''' Define una interfaz para la implementacion del Repositorio de Empleado
''' </summary>
''' <inheritdoc/>
Public Interface IEmployeeRepository
    Inherits IGenericRepository(Of Employee)

    '''<summary>Método iniciar sesion a un empleado</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Function Login(user As String, pass As String) As Integer

    '''<summary>Método para verificar permiso</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="permission">Slug del permiso a verificar</param>
    '''<returns>Si tiene el permiso o no</returns>
    Function Can(employeeId As Integer, roleId As Integer, permission As String) As Boolean

    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="roleId">Identificador del Rol</param>
    '''<returns>Filas afectadas</returns>
    Function AddRole(employeeId As Integer, roleId As Integer) As Integer

    '''<summary>Método eliminar roles</summary>
    '''<param name="id">Identificador del Empleado</param>
    '''<returns>Filas afectadas</returns>
    Function DeleteRoles(id As Integer) As Integer

    '''<summary>Método obtener empleado por correo</summary>
    '''<param name="email">Correo electrónico</param>
    '''<returns>Mensaje de confirmacion de envio</returns>
    Function GetPassByEmail(email As String) As String

End Interface
