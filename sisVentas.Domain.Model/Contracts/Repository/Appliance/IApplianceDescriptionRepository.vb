﻿''' <summary>
''' Interfaz para Repositorio de Equipo de Cómputo: 
''' Define una interfaz para la implementacion del Repositorio de Equipo de Cómputo
''' </summary>
''' <inheritdoc/>
Public Interface IApplianceDescriptionRepository
    Inherits IGenericRepository(Of ApplianceDescription)

    '''<summary>Método para obtener el stock</summary>
    '''<param name="applianceDescriptionId">Identificador del equipo de computo</param>
    '''<returns>Cantidad de equipos disponibles</returns>
    Function GetStock(applianceDescriptionId As Integer) As Integer

End Interface
