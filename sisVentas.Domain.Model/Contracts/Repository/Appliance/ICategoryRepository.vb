﻿''' <summary>
''' Interfaz para Repositorio de Categoria: 
''' Define una interfaz para la implementacion del Repositorio de Categoria
''' </summary>
''' <inheritdoc/>
Public Interface ICategoryRepository
    Inherits IGenericRepository(Of Category)

End Interface
