﻿''' <summary>
''' Interfaz para Repositorio de Detalle de Equipo de Cómputo: 
''' Define una interfaz para la implementacion del Repositorio de Detalle Equipo de Cómputo
''' </summary>
''' <inheritdoc/>
Public Interface IApplianceRepository
    Inherits IGenericRepository(Of Appliance)

    '''<summary>Método para obtener una lista de registros de detalle de equipos de computo que pertenecen a una descripcion</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance)

    '''<summary>Método para obtener una lista de registros de detalle de equipos de computo que pertenecen a una descripcion</summary>
    '''<param name="applianceId">Identificador de detalle de equipo de computo</param>
    '''<param name="state">Estado a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function ChangeSatate(applianceId As Integer, state As Boolean) As Integer

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetBySerie(search As String) As Appliance

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="model">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetByModel(model As String) As ApplianceDescription

    '''<summary>Método eliminar por Identificador de Entrada</summary>
    '''<param name="id">Identificador de la entrada</param>
    '''<returns> cantidad de filas afectadas</returns>
    Function DeleteByEntryId(id As Integer) As Integer

End Interface
