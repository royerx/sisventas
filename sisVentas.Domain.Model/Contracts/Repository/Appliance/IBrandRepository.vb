﻿''' <summary>
''' Interfaz para Repositorio de Marca: 
''' Define una interfaz para la implementacion del Repositorio de Marca
''' </summary>
''' <inheritdoc/>
Public Interface IBrandRepository
    Inherits IGenericRepository(Of Brand)

End Interface
