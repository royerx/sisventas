﻿''' <summary>
''' Interfaz para Repositorio de Entrada: 
''' Define una interfaz para la implementacion del Repositorio de Entrada
''' </summary>
''' <inheritdoc/>
Public Interface IEntryRepository
    Inherits IGenericRepository(Of Entry)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry)

    '''<summary>Método para obtener una lista de registros de Equipos asignados</summary>
    '''<param name="id">Identificador de Entrada</param>
    '''<returns>Lista de equipos</returns>
    Overloads Function GetAppliances(id As Integer) As IEnumerable(Of Appliance)

End Interface

