﻿''' <summary>
''' Interfaz para Repositorio de Nota de Pedido: 
''' Define una interfaz para la implementacion del Repositorio de Nota de Pedido
''' </summary>
''' <inheritdoc/>
Public Interface IOrderRepository
    Inherits IGenericRepository(Of Order)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order)

    '''<summary>Método obtener entidad mediante identificador comprobante de pago</summary>
    '''<param name="id">Identificador comprobante de pago</param>
    '''<returns>Nota de Pedido</returns>
    Function GetByVoucherId(id As Integer) As Order

End Interface
