﻿''' <summary>
''' Interfaz para Repositorio de Detalle de Nota de Pedido: 
''' Define una interfaz para la implementacion del Repositorio de Detalle de Nota de Pedido
''' </summary>
''' <inheritdoc/>
Public Interface IOrderDetailRepository
    Inherits IGenericRepository(Of OrderDetail)

    '''<summary>Método obtener detalles de nota de pedido mediante identificador de nota de pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns>Lista de detalles de nota de pedido</returns>
    Function GetByOrderId(id As Integer) As IEnumerable(Of OrderDetail)

    '''<summary>Método eliminar por Identificador de Nota de Pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns> cantidad de filas afectadas</returns>
    Function DeleteByOrderId(id As Integer) As Integer

End Interface
