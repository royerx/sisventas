﻿''' <summary>
''' Interfaz para Repositorio de Comprobante de Pago: 
''' Define una interfaz para la implementacion del Repositorio de Comprobante de Pago
''' </summary>
''' <inheritdoc/>
Public Interface IVoucherRepository
    Inherits IGenericRepository(Of Voucher)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher)

    '''<summary>Método obtener comprobantes de pago mediante identificador de nota de pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns>Lista de comprobantes de pago</returns>
    Function GetByOrderId(id As Integer) As IEnumerable(Of Voucher)

    '''<summary>Método para agregar una asignacion de comprobante de pago a una nota de pedido </summary>
    '''<param name="voucherId">Identificador de Comprobante de Pago</param>
    '''<param name="orderId">Identificador de Nota de Pedido</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function AddOrder(voucherId As Integer, orderId As Integer) As Integer

    '''<summary>Método eliminar una asignacion de comprobante de pago a una nota de pedido</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function DeleteOrder(id As Integer) As Integer

    '''<summary>Método para agregar una asignacion de comprobante de pago a un reporte técnico </summary>
    '''<param name="voucherId">Identificador de Comprobante de Pago</param>
    '''<param name="technicalReportId">Identificador de Reporte Técnico</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function AddTechnicalReport(voucherId As Integer, technicalReportId As Integer) As Integer

    '''<summary>Método eliminar una asignacion de comprobante de pago a un reporte técnico</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function DeleteTechnicalReport(id As Integer) As Integer

    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String)

    '''<summary>Método obtener comprobantes de pago mediante identificador de reporte técnico</summary>
    '''<param name="id">Identificador del reporte técnico</param>
    '''<returns>Lista de comprobantes de pago</returns>
    Function GetByTechnicalReportId(id As Integer) As IEnumerable(Of Voucher)

End Interface

