﻿''' <summary>
''' Interfaz que permite guardar cambios: 
''' Permite confirmar los cambios y desechar los recursos
''' </summary>
Public Interface IUnitOfWorkAdapter
    Inherits IDisposable

    '''<summary>Instancia a los repositorios</summary>
    ReadOnly Property Repository As IUnitOfWorkRepository

    '''<summary>Método para confirmar los cambios</summary>
    Sub SaveChanges()

End Interface
