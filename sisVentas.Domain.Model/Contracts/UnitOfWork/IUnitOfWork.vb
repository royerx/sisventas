﻿''' <summary>
''' Interfaz que crea unidad de trabajo
''' </summary>
Public Interface IUnitOfWork

    '''<summary>Método para crear una unidad de trabajo</summary>
    '''<returns>Unidad de trabajo a ser utilizada</returns>
    Function Create() As IUnitOfWorkAdapter

End Interface
