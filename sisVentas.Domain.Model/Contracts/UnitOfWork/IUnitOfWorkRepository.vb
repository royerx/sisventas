﻿''' <summary>
''' Interfaz que gestiona los repositorios: 
''' Instancia cada uno de los diferentes repositorios que se implementan
''' </summary>
Public Interface IUnitOfWorkRepository
    ReadOnly Property ApplianceDescriptionRepository As IApplianceDescriptionRepository
    ReadOnly Property ApplianceRepository As IApplianceRepository
    ReadOnly Property BrandRepository As IBrandRepository
    ReadOnly Property CategoryRepository As ICategoryRepository
    ReadOnly Property EmployeeRepository As IEmployeeRepository
    ReadOnly Property EntryRepository As IEntryRepository
    ReadOnly Property OrderRepository As IOrderRepository
    ReadOnly Property OrderDetailRepository As IOrderDetailRepository
    ReadOnly Property PermissionRepository As IPermissionRepository
    ReadOnly Property PersonRepository As IPersonRepository
    ReadOnly Property PersonTypeRepository As IPersonTypeRepository
    ReadOnly Property ReceptionSheetRepository As IReceptionSheetRepository
    ReadOnly Property RoleRepository As IRoleRepository
    ReadOnly Property TechnicalReportRepository As ITechnicalReportRepository
    ReadOnly Property VoucherRepository As IVoucherRepository

End Interface
