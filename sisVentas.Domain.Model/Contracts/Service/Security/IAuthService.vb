﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Autorizacion: 
''' Define una interfaz para la implementacion del Servicio de Autorizacion
''' </summary>
''' <inheritdoc/>
Public Interface IAuthService

    '''<summary>Método iniciar sesion</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Function Login(user As String, pass As String) As Boolean

    '''<summary>Método inicializar rol escogido</summary>
    '''<param name="role">Rol seleccionado</param>
    Sub SelectedRole(role As Role)

    '''<summary>Método verificar permiso</summary>
    '''<param name="permission">Slug del permiso a verificar</param>
    '''<returns>Si tiene o no el permiso</returns>
    Function Can(permission As String) As Boolean

    '''<summary>Método para enviar correo de recuperacion de contraseña a un empleado</summary>
    '''<param name="email">Correo Electrónico Empleado</param>
    '''<returns>Mensaje de Cofirmacion</returns>
    Function GetPassByEmail(email As String) As String

End Interface
