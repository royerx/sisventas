﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Permiso: 
''' Define una interfaz para la implementacion del Servicio de Permiso
''' </summary>
''' <inheritdoc/>
Public Interface IPermissionService
    Inherits IGenericService(Of Permission)

    '''<summary>Método listar tablas</summary>
    '''<returns>Lista de nombres de tablas de la base de datos</returns>
    Function Tables() As IEnumerable(Of String)

End Interface
