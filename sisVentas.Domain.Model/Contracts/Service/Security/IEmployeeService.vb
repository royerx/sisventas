﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Empleado: 
''' Define una interfaz para la implementacion del Servicio de Empleado
''' </summary>
''' <inheritdoc/>
Public Interface IEmployeeService
    Inherits IGenericService(Of Employee)

    '''<summary>Método para agregar un empleado y su tipo</summary>
    '''<param name="employee">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Overloads Function Add(employee As Employee, typeId As Integer) As Integer

    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="rolesId">Identificadores de Roles</param>
    '''<returns>Filas afectadas</returns>
    Function AddRoles(employeeId As Integer, rolesId As List(Of Integer)) As Integer

End Interface
