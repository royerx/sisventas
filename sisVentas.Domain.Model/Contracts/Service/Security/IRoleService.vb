﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Rol: 
''' Define una interfaz para la implementacion del Servicio de Rol
''' </summary>
''' <inheritdoc/>
Public Interface IRoleService
    Inherits IGenericService(Of Role)

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="ids">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Function SavePermissions(roleId As Integer, ids As IEnumerable(Of Integer)) As Integer

End Interface
