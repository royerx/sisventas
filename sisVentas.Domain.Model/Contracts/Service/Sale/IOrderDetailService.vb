﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Detalle de Nota de Pedido: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Detalle de Nota de Pedido
''' </summary>
''' <inheritdoc/>
Public Interface IOrderDetailService
    Inherits IGenericService(Of OrderDetail)

End Interface
