﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Documento de Venta: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Documento de Venta
''' </summary>
''' <inheritdoc/>
Public Interface IVoucherService
    Inherits IGenericService(Of Voucher)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher)

    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String)

End Interface
