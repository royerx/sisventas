﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Equipo de Cómputo: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Equipo de Cómputo
''' </summary>
''' <inheritdoc/>
Public Interface IApplianceDescriptionService
    Inherits IGenericService(Of ApplianceDescription)

End Interface
