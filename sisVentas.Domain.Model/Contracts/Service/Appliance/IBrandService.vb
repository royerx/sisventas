﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Marca: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Marca
''' </summary>
''' <inheritdoc/>
Public Interface IBrandService
    Inherits IGenericService(Of Brand)

End Interface
