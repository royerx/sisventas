﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Categoria: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Categoria
''' </summary>
''' <inheritdoc/>
Public Interface ICategoryService
    Inherits IGenericService(Of Category)

End Interface
