﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio Detalle de Equipo de Cómputo: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Detalle Equipo de Cómputo
''' </summary>
''' <inheritdoc/>
Public Interface IApplianceService
    Inherits IGenericService(Of Appliance)

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance)

    '''<summary>Método para obtener una lista de registros de la Entidad Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAllMemory(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance)

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetBySerie(search As String) As Appliance

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetBySerieMemory(search As String) As Appliance

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="model">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetByModel(model As String) As ApplianceDescription

End Interface
