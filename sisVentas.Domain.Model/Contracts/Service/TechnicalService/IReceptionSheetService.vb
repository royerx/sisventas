﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Ficha de Recepción: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Ficha de Recepción
''' </summary>
''' <inheritdoc/>
Public Interface IReceptionSheetService
    Inherits IGenericService(Of ReceptionSheet)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheet)

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheet)

End Interface
