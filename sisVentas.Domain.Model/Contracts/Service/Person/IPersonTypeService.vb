﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Tipo Persona: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Tipo de Persona
''' </summary>
''' <inheritdoc/>
Public Interface IPersonTypeService
    Inherits IGenericService(Of PersonType)

End Interface
