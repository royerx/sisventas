﻿Imports sisVentas.Domain.Model

''' <summary>
''' Interfaz para Servicio de Dominio de Persona: 
''' Define una interfaz para la implementacion del Servicio de Dominio de Persona
''' </summary>
''' <inheritdoc/>
Public Interface IPersonService
    Inherits IGenericService(Of Person)

    '''<summary>Método para obtener una lista de personas mediate el tipo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Overloads Function GetAll(search As String, type As String) As IEnumerable(Of Person)

    '''<summary>Método para obtener una lista de personas mediate el tipo Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Overloads Function GetAllMemory(search As String, type As String) As IEnumerable(Of Person)

    '''<summary>Método para agregar una persona y su tipo</summary>
    '''<param name="person">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Overloads Function Add(person As Person, typeId As Integer) As Integer

    '''<summary>Método para eliminar una persona y su tipo</summary>
    '''<param name="personId">Identificador de la Entidad</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Overloads Function Delete(personId As Integer, typeId As Integer) As Integer

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As Person


End Interface
