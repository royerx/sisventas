﻿''' <summary>
''' Entidad Ficha de Recepción: 
''' Representa a una Ficha de Recepción
''' </summary>
Public Class ReceptionSheet
    'Atributos
    Public Property Id As Integer
    Public Property TechnicalReportId As Integer
    Public Property ClientId As Integer
    Public Property Rdate As Date
    Public Property Detail As String

    'Empleado al que pertenece el reporte técnico
    Public Overridable Property TechnicalReport As TechnicalReport

    'Cliente al que pertenece el reporte técnico
    Public Overridable Property Client As Person

    'Reglas de Negocio
End Class
