﻿''' <summary>
''' Entidad Reporte Técnico: 
''' Representa a un Reporte Técnico
''' </summary>
Public Class TechnicalReport
    'Atributos
    Public Property Id As Integer
    Public Property ClientId As Integer
    Public Property EmployeeId As Integer
    Public Property ApplianceId As Integer
    Public Property EntryDate As Date
    Public Property DeliveryDate As Date
    Public Property Failure As String
    Public Property Diagnosis As String
    Public Property Service As String
    Public Property Price As Double
    Public Property Observation As String

    Public Overridable Property Vouchers As ICollection(Of Voucher)

    'Cliente al que pertenece el reporte técnico
    Public Overridable Property Client As Person

    'Empleado al que pertenece el reporte técnico
    Public Overridable Property Employee As Employee

    'Equipo de cómputo registrado en el reporte técnico
    Public Overridable Property Appliance As Appliance

    'Reglas de Negocio
End Class
