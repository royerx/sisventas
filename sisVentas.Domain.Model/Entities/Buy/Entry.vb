﻿Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Entrada: 
''' Representa una Entrada
''' </summary>
Public Class Entry
    Public Property Id As Integer

    Public Property EmployeeId As Integer

    Public Property SupplierId As Integer

    Public Property Edate As Date

    Public Property DocumentType As String

    Public Property Serie As String

    Public Property Correlative As String

    Public Property State As String

    Public Property Detail As String

    Public Overridable Property Employee As Employee

    Public Overridable Property Supplier As Person

    Public Overridable Property Appliances As ICollection(Of Appliance)

    Private _Total As Double

    Public Property Total As Double
        Get
            If Appliances IsNot Nothing Then
                For Each item In Appliances
                    _Total += item.BuyPrice
                Next
            End If
            Return _Total
        End Get
        Set(value As Double)
            _Total = value
        End Set
    End Property

    Public ReadOnly Property Igv As Double
        Get
            Return Math.Round(Total * Values.IGV, 2)
        End Get
    End Property

End Class
