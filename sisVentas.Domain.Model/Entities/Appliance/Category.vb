﻿''' <summary>
''' Entidad Categoria: 
''' Representa las categorias a la cual pertenece un Equipo de Cómputo
''' </summary>
Public Class Category
    'Atributos
    Public Property Id As Integer
    Public Property Name As String
    Public Property Detail As String

    'Equipos de Cómputo que pertenecen a la categoria
    Public Overridable Property ApplianceDescriptions As ICollection(Of ApplianceDescription)

    'Reglas de Negocio
End Class
