﻿''' <summary>
''' Entidad Equipo de Computo: 
''' Representa a un Equipo de Cómputo
''' </summary>
Public Class ApplianceDescription
    'Atributos
    Public Property Id As Integer
    Public Property CategoryId As Integer
    Public Property BrandId As Integer
    Public Property ApplianceName As String
    Public Property Model As String
    Public Property PartNumber As String
    Public Property Price As Double
    Public Property Stock As Integer
    Public Property Detail As String

    'Categoria a la cual pertenece el Equipo de Cómputo
    Public Overridable Property Category As Category

    'Marca a la cual pertenece el Equipo de Cómputo
    Public Overridable Property Brand As Brand

    'Reglas de Negocio
End Class
