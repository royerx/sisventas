﻿''' <summary>
''' Entidad Detalle de Equipo de Computo: 
''' Representa a un Detalle de Equipo de Cómputo
''' </summary>
Public Class Appliance
    'Atributos
    Public Property Id As Integer
    Public Property ApplianceDescriptionId As Integer
    Public Property Serie As String
    Public Property State As Boolean
    Public Property EntryId As Integer
    Public Property BuyPrice As Double

    'Descripcion a la cual pertenece el Detalle de Equipo de Cómputo
    Public Overridable Property ApplianceDescription As ApplianceDescription

    'Nota de pedido a la cual pertece el equipo
    Public Overridable Property Order As Order

    'Entrada al cual pertece el equipo
    Public Overridable Property Entry As Entry

    'Reglas de Negocio
End Class
