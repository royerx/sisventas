﻿''' <summary>
''' Entidad Tipo Persona: 
''' Representa los tipos de personas ya sea clientes o empleados u otros
''' </summary>
Public Class PersonType
    'Atributos
    Public Property Id As Integer
    Public Property Type As String

    'Personas que se encuentran en este tipo
    Public Overridable Property Persons As ICollection(Of Person)

    'Reglas de Negocio
End Class
