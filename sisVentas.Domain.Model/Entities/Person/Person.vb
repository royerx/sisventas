﻿''' <summary>
''' Entidad Persona: 
''' Representa a las personas que pertenecen al sistema
''' </summary>
Public Class Person
    'Atributos
    Public Property Id As Integer
    Public Property Name As String
    Public Property LastName As String
    Public Property DocumentNumber As String
    Public Property Address As String
    Public Property Phone As String
    Public Property Email As String

    'Tipos de persona a la cual pertenece la persona
    Public Overridable Property PersonTypes As ICollection(Of PersonType)

    'Empleado asociado a la persona
    Public Overridable Property Employee As Employee

    'Reglas de Negocio
    Public Function GetFullName() As String
        Return Name & " " & LastName
    End Function

End Class
