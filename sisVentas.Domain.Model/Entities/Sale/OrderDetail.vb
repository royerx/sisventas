﻿''' <summary>
''' Entidad de Detalle de Cotización o Nota de Pedido: 
''' Representa un detalle de una cotización  o nota de pedido que incluye a un equipo de computo
''' </summary>
Public Class OrderDetail
    Public Property Id As Long

    Public Property OrderId As Integer

    Public Property ApplianceId As Integer

    Public Property Price As Double

    Public Property Quantity As Integer

    Public Overridable Property Order As Order

    Public Overridable Property Appliance As Appliance

    Public Overridable Property ApplianceDescription As ApplianceDescription

    Public ReadOnly Property SubTotal As Double
        Get
            Return Math.Round(Price * Quantity, 2)
        End Get
    End Property

End Class
