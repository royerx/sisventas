﻿Imports sisVentas.CrossCutting
''' <summary>
''' Entidad de Comprobante de Pago: 
''' Representa un Comprobante de Pago
''' </summary>
Public Class Voucher
    Public Property Id As Integer

    Public Property EmployeeId As Integer

    Public Property Vdate As Date

    Public Property Type As String

    Public Property Serie As String

    Public Property Correlative As String

    Public Property State As String

    Public Property Detail As String

    Public Property VoucherType As String

    Public Overridable Property Employee As Employee

    'Lista de Equipos de Cómuto a los cuales tiene la Nota de Pedido
    Public Overridable Property Order As Order

    'Lista de Equipos de Cómuto a los cuales tiene un reporte técnico
    Public Overridable Property TechnicalReport As TechnicalReport

    Public ReadOnly Property Total As Double
        Get
            If Order IsNot Nothing Then
                Return Math.Round(Order.Total, 2)
            ElseIf TechnicalReport IsNot Nothing Then
                Return Math.Round(TechnicalReport.Price, 2)
            Else
                Return 0.00
            End If
        End Get
    End Property

    Public ReadOnly Property Igv As Double
        Get
            Return Math.Round(Total * Values.IGV, 2)
        End Get
    End Property

End Class
