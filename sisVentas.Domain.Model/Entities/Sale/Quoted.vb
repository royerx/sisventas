﻿''' <summary>
''' Entidad de Cotización: 
''' Representa una cotización que se realiza por un empleado
''' </summary>
Public Class Quoted
    Public Property Id As Integer

    Public Overridable Property Employee As Employee

    Public Property Qdate As Date

    'Lista de Equipos de Cómuto a los cuales tiene la cotización
    Public Overridable Property OrderDetails As ICollection(Of OrderDetail)

    'Logica de Negocio
    Public ReadOnly Property Total As Double
        Get
            Dim t = 0.00
            If OrderDetails IsNot Nothing Then
                For Each item In OrderDetails
                    t += item.SubTotal
                Next
            End If
            Return Math.Round(t, 2)
        End Get
    End Property

End Class
