﻿''' <summary>
''' Entidad de Nota de Pedido: 
''' Representa una nota de pedido
''' </summary>
Public Class Order
    Public Property Id As Integer

    Public Property ClientId As Integer

    Public Property EmployeeId As Integer

    Public Property Ndate As Date

    Public Property Detail As String

    Public Overridable Property Client As Person

    Public Overridable Property Employee As Employee

    'Lista de Equipos de Cómuto a los cuales tiene la Nota de Pedido
    Public Overridable Property OrderDetails As ICollection(Of OrderDetail)

    Public Overridable Property Vouchers As ICollection(Of Voucher)

    Private _Total As Double
    Public Property Total As Double
        Get
            If OrderDetails IsNot Nothing Then
                Dim t = 0.00

                For Each item In OrderDetails
                    t += item.SubTotal
                Next
                _Total = t
            End If
            Return Math.Round(_Total, 2)
        End Get
        Set(value As Double)
            _Total = value
        End Set
    End Property

End Class
