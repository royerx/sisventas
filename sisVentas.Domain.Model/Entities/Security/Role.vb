﻿''' <summary>
''' Entidad Rol: 
''' Representa roles de los diferentes usuarios del sistema
''' </summary>
Public Class Role
    'Atributos
    Public Property Id As Integer
    Public Property Name As String
    Public Property Description As String

    'Emploados que tienen este rol
    Public Overridable Property Employees As ICollection(Of Employee)

    'Permisos que tienen este rol
    Public Overridable Property Permissions As ICollection(Of Permission)

    'Reglas de Negocio

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre del Rol</returns>
    Public Overrides Function ToString() As String
        Return Name
    End Function

End Class
