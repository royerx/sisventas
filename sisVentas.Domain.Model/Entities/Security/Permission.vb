﻿''' <summary>
''' Entidad Permiso: 
''' Representa permisos de los diferentes usuarios del sistema
''' </summary>
Public Class Permission
    'Atributos
    Public Property Id As Integer
    Public Property Name As String
    Public Property Slug As String
    Public Property Description As String

    'Roles que tienen este permiso
    Public Overridable Property Roles As ICollection(Of Role)

    'Reglas de Negocio

    '''<summary>Método que sorbreescribe el metodo toString de la clase</summary>
    '''<returns>Nombre del Permiso</returns>
    Public Overrides Function ToString() As String
        Return Name
    End Function

End Class
