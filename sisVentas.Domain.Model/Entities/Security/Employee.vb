﻿Imports sisVentas.CrossCutting
''' <summary>
''' Entidad Empleado: 
''' Representa a los empleados que sol los usuarios del sistemas
''' </summary>
Public Class Employee
    'Atributos
    Public Property Id As Integer
    Public Property Login As String
    Public Property Pass As String

    'Persona asociada al empleado
    Public Overridable Property Person As Person

    'Roles que tiene el empleado
    Public Overridable Property Roles As ICollection(Of Role)

    'Reglas de Negocio

    '''<summary>Método que inicializa el usuario activo del sistema</summary>
    Public Sub UserActive(user As Employee)
        ActiveUser.Employee = user
    End Sub

    '''<summary>Método que inicializa el rol elegido</summary>
    Public Sub SelectedRole(role As Role)
        ActiveUser.SelectedRole = role
    End Sub
End Class
