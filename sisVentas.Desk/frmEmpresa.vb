﻿Imports capaLogica
Public Class frmEmpresa
    Private dt As New DataTable
    Private Shared _formulario As frmEmpresa = Nothing
    Private Sub New()
        InitializeComponent()
    End Sub
    Public Shared Function getInstancia() As frmEmpresa
        If _formulario Is Nothing OrElse _formulario.IsDisposed = True Then
            _formulario = New frmEmpresa
        End If
        Return _formulario
    End Function
    Private Sub placeholder()
        If txtBuscar.Text = "Buscar..." Then
            txtBuscar.Text = ""
        ElseIf txtBuscar.Text = "" Then
            txtBuscar.Text = "Buscar..."
        End If
    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub
    Private Sub frmEquipo_Load(sender As Object, e As EventArgs) Handles Me.Load
        mostrar("")
    End Sub
    Private Sub txtBuscar_MouseClick(sender As Object, e As MouseEventArgs) Handles txtBuscar.MouseClick
        placeholder()
    End Sub

    Private Sub txtBuscar_LostFocus(sender As Object, e As EventArgs) Handles txtBuscar.LostFocus
        placeholder()
    End Sub

    Public Sub mostrar(ByVal buscar As String)
        Try
            dt = lEmpresa.getInstancia.mostrar(buscar)
            dtgListado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                dtgListado.DataSource = dt
                txtBuscar.Enabled = True
                dtgListado.ColumnHeadersVisible = True
                dtgListado.Columns.Item(1).Visible = False
                dtgListado.Columns.Item(2).Visible = False
            Else
                dtgListado.DataSource = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        frmEmpresaGuardar.getInstancia.lblTitulo.Text = "AGREGAR EMPRESA"
        frmEmpresaGuardar.getInstancia.ShowDialog()
    End Sub
    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        Try
            frmEmpresaGuardar.getInstancia.lblTitulo.Text = "EDITAR EMPRESA"
            frmEmpresaGuardar.getInstancia.id = dtgListado.SelectedCells.Item(2).Value
            frmEmpresaGuardar.getInstancia.txtRuc.Text = dtgListado.SelectedCells.Item(3).Value
            frmEmpresaGuardar.getInstancia.txtRazonSocial.Text = dtgListado.SelectedCells.Item(4).Value
            frmEmpresaGuardar.getInstancia.txtDireccion.Text = dtgListado.SelectedCells.Item(5).Value
            frmEmpresaGuardar.getInstancia.txtTelefono.Text = dtgListado.SelectedCells.Item(6).Value
            frmEmpresaGuardar.getInstancia.txtEmail.Text = dtgListado.SelectedCells.Item(7).Value
            frmEmpresaGuardar.getInstancia.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Try
            Dim result As DialogResult

            result = MessageBox.Show("¿Realmente desea eliminar los registros seleccionados?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

            If result = DialogResult.OK Then
                If chkEliminar.Checked Then
                    Dim onekey As Integer
                    For Each row As DataGridViewRow In dtgListado.Rows
                        Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                        If marcado Then

                            onekey = Convert.ToInt32(row.Cells(2).Value)
                            If lEmpresa.getInstancia.eliminar(onekey) Then
                            Else
                                MessageBox.Show("El registro no fue eliminado", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                    Next
                    chkEliminar.Checked = False
                Else
                    If lEmpresa.getInstancia.eliminar(dtgListado.SelectedCells.Item(2).Value) Then
                    Else
                        MessageBox.Show("El registro no fue eliminado", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            Else
                MessageBox.Show("Cancelando eliminación de registros", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkEliminar.Checked = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        btnEditar.Enabled = True
        btnNuevo.Enabled = True
        mostrar("")
    End Sub
    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chkEliminar.CheckedChanged
        If chkEliminar.CheckState = CheckState.Checked Then
            dtgListado.Columns.Item("Eliminar").Visible = True
            btnNuevo.Enabled = False
            btnEditar.Enabled = False
        Else
            dtgListado.Columns.Item("Eliminar").Visible = False
            btnNuevo.Enabled = True
            btnEditar.Enabled = True
        End If
    End Sub
    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        If txtBuscar.Text = "" Or txtBuscar.Text = "Buscar..." Then
            mostrar("")
        Else
            mostrar(txtBuscar.Text)
        End If
    End Sub
    Private Sub dtgListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtgListado.CellContentClick
        If e.ColumnIndex = Me.dtgListado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dtgListado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value
        End If
    End Sub
End Class