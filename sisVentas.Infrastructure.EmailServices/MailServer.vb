﻿Imports System.Net
Imports System.Net.Mail
Public MustInherit Class MailServer

    Private SmtpClient As SmtpClient
    Protected SenderMail As String
    Protected Password As String
    Protected Host As String
    Protected Port As Integer
    Protected Ssl As Boolean

    Protected Sub IntializeSmtpClient()
        SmtpClient = New SmtpClient With {
            .Credentials = New NetworkCredential(SenderMail, Password),
            .Host = Host,
            .Port = Port,
            .EnableSsl = Ssl
        }
    End Sub

    Public Sub SendMail(Subject As String, Body As String, ReceiverMail As List(Of String))
        Dim MailMessage = New MailMessage()
        Try
            MailMessage.From = New MailAddress(SenderMail)
            For Each item In ReceiverMail
                MailMessage.To.Add(item)
            Next
            MailMessage.Subject = Subject
            MailMessage.Body = Body
            MailMessage.Priority = MailPriority.Normal
            SmtpClient.Send(MailMessage)
        Catch ex As Exception
        Finally
            MailMessage.Dispose()
            SmtpClient.Dispose()
        End Try
    End Sub

End Class
