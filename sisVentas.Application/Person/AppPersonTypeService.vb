﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Tipo de Persona: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de tipo de persona
''' </summary>
''' <inheritdoc/>
Public Class AppPersonTypeService
    Inherits AppGenericService(Of PersonType)

    'Declaracion del servicio de dominio de tipo de persona para agregar los servicios de aplicacion
    Private ReadOnly personTypeService As IPersonTypeService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de tipo de persona al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        personTypeService = New PersonTypeService(unitOfWork)
        MyBase.Repository(personTypeService)
    End Sub

End Class
