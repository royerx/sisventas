﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Persona: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de persona
''' </summary>
''' <inheritdoc/>
Public Class AppPersonService
    Inherits AppGenericService(Of Person)

    'Declaracion del servicio de dominio de persona para agregar los servicios de aplicacion
    Private ReadOnly personService As IPersonService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de persona al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        personService = New PersonService(unitOfWork)
        MyBase.Repository(personService)
    End Sub

    '''<summary>Método para obtener una lista de personas mediate el tipo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Overloads Function GetAll(search As String, type As String) As IEnumerable(Of Person)
        Return personService.GetAll(search, type)
    End Function

    '''<summary>Método para obtener una lista de personas mediate el tipo Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, type As String) As IEnumerable(Of Person)
        Return personService.GetAllMemory(search, type)
    End Function

    '''<summary>Método para agregar una persona y su tipo</summary>
    '''<param name="person">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de Persona</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Overloads Function Add(person As Person, typeId As Integer) As Integer
        Return personService.Add(person, typeId)
    End Function

    '''<summary>Método para eliminar una persona y su tipo</summary>
    '''<param name="personId">Identificador de Entidad</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Public Overloads Function Delete(personId As Integer, typeId As Integer) As Integer
        Return personService.Delete(personId, typeId)
    End Function

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As Person
        Return personService.GetByDocument(document)
    End Function


End Class
