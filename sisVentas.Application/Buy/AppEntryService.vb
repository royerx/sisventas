﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Entrada: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de entrada
''' </summary>
''' <inheritdoc/>
Public Class AppEntryService
    Inherits AppGenericService(Of Entry)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de nota de pedido para agregar los servicios de aplicacion
    Private ReadOnly entryService As IEntryService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        entryService = New EntryService(unitOfWork)
        MyBase.Repository(entryService)
    End Sub


    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry)
        Return entryService.GetAll(search, beginDate, endDate)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry)
        Return entryService.GetAllMemory(search, beginDate, endDate)
    End Function

End Class
