﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services

''' <summary>
''' Clase Servicio de Aplicacion Generica: 
''' Implementa los metodos base para manipular los servicio de aplicación
''' </summary>
''' <typeparam name="Entity">Entidad de dominio que va a instanciar el repositorio a ser usado</typeparam>
Public Class AppGenericService(Of Entity As Class)

    'Declaracion del servicio generico al cual se le va a agregar los servicios de aplicacion
    Protected genericService As IGenericService(Of Entity)

    '''<summary>Constructor de la clase que inyecta un repositorio generico</summary>
    '''<param name="service">Repositorio a ser usado</param>
    Public Sub Repository(service As IGenericService(Of Entity))
        genericService = service
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Entity)
        Return genericService.GetAll(search)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guardada en la memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAllMemory(search As String) As IEnumerable(Of Entity)
        Return genericService.GetAllMemory(search)
    End Function

    '''<summary>Método para obtener un registro por el Id</summary>
    '''<param name="id">Identificador de la entidad a ser encontrada</param>
    '''<returns>Entidad encontrada</returns>
    Public Function GetById(id As Integer) As Entity
        Return genericService.GetById(id)
    End Function

    '''<summary>Método para agregar una Entidad </summary>
    '''<param name="entity">Entidad a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Entity) As Integer
        Return genericService.Add(entity)
    End Function

    '''<summary>Método para modificar una Entidad</summary>
    '''<param name="entity">Entidad a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Entity) As Integer
        Return genericService.Update(entity)
    End Function

    '''<summary>Método eliminar una Entidad</summary>
    '''<param name="id">Identificador de la entidad a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer
        Return genericService.Delete(id)
    End Function

End Class
