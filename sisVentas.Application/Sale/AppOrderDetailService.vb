﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Detalle de Nota de Pedido: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de detalle de nota de pedido
''' </summary>
''' <inheritdoc/>
Public Class AppOrderDetailService
    Inherits AppGenericService(Of OrderDetail)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de detalle de nota de pedido para agregar los servicios de aplicacion
    Private ReadOnly orderDetailService As IOrderDetailService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        orderDetailService = New OrderDetailService(unitOfWork)
        MyBase.Repository(orderDetailService)
    End Sub

End Class
