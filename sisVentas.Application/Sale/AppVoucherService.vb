﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Comprobante de Pago: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de comprobante de pago
''' </summary>
''' <inheritdoc/>
Public Class AppVoucherService
    Inherits AppGenericService(Of Voucher)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de nota de pedido para agregar los servicios de aplicacion
    Private ReadOnly voucherService As IVoucherService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        voucherService = New VoucherService(unitOfWork)
        MyBase.Repository(voucherService)
    End Sub


    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher)
        Return voucherService.GetAll(search, beginDate, endDate)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher)
        Return voucherService.GetAllMemory(search, beginDate, endDate)
    End Function


    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String)
        Return voucherService.GetNumber(type)
    End Function

End Class
