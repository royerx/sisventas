﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Nota de Pedido: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de nota de pedido
''' </summary>
''' <inheritdoc/>
Public Class AppOrderService
    Inherits AppGenericService(Of Order)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de nota de pedido para agregar los servicios de aplicacion
    Private ReadOnly orderService As IOrderService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        orderService = New OrderService(unitOfWork)
        MyBase.Repository(orderService)
    End Sub


    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order)
        Return orderService.GetAll(search, beginDate, endDate)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order)
        Return orderService.GetAllMemory(search, beginDate, endDate)
    End Function

    '''<summary>Método agregar y verificar duplicado de series</summary>
    '''<param name="orderDetails">Lista de detalles de nota de pedido</param>
    '''<param name="detail">detalle a ser agregago</param>
    '''<returns> si hay repetidos o no</returns>
    Function AddAppliance(orderDetails As List(Of OrderDetail), detail As OrderDetail) As IEnumerable(Of OrderDetail)
        Return orderService.AddAppliance(orderDetails, detail)
    End Function

End Class
