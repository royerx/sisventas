﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Empleado: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de empleado
''' </summary>
''' <inheritdoc/>
Public Class AppAuthService

    'Declaracion del servicio de dominio de empleado para agregar los servicios de aplicacion
    Private ReadOnly authService As IAuthService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de empleado al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        authService = New AuthService(unitOfWork)
    End Sub

    '''<summary>Método iniciar sesion</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Public Function Login(user As String, pass As String) As Boolean
        Return authService.Login(user, pass)
    End Function

    '''<summary>Método inicializar rol escogido</summary>
    '''<param name="role">Rol seleccionado</param>
    Public Sub SelectedRole(role As Role)
        authService.SelectedRole(role)
    End Sub

    '''<summary>Método verificar permiso</summary>
    '''<param name="permission">Slug del permiso a verificar</param>
    '''<returns>Si tiene o no el permiso</returns>
    Public Function Can(permission As String) As Boolean
        Return authService.Can(permission)
    End Function

    '''<summary>Método para enviar correo de recuperacion de contraseña a un empleado</summary>
    '''<param name="email">Correo Electrónico Empleado</param>
    '''<returns>Mensaje de Cofirmacion</returns>
    Function GetPassByEmail(email As String) As String
        Return authService.GetPassByEmail(email)
    End Function

End Class
