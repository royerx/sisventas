﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Permiso: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de permiso
''' </summary>
''' <inheritdoc/>
Public Class AppPermissionService
    Inherits AppGenericService(Of Permission)

    'Declaracion del servicio de dominio de permiso para agregar los servicios de aplicacion
    Private ReadOnly permissionService As IPermissionService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de permiso al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        permissionService = New PermissionService(unitOfWork)
        MyBase.Repository(permissionService)
    End Sub

    '''<summary>Método listar tablas</summary>
    '''<returns>Lista de nombres de tablas de la base de datos</returns>
    Public Function Tables() As IEnumerable(Of String)
        Return permissionService.Tables()
    End Function

End Class
