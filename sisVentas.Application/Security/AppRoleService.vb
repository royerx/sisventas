﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Rol: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de rol
''' </summary>
''' <inheritdoc/>
Public Class AppRoleService
    Inherits AppGenericService(Of Role)

    'Declaracion del servicio de dominio de rol para agregar los servicios de rol
    Private ReadOnly roleService As IRoleService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de rol al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        roleService = New RoleService(unitOfWork)
        MyBase.Repository(roleService)
    End Sub

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="permissionIds">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Public Function SavePermissions(roleId As Integer, permissionIds As IEnumerable(Of Integer)) As Integer
        Return roleService.SavePermissions(roleId, permissionIds)
    End Function

End Class
