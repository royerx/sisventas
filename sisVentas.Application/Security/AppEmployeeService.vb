﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Empleado: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de empleado
''' </summary>
''' <inheritdoc/>
Public Class AppEmployeeService
    Inherits AppGenericService(Of Employee)

    'Declaracion del servicio de dominio de empleado para agregar los servicios de aplicacion
    Private ReadOnly employeeService As IEmployeeService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de empleado al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        employeeService = New EmployeeService(unitOfWork)
        MyBase.Repository(employeeService)
    End Sub

    '''<summary>Método para agregar un empleado y su tipo</summary>
    '''<param name="employee">Entidad a ser agregada</param>
    '''<param name="typeId">Tipo de persona</param>
    '''<returns>Filas afectadas</returns>
    Public Overloads Function Add(employee As Employee, typeId As Integer) As Integer
        Return employeeService.Add(employee, typeId)
    End Function


    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="rolesId">Identificadores de Roles</param>
    '''<returns>Filas afectadas</returns>
    Public Function AddRoles(employeeId As Integer, rolesId As List(Of Integer)) As Integer
        Return employeeService.AddRoles(employeeId, rolesId)
    End Function
End Class
