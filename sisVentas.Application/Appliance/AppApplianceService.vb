﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Detalle de Equipo de Cómputo: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de detalle equipo de cómputo
''' </summary>
''' <inheritdoc/>
Public Class AppApplianceService
    Inherits AppGenericService(Of Appliance)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de equipo de cómputo para agregar los servicios de aplicacion
    Private ReadOnly applianceService As IApplianceService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        applianceService = New ApplianceService(unitOfWork)
        MyBase.Repository(applianceService)
    End Sub

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAll(search As String, applianceDescriptionId As Integer)
        Return applianceService.GetAll(search, applianceDescriptionId)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, applianceDescriptionId As Integer)
        Return applianceService.GetAllMemory(search, applianceDescriptionId)
    End Function

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad Guardada en memoria</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetBySerieMemory(search As String) As Appliance
        Return applianceService.GetBySerieMemory(search)
    End Function

    '''<summary>Método para obtener un equipo de una lista de registros de la Entidad</summary>
    '''<param name="serie">Serie que va a ser filtrada</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetBySerie(serie As String) As Appliance
        Return applianceService.GetBySerie(serie)
    End Function

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="model">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Public Function GetByModel(model As String) As ApplianceDescription
        Return applianceService.GetByModel(model)
    End Function

End Class
