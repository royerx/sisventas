﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Categoria: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de categoria
''' </summary>
''' <inheritdoc/>
Public Class AppCategoryService
    Inherits AppGenericService(Of Category)

    'Declaracion del servicio de dominio de categoria para agregar los servicios de aplicacion
    Private ReadOnly categoryService As ICategoryService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de categoria al de aplicacion</summary>
    Public Sub New()
        Dim unitOfWork = New UnitOfWorkSqlServer()
        categoryService = New CategoryService(unitOfWork)
        MyBase.Repository(categoryService)
    End Sub

End Class
