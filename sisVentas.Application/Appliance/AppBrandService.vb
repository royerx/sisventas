﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Marca: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de marca
''' </summary>
''' <inheritdoc/>
Public Class AppBrandService
    Inherits AppGenericService(Of Brand)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de marca para agregar los servicios de aplicacion
    Private ReadOnly brandService As IBrandService

    '''<summary>Constructor de la clase que inyecta el servicio de dominio de marca al de aplicacion</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        brandService = New BrandService(unitOfWork)
        MyBase.Repository(brandService)
    End Sub

End Class
