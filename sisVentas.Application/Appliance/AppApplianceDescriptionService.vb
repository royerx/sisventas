﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Equipo de Cómputo: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de equipo de cómputo
''' </summary>
''' <inheritdoc/>
Public Class AppApplianceDescriptionService
    Inherits AppGenericService(Of ApplianceDescription)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de equipo de cómputo para agregar los servicios de aplicacion
    Private ReadOnly applianceDescriptionService As IApplianceDescriptionService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        applianceDescriptionService = New ApplianceDescriptionService(unitOfWork)
        MyBase.Repository(applianceDescriptionService)
    End Sub

End Class
