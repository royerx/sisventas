﻿Imports sisVentas.Domain.Model
Imports sisVentas.Domain.Services
Imports sisVentas.Infrastructure.DataAccess

''' <summary>
''' Clase Servicio de Aplicacion de Reporte Técnico: 
''' Hereda los metodos del servicio de aplicacion generico e implenta los metodos correspondientes al servicio de aplicacion de reporte técnico
''' </summary>
''' <inheritdoc/>
Public Class AppTechnicalReportService
    Inherits AppGenericService(Of TechnicalReport)

    'Declaracion de la unidad de trabajo para agregar los servicios de aplicacion
    Private ReadOnly unitOfWork As IUnitOfWork

    'Declaracion del servicio de dominio de nota de pedido para agregar los servicios de aplicacion
    Private ReadOnly technicalReportService As ITechnicalReportService

    '''<summary>Constructor de la clase que inyecta la implementacion de la unidad de trabajo a ser utilizada</summary>
    Public Sub New()
        unitOfWork = New UnitOfWorkSqlServer()
        technicalReportService = New TechnicalReportService(unitOfWork)
        MyBase.Repository(technicalReportService)
    End Sub


    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport)
        Return technicalReportService.GetAll(search, beginDate, endDate)
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Overloads Function GetAllMemory(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport)
        Return technicalReportService.GetAllMemory(search, beginDate, endDate)
    End Function

    '''<summary>Método obtener servicios</summary>
    '''<returns>Lista de Servicios</returns>
    Function GetServices() As List(Of String)
        Return technicalReportService.GetServices
    End Function

    '''<summary>Método obtener el precio de un servicio</summary>
    '''<param name="service">Nombre del servicio</param>
    '''<returns>Precio</returns>
    Function GetServicePrice(service As String) As Double
        Return technicalReportService.GetServicePrice(service)
    End Function

    '''<summary>Método obtener ficha de recepción por reporte técnico</summary>
    '''<param name="technicalReportId">Identificador de reporte técnico</param>
    '''<returns>ficha de recepción</returns>
    Function GetReceptionSheet(technicalReportId As Integer) As ReceptionSheet
        Return technicalReportService.GetReceptionSheet(technicalReportId)
    End Function

End Class
