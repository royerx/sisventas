﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model
''' <summary>
''' Clase Unidad de Trabajo para Repositorios: 
''' Implemente la interfaz de unidad de trabajo para repositorios
''' </summary>
Public Class UnitOfWorkRepositorySqlServer
    Implements IUnitOfWorkRepository

    Private ReadOnly context
    Private ReadOnly transaction

    '''<summary>Constructor de la clase que inyecta la cadena de coneccion y la transaccion a usar</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para inicializar un repositorio de Descripción de Equipo de Cómputo</summary>
    '''<return>Repositorio de Descripción de Equipo de Cómputo</return>
    Public ReadOnly Property ApplianceDescriptionRepository As IApplianceDescriptionRepository Implements IUnitOfWorkRepository.ApplianceDescriptionRepository
        Get
            Return New ApplianceDescriptionRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Detalle de Equipo de Cómputo</summary>
    '''<return>Repositorio de Descripción de Detalle de Equipo de Cómputo</return>
    Public ReadOnly Property ApplianceRepository As IApplianceRepository Implements IUnitOfWorkRepository.ApplianceRepository
        Get
            Return New ApplianceRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Marca</summary>
    '''<return>Repositorio de Marca</return>
    Public ReadOnly Property BrandRepository As IBrandRepository Implements IUnitOfWorkRepository.BrandRepository
        Get
            Return New BrandRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Categoria</summary>
    '''<return>Repositorio de Categoria</return>
    Public ReadOnly Property CategoryRepository As ICategoryRepository Implements IUnitOfWorkRepository.CategoryRepository
        Get
            Return New CategoryRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Empleado</summary>
    '''<return>Repositorio de Empleado</return>
    Public ReadOnly Property EmployeeRepository As IEmployeeRepository Implements IUnitOfWorkRepository.EmployeeRepository
        Get
            Return New EmployeeRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Entrada</summary>
    '''<return>Repositorio de Entrada</return>
    Public ReadOnly Property EntryRepository As IEntryRepository Implements IUnitOfWorkRepository.EntryRepository
        Get
            Return New EntryRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Nota de Pedido</summary>
    '''<return>Repositorio de Nota de Pedido</return>
    Public ReadOnly Property OrderRepository As IOrderRepository Implements IUnitOfWorkRepository.OrderRepository
        Get
            Return New OrderRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Detalle de Nota de Pedido</summary>
    '''<return>Repositorio de Detalle de Nota de Pedido</return>
    Public ReadOnly Property OrderDetailRepository As IOrderDetailRepository Implements IUnitOfWorkRepository.OrderDetailRepository
        Get
            Return New OrderDetailRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Permiso</summary>
    '''<return>Repositorio de Permiso</return>
    Public ReadOnly Property PermissionRepository As IPermissionRepository Implements IUnitOfWorkRepository.PermissionRepository
        Get
            Return New PermissionRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Persona</summary>
    '''<return>Repositorio de Persona</return>
    Public ReadOnly Property PersonRepository As IPersonRepository Implements IUnitOfWorkRepository.PersonRepository
        Get
            Return New PersonRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Tipo de Persona</summary>
    '''<return>Repositorio de Tipo de Persona</return>
    Public ReadOnly Property PersonTypeRepository As IPersonTypeRepository Implements IUnitOfWorkRepository.PersonTypeRepository
        Get
            Return New PersonTypeRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Rol</summary>
    '''<return>Repositorio de Rol</return>
    Public ReadOnly Property RoleRepository As IRoleRepository Implements IUnitOfWorkRepository.RoleRepository
        Get
            Return New RoleRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Ficha de Recepción</summary>
    '''<return>Repositorio de Rol</return>
    Public ReadOnly Property ReceptionSheetRepository As IReceptionSheetRepository Implements IUnitOfWorkRepository.ReceptionSheetRepository
        Get
            Return New ReceptionSheetRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Reporte Técnico</summary>
    '''<return>Repositorio de Reporte Técnico</return>
    Public ReadOnly Property TechnicalReportRepository As ITechnicalReportRepository Implements IUnitOfWorkRepository.TechnicalReportRepository
        Get
            Return New TechnicalReportRepository(context, transaction)
        End Get
    End Property

    '''<summary>Método para inicializar un repositorio de Comprobante de Pago</summary>
    '''<return>Repositorio de Comprobante de Pago</return>
    Public ReadOnly Property VoucherRepository As IVoucherRepository Implements IUnitOfWorkRepository.VoucherRepository
        Get
            Return New VoucherRepository(context, transaction)
        End Get
    End Property
End Class
