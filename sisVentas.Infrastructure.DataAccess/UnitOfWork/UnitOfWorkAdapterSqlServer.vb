﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Adaptador de Unidad de Trabajo: 
''' Implementa la interfaz adaptador de unidad de trabajo
''' </summary>
Public Class UnitOfWorkAdapterSqlServer
    Implements IUnitOfWorkAdapter

    Public Property Repository As IUnitOfWorkRepository Implements IUnitOfWorkAdapter.Repository
    Private Property Context As SqlConnection
    Private Property Transaction As SqlTransaction

    '''<summary>Constructor de la clase que inyecta la cadena de conexion e inicia la transaccion</summary>
    '''<param name="connectionString">Cadena de Conexion</param>
    Public Sub New(connectionString As String)
        Context = New SqlConnection(connectionString)
        Context.Open()
        Transaction = Context.BeginTransaction
        Repository = New UnitOfWorkRepositorySqlServer(Context, Transaction)
    End Sub

    '''<summary>Método desechar los recursos utilizados</summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        If Transaction IsNot Nothing Then
            Transaction.Dispose()
        End If
        If Context IsNot Nothing Then
            Context.Close()
            Context.Dispose()
        End If
        Repository = Nothing
    End Sub

    '''<summary>Método para confirmar y guardar los cambios</summary>
    Public Sub SaveChanges() Implements IUnitOfWorkAdapter.SaveChanges
        Transaction.Commit()
    End Sub
End Class
