﻿Imports System.Configuration
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Unidad de Trabajo: 
''' Implementa la interfaz Unidad de Trabajo
''' </summary>
Public Class UnitOfWorkSqlServer
    Implements IUnitOfWork

    '''<summary>Método para crear una unidad de trabajo e inicializar la cadena de conexion</summary>
    '''<return>Adaptador de Unidad de Trabajo</return>
    Public Function Create() As IUnitOfWorkAdapter Implements IUnitOfWork.Create
        Dim connectionString = IIf(ConfigurationManager.ConnectionStrings("sisVentas").ToString() Is Nothing, "", ConfigurationManager.ConnectionStrings("sisVentas").ToString())
        Return New UnitOfWorkAdapterSqlServer(connectionString)
    End Function
End Class
