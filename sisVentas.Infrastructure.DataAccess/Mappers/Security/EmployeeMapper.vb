﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Empleado: 
''' Mapea los valores recibidos de una fila a una entidad Empleado
''' </summary>
Public MustInherit Class EmployeeMapper

    '''<summary>Método para mapear empleado de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Empleado</returns>
    Public Shared Function Map(row As DataRow) As Employee
        Dim employee = New Employee With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Login = IIf(IsDBNull(row("Login")), Nothing, row("Login")),
                            .Pass = IIf(IsDBNull(row("Pass")), Nothing, row("Pass"))}
        If row.Table.Columns.Count > 3 Then
            employee.Person = New Person With {
                                    .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                                    .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                    .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                    .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                    .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                    .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
        End If
        Return employee
    End Function
End Class
