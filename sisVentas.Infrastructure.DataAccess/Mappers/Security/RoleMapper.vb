﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Rol: 
''' Mapea los valores recibidos de una fila a una entidad Rol
''' </summary>
Public MustInherit Class RoleMapper

    '''<summary>Método para mapear rol de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Rol</returns>
    Public Shared Function Map(row As DataRow) As Role
        Return New Role With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                            .Description = IIf(IsDBNull(row("Description")), Nothing, row("Description"))}
    End Function
End Class
