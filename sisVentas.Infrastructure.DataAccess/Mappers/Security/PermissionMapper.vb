﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Permiso: 
''' Mapea los valores recibidos de una fila a una entidad Permiso
''' </summary>
Public MustInherit Class PermissionMapper

    '''<summary>Método para mapear permiso de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Permiso</returns>
    Public Shared Function Map(row As DataRow) As Permission
        Return New Permission With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                            .Slug = IIf(IsDBNull(row("Slug")), Nothing, row("Slug")),
                            .Description = IIf(IsDBNull(row("Description")), Nothing, row("Description"))}
    End Function
End Class
