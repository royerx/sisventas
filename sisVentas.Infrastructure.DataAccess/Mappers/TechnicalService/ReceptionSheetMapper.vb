﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Ficha de Recepción: 
''' Mapea los valores recibidos de una fila a una entidad Ficha de Recepción
''' </summary>
Public MustInherit Class ReceptionSheetMapper

    '''<summary>Método para mapear ficha de recepción de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Ficha de Recepción</returns>
    Public Shared Function Map(row As DataRow) As ReceptionSheet
        Dim receptionSheet = New ReceptionSheet With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .ClientId = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                            .TechnicalReportId = IIf(IsDBNull(row("TechnicalReportId")), Nothing, row("TechnicalReportId")),
                            .Rdate = IIf(IsDBNull(row("Date")), Nothing, row("Date")),
                            .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
        If row.Table.Columns.Count > 5 Then
            receptionSheet.Client = New Person With {
                                                .Id = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                                                .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                                .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                                .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                                .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                                .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                                                .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
            receptionSheet.TechnicalReport = New TechnicalReport With {
                                       .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .ClientId = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                            .EmployeeId = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                            .ApplianceId = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                            .EntryDate = IIf(IsDBNull(row("EntryDate")), Nothing, row("EntryDate")),
                            .DeliveryDate = IIf(IsDBNull(row("DeliveryDate")), Nothing, row("DeliveryDate")),
                            .Failure = IIf(IsDBNull(row("Failure")), Nothing, row("Failure")),
                            .Diagnosis = IIf(IsDBNull(row("Diagnosis")), Nothing, row("Diagnosis")),
                            .Service = IIf(IsDBNull(row("Service")), Nothing, row("Service")),
                            .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price")),
                            .Observation = IIf(IsDBNull(row("Observation")), Nothing, row("Observation"))}
            receptionSheet.TechnicalReport.Appliance = New Appliance With {
                                        .Id = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                                        .ApplianceDescriptionId = IIf(IsDBNull(row("ApplianceDescriptionId")), Nothing, row("ApplianceDescriptionId")),
                                        .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                                        .State = IIf(IsDBNull(row("State")), Nothing, row("State"))}
        End If
        Return receptionSheet
    End Function
End Class
