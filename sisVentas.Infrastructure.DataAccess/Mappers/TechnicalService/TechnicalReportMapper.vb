﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Reporte Técnico: 
''' Mapea los valores recibidos de una fila a una entidad Reporte Técnico
''' </summary>
Public MustInherit Class TechnicalReportMapper

    '''<summary>Método para mapear reporte técnico de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Reporte Técnico</returns>
    Public Shared Function Map(row As DataRow) As TechnicalReport
        Dim technicalReport = New TechnicalReport With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .ClientId = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                            .EmployeeId = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                            .ApplianceId = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                            .EntryDate = IIf(IsDBNull(row("EntryDate")), Nothing, row("EntryDate")),
                            .DeliveryDate = IIf(IsDBNull(row("DeliveryDate")), Nothing, row("DeliveryDate")),
                            .Failure = IIf(IsDBNull(row("Failure")), Nothing, row("Failure")),
                            .Diagnosis = IIf(IsDBNull(row("Diagnosis")), Nothing, row("Diagnosis")),
                            .Service = IIf(IsDBNull(row("Service")), Nothing, row("Service")),
                            .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price")),
                            .Observation = IIf(IsDBNull(row("Observation")), Nothing, row("Observation"))}
        If row.Table.Columns.Count > 11 Then
            technicalReport.Client = New Person With {
                                                .Id = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                                                .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                                .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                                .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                                .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                                .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                                                .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
            technicalReport.Employee = New Employee With {
                                        .Id = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                                        .Login = IIf(IsDBNull(row("Login")), Nothing, row("Login")),
                                        .Pass = IIf(IsDBNull(row("Pass")), Nothing, row("Pass"))}
            technicalReport.Appliance = New Appliance With {
                                        .Id = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                                        .ApplianceDescriptionId = IIf(IsDBNull(row("ApplianceDescriptionId")), Nothing, row("ApplianceDescriptionId")),
                                        .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                                        .State = IIf(IsDBNull(row("State")), Nothing, row("State"))}
        End If
        Return technicalReport
    End Function
End Class
