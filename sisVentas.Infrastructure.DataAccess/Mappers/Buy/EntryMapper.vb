﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Entrada: 
''' Mapea los valores recibidos de una fila a una entidad Entrada
''' </summary>
Public MustInherit Class EntryMapper

    '''<summary>Método para mapear entrada de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Entrada</returns>
    Public Shared Function Map(row As DataRow) As Entry
        Dim entry = New Entry With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .EmployeeId = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                            .SupplierId = IIf(IsDBNull(row("SupplierId")), Nothing, row("SupplierId")),
                            .Edate = IIf(IsDBNull(row("Date")), Nothing, row("Date")),
                            .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                            .Correlative = IIf(IsDBNull(row("Correlative")), Nothing, row("Correlative")),
                            .DocumentType = IIf(IsDBNull(row("DocumentType")), Nothing, row("DocumentType")),
                            .State = IIf(IsDBNull(row("State")), Nothing, row("State")),
                            .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
        If row.Table.Columns.Count > 9 Then
            entry.Total = IIf(IsDBNull(row("Total")), Nothing, row("Total"))
            entry.Employee = New Employee With {
                                            .Id = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                                            .Login = IIf(IsDBNull(row("Login")), Nothing, row("Login")),
                                            .Pass = IIf(IsDBNull(row("Pass")), Nothing, row("Pass"))}
            entry.Supplier = New Person With {
                                            .Id = IIf(IsDBNull(row("SupplierId")), Nothing, row("SupplierId")),
                                            .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                            .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                            .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                            .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                            .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                                            .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
        End If
        Return entry
    End Function
End Class
