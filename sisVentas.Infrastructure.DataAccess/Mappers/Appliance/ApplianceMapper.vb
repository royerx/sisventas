﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Detalle de Equipo de Cómputo: 
''' Mapea los valores recibidos de una fila a una entidad Detalle de Equipo de Cómputo
''' </summary>
Public MustInherit Class ApplianceMapper

    '''<summary>Método para mapear detalle equipos de cómputo de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Detalle de Equipo de Cómputo</returns>
    Public Shared Function Map(row As DataRow) As Appliance
        Dim appliance = New Appliance With {
                             .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                             .ApplianceDescriptionId = IIf(IsDBNull(row("ApplianceDescriptionId")), Nothing, row("ApplianceDescriptionId")),
                             .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                             .State = IIf(IsDBNull(row("State")), Nothing, row("State")),
                             .EntryId = IIf(IsDBNull(row("EntryId")), Nothing, row("EntryId")),
                             .BuyPrice = IIf(IsDBNull(row("BuyPrice")), Nothing, row("BuyPrice"))}
        If row.Table.Columns.Count > 6 Then
            appliance.ApplianceDescription = New ApplianceDescription With {
                                      .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                                      .CategoryId = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                      .BrandId = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                      .ApplianceName = IIf(IsDBNull(row("ApplianceName")), Nothing, row("ApplianceName")),
                                      .Model = IIf(IsDBNull(row("Model")), Nothing, row("Model")),
                                      .PartNumber = IIf(IsDBNull(row("PartNumber")), Nothing, row("PartNumber")),
                                      .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price")),
                                      .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
            appliance.ApplianceDescription.Category = New Category With {
                                            .Id = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                            .Name = IIf(IsDBNull(row("CategoryName")), Nothing, row("CategoryName")),
                                            .Detail = IIf(IsDBNull(row("CategoryDetail")), Nothing, row("CategoryDetail"))}
            appliance.ApplianceDescription.Brand = New Brand With {
                                            .Id = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                            .Name = IIf(IsDBNull(row("BrandName")), Nothing, row("BrandName")),
                                            .Detail = IIf(IsDBNull(row("BrandDetail")), Nothing, row("BrandDetail"))}
        End If
        Return appliance
    End Function
End Class
