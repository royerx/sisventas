﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Equipo de Cómputo: 
''' Mapea los valores recibidos de una fila a una entidad Equipo de Cómputo
''' </summary>
Public MustInherit Class ApplianceDescriptionMapper

    '''<summary>Método para mapear equipos de cómputo de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Equipo de Cómputo</returns>
    Public Shared Function Map(row As DataRow) As ApplianceDescription
        Dim applianceDescription = New ApplianceDescription With {
                                      .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                                      .CategoryId = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                      .BrandId = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                      .ApplianceName = IIf(IsDBNull(row("ApplianceName")), Nothing, row("ApplianceName")),
                                      .Model = IIf(IsDBNull(row("Model")), Nothing, row("Model")),
                                      .PartNumber = IIf(IsDBNull(row("PartNumber")), Nothing, row("PartNumber")),
                                      .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price")),
                                      .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
        If row.Table.Columns.Count > 8 Then
            applianceDescription.Stock = IIf(IsDBNull(row("Stock")), Nothing, row("Stock"))
            applianceDescription.Category = New Category With {
                                            .Id = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                            .Name = IIf(IsDBNull(row("CategoryName")), Nothing, row("CategoryName")),
                                            .Detail = IIf(IsDBNull(row("CategoryDetail")), Nothing, row("CategoryDetail"))}
            applianceDescription.Brand = New Brand With {
                                            .Id = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                            .Name = IIf(IsDBNull(row("BrandName")), Nothing, row("BrandName")),
                                            .Detail = IIf(IsDBNull(row("BrandDetail")), Nothing, row("BrandDetail"))}
        End If
        Return applianceDescription
    End Function
End Class
