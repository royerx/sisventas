﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Categoria: 
''' Mapea los valores recibidos de una fila a una entidad Categoria
''' </summary>
Public MustInherit Class CategoryMapper

    '''<summary>Método para mapear categoria de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Categoria</returns>
    Public Shared Function Map(row As DataRow) As Category
        Return New Category With {
                            .Id = IIf(row("Id") <> Nothing, row("Id"), ""),
                            .Name = IIf(row("Name") <> Nothing, row("Name"), ""),
                            .Detail = IIf(row("Detail") <> Nothing, row("Detail"), "")}
    End Function
End Class
