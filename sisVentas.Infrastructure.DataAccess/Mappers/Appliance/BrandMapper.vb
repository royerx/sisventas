﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Marca: 
''' Mapea los valores recibidos de una fila a una entidad Marca
''' </summary>
Public MustInherit Class BrandMapper

    '''<summary>Método para mapear marca de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Marca</returns>
    Public Shared Function Map(row As DataRow) As Brand
        Return New Brand With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                            .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
    End Function
End Class
