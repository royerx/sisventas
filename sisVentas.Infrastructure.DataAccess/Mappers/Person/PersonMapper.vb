﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Persona: 
''' Mapea los valores recibidos de una fila a una entidad Persona
''' </summary>
Public MustInherit Class PersonMapper

    '''<summary>Método para mapear persona de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Persona</returns>
    Public Shared Function Map(row As DataRow) As Person
        Return New Person With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                            .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                            .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                            .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                            .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                            .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
    End Function
End Class
