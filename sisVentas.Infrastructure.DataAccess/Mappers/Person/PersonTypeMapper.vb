﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Tipo de Persona: 
''' Mapea los valores recibidos de una fila a una entidad Tipo de Persona
''' </summary>
Public MustInherit Class PersonTypeMapper

    '''<summary>Método para mapear tipo de persona de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Tipo de Persona</returns>
    Public Shared Function Map(row As DataRow) As PersonType
        Return New PersonType With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .Type = IIf(IsDBNull(row("Type")), Nothing, row("Type"))}
    End Function
End Class
