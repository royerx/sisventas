﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Detalle de Nota de Pedido: 
''' Mapea los valores recibidos de una fila a una entidad Detalle de Nota de Pedido
''' </summary>
Public MustInherit Class OrderDetailMapper

    '''<summary>Método para mapear detalle de nota de pedido de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Detalle de Nota de Pedido</returns>
    Public Shared Function Map(row As DataRow) As OrderDetail
        Dim orderDetail = New OrderDetail With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .OrderId = IIf(IsDBNull(row("OrderId")), Nothing, row("OrderId")),
                            .ApplianceId = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                            .Quantity = IIf(IsDBNull(row("Quantity")), Nothing, row("Quantity")),
                            .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price"))}
        If row.Table.Columns.Count > 5 Then
            orderDetail.Appliance = New Appliance With {
                .Id = IIf(IsDBNull(row("ApplianceId")), Nothing, row("ApplianceId")),
                .ApplianceDescriptionId = IIf(IsDBNull(row("ApplianceDescriptionId")), Nothing, row("ApplianceDescriptionId")),
                .ApplianceDescription = New ApplianceDescription With {
                                      .Id = IIf(IsDBNull(row("ApplianceDescriptionId")), Nothing, row("ApplianceDescriptionId")),
                                      .CategoryId = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                      .BrandId = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                      .ApplianceName = IIf(IsDBNull(row("ApplianceName")), Nothing, row("ApplianceName")),
                                      .Model = IIf(IsDBNull(row("Model")), Nothing, row("Model")),
                                      .PartNumber = IIf(IsDBNull(row("PartNumber")), Nothing, row("PartNumber")),
                                      .Price = IIf(IsDBNull(row("Price")), Nothing, row("Price")),
                                      .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail")),
                                      .Category = New Category With {
                                            .Id = IIf(IsDBNull(row("CategoryId")), Nothing, row("CategoryId")),
                                            .Name = IIf(IsDBNull(row("CategoryName")), Nothing, row("CategoryName")),
                                            .Detail = IIf(IsDBNull(row("CategoryDetail")), Nothing, row("CategoryDetail"))},
                                      .Brand = New Brand With {
                                            .Id = IIf(IsDBNull(row("BrandId")), Nothing, row("BrandId")),
                                            .Name = IIf(IsDBNull(row("BrandName")), Nothing, row("BrandName")),
                                            .Detail = IIf(IsDBNull(row("BrandDetail")), Nothing, row("BrandDetail"))}},
                .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                .State = IIf(IsDBNull(row("State")), Nothing, row("State")),
                .Order = New Order With {.Id = IIf(IsDBNull(row("OrderId")), Nothing, row("OrderId"))}
                }
        End If
        Return orderDetail
    End Function
End Class
