﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Nota de Pedido: 
''' Mapea los valores recibidos de una fila a una entidad Nota de Pedido
''' </summary>
Public MustInherit Class OrderMapper

    '''<summary>Método para mapear nota de pedido de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Nota de Pedido</returns>
    Public Shared Function Map(row As DataRow) As Order
        Dim order = New Order With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .ClientId = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                            .EmployeeId = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                            .Ndate = IIf(IsDBNull(row("Date")), Nothing, row("Date")),
                            .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
        If row.Table.Columns.Count > 5 Then
            order.Total = IIf(IsDBNull(row("Total")), Nothing, row("Total"))
            If row.Table.Columns.Count > 6 Then
                order.Client = New Person With {
                                                    .Id = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                                                    .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                                    .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                                    .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                                    .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                                    .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                                                    .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))}
                order.Employee = New Employee With {
                                            .Id = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                                            .Login = IIf(IsDBNull(row("Login")), Nothing, row("Login")),
                                            .Pass = IIf(IsDBNull(row("Pass")), Nothing, row("Pass"))}
            End If
        End If
        Return order
    End Function
End Class
