﻿Imports sisVentas.Domain.Model

''' <summary>
''' Clase Mapeador row a entidad Comprobante de Pago: 
''' Mapea los valores recibidos de una fila a una entidad Comprobante de Pago
''' </summary>
Public MustInherit Class VoucherMapper

    '''<summary>Método para mapear comprobante de pago de una row</summary>
    '''<param name="row">Columna de un datatable</param>
    '''<returns>Comprobante de Pago</returns>
    Public Shared Function Map(row As DataRow) As Voucher
        Dim voucher = New Voucher With {
                            .Id = IIf(IsDBNull(row("Id")), Nothing, row("Id")),
                            .EmployeeId = IIf(IsDBNull(row("EmployeeId")), Nothing, row("EmployeeId")),
                            .Vdate = IIf(IsDBNull(row("Date")), Nothing, row("Date")),
                            .Serie = IIf(IsDBNull(row("Serie")), Nothing, row("Serie")),
                            .Correlative = IIf(IsDBNull(row("Correlative")), Nothing, row("Correlative")),
                            .Type = IIf(IsDBNull(row("Type")), Nothing, row("Type")),
                            .State = IIf(IsDBNull(row("State")), Nothing, row("State")),
                            .VoucherType = IIf(IsDBNull(row("VoucherType")), Nothing, row("VoucherType")),
                            .Detail = IIf(IsDBNull(row("Detail")), Nothing, row("Detail"))}
        If row.Table.Columns.Count > 9 Then
            voucher.Order = New Order With {
                                .Id = IIf(IsDBNull(row("OrderId")), Nothing, row("OrderId")),
                                .ClientId = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                                .EmployeeId = IIf(IsDBNull(row("OrderEmployeeId")), Nothing, row("OrderEmployeeId")),
                                .Ndate = IIf(IsDBNull(row("OrderDate")), Nothing, row("OrderDate")),
                                .Total = IIf(IsDBNull(row("Total")), Nothing, row("Total")),
                                .Detail = IIf(IsDBNull(row("OrderDetail")), Nothing, row("OrderDetail")),
                                .Client = New Person With {
                                                    .Id = IIf(IsDBNull(row("ClientId")), Nothing, row("ClientId")),
                                                    .Name = IIf(IsDBNull(row("Name")), Nothing, row("Name")),
                                                    .LastName = IIf(IsDBNull(row("LastName")), Nothing, row("LastName")),
                                                    .DocumentNumber = IIf(IsDBNull(row("DocumentNumber")), Nothing, row("DocumentNumber")),
                                                    .Address = IIf(IsDBNull(row("Address")), Nothing, row("Address")),
                                                    .Phone = IIf(IsDBNull(row("Phone")), Nothing, row("Phone")),
                                                    .Email = IIf(IsDBNull(row("Email")), Nothing, row("Email"))},
                                .Employee = New Employee With {
                                            .Id = IIf(IsDBNull(row("OrderEmployeeId")), Nothing, row("OrderEmployeeId")),
                                            .Login = IIf(IsDBNull(row("Login")), Nothing, row("Login")),
                                            .Pass = IIf(IsDBNull(row("Pass")), Nothing, row("Pass"))}}
        End If
        Return voucher
    End Function
End Class
