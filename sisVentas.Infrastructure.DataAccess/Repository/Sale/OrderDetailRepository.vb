﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Detalle de Nota de Pedido: 
''' Implementa la Interfaz de Repositorio de Detalle de Nota de Podido como contrato del dominio para SQL Server
''' </summary>
Public Class OrderDetailRepository
    Inherits Repository
    Implements IOrderDetailRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de detalles de nota de pedido</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de detalles de nota de pedido filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of OrderDetail) Implements IGenericRepository(Of OrderDetail).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("OrderDetail_Read")
        Dim details = New List(Of OrderDetail)
        For Each item In resulTable.Rows
            details.Add(OrderDetailMapper.Map(item))
        Next
        Return details
    End Function

    '''<summary>Método para obtener un detalle de nota de pedido por el Id</summary>
    '''<param name="id">Identificador del detalle de nota de pedido a ser encontrado</param>
    '''<returns>Detalle de Nota de pedido encontrado</returns>
    Public Function GetById(id As Integer) As OrderDetail Implements IGenericRepository(Of OrderDetail).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("OrderDetail_GetById")
        If result.Rows.Count > 0 Then
            Return OrderDetailMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un detalle de nota de pedido</summary>
    '''<param name="entity">Detalle de nota de pedido a a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As OrderDetail) As Integer Implements IGenericRepository(Of OrderDetail).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@applianceId", entity.ApplianceId),
            New SqlParameter("@orderId", entity.OrderId),
            New SqlParameter("@quantity", entity.Quantity),
            New SqlParameter("@price", entity.Price)
        }
        Return ExecuteScalar("OrderDetail_Create")
    End Function

    '''<summary>Método para modificar un detalle de nota de pedido</summary>
    '''<param name="entity">Detalle de nota de pedido a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As OrderDetail) As Integer Implements IGenericRepository(Of OrderDetail).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@applianceId", entity.ApplianceId),
            New SqlParameter("@orderId", entity.OrderId),
            New SqlParameter("@quantity", entity.Quantity),
            New SqlParameter("@price", entity.Price)
        }
        Return ExecuteNonQuery("OrderDetail_Update")
    End Function

    '''<summary>Método eliminar un detalle de nota de pedido</summary>
    '''<param name="id">Identificador del detalle de nota de pedido a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of OrderDetail).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("OrderDetail_Delete")
    End Function

    '''<summary>Método obtener detalles de nota de pedido mediante identificador de nota de pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns>Lista de detalles de nota de pedido</returns>
    Public Function GetByOrderId(id As Integer) As IEnumerable(Of OrderDetail) Implements IOrderDetailRepository.GetByOrderId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteReader("OrderDetail_GetByOrderId")
        Dim details = New List(Of OrderDetail)
        For Each item In resulTable.Rows
            details.Add(OrderDetailMapper.Map(item))
        Next
        Return details
    End Function

    '''<summary>Método eliminar por Identificador de Nota de Pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns> cantidad de filas afectadas</returns>
    Function DeleteByOrderId(id As Integer) As Integer Implements IOrderDetailRepository.DeleteByOrderId
        parameters = New List(Of SqlParameter) From {
           New SqlParameter("@id", id)
       }
        Return ExecuteNonQuery("OrderDetail_DeleteByOrderId")
    End Function

End Class
