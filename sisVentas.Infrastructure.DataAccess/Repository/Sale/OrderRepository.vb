﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Nota de Pedido: 
''' Implementa la Interfaz de Repositorio de Nota de Podido como contrato del dominio para SQL Server
''' </summary>
Public Class OrderRepository
    Inherits Repository
    Implements IOrderRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de notas de pedidos</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de notas de pedidos filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Order) Implements IGenericRepository(Of Order).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@beginDate", ""),
            New SqlParameter("@endDate", "")
        }
        Dim resulTable = ExecuteReader("Order_Read")
        Dim orders = New List(Of Order)
        For Each item In resulTable.Rows
            orders.Add(OrderMapper.Map(item))
        Next
        Return orders
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Order) Implements IOrderRepository.GetAll
        If endDate.Date = New Date(Nothing) Then
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", "")
            }
        Else
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", endDate)
            }
        End If

        Dim resulTable = ExecuteReader("Order_Read")
        Dim orders = New List(Of Order)
        For Each item In resulTable.Rows
            orders.Add(OrderMapper.Map(item))
        Next
        Return orders
    End Function

    '''<summary>Método para obtener una nota de pedido por el Id</summary>
    '''<param name="id">Identificador de la nota de pedido a ser encontrada</param>
    '''<returns>Nota de pedido encontrada</returns>
    Public Function GetById(id As Integer) As Order Implements IGenericRepository(Of Order).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Order_GetById")
        If result.Rows.Count > 0 Then
            Return OrderMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una nota de pedido</summary>
    '''<param name="entity">Nota de pedido a a ser agregada</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(entity As Order) As Integer Implements IGenericRepository(Of Order).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@date", entity.Ndate),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteScalar("Order_Create")
    End Function

    '''<summary>Método para modificar una nota de pedido</summary>
    '''<param name="entity">Nota de pedido a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Order) As Integer Implements IGenericRepository(Of Order).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@date", entity.Ndate),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Order_Update")
    End Function

    '''<summary>Método eliminar una nota de pedido</summary>
    '''<param name="id">Identificador de la nota de pedido a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Order).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Order_Delete")
    End Function

    '''<summary>Método obtener nota de pedido mediante identificador de comprobante de pago</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Nota de pedido</returns>
    Public Function GetByVoucherId(id As Integer) As Order Implements IOrderRepository.GetByVoucherId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteQueryReader("SELECT O.* FROM [Order] O WITH(NOLOCK) INNER JOIN OrderVoucher OV WITH(NOLOCK) ON O.Id=OV.OrderId WHERE OV.VoucherId = @id")
        If Result.Rows.Count > 0 Then
            Return OrderMapper.Map(Result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

End Class
