﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Comprobante de Pago: 
''' Implementa la Interfaz de Repositorio de Comprobante de Pago como contrato del dominio para SQL Server
''' </summary>
Public Class VoucherRepository
    Inherits Repository
    Implements IVoucherRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de comprobantes de pago</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de comprobantes de pago filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Voucher) Implements IGenericRepository(Of Voucher).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@beginDate", ""),
            New SqlParameter("@endDate", "")
        }
        Dim resulTable = ExecuteReader("Voucher_Read")
        Dim orders = New List(Of Voucher)
        For Each item In resulTable.Rows
            orders.Add(VoucherMapper.Map(item))
        Next
        Return orders
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Voucher) Implements IVoucherRepository.GetAll
        If endDate.Date = New Date(Nothing) Then
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", "")
            }
        Else
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", endDate)
            }
        End If

        Dim resulTable = ExecuteReader("Voucher_Read")
        Dim vouchers = New List(Of Voucher)
        For Each item In resulTable.Rows
            vouchers.Add(VoucherMapper.Map(item))
        Next
        Return vouchers
    End Function

    '''<summary>Método para obtener un comprobante de pago por el Id</summary>
    '''<param name="id">Identificador de comprobante de pago a ser encontrado</param>
    '''<returns>Comprobante de Pago encontrado</returns>
    Public Function GetById(id As Integer) As Voucher Implements IGenericRepository(Of Voucher).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Voucher_GetById")
        If result.Rows.Count > 0 Then
            Return VoucherMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un comprobante de pago</summary>
    '''<param name="entity">Comprobante de pago a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Voucher) As Integer Implements IGenericRepository(Of Voucher).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@date", entity.Vdate),
            New SqlParameter("@type", entity.Type),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@correlative", entity.Correlative),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@voucherType", entity.VoucherType),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteScalar("Voucher_Create")
    End Function

    '''<summary>Método para modificar un comprobante de pago</summary>
    '''<param name="entity">Comprobante de Pago a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Voucher) As Integer Implements IGenericRepository(Of Voucher).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@type", entity.Type),
            New SqlParameter("@date", entity.Vdate),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@correlative", entity.Correlative),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@voucherType", entity.VoucherType),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Voucher_Update")
    End Function

    '''<summary>Método eliminar un comprobante de pago</summary>
    '''<param name="id">Identificador del comprobante de pago a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Voucher).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Voucher_Delete")
    End Function

    '''<summary>Método obtener comprobantes de pago mediante identificador de nota de pedido</summary>
    '''<param name="id">Identificador de la nota de pedido</param>
    '''<returns>Lista de comprobantes de pago</returns>
    Function GetByOrderId(id As Integer) As IEnumerable(Of Voucher) Implements IVoucherRepository.GetByOrderId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteQueryReader("SELECT V.* FROM Voucher V WITH(NOLOCK) INNER JOIN OrderVoucher O WITH(NOLOCK) ON V.Id=O.VoucherId WHERE O.OrderId=@id")
        Dim vouchers = New List(Of Voucher)
        For Each item In resulTable.Rows
            vouchers.Add(VoucherMapper.Map(item))
        Next
        Return vouchers
    End Function

    '''<summary>Método para agregar una asignacion de comprobante de pago a una nota de pedido </summary>
    '''<param name="voucherId">Identificador de Comprobante de Pago</param>
    '''<param name="orderId">Identificador de Nota de Pedido</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function AddOrder(voucherId As Integer, orderId As Integer) As Integer Implements IVoucherRepository.AddOrder
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@voucherId", voucherId),
            New SqlParameter("@orderId", orderId)
        }
        Return ExecuteQueryNonQuery("INSERT INTO OrderVoucher VALUES(@orderId,@voucherId)")
    End Function

    '''<summary>Método eliminar una asignacion de comprobante de pago a una nota de pedido</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function DeleteOrder(id As Integer) As Integer Implements IVoucherRepository.DeleteOrder
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteQueryNonQuery("DELETE OrderVoucher WHERE VoucherId=@id")
    End Function

    '''<summary>Método para agregar una asignacion de comprobante de pago a un reporte técnico </summary>
    '''<param name="voucherId">Identificador de Comprobante de Pago</param>
    '''<param name="technicalReportId">Identificador de Reporte Técnico</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function AddTechnicalReport(voucherId As Integer, technicalReportId As Integer) As Integer Implements IVoucherRepository.AddTechnicalReport
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@voucherId", voucherId),
            New SqlParameter("@technicalReportId", technicalReportId)
        }
        Return ExecuteQueryNonQuery("INSERT INTO TechnicalReportVoucher VALUES(@technicalReportId,@voucherId)")
    End Function

    '''<summary>Método eliminar una asignacion de comprobante de pago a un reporte técnico</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function DeleteTechnicalReport(id As Integer) As Integer Implements IVoucherRepository.DeleteTechnicalReport
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteQueryNonQuery("DELETE TechnicalReportVoucher WHERE VoucherId=@id")
    End Function

    '''<summary>Método obtener número de comprobante de pago</summary>
    '''<param name="type">Tipo de Comprobante</param>
    '''<returns>Objeto clave valor con serie y correlativo</returns>
    Function GetNumber(type As String) As Dictionary(Of String, String) Implements IVoucherRepository.GetNumber
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@type", type)
        }
        Dim result = ExecuteReader("Voucher_GetNumber")
        Dim number = New Dictionary(Of String, String)
        If result.Rows.Count > 0 Then
            number.Add("Serie", result.Rows(0).Item("Serie"))
            number.Add("Correlative", result.Rows(0).Item("Correlative"))
            Return number
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener comprobantes de pago mediante identificador de reporte técnico</summary>
    '''<param name="id">Identificador del reporte técnico</param>
    '''<returns>Lista de comprobantes de pago</returns>
    Function GetByTechnicalReportId(id As Integer) As IEnumerable(Of Voucher) Implements IVoucherRepository.GetByTechnicalReportId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteQueryReader("SELECT V.* FROM Voucher V WITH(NOLOCK) INNER JOIN TechnicalReportVoucher T WITH(NOLOCK) ON V.Id=T.VoucherId WHERE T.TechnicalReportId=@id")
        Dim vouchers = New List(Of Voucher)
        For Each item In resulTable.Rows
            vouchers.Add(VoucherMapper.Map(item))
        Next
        Return vouchers
    End Function

End Class
