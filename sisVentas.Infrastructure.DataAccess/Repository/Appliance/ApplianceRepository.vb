﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Detalle de Equipo de Cómputo: 
''' Implementa la Interfaz de Repositorio de Detalle Equipo de Cómputo como contrato del dominio para SQL Server
''' </summary>
Public Class ApplianceRepository
    Inherits Repository
    Implements IApplianceRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de detalles de equipos de cómputo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de detalles equipos de computo filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Appliance) Implements IGenericRepository(Of Appliance).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@applianceDescriptionId", 0)
        }
        Dim resulTable = ExecuteReader("Appliance_Read")
        Dim applianceDescriptions = New List(Of Appliance)
        For Each item In resulTable.Rows
            applianceDescriptions.Add(ApplianceMapper.Map(item))
        Next
        Return applianceDescriptions
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="applianceDescriptionId">Identificador de Descripción de Equipo de Cómputo</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, applianceDescriptionId As Integer) As IEnumerable(Of Appliance) Implements IApplianceRepository.GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@applianceDescriptionId", applianceDescriptionId)
        }
        Dim resulTable = ExecuteReader("Appliance_Read")
        Dim applianceDescriptions = New List(Of Appliance)
        For Each item In resulTable.Rows
            applianceDescriptions.Add(ApplianceMapper.Map(item))
        Next
        Return applianceDescriptions
    End Function

    '''<summary>Método para obtener un detalle de equipo de cómputo por el Id</summary>
    '''<param name="id">Identificador del detalle de equipo de cómputo a ser encontrado</param>
    '''<returns>Detalle Equipo de cómputo encontrado</returns>
    Public Function GetById(id As Integer) As Appliance Implements IGenericRepository(Of Appliance).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Appliance_GetById")
        If result.Rows.Count > 0 Then
            Return ApplianceMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un detalle de equipo de cómputo </summary>
    '''<param name="entity">Detalle de Equipo de cómputo a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Appliance) As Integer Implements IGenericRepository(Of Appliance).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@applianceDescriptionId", entity.ApplianceDescriptionId),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@buyPrice", entity.BuyPrice),
            New SqlParameter("@entryId", entity.EntryId)
        }
        Return ExecuteNonQuery("Appliance_Create")
    End Function

    '''<summary>Método para modificar un detalle equipo de cómputo</summary>
    '''<param name="entity">Detalle de Equipo de cómputo a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Appliance) As Integer Implements IGenericRepository(Of Appliance).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@applianceDescriptionId", entity.ApplianceDescriptionId),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@buyPrice", entity.BuyPrice),
            New SqlParameter("@entryId", entity.EntryId)
        }
        Return ExecuteNonQuery("Appliance_Update")
    End Function

    '''<summary>Método eliminar un detalle de equipo de cómputo</summary>
    '''<param name="id">Identificador del detalle de equipo de cómputo a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Appliance).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Appliance_Delete")
    End Function

    '''<summary>Método Cambiar el estado de un equipo</summary>
    '''<param name="applianceId">Identificador de detalle de equipo de computo</param>
    '''<param name="state">Estado a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Function ChangeSatate(applianceId As Integer, state As Boolean) As Integer Implements IApplianceRepository.ChangeSatate
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", applianceId),
            New SqlParameter("@state", state)
        }
        Return ExecuteQueryNonQuery("UPDATE Appliance SET state=@state WHERE id = @id")
    End Function

    '''<summary>Método para obtener un equipo de cómputo por la serie</summary>
    '''<param name="serie">Serie de equipo de cómputo a ser encontrado</param>
    '''<returns>Detalle Equipo de cómputo encontrado</returns>
    Public Function GetBySerie(serie As String) As Appliance Implements IApplianceRepository.GetBySerie
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@serie", serie)
        }
        Dim result = ExecuteQueryReader("SELECT * FROM Appliance WITH(NOLOCK) WHERE Serie=@serie")
        If result.Rows.Count > 0 Then
            Return ApplianceMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para obtener un equipo Por el Modelo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Entidad filtrada</returns>
    Function GetByModel(search As String) As ApplianceDescription Implements IApplianceRepository.GetByModel
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@model", search)
        }
        Dim result = ExecuteQueryReader("SELECT * FROM ApplianceDescription WITH(NOLOCK) WHERE Model=@model")
        If result.Rows.Count > 0 Then
            Return ApplianceDescriptionMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método eliminar por Identificador de Entrada</summary>
    '''<param name="id">Identificador de la entrada</param>
    '''<returns> cantidad de filas afectadas</returns>
    Function DeleteByEntryId(id As Integer) As Integer Implements IApplianceRepository.DeleteByEntryId
        parameters = New List(Of SqlParameter) From {
           New SqlParameter("@id", id)
       }
        Return ExecuteQueryNonQuery("DELETE Appliance WHERE EntryId=@id")
    End Function

End Class
