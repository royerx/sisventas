﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Categoria: 
''' Implementa la Interfaz de Repositorio de Categoria como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class CategoryRepository
    Inherits Repository
    Implements ICategoryRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de categorias</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de categorias filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Category) Implements IGenericRepository(Of Category).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("Category_Read")
        Dim categories = New List(Of Category)
        For Each item In resulTable.Rows
            categories.Add(CategoryMapper.Map(item))
        Next
        Return categories
    End Function

    '''<summary>Método para obtener una categoria por el Id</summary>
    '''<param name="id">Identificador de la categoria a ser encontrada</param>
    '''<returns>Categoria encontrada</returns>
    Public Function GetById(id As Integer) As Category Implements IGenericRepository(Of Category).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Category_GetById")
        If result.Rows.Count > 0 Then
            Return CategoryMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una categoria </summary>
    '''<param name="entity">Categoria a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Category) As Integer Implements IGenericRepository(Of Category).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Category_Create")
    End Function

    '''<summary>Método para modificar una categoria</summary>
    '''<param name="entity">Categoria a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Category) As Integer Implements IGenericRepository(Of Category).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Category_Update")
    End Function

    '''<summary>Método eliminar una categoria</summary>
    '''<param name="id">Identificador de la categoria a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Category).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Category_Delete")
    End Function
End Class
