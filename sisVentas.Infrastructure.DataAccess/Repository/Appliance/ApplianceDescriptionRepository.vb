﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Equipo de Cómputo: 
''' Implementa la Interfaz de Repositorio de Equipo de Cómputo como contrato del dominio para SQL Server
''' </summary>
Public Class ApplianceDescriptionRepository
    Inherits Repository
    Implements IApplianceDescriptionRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de equipos de cómputo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de equipos de computo filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ApplianceDescription) Implements IGenericRepository(Of ApplianceDescription).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("ApplianceDescription_Read")
        Dim applianceDescriptions = New List(Of ApplianceDescription)
        For Each item In resulTable.Rows
            applianceDescriptions.Add(ApplianceDescriptionMapper.Map(item))
        Next
        Return applianceDescriptions
    End Function

    '''<summary>Método para obtener un equipo de cómputo por el Id</summary>
    '''<param name="id">Identificador de la equipo de cómputo a ser encontrado</param>
    '''<returns>Equipo de cómputo encontrado</returns>
    Public Function GetById(id As Integer) As ApplianceDescription Implements IGenericRepository(Of ApplianceDescription).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("ApplianceDescription_GetById")
        If result.Rows.Count > 0 Then
            Return ApplianceDescriptionMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un equipo de cómputo </summary>
    '''<param name="entity">Equipo de cómputo a ser agregado</param>
    '''<returns>Id generado por la insercion</returns>
    Public Function Add(entity As ApplianceDescription) As Integer Implements IGenericRepository(Of ApplianceDescription).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@categoryId", entity.CategoryId),
            New SqlParameter("@brandId", entity.BrandId),
            New SqlParameter("@applianceName", entity.ApplianceName),
            New SqlParameter("@model", entity.Model),
            New SqlParameter("@partNumber", entity.PartNumber),
            New SqlParameter("@price", entity.Price),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteScalar("ApplianceDescription_Create")
    End Function

    '''<summary>Método para modificar un equipo de cómputo</summary>
    '''<param name="entity">Equipo de cómputo a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As ApplianceDescription) As Integer Implements IGenericRepository(Of ApplianceDescription).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@categoryId", entity.CategoryId),
            New SqlParameter("@brandId", entity.BrandId),
            New SqlParameter("@applianceName", entity.ApplianceName),
            New SqlParameter("@model", entity.Model),
            New SqlParameter("@partNumber", entity.PartNumber),
            New SqlParameter("@price", entity.Price),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("ApplianceDescription_Update")
    End Function

    '''<summary>Método eliminar un equipo de cómputo</summary>
    '''<param name="id">Identificador del equipo de cómputo a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of ApplianceDescription).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("ApplianceDescription_Delete")
    End Function

    '''<summary>Método para obtener el stock</summary>
    '''<param name="applianceDescriptionId">Identificador del equipo de computo</param>
    '''<returns>Cantidad de equipos disponibles</returns>
    Function GetStock(applianceDescriptionId As Integer) As Integer Implements IApplianceDescriptionRepository.GetStock
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", applianceDescriptionId)
        }
        Return ExecuteQueryScalar("SELECT COUNT(*) FROM Appliance WITH(NOLOCK) WHERE ApplianceDescriptionId=@id AND State=1")
    End Function
End Class
