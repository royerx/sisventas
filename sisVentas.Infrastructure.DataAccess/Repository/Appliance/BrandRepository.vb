﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Marca: 
''' Implementa la Interfaz de Repositorio de Marca como contrato del dominio para SQL Server
''' </summary>
Public Class BrandRepository
    Inherits Repository
    Implements IBrandRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de marcas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de marcas filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Brand) Implements IGenericRepository(Of Brand).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("Brand_Read")
        Dim brands = New List(Of Brand)
        For Each item In resulTable.Rows
            brands.Add(BrandMapper.Map(item))
        Next
        Return brands
    End Function


    '''<summary>Método para obtener una marca por el Id</summary>
    '''<param name="id">Identificador de la marca a ser encontrada</param>
    '''<returns>Marca encontrada</returns>
    Public Function GetById(id As Integer) As Brand Implements IGenericRepository(Of Brand).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Brand_GetById")
        If result.Rows.Count > 0 Then
            Return BrandMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una marca </summary>
    '''<param name="entity">Marca a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Brand) As Integer Implements IGenericRepository(Of Brand).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Brand_Create")
    End Function

    '''<summary>Método para modificar una marca</summary>
    '''<param name="entity">Marca a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Brand) As Integer Implements IGenericRepository(Of Brand).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Brand_Update")
    End Function

    '''<summary>Método eliminar una marca</summary>
    '''<param name="id">Identificador de la marca a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Brand).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Brand_Delete")
    End Function
End Class
