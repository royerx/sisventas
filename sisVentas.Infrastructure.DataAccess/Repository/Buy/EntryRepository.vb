﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Entrada: 
''' Implementa la Interfaz de Repositorio de Entrada como contrato del dominio para SQL Server
''' </summary>
Public Class EntryRepository
    Inherits Repository
    Implements IEntryRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de entradas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de entradas filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Entry) Implements IGenericRepository(Of Entry).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@beginDate", ""),
            New SqlParameter("@endDate", "")
        }
        Dim resulTable = ExecuteReader("Entry_Read")
        Dim entries = New List(Of Entry)
        For Each item In resulTable.Rows
            entries.Add(EntryMapper.Map(item))
        Next
        Return entries
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of Entry) Implements IEntryRepository.GetAll
        If endDate.Date = New Date(Nothing) Then
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", "")
            }
        Else
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", endDate)
            }
        End If

        Dim resulTable = ExecuteReader("Entry_Read")
        Dim entries = New List(Of Entry)
        For Each item In resulTable.Rows
            entries.Add(EntryMapper.Map(item))
        Next
        Return entries
    End Function

    '''<summary>Método para obtener una entrada por el Id</summary>
    '''<param name="id">Identificador de entrada a ser encontrada</param>
    '''<returns>Entrada encontrada</returns>
    Public Function GetById(id As Integer) As Entry Implements IGenericRepository(Of Entry).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Entry_GetById")
        If result.Rows.Count > 0 Then
            Return EntryMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una entrada</summary>
    '''<param name="entity">Entrada a ser agregada</param>
    '''<returns>Id Generado</returns>
    Public Function Add(entity As Entry) As Integer Implements IGenericRepository(Of Entry).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@supplierId", entity.SupplierId),
            New SqlParameter("@date", entity.Edate),
            New SqlParameter("@documentType", entity.DocumentType),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@correlative", entity.Correlative),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteScalar("Entry_Create")
    End Function

    '''<summary>Método para modificar una entrada</summary>
    '''<param name="entity">Entrada a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Entry) As Integer Implements IGenericRepository(Of Entry).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@supplierId", entity.SupplierId),
            New SqlParameter("@date", entity.Edate),
            New SqlParameter("@documentType", entity.DocumentType),
            New SqlParameter("@serie", entity.Serie),
            New SqlParameter("@correlative", entity.Correlative),
            New SqlParameter("@state", entity.State),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("Entry_Update")
    End Function

    '''<summary>Método eliminar una entrada</summary>
    '''<param name="id">Identificador de la entrada a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Entry).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Entry_Delete")
    End Function

    '''<summary>Método para obtener una lista de registros de Equipos asignados</summary>
    '''<param name="id">Identificador de Entrada</param>
    '''<returns>Lista de equipos</returns>
    Overloads Function GetAppliances(id As Integer) As IEnumerable(Of Appliance) Implements IEntryRepository.GetAppliances
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteQueryReader("SELECT * FROM Appliance WHERE EntryId=@id")
        Dim appliances = New List(Of Appliance)
        For Each item In resulTable.Rows
            appliances.Add(ApplianceMapper.Map(item))
        Next
        Return appliances
    End Function

End Class
