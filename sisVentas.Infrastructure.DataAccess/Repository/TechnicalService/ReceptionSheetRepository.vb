﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Ficha de Recepción: 
''' Implementa la Interfaz de Repositorio de Ficha de Recepción como contrato del dominio para SQL Server
''' </summary>
Public Class ReceptionSheetRepository
    Inherits Repository
    Implements IReceptionSheetRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de Fichas de Recepción</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Reportes filtrados</returns>
    Public Function GetAll(search As String) As IEnumerable(Of ReceptionSheet) Implements IGenericRepository(Of ReceptionSheet).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@beginDate", ""),
            New SqlParameter("@endDate", "")
        }
        Dim resulTable = ExecuteReader("ReceptionSheet_Read")
        Dim reports = New List(Of ReceptionSheet)
        For Each item In resulTable.Rows
            reports.Add(ReceptionSheetMapper.Map(item))
        Next
        Return reports
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of ReceptionSheet) Implements IReceptionSheetRepository.GetAll
        If endDate.Date = New Date(Nothing) Then
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", "")
            }
        Else
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", endDate)
            }
        End If

        Dim resulTable = ExecuteReader("ReceptionSheet_Read")
        Dim reports = New List(Of ReceptionSheet)
        For Each item In resulTable.Rows
            reports.Add(ReceptionSheetMapper.Map(item))
        Next
        Return reports
    End Function

    '''<summary>Método para obtener una fiche de recepción por el Id</summary>
    '''<param name="id">Identificador de la ficha de recepción a ser encontrada</param>
    '''<returns>Ficha de Recepción encontrada</returns>
    Public Function GetById(id As Integer) As ReceptionSheet Implements IGenericRepository(Of ReceptionSheet).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("ReceptionSheet_GetById")
        If result.Rows.Count > 0 Then
            Return ReceptionSheetMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una ficha de recepción</summary>
    '''<param name="entity">Ficha de Recepción a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As ReceptionSheet) As Integer Implements IGenericRepository(Of ReceptionSheet).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@technicalReportId", entity.TechnicalReportId),
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@date", entity.Rdate),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("ReceptionSheet_Create")
    End Function

    '''<summary>Método para modificar una ficha de recepción</summary>
    '''<param name="entity">Ficha de recepción a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As ReceptionSheet) As Integer Implements IGenericRepository(Of ReceptionSheet).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@technicalReportId", entity.TechnicalReportId),
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@date", entity.Rdate),
            New SqlParameter("@detail", entity.Detail)
        }
        Return ExecuteNonQuery("ReceptionSheet_Update")
    End Function

    '''<summary>Método eliminar una ficha de recepción</summary>
    '''<param name="id">Identificador de la ficha de recepción a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of ReceptionSheet).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("ReceptionSheet_Delete")
    End Function

End Class
