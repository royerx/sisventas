﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Reporte Técnico: 
''' Implementa la Interfaz de Repositorio de Reporte Técnico como contrato del dominio para SQL Server
''' </summary>
Public Class TechnicalReportRepository
    Inherits Repository
    Implements ITechnicalReportRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de reportes tecnicos</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Reportes filtrados</returns>
    Public Function GetAll(search As String) As IEnumerable(Of TechnicalReport) Implements IGenericRepository(Of TechnicalReport).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@beginDate", ""),
            New SqlParameter("@endDate", "")
        }
        Dim resulTable = ExecuteReader("TechnicalReport_Read")
        Dim reports = New List(Of TechnicalReport)
        For Each item In resulTable.Rows
            reports.Add(TechnicalReportMapper.Map(item))
        Next
        Return reports
    End Function

    '''<summary>Método para obtener una lista de registros de la Entidad por fechas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="beginDate">Fecha Inicial</param>
    '''<param name="endDate">Fecha Final</param>
    '''<returns>Lista de la Entidad filtrada</returns>
    Public Function GetAll(search As String, beginDate As Date, endDate As Date) As IEnumerable(Of TechnicalReport) Implements ITechnicalReportRepository.GetAll
        If endDate.Date = New Date(Nothing) Then
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", "")
            }
        Else
            parameters = New List(Of SqlParameter) From {
                New SqlParameter("@search", search),
                New SqlParameter("@beginDate", beginDate),
                New SqlParameter("@endDate", endDate)
            }
        End If

        Dim resulTable = ExecuteReader("TechnicalReport_Read")
        Dim reports = New List(Of TechnicalReport)
        For Each item In resulTable.Rows
            reports.Add(TechnicalReportMapper.Map(item))
        Next
        Return reports
    End Function

    '''<summary>Método para obtener un reporte técnico por el Id</summary>
    '''<param name="id">Identificador del reporte técnico a ser encontrado</param>
    '''<returns>Reporte Técnico encontrado</returns>
    Public Function GetById(id As Integer) As TechnicalReport Implements IGenericRepository(Of TechnicalReport).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("TechnicalReport_GetById")
        If result.Rows.Count > 0 Then
            Return TechnicalReportMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un reporte técnico</summary>
    '''<param name="entity">Reporte técnico a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As TechnicalReport) As Integer Implements IGenericRepository(Of TechnicalReport).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@applianceId", entity.ApplianceId),
            New SqlParameter("@entryDate", entity.EntryDate),
            New SqlParameter("@deliveryDate", entity.DeliveryDate),
            New SqlParameter("@failure", entity.Failure),
            New SqlParameter("@diagnosis", entity.Diagnosis),
            New SqlParameter("@service", entity.Service),
            New SqlParameter("@price", entity.Price),
            New SqlParameter("@observation", entity.Observation)
        }
        Return ExecuteScalar("TechnicalReport_Create")
    End Function

    '''<summary>Método para modificar un reporte técnico</summary>
    '''<param name="entity">Reporte técnico a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As TechnicalReport) As Integer Implements IGenericRepository(Of TechnicalReport).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@clientId", entity.ClientId),
            New SqlParameter("@employeeId", entity.EmployeeId),
            New SqlParameter("@applianceId", entity.ApplianceId),
            New SqlParameter("@entryDate", entity.EntryDate),
            New SqlParameter("@deliveryDate", entity.DeliveryDate),
            New SqlParameter("@failure", entity.Failure),
            New SqlParameter("@diagnosis", entity.Diagnosis),
            New SqlParameter("@service", entity.Service),
            New SqlParameter("@price", entity.Price),
            New SqlParameter("@observation", entity.Observation)
        }
        Return ExecuteNonQuery("TechnicalReport_Update")
    End Function

    '''<summary>Método eliminar un reporte técnico</summary>
    '''<param name="id">Identificador del reporte técnico a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of TechnicalReport).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("TechnicalReport_Delete")
    End Function

    '''<summary>Método obtener reporte técnico mediante identificador de comprobante de pago</summary>
    '''<param name="id">Identificador del comprobante de pago</param>
    '''<returns>Reporte técnico</returns>
    Public Function GetByVoucherId(id As Integer) As TechnicalReport Implements ITechnicalReportRepository.GetByVoucherId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteQueryReader("SELECT T.* FROM TechnicalReport T WITH(NOLOCK) INNER JOIN TechnicalReportVoucher V WITH(NOLOCK) ON T.Id=V.TechnicalReportId WHERE V.VoucherId = @id")
        If result.Rows.Count > 0 Then
            Return TechnicalReportMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método obtener servicios</summary>
    '''<returns>Lista de Servicios</returns>
    Public Function GetServices() As List(Of String) Implements ITechnicalReportRepository.GetServices
        Dim resulTable = ExecuteQueryReader("SELECT DISTINCT Service FROM TechnicalReport")
        Dim services = New List(Of String)
        For Each item In resulTable.Rows
            services.Add(item("Service"))
        Next
        Return services
    End Function

    '''<summary>Método obtener el precio de un servicio</summary>
    '''<param name="service">Nombre del servicio</param>
    '''<returns>Precio</returns>
    Public Function GetServicePrice(service As String) As Double Implements ITechnicalReportRepository.GetServicePrice
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@service", service)
        }
        Return ExecuteQueryScalar("SELECT Price FROM TechnicalReport WHERE Service=@service")
    End Function

    '''<summary>Método obtener ficha de recepción por reporte técnico</summary>
    '''<param name="technicalReportId">Identificador de reporte técnico</param>
    '''<returns>ficha de recepción</returns>
    Function GetReceptionSheet(technicalReportId As Integer) As ReceptionSheet Implements ITechnicalReportRepository.GetReceptionSheet
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@technicalReportId", technicalReportId)
        }
        Dim result = ExecuteQueryReader("SELECT * FROM ReceptionSheet WITH(NOLOCK) WHERE TechnicalReportId = @technicalReportId")
        If result.Rows.Count > 0 Then
            Return ReceptionSheetMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

End Class
