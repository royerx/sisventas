﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Persona: 
''' Implementa la Interfaz de Repositorio de Persona como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class PersonRepository
    Inherits Repository
    Implements IPersonRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de personas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Person) Implements IGenericRepository(Of Person).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@type", "")
        }
        Dim resulTable = ExecuteReader("Person_Read")
        Dim persons = New List(Of Person)
        For Each item In resulTable.Rows
            persons.Add(PersonMapper.Map(item))
        Next
        Return persons
    End Function

    '''<summary>Método para obtener una lista de personas mediate el tipo</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<param name="type">Tipo de persona</param>
    '''<returns>Lista de personas filtrada</returns>
    Public Function GetAll(search As String, type As String) As IEnumerable(Of Person) Implements IPersonRepository.GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search),
            New SqlParameter("@type", type)
        }
        Dim resulTable = ExecuteReader("Person_Read")
        Dim persons = New List(Of Person)
        For Each item In resulTable.Rows
            persons.Add(PersonMapper.Map(item))
        Next
        Return persons
    End Function

    '''<summary>Método verificar si una persona tiene tipos asignados</summary>
    '''<param name="id">Id de persona</param>
    '''<returns>Cantidad de registros</returns>
    Overloads Function HasPersonTypes(id As Integer) As Integer Implements IPersonRepository.HasPersonTypes
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@idPerson", id)
        }
        Return ExecuteQueryScalar("SELECT COUNT(*) FROM PersonPersonType WITH(NOLOCK) WHERE personId = @idPerson")
    End Function

    '''<summary>Método para obtener una persona por el Id</summary>
    '''<param name="id">Identificador de la persona a ser encontrada</param>
    '''<returns>Persona encontrada</returns>
    Public Function GetById(id As Integer) As Person Implements IGenericRepository(Of Person).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Person_GetById")
        If result.Rows.Count > 0 Then
            Return PersonMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar una persona </summary>
    '''<param name="entity">Persona a ser agregada</param>
    '''<returns>Id Insertado</returns>
    Public Function Add(entity As Person) As Integer Implements IGenericRepository(Of Person).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@lastName", entity.LastName),
            New SqlParameter("@documentNumber", entity.DocumentNumber),
            New SqlParameter("@address", entity.Address),
            New SqlParameter("@phone", entity.Phone),
            New SqlParameter("@email", entity.Email)
        }
        Return ExecuteScalar("Person_Create")
    End Function

    '''<summary>Método para asignar tipo de persona</summary>
    '''<param name="personId">Identificador de persona</param>
    '''<param name="typeId">Identificador de Tipo de Persona</param>
    '''<returns>Filas afectadas</returns>
    Public Function AddTypePerson(personId As Integer, typeId As Integer) As Integer Implements IPersonRepository.AddTypePerson
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@personId", personId),
            New SqlParameter("@personTypeId", typeId)
        }
        Return ExecuteQueryNonQuery("INSERT INTO PersonPersonType VALUES(@personId,@personTypeId)")
    End Function

    '''<summary>Método para modificar una persona</summary>
    '''<param name="entity">Persona a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Person) As Integer Implements IGenericRepository(Of Person).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@lastName", entity.LastName),
            New SqlParameter("@documentNumber", entity.DocumentNumber),
            New SqlParameter("@address", entity.Address),
            New SqlParameter("@phone", entity.Phone),
            New SqlParameter("@email", entity.Email)
        }
        Return ExecuteNonQuery("Person_Update")
    End Function

    '''<summary>Método eliminar una persona</summary>
    '''<param name="id">Identificador de la persona a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Person).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Person_Delete")
    End Function

    '''<summary>Método eliminar el tipo de persona asignado</summary>
    '''<param name="typeId">Identificador de Tipo de Persona</param>
    '''<param name="personId">Identificador de persona</param>
    '''<returns>Cantidad e Filas Afectadas</returns>
    Public Function DeleteTypePerson(personId As Integer, typeId As Integer) As Integer Implements IPersonRepository.DeleteTypePerson
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@personId", personId),
            New SqlParameter("@typeId", typeId)
        }
        Return ExecuteQueryNonQuery("DELETE PersonPersonType WHERE PersonId = @personId AND PersonTypeId = @typeId")
    End Function

    '''<summary>Método obtener persona por número de documento</summary>
    '''<param name="document">Número de documento</param>
    '''<returns>Persona identificada</returns>
    Function GetByDocument(document As String) As Person Implements IPersonRepository.GetByDocument
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@documentNumber", document)
        }
        Dim result = ExecuteQueryReader("SELECT * FROM Person WITH(NOLOCK) WHERE documentNumber = @documentNumber")
        If result.Rows.Count > 0 Then
            Return PersonMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método verificar si tiene asignación de tipo de persona</summary>
    '''<param name="personId">Identificador de Persona</param>
    '''<param name="typeId">Identificador Tipo de Persona</param>
    '''<returns>si cuenta o no con el tipo</returns>
    Function HasType(personId As Integer, typeId As Integer) As Boolean Implements IPersonRepository.HasType
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@personId", personId),
            New SqlParameter("@personTypeId", typeId)
        }
        Dim result = ExecuteQueryScalar("SELECT COUNT(*) FROM PersonPersonType WITH(NOLOCK) WHERE PersonId = @personId AND PersonTypeId=@personTypeId")
        If result > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class
