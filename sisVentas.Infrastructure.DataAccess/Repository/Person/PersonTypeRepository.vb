﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Tipo de Persona: 
''' Implementa la Interfaz de Repositorio de Tipo de Persona como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class PersonTypeRepository
    Inherits Repository
    Implements IPersonTypeRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de tipos de personas</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de tipos de personas filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of PersonType) Implements IGenericRepository(Of PersonType).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("PersonType_Read")
        Dim personTypes = New List(Of PersonType)
        For Each item In resulTable.Rows
            personTypes.Add(PersonTypeMapper.Map(item))
        Next
        Return personTypes
    End Function

    '''<summary>Método para obtener un tipo de persona por el Id</summary>
    '''<param name="id">Identificador de la tipo de persona a ser encontrada</param>
    '''<returns>Tipo de Persona encontrada</returns>
    Public Function GetById(id As Integer) As PersonType Implements IGenericRepository(Of PersonType).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("PersonType_GetById")
        If result.Rows.Count > 0 Then
            Return PersonTypeMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un tipo de persona </summary>
    '''<param name="entity">Tipo de Persona a ser agregada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As PersonType) As Integer Implements IGenericRepository(Of PersonType).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@type", entity.Type)
        }
        Return ExecuteNonQuery("PersonType_Create")
    End Function

    '''<summary>Método para modificar un tipo de persona</summary>
    '''<param name="entity">Tipo de Persona a ser modificada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As PersonType) As Integer Implements IGenericRepository(Of PersonType).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@type", entity.Type)
        }
        Return ExecuteNonQuery("PersonType_Update")
    End Function

    '''<summary>Método eliminar un tipo de persona</summary>
    '''<param name="id">Identificador del tipo de persona a ser eliminada</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of PersonType).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("PersonType_Delete")
    End Function
End Class
