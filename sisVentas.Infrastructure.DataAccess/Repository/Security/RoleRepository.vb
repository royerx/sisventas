﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Rol: 
''' Implementa la Interfaz de Repositorio de Rol como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class RoleRepository
    Inherits Repository
    Implements IRoleRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de roles</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de roles filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Role) Implements IGenericRepository(Of Role).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("Role_Read")
        Dim roles = New List(Of Role)
        For Each item In resulTable.Rows
            roles.Add(RoleMapper.Map(item))
        Next
        Return roles
    End Function

    '''<summary>Método para obtener un rol por el Id</summary>
    '''<param name="id">Identificador de la rol a ser encontrado</param>
    '''<returns>Rol encontrado</returns>
    Public Function GetById(id As Integer) As Role Implements IGenericRepository(Of Role).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Role_GetById")
        If result.Rows.Count > 0 Then
            Return RoleMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un rol </summary>
    '''<param name="entity">Rol a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Role) As Integer Implements IGenericRepository(Of Role).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@description", entity.Description)
        }
        Return ExecuteNonQuery("Role_Create")
    End Function

    '''<summary>Método para modificar un rol</summary>
    '''<param name="entity">Rol a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Role) As Integer Implements IGenericRepository(Of Role).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@description", entity.Description)
        }
        Return ExecuteNonQuery("Role_Update")
    End Function

    '''<summary>Método eliminar un rol</summary>
    '''<param name="id">Identificador del rol a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Role).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Role_Delete")
    End Function

    '''<summary>Método listar roles por identificador de empleado</summary>
    '''<param name="id">Identificador del Empleado</param>
    '''<returns>Roles que tiene el empleado</returns>
    Public Function GetByEmployeeId(id As Integer) As IEnumerable(Of Role) Implements IRoleRepository.GetByEmployeeId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteReader("Role_GetByEmployeeId")
        Dim roles = New List(Of Role)
        For Each item In resulTable.Rows
            roles.Add(RoleMapper.Map(item))
        Next
        Return roles
    End Function

    '''<summary>Método eliminar permisos de un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<returns>Numero de filas afectadas</returns>
    Public Function DeletePermissions(roleId As Integer) As Integer Implements IRoleRepository.DeletePermissions
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", roleId)
        }
        Return ExecuteQueryNonQuery("DELETE PermissionRole WHERE roleId=@id")
    End Function

    '''<summary>Agregar permisos a un rol</summary>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="ids">Lista de identificadores de permisos</param>
    '''<returns>Numero de filas afectadas</returns>
    Public Function SavePermissions(roleId As Integer, ids As IEnumerable(Of Integer)) As Integer Implements IRoleRepository.SavePermissions
        Dim res As Integer
        For Each item In ids
            parameters = New List(Of SqlParameter) From {
                 New SqlParameter("@permissionId", item),
                 New SqlParameter("@roleId", roleId)
            }
            res += ExecuteQueryNonQuery("INSERT INTO PermissionRole VALUES (@permissionId,@roleId)")
        Next
        Return res
    End Function

End Class
