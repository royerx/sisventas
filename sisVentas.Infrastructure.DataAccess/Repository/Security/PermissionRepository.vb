﻿Imports System.Data.SqlClient
Imports sisVentas.Domain.Model

''' <summary>
''' Clase Repositorio de Permiso: 
''' Implementa la Interfaz de Repositorio de Permiso como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class PermissionRepository
    Inherits Repository
    Implements IPermissionRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de permisos</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de permisos filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Permission) Implements IGenericRepository(Of Permission).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("Permission_Read")
        Dim permissions = New List(Of Permission)
        For Each item In resulTable.Rows
            permissions.Add(PermissionMapper.Map(item))
        Next
        Return permissions
    End Function

    '''<summary>Método para obtener un permiso por el Id</summary>
    '''<param name="id">Identificador de la permiso a ser encontrado</param>
    '''<returns>Permiso encontrado</returns>
    Public Function GetById(id As Integer) As Permission Implements IGenericRepository(Of Permission).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Permission_GetById")
        If result.Rows.Count > 0 Then
            Return PermissionMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un permiso </summary>
    '''<param name="entity">Permiso a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Permission) As Integer Implements IGenericRepository(Of Permission).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@slug", entity.Slug),
            New SqlParameter("@description", entity.Description)
        }
        Return ExecuteNonQuery("Permission_Create")
    End Function

    '''<summary>Método para modificar un permiso</summary>
    '''<param name="entity">Permiso a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Permission) As Integer Implements IGenericRepository(Of Permission).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@name", entity.Name),
            New SqlParameter("@slug", entity.Slug),
            New SqlParameter("@description", entity.Description)
        }
        Return ExecuteNonQuery("Permission_Update")
    End Function

    '''<summary>Método eliminar un permiso</summary>
    '''<param name="id">Identificador deL Permiso a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Permission).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Permission_Delete")
    End Function

    '''<summary>Método obtener permisos por Identificador del rol</summary>
    '''<param name="id">Identificador del rol</param>
    '''<returns>Lista de permisos</returns>
    Public Function GetByRoleId(id As Integer) As IEnumerable(Of Permission) Implements IPermissionRepository.GetByRoleId
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim resulTable = ExecuteReader("Permission_GetByRoleId")
        Dim permissions = New List(Of Permission)
        For Each item In resulTable.Rows
            permissions.Add(PermissionMapper.Map(item))
        Next
        Return permissions
    End Function

    '''<summary>Método listar tablas</summary>
    '''<returns>Lista de nombres de tablas</returns>
    Public Function Tables() As IEnumerable(Of String) Implements IPermissionRepository.Tables
        Dim resulTable = ExecuteQueryReader("SELECT CAST(table_name as varchar) as TableName FROM INFORMATION_SCHEMA.TABLES WITH(NOLOCK) order by TableName")
        Dim listTables = New List(Of String)
        For Each item In resulTable.Rows
            listTables.Add(item("TableName"))
        Next
        Return listTables
    End Function
End Class
