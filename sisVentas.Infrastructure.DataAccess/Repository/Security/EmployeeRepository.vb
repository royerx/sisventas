﻿Imports System.Data.SqlClient
Imports sisVentas.CrossCutting
Imports sisVentas.Domain.Model
Imports sisVentas.Infrastructure.EmailServices

''' <summary>
''' Clase Repositorio de Empleado: 
''' Implementa la Interfaz de Repositorio de Empleado como contrato del dominio para SQL Server
''' </summary>
''' <inheritdoc/>
Public Class EmployeeRepository
    Inherits Repository
    Implements IEmployeeRepository

    '''<summary>Constructor de la clase que inyecta la cadena de conexion y la transaccion</summary>
    '''<param name="context">Cadena de Conexion</param>
    '''<param name="transaction">Transaccion</param>
    Public Sub New(context As SqlConnection, transaction As SqlTransaction)
        Me.context = context
        Me.transaction = transaction
    End Sub

    '''<summary>Método para obtener una lista de empleados</summary>
    '''<param name="search">Texto que va a ser filtrado</param>
    '''<returns>Lista de empleados filtrada</returns>
    Public Function GetAll(search As String) As IEnumerable(Of Employee) Implements IGenericRepository(Of Employee).GetAll
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@search", search)
        }
        Dim resulTable = ExecuteReader("Employee_Read")
        Dim employees = New List(Of Employee)
        For Each item In resulTable.Rows
            employees.Add(EmployeeMapper.Map(item))
        Next
        Return employees
    End Function

    '''<summary>Método para obtener un empleado por el Id</summary>
    '''<param name="id">Identificador de la empleado a ser encontrado</param>
    '''<returns>Empleado encontrado</returns>
    Public Function GetById(id As Integer) As Employee Implements IGenericRepository(Of Employee).GetById
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Dim result = ExecuteReader("Employee_GetById")
        If result.Rows.Count > 0 Then
            Return EmployeeMapper.Map(result.Rows(0))
        Else
            Return Nothing
        End If
    End Function

    '''<summary>Método para agregar un empleado </summary>
    '''<param name="entity">Empleado a ser agregado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Add(entity As Employee) As Integer Implements IGenericRepository(Of Employee).Add
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@login", entity.Login),
            New SqlParameter("@pass", entity.Pass)
        }
        Return ExecuteNonQuery("Employee_Create")
    End Function

    '''<summary>Método para modificar un empleado</summary>
    '''<param name="entity">Empleado a ser modificado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Update(entity As Employee) As Integer Implements IGenericRepository(Of Employee).Update
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", entity.Id),
            New SqlParameter("@login", entity.Login),
            New SqlParameter("@pass", entity.Pass)
        }
        Return ExecuteNonQuery("Employee_Update")
    End Function

    '''<summary>Método eliminar un empleado</summary>
    '''<param name="id">Identificador del empleado a ser eliminado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Public Function Delete(id As Integer) As Integer Implements IGenericRepository(Of Employee).Delete
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteNonQuery("Employee_Delete")
    End Function

    '''<summary>Método iniciar sesion a un empleado</summary>
    '''<param name="user">Nombre de usuario a iniciar sesion</param>
    '''<param name="pass">Contraseña del usuario a iniciar sesion</param>
    '''<returns>Si pasa o no el logueo</returns>
    Public Function Login(user As String, pass As String) As Integer Implements IEmployeeRepository.Login
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@login", user),
            New SqlParameter("@pass", pass)
        }
        Return ExecuteQueryScalar("SELECT * FROM Employee WITH(NOLOCK) WHERE login=@login AND pass = @pass")
    End Function

    '''<summary>Método verificar permiso</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="roleId">Identificador del Rol</param>
    '''<param name="permission">Slug del permiso a ser verificado</param>
    '''<returns>Si tiene el permiso o no</returns>
    Public Function Can(employeeId As Integer, roleId As Integer, permission As String) As Boolean Implements IEmployeeRepository.Can
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@employeeId", employeeId),
            New SqlParameter("@roleId", roleId),
            New SqlParameter("@permission", permission)
        }
        Dim result = ExecuteReader("Employee_Can")
        If result.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    '''<summary>Método para agregar un rol a un empleado</summary>
    '''<param name="employeeId">Identificador del Empleado</param>
    '''<param name="roleId">Identificador del Rol</param>
    '''<returns>Filas afectadas</returns>
    Public Function AddRole(employeeId As Integer, roleId As Integer) As Integer Implements IEmployeeRepository.AddRole
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@employeeId", employeeId),
            New SqlParameter("@roleId", roleId)
        }
        Return ExecuteQueryNonQuery("INSERT INTO EmployeeRole VALUES(@employeeId,@roleId)")
    End Function

    '''<summary>Método eliminar roles</summary>
    '''<param name="id">Identificador del Empleado</param>
    '''<returns>Filas afectadas</returns>
    Public Function DeleteRoles(id As Integer) As Integer Implements IEmployeeRepository.DeleteRoles
        parameters = New List(Of SqlParameter) From {
            New SqlParameter("@id", id)
        }
        Return ExecuteQueryNonQuery("DELETE EmployeeRole WHERE EmployeeId=@id")
    End Function

    '''<summary>Método obtener empleado por correo</summary>
    '''<param name="email">Correo electrónico</param>
    '''<returns>Usuario encontrado</returns>
    Function GetPassByEmail(email As String) As String Implements IEmployeeRepository.GetPassByEmail
        parameters = New List(Of SqlParameter) From {
           New SqlParameter("@email", email)
       }
        Dim result = ExecuteQueryReader("SELECT * FROM Employee E INNER JOIN Person P ON E.Id = P.Id WHERE P.email=@email")
        If result.Rows.Count > 0 Then
            Dim employee = EmployeeMapper.Map(result.Rows(0))
            employee.Pass = Crypt.Decrypt(employee.Pass, Values.PRIVATE_KEY, Integer.Parse("256"))
            Dim mailSupport = New SupportMail()
            mailSupport.SendMail(Subject:="SISTEMA: Solicitud de Recuperación de Contraseña",
                                 Body:="Hola, " & employee.Person.Name & " " & employee.Person.LastName & vbNewLine & "Solicitaste una recuperación de tu contraseña." & vbNewLine & "Tu nombre de usuario es: " & employee.Login & vbNewLine & "Tu contraseña registrada es: " & employee.Pass & vbNewLine & "Se recomienda cambiar la contraseña una vez que hayas ingresado al sistema",
                                 ReceiverMail:=New List(Of String) From {employee.Person.Email})

            Return "Hola, " & employee.Person.Name & " " & employee.Person.LastName & vbNewLine & "Solicitaste una recuperación de tu contraseña." & vbNewLine & "Se a enviado sus credenciales al correo electrónico " & employee.Person.Email & vbNewLine & "Se recomienda cambiar la contraseña una vez que hayas ingresado al sistema"
        Else
            Return "El correo electrónico no se encentra registrado en la base de datos"
        End If
    End Function

End Class
