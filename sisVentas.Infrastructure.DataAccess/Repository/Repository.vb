﻿Imports System.Data.SqlClient

Public MustInherit Class Repository

    Protected context As SqlConnection
    Protected transaction As SqlTransaction
    'Lista de Parametros para la consulta y/o procedimiento almacanado
    Protected parameters As List(Of SqlParameter)

    '''<summary>Método que ejecuta procedimiento almacenado que solo retorna filas afectadas </summary>
    '''<param name="query">procedimiento almacenado a ser ejecutado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Protected Function ExecuteNonQuery(query As String) As Integer

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.StoredProcedure
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            Dim result = command.ExecuteNonQuery()
            If parameters IsNot Nothing Then
                parameters.Clear()
            End If
            Return result
        End Using
    End Function

    '''<summary>Método que ejecuta procedimiento almacenado que retorna una tabla </summary>
    '''<param name="query">procedimiento almacenado a ser ejecutado</param>
    '''<returns>DataTable como tabla resultante de la consulta</returns>
    Protected Function ExecuteReader(query As String) As DataTable

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.StoredProcedure
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            Using reader = command.ExecuteReader()
                Using table = New DataTable()
                    table.Load(reader)
                    If parameters IsNot Nothing Then
                        parameters.Clear()
                    End If
                    Return table
                End Using
            End Using
        End Using
    End Function

    '''<summary>Método que ejecuta procedimiento almacenado que retorna un valor </summary>
    '''<param name="query">procedimiento almacenado a ser ejecutado</param>
    '''<returns>valor</returns>
    Protected Function ExecuteScalar(query As String)

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.StoredProcedure
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            If parameters IsNot Nothing Then
                parameters.Clear()
            End If
            Return command.ExecuteScalar()
        End Using
    End Function

    '''<summary>Método que ejecuta consulta que retorna una tabla </summary>
    '''<param name="query">Consulta a ser ejecutada</param>
    '''<returns>DataTable como tabla resultante de la consulta</returns>
    Protected Function ExecuteQueryReader(query As String) As DataTable

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.Text
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            Using reader = command.ExecuteReader()
                Using table = New DataTable()
                    table.Load(reader)
                    If parameters IsNot Nothing Then
                        parameters.Clear()
                    End If
                    Return table
                End Using
            End Using
        End Using
    End Function

    '''<summary>Método que ejecuta consulta que retorna un valor</summary>
    '''<param name="query">Consulta a ser ejecutada</param>
    '''<returns>valor resultante de la consulta</returns>
    Protected Function ExecuteQueryScalar(query As String)

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.Text
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            If parameters IsNot Nothing Then
                parameters.Clear()
            End If
            Return command.ExecuteScalar()
        End Using
    End Function

    '''<summary>Método que ejecuta consulta almacenado que solo retorna filas afectadas </summary>
    '''<param name="query">procedimiento almacenado a ser ejecutado</param>
    '''<returns>Cantidad de filas afectadas</returns>
    Protected Function ExecuteQueryNonQuery(query As String) As Integer

        Using command = New SqlCommand()
            command.Connection = context
            command.CommandText = query
            command.Transaction = transaction
            command.CommandType = CommandType.Text
            If parameters IsNot Nothing Then
                For Each item In parameters
                    command.Parameters.Add(item)
                Next
            End If
            Dim result = command.ExecuteNonQuery()
            If parameters IsNot Nothing Then
                parameters.Clear()
            End If
            Return result
        End Using
    End Function

End Class
